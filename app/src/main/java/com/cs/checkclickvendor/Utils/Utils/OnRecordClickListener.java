package com.cs.checkclickvendor.Utils.Utils;

import android.view.View;

public interface OnRecordClickListener {
    void onClick(View v);
}