package com.cs.checkclickvendor.Utils.Utils;

public interface RecordListener {
    void onStart();
    void onCancel();
    void onFinish(long time);
    void onLessTime();
}