package com.cs.checkclickvendor.Utils.Utils;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.List;
import java.util.Locale;

public class GPSTracker extends Service implements LocationListener {
	private final Context mContext;

	// flag for GPS Status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;

	boolean canGetLocation = false;

	Location location;
	double latitude;
	double longitude;

	// The minimum distance to change updates in metters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 0
																	// meters

	// The minimum time beetwen updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 1000 * 5 * 1; // 0 minute

	// Declaring a Location Manager
	protected LocationManager locationManager;

	public GPSTracker(Context context) {
		this.mContext = context;
		getLocation();
	}

	public Location getLocation() {

		try {
			locationManager = (LocationManager) mContext
					.getSystemService(LOCATION_SERVICE);

			// getting GPS status
			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			isNetworkEnabled = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled) {
				// no network provider is enabled
			} else {

				if (locationManager != null && isGPSEnabled) {
					location = locationManager
							.getLastKnownLocation(LocationManager.GPS_PROVIDER);
					updateGPSCoordinates();
				}
				else if (locationManager != null) {
					location = locationManager
							.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					updateGPSCoordinates();
				}

				this.canGetLocation = true;

				Criteria criteria = new Criteria();
				criteria.setAccuracy(Criteria.ACCURACY_FINE);
				criteria.setPowerRequirement(Criteria.POWER_HIGH);
				criteria.setAltitudeRequired(false);
				criteria.setSpeedRequired(true);
				criteria.setSpeedAccuracy(Criteria.ACCURACY_HIGH);
				criteria.setCostAllowed(true);
				criteria.setBearingRequired(false);
				criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
				criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);

				locationManager.requestLocationUpdates(MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, criteria, this, null);

//				if (isGPSEnabled) {
//					if (location == null) {
//						locationManager.requestLocationUpdates(
//								LocationManager.GPS_PROVIDER,
//								MIN_TIME_BW_UPDATES,
//								MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
//
//						Log.d("GPS Enabled",
//								"========================GPS Enabled");
//
//						if (locationManager != null) {
//							location = locationManager
//									.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//							updateGPSCoordinates();
//						}
//					}
//				}
//
////				 First get location from Network Provider
//				if (null == location && isNetworkEnabled) {
//					locationManager.requestLocationUpdates(
//							LocationManager.NETWORK_PROVIDER,
//							MIN_TIME_BW_UPDATES,
//							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
//
//					Log.d("Network", "====================Network");
//
//					if (locationManager != null) {
//						location = locationManager
//								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//						updateGPSCoordinates();
//					}
//				}

				// if GPS Enabled get lat/long using GPS Services

			}
		} catch (Exception e) {
			// e.printStackTrace();
			Log.e("Error : Location",
					"Impossible to connect to LocationManager", e);
		}

		return location;
	}

	public void updateGPSCoordinates() {
		if (location != null) {
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			Log.d("GPS Enabled", "========================GPS Enabled"
					+ latitude);
		}
	}

	/**
	 * Stop using GPS listener Calling this function will stop using GPS in your
	 * app
	 */

	public void stopUsingGPS() {
		if (locationManager != null) {
			locationManager.removeUpdates(GPSTracker.this);
		}
	}

	/**
	 * Function to get latitude
	 */
	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		}

		System.out.println(latitude);
		return latitude;
	}

	/**
	 * Function to get longitude
	 */
	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		}

		return longitude;
	}

	/**
	 * Function to check GPS/wifi enabled
	 */
	public boolean canGetLocation() {
		return this.canGetLocation;
	}

	/**
	 * Function to show settings alert dialog
	 */
	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

		// Setting Dialog Title
		alertDialog.setTitle("GPS");

		// Setting Dialog Message
		alertDialog.setMessage("GPS Alert");

		// On Pressing Setting button
		alertDialog.setPositiveButton("ok",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						mContext.startActivity(intent);
					}
				});

		// On pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		alertDialog.show();
	}

	/**
	 * Get list of address by latitude and longitude
	 * 
	 * @return null or List<Address>
	 */
	public List<Address> getGeocoderAddress(Context context) {
		if (location != null) {
			try {
                Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
                Log.i("", "==========================" + latitude + "---"
						+ longitude);
				List<Address> addresses = geocoder.getFromLocation(latitude,
						longitude, 1);
				Log.i("", "===========address size==============="
						+ addresses.size());
				return addresses;
			} catch (Exception e) {
				// e.printStackTrace();
				Log.e("Error : Geocoder", "Impossible to connect to Geocoder",
						e);
			}
		}

		return null;
	}

	/**
	 * Try to get AddressLine
	 * 
	 * @return null or addressLine
	 */
	public String getAddressLine(Context context) {
		List<Address> addresses = getGeocoderAddress(context);
		if (addresses != null && addresses.size() > 0) {
			Address address = addresses.get(0);
			String addressLine = address.getAddressLine(0);

			return addressLine;
		} else {
			return null;
		}
	}

	/**
	 * Try to get Locality
	 * 
	 * @return null or locality
	 */
	public String getSubLocality(Context context) {
		List<Address> addresses = getGeocoderAddress(context);

		Log.i("TAG", "=======================getGeocoderAddress==========="
				+ addresses);
		if (addresses != null && addresses.size() > 0) {
			Address address = addresses.get(0);
			String locality = address.getLocality();

			return locality;
		} else {
			return "";
		}
	}

	/**
	 * Try to get Postal Code
	 * 
	 * @return null or postalCode
	 */
	public String getPostalCode(Context context) {
		List<Address> addresses = getGeocoderAddress(context);
		if (addresses != null && addresses.size() > 0) {
			Address address = addresses.get(0);
			String postalCode = address.getPostalCode();

			return postalCode;
		} else {
			return null;
		}
	}

	/**
	 * Try to get CountryName
	 * 
	 * @return null or postalCode
	 */
	public String getCountryName(Context context) {
		List<Address> addresses = getGeocoderAddress(context);
		if (addresses != null && addresses.size() > 0) {
			Address address = addresses.get(0);
			String countryName = address.getCountryName();

			return countryName;
		} else {
			return null;
		}
	}

	@Override
	public void onLocationChanged(Location location) {
        Intent intent = new Intent("GPSLocationUpdates");
        // You can also include some extra data.
        Bundle b = new Bundle();
        b.putParcelable("Location", location);
        intent.putExtra("Location", b);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
