package com.cs.checkclickvendor.Utils.Utils;

public interface KeyboardVisibilityEventListener {
    void onVisibilityChanged(boolean var1);
}