package com.cs.checkclickvendor.Utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.cs.checkclickvendor.R;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import java.text.DecimalFormat;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

public class Constants {
    public static String Country_Code = "+966 ";
    public static String STORE_IMAGE_URL = "http://csadms.com/CheckClickAPI/Uploads/stores/";
    public static String OfferIP_IMAGE_URL = "http://csadms.com/CheckClickAPI/Uploads/Products/";
    public static String USERS_IMAGE_URL = "http://csadms.com/CheckClickAPI/Uploads/Users/";
    public static String PRODUCTS_IMAGE_URL = "http://csadms.com/CheckClickAPI/Uploads/products/";
    public static String ADS_IMAGE_URL = "http://csadms.com/CheckClickAPI/Uploads/AdvOfr/";
    public static String RETURN_IMAGE_URL = "http://csadms.com/CheckClickAPI/Uploads/ReturnImages/";
    public static String ACCESS_IMAGE_URL = "http://csadms.com/CheckClickAPI/Uploads/ProfilePictures/";
    public static final DecimalFormat priceFormat1 = new DecimalFormat("##,##,##0.00");
    public static AlertDialog customDialog;
    public static String appColor;

    public static Typeface getBookTypeFace(Context context){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),
                "GothamBook.ttf");
        return typeface;
    }

    public static Typeface getMediumTypeFace(Context context){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),
                "GothamMedium.ttf");
        return typeface;
    }

    public static String getDeviceType(Context context){
        String device = "Andr v";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            device = device+version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return device;
    }
    public static void showOneButtonAlertDialog(String descriptionStr, String titleStr, String buttonStr, Activity context){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View line = (View) dialogView.findViewById(R.id.vert_line);

        line.setVisibility(View.GONE);
        no.setVisibility(View.GONE);

        title.setText(titleStr);
        yes.setText(buttonStr);
        desc.setText(descriptionStr);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void requestEditTextFocus(View view, Activity activity) {
        if (view.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public static void showLoadingDialog(Activity context) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.loading_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

//        ImageView loadingImage = (ImageView) dialogView.findViewById(R.id.loading);
//        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);
//        Glide.with(context).asGif().load(R.raw.loading_icon).into(loadingImage);

        CamomileSpinner spinner1 = (CamomileSpinner) dialogView.findViewById(R.id.spinner1);
        spinner1.start();

        spinner1.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.45;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public static void closeLoadingDialog(){
        if(customDialog != null) {
            customDialog.dismiss();
        }
    }
}
