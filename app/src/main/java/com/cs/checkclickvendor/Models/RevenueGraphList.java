package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RevenueGraphList {


    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("TotalExpense")
        private float totalexpense;
        @Expose
        @SerializedName("TotalIncome")
        private float totalincome;
        @Expose
        @SerializedName("DifferenceAmount")
        private float differenceamount;
        @Expose
        @SerializedName("PastAmount")
        private float pastamount;
        @Expose
        @SerializedName("CurrentAmount")
        private float currentamount;
        @Expose
        @SerializedName("FilteredPA")
        private float filteredpa;
        @Expose
        @SerializedName("FilteredCA")
        private float filteredca;
        @Expose
        @SerializedName("CurrentYearGraph")
        private ArrayList<CurrentYearGraph> currentyeargraph;
        @Expose
        @SerializedName("FilteredGraph")
        private ArrayList<FilteredGraph> filteredgraph;

        public float getTotalexpense() {
            return totalexpense;
        }

        public void setTotalexpense(float totalexpense) {
            this.totalexpense = totalexpense;
        }

        public float getTotalincome() {
            return totalincome;
        }

        public void setTotalincome(float totalincome) {
            this.totalincome = totalincome;
        }

        public float getDifferenceamount() {
            return differenceamount;
        }

        public void setDifferenceamount(float differenceamount) {
            this.differenceamount = differenceamount;
        }

        public float getPastamount() {
            return pastamount;
        }

        public void setPastamount(float pastamount) {
            this.pastamount = pastamount;
        }

        public float getCurrentamount() {
            return currentamount;
        }

        public void setCurrentamount(float currentamount) {
            this.currentamount = currentamount;
        }

        public float getFilteredpa() {
            return filteredpa;
        }

        public void setFilteredpa(float filteredpa) {
            this.filteredpa = filteredpa;
        }

        public float getFilteredca() {
            return filteredca;
        }

        public void setFilteredca(float filteredca) {
            this.filteredca = filteredca;
        }

        public ArrayList<CurrentYearGraph> getCurrentyeargraph() {
            return currentyeargraph;
        }

        public void setCurrentyeargraph(ArrayList<CurrentYearGraph> currentyeargraph) {
            this.currentyeargraph = currentyeargraph;
        }

        public ArrayList<FilteredGraph> getFilteredgraph() {
            return filteredgraph;
        }

        public void setFilteredgraph(ArrayList<FilteredGraph> filteredgraph) {
            this.filteredgraph = filteredgraph;
        }
    }

    public static class CurrentYearGraph {
        @Expose
        @SerializedName("Amount")
        private float amount;
        @Expose
        @SerializedName("Month")
        private String month;

        public float getAmount() {
            return amount;
        }

        public void setAmount(float amount) {
            this.amount = amount;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }
    }

    public static class FilteredGraph {
        @Expose
        @SerializedName("PastAmount")
        private float pastamount;
        @Expose
        @SerializedName("PastDates")
        private String pastdates;
        @Expose
        @SerializedName("CurrentAmount")
        private float currentamount;
        @Expose
        @SerializedName("CurrentDates")
        private String currentdates;

        public float getPastamount() {
            return pastamount;
        }

        public void setPastamount(float pastamount) {
            this.pastamount = pastamount;
        }

        public String getPastdates() {
            return pastdates;
        }

        public void setPastdates(String pastdates) {
            this.pastdates = pastdates;
        }

        public float getCurrentamount() {
            return currentamount;
        }

        public void setCurrentamount(float currentamount) {
            this.currentamount = currentamount;
        }

        public String getCurrentdates() {
            return currentdates;
        }

        public void setCurrentdates(String currentdates) {
            this.currentdates = currentdates;
        }
    }
}
