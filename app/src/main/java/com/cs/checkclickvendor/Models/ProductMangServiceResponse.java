package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProductMangServiceResponse {

    @Expose
    @SerializedName("IsActive")
    private boolean IsActive;
    @Expose
    @SerializedName("ServiceType")
    private int ServiceType;
    @Expose
    @SerializedName("ServiceProviderId")
    private int ServiceProviderId;
    @Expose
    @SerializedName("SubCategoryId")
    private int SubCategoryId;
    @Expose
    @SerializedName("CategoryId")
    private int CategoryId;
    @Expose
    @SerializedName("Publised")
    private int Publised;
    @Expose
    @SerializedName("ServiceName")
    private String ServiceName;
    @Expose
    @SerializedName("ToDate")
    private String ToDate;
    @Expose
    @SerializedName("FromDate")
    private String FromDate;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("TotalRecords")
    private int TotalRecords;
    @Expose
    @SerializedName("PageNumber")
    private int PageNumber;
    @Expose
    @SerializedName("pagingNumber")
    private int pagingNumber;
    @Expose
    @SerializedName("Mapjson")
    private String Mapjson;
    @Expose
    @SerializedName("Branchjson")
    private String Branchjson;
    @Expose
    @SerializedName("ServiceJson")
    private String ServiceJson;
    @Expose
    @SerializedName("ImagesJson")
    private String ImagesJson;
    @Expose
    @SerializedName("return_url")
    private String return_url;
    @Expose
    @SerializedName("_URLLocation")
    private String _URLLocation;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("CreatedBy")
    private int CreatedBy;
    @Expose
    @SerializedName("Id")
    private int Id;
    @Expose
    @SerializedName("Bytes")
    private String Bytes;
    @Expose
    @SerializedName("FileUploadLocation")
    private String FileUploadLocation;
    @Expose
    @SerializedName("FileName")
    private String FileName;
    @Expose
    @SerializedName("base64")
    private String base64;
    @Expose
    @SerializedName("Images_dt")
    private String Images_dt;
    @Expose
    @SerializedName("StoreId")
    private int StoreId;
    @Expose
    @SerializedName("datasetxml")
    private String datasetxml;
    @Expose
    @SerializedName("BranchId")
    private int BranchId;
    @Expose
    @SerializedName("MapId")
    private int MapId;
    @Expose
    @SerializedName("ActionName")
    private String ActionName;
    @Expose
    @SerializedName("ControllerName")
    private String ControllerName;
    @Expose
    @SerializedName("Data")
    private DataEntity Data;
    @Expose
    @SerializedName("UniqueNumber")
    private int UniqueNumber;
    @Expose
    @SerializedName("UniqueIdLong")
    private int UniqueIdLong;
    @Expose
    @SerializedName("ServiceId")
    private int ServiceId;
    @Expose
    @SerializedName("ImageId")
    private int ImageId;

    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean IsActive) {
        this.IsActive = IsActive;
    }

    public int getServiceType() {
        return ServiceType;
    }

    public void setServiceType(int ServiceType) {
        this.ServiceType = ServiceType;
    }

    public int getServiceProviderId() {
        return ServiceProviderId;
    }

    public void setServiceProviderId(int ServiceProviderId) {
        this.ServiceProviderId = ServiceProviderId;
    }

    public int getSubCategoryId() {
        return SubCategoryId;
    }

    public void setSubCategoryId(int SubCategoryId) {
        this.SubCategoryId = SubCategoryId;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int CategoryId) {
        this.CategoryId = CategoryId;
    }

    public int getPublised() {
        return Publised;
    }

    public void setPublised(int Publised) {
        this.Publised = Publised;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String ServiceName) {
        this.ServiceName = ServiceName;
    }

    public String getToDate() {
        return ToDate;
    }

    public void setToDate(String ToDate) {
        this.ToDate = ToDate;
    }

    public String getFromDate() {
        return FromDate;
    }

    public void setFromDate(String FromDate) {
        this.FromDate = FromDate;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getTotalRecords() {
        return TotalRecords;
    }

    public void setTotalRecords(int TotalRecords) {
        this.TotalRecords = TotalRecords;
    }

    public int getPageNumber() {
        return PageNumber;
    }

    public void setPageNumber(int PageNumber) {
        this.PageNumber = PageNumber;
    }

    public int getPagingNumber() {
        return pagingNumber;
    }

    public void setPagingNumber(int pagingNumber) {
        this.pagingNumber = pagingNumber;
    }

    public String getMapjson() {
        return Mapjson;
    }

    public void setMapjson(String Mapjson) {
        this.Mapjson = Mapjson;
    }

    public String getBranchjson() {
        return Branchjson;
    }

    public void setBranchjson(String Branchjson) {
        this.Branchjson = Branchjson;
    }

    public String getServiceJson() {
        return ServiceJson;
    }

    public void setServiceJson(String ServiceJson) {
        this.ServiceJson = ServiceJson;
    }

    public String getImagesJson() {
        return ImagesJson;
    }

    public void setImagesJson(String ImagesJson) {
        this.ImagesJson = ImagesJson;
    }

    public String getReturn_url() {
        return return_url;
    }

    public void setReturn_url(String return_url) {
        this.return_url = return_url;
    }

    public String get_URLLocation() {
        return _URLLocation;
    }

    public void set_URLLocation(String _URLLocation) {
        this._URLLocation = _URLLocation;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getBytes() {
        return Bytes;
    }

    public void setBytes(String Bytes) {
        this.Bytes = Bytes;
    }

    public String getFileUploadLocation() {
        return FileUploadLocation;
    }

    public void setFileUploadLocation(String FileUploadLocation) {
        this.FileUploadLocation = FileUploadLocation;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String FileName) {
        this.FileName = FileName;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public String getImages_dt() {
        return Images_dt;
    }

    public void setImages_dt(String Images_dt) {
        this.Images_dt = Images_dt;
    }

    public int getStoreId() {
        return StoreId;
    }

    public void setStoreId(int StoreId) {
        this.StoreId = StoreId;
    }

    public String getDatasetxml() {
        return datasetxml;
    }

    public void setDatasetxml(String datasetxml) {
        this.datasetxml = datasetxml;
    }

    public int getBranchId() {
        return BranchId;
    }

    public void setBranchId(int BranchId) {
        this.BranchId = BranchId;
    }

    public int getMapId() {
        return MapId;
    }

    public void setMapId(int MapId) {
        this.MapId = MapId;
    }

    public String getActionName() {
        return ActionName;
    }

    public void setActionName(String ActionName) {
        this.ActionName = ActionName;
    }

    public String getControllerName() {
        return ControllerName;
    }

    public void setControllerName(String ControllerName) {
        this.ControllerName = ControllerName;
    }

    public DataEntity getData() {
        return Data;
    }

    public void setData(DataEntity Data) {
        this.Data = Data;
    }

    public int getUniqueNumber() {
        return UniqueNumber;
    }

    public void setUniqueNumber(int UniqueNumber) {
        this.UniqueNumber = UniqueNumber;
    }

    public int getUniqueIdLong() {
        return UniqueIdLong;
    }

    public void setUniqueIdLong(int UniqueIdLong) {
        this.UniqueIdLong = UniqueIdLong;
    }

    public int getServiceId() {
        return ServiceId;
    }

    public void setServiceId(int ServiceId) {
        this.ServiceId = ServiceId;
    }

    public int getImageId() {
        return ImageId;
    }

    public void setImageId(int ImageId) {
        this.ImageId = ImageId;
    }

    public static class DataEntity {
        @Expose
        @SerializedName("TotalRecords")
        private ArrayList<TotalRecordsEntity> TotalRecords;
        @Expose
        @SerializedName("ServiceList")
        private ArrayList<ServiceListEntity> ServiceList;

        public ArrayList<TotalRecordsEntity> getTotalRecords() {
            return TotalRecords;
        }

        public void setTotalRecords(ArrayList<TotalRecordsEntity> TotalRecords) {
            this.TotalRecords = TotalRecords;
        }

        public ArrayList<ServiceListEntity> getServiceList() {
            return ServiceList;
        }

        public void setServiceList(ArrayList<ServiceListEntity> ServiceList) {
            this.ServiceList = ServiceList;
        }
    }

    public static class TotalRecordsEntity {
        @Expose
        @SerializedName("TotalRecords")
        private int TotalRecords;

        public int getTotalRecords() {
            return TotalRecords;
        }

        public void setTotalRecords(int TotalRecords) {
            this.TotalRecords = TotalRecords;
        }
    }

    public static class ServiceListEntity {
        @Expose
        @SerializedName("MainImage")
        private String MainImage;
        @Expose
        @SerializedName("CreatedOn")
        private String CreatedOn;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("MeasureAr")
        private String MeasureAr;
        @Expose
        @SerializedName("MeasureEn")
        private String MeasureEn;
        @Expose
        @SerializedName("Price")
        private double Price;
        @Expose
        @SerializedName("ServiceNameAr")
        private String ServiceNameAr;
        @Expose
        @SerializedName("ServiceNameEn")
        private String ServiceNameEn;
        @Expose
        @SerializedName("ServiceId")
        private int ServiceId;
        @Expose
        @SerializedName("rowNum")
        private int rowNum;

        public String getMainImage() {
            return MainImage;
        }

        public void setMainImage(String MainImage) {
            this.MainImage = MainImage;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String CreatedOn) {
            this.CreatedOn = CreatedOn;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getMeasureAr() {
            return MeasureAr;
        }

        public void setMeasureAr(String MeasureAr) {
            this.MeasureAr = MeasureAr;
        }

        public String getMeasureEn() {
            return MeasureEn;
        }

        public void setMeasureEn(String MeasureEn) {
            this.MeasureEn = MeasureEn;
        }

        public double getPrice() {
            return Price;
        }

        public void setPrice(double Price) {
            this.Price = Price;
        }

        public String getServiceNameAr() {
            return ServiceNameAr;
        }

        public void setServiceNameAr(String ServiceNameAr) {
            this.ServiceNameAr = ServiceNameAr;
        }

        public String getServiceNameEn() {
            return ServiceNameEn;
        }

        public void setServiceNameEn(String ServiceNameEn) {
            this.ServiceNameEn = ServiceNameEn;
        }

        public int getServiceId() {
            return ServiceId;
        }

        public void setServiceId(int ServiceId) {
            this.ServiceId = ServiceId;
        }

        public int getRowNum() {
            return rowNum;
        }

        public void setRowNum(int rowNum) {
            this.rowNum = rowNum;
        }
    }
}
