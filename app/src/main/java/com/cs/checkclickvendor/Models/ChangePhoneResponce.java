package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.SerializedName;

public  class ChangePhoneResponce {

    @SerializedName("Data")
    private DataEntity Data;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Status")
    private boolean Status;

    public DataEntity getData() {
        return Data;
    }

    public void setData(DataEntity Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }
}
