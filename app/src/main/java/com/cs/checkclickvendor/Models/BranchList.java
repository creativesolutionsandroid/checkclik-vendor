package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class BranchList implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("Branches")
        private ArrayList<Branches> branches;

        public ArrayList<Branches> getBranches() {
            return branches;
        }

        public void setBranches(ArrayList<Branches> branches) {
            this.branches = branches;
        }
    }

    public static class Branches implements Serializable {
        @Expose
        @SerializedName("StoreType")
        private int storetype;
        @Expose
        @SerializedName("EmailId")
        private String emailid;
        @Expose
        @SerializedName("PlanExpiry")
        private String planexpiry;
        @Expose
        @SerializedName("Communications")
        private String communications;
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("BranchShiftTimingsJson")
        private String branchshifttimingsjson;
        @Expose
        @SerializedName("StatusMessage")
        private String statusmessage;
        @Expose
        @SerializedName("StatusCode")
        private String statuscode;
        @Expose
        @SerializedName("FlagId")
        private int flagid;
        @Expose
        @SerializedName("IsDeleted")
        private boolean isdeleted;
        @Expose
        @SerializedName("IsActive")
        private boolean isactive;
        @Expose
        @SerializedName("DeletedBy")
        private int deletedby;
        @Expose
        @SerializedName("ModifiedBy")
        private int modifiedby;
        @Expose
        @SerializedName("CreatedBy")
        private int createdby;
        @Expose
        @SerializedName("Status")
        private boolean status;
        @Expose
        @SerializedName("Longitude")
        private String longitude;
        @Expose
        @SerializedName("Latitude")
        private String latitude;
        @Expose
        @SerializedName("is24x7")
        private boolean is24x7;
        @Expose
        @SerializedName("branchCommunications")
        private String branchcommunications;
        @Expose
        @SerializedName("Maroof")
        private String maroof;
        @Expose
        @SerializedName("Youtube")
        private String youtube;
        @Expose
        @SerializedName("Snapchat")
        private String snapchat;
        @Expose
        @SerializedName("Instagram")
        private String instagram;
        @Expose
        @SerializedName("LinkedIn")
        private String linkedin;
        @Expose
        @SerializedName("TwitterHandle")
        private String twitterhandle;
        @Expose
        @SerializedName("Facebook")
        private String facebook;
        @Expose
        @SerializedName("QRCode")
        private String qrcode;
        @Expose
        @SerializedName("AvailabilityStatus")
        private boolean availabilitystatus;
        @Expose
        @SerializedName("PlanStatus")
        private boolean planstatus;
        @Expose
        @SerializedName("MembershipPlan")
        private int membershipplan;
        @Expose
        @SerializedName("BranchColor")
        private String branchcolor;
        @Expose
        @SerializedName("BranchLogoImage")
        private String branchlogoimage;
        @Expose
        @SerializedName("BranchLogoImageAttr")
        private String branchlogoimageattr;
        @Expose
        @SerializedName("BranchLogoImageFile")
        private String branchlogoimagefile;
        @Expose
        @SerializedName("BackgroundImage")
        private String backgroundimage;
        @Expose
        @SerializedName("BackgroundImageAttr")
        private String backgroundimageattr;
        @Expose
        @SerializedName("BackgroundImageFile")
        private String backgroundimagefile;
        @Expose
        @SerializedName("ReturnPolicyAr")
        private String returnpolicyar;
        @Expose
        @SerializedName("ReturnPolicyEn")
        private String returnpolicyen;
        @Expose
        @SerializedName("TermsAndConditionsAr")
        private String termsandconditionsar;
        @Expose
        @SerializedName("TermsAndConditionsEn")
        private String termsandconditionsen;
        @Expose
        @SerializedName("ShippingProvider")
        private int shippingprovider;
        @Expose
        @SerializedName("IsShippingWorldwide")
        private boolean isshippingworldwide;
        @Expose
        @SerializedName("TransportationCharges")
        private int transportationcharges;
        @Expose
        @SerializedName("FreeDeliveryAboveValue")
        private double freedeliveryabovevalue;
        @Expose
        @SerializedName("MinimumOrderValue")
        private int minimumordervalue;
        @Expose
        @SerializedName("DeliveryCharges")
        private int deliverycharges;
        @Expose
        @SerializedName("PreBookingDeliveryTime")
        private int prebookingdeliverytime;
        @Expose
        @SerializedName("PreBookingDeliveryTimeUnit")
        private int prebookingdeliverytimeunit;
        @Expose
        @SerializedName("IsDeliveryCompany")
        private int isdeliverycompany;
        @Expose
        @SerializedName("IsDelivery")
        private boolean isdelivery;
        @Expose
        @SerializedName("PreBookingInStoreTime")
        private int prebookinginstoretime;
        @Expose
        @SerializedName("PreBookingInStoreTimeUnit")
        private int prebookinginstoretimeunit;
        @Expose
        @SerializedName("InStoreService")
        private boolean instoreservice;
        @Expose
        @SerializedName("Pickup")
        private boolean pickup;
        @Expose
        @SerializedName("BusinessSince")
        private String businesssince;
        @Expose
        @SerializedName("strBusinessSince")
        private String strbusinesssince;
        @Expose
        @SerializedName("AddressTypeId")
        private int addresstypeid;
        @Expose
        @SerializedName("ApartmentOfficeNo")
        private String apartmentofficeno;
        @Expose
        @SerializedName("FloorNo")
        private String floorno;
        @Expose
        @SerializedName("BuildingNo")
        private String buildingno;
        @Expose
        @SerializedName("StreetNo")
        private String streetno;
        @Expose
        @SerializedName("ZipCode")
        private String zipcode;
        @Expose
        @SerializedName("DistrictAr")
        private String districtar;
        @Expose
        @SerializedName("CityAr")
        private String cityar;
        @Expose
        @SerializedName("RegionAr")
        private String regionar;
        @Expose
        @SerializedName("CountryAr")
        private String countryar;
        @Expose
        @SerializedName("DistrictEn")
        private String districten;
        @Expose
        @SerializedName("CityEn")
        private String cityen;
        @Expose
        @SerializedName("RegionEn")
        private String regionen;
        @Expose
        @SerializedName("CountryEn")
        private String countryen;
        @Expose
        @SerializedName("DistrictId")
        private int districtid;
        @Expose
        @SerializedName("CityId")
        private int cityid;
        @Expose
        @SerializedName("RegionId")
        private int regionid;
        @Expose
        @SerializedName("CountryId")
        private int countryid;
        @Expose
        @SerializedName("DescriptionAr")
        private String descriptionar;
        @Expose
        @SerializedName("RatingCount")
        private int RatingCount;
        @Expose
        @SerializedName("Review")
        private float Review;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("DescriptionEn")
        private String descriptionen;
        @Expose
        @SerializedName("BranchNameAr")
        private String branchnamear;
        @Expose
        @SerializedName("BranchNameEn")
        private String branchnameen;
        @Expose
        @SerializedName("StoreId")
        private int storeid;
        @Expose
        @SerializedName("Id")
        private int id;
        @Expose
        @SerializedName("ActionName")
        private String actionname;
        @Expose
        @SerializedName("ControllerName")
        private String controllername;

        public int getRatingCount() {
            return RatingCount;
        }

        public void setRatingCount(int ratingCount) {
            RatingCount = ratingCount;
        }

        public float getReview() {
            return Review;
        }

        public void setReview(float review) {
            Review = review;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public int getStoretype() {
            return storetype;
        }

        public void setStoretype(int storetype) {
            this.storetype = storetype;
        }

        public String getEmailid() {
            return emailid;
        }

        public void setEmailid(String emailid) {
            this.emailid = emailid;
        }

        public String getPlanexpiry() {
            return planexpiry;
        }

        public void setPlanexpiry(String planexpiry) {
            this.planexpiry = planexpiry;
        }

        public String getCommunications() {
            return communications;
        }

        public void setCommunications(String communications) {
            this.communications = communications;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getBranchshifttimingsjson() {
            return branchshifttimingsjson;
        }

        public void setBranchshifttimingsjson(String branchshifttimingsjson) {
            this.branchshifttimingsjson = branchshifttimingsjson;
        }

        public String getStatusmessage() {
            return statusmessage;
        }

        public void setStatusmessage(String statusmessage) {
            this.statusmessage = statusmessage;
        }

        public String getStatuscode() {
            return statuscode;
        }

        public void setStatuscode(String statuscode) {
            this.statuscode = statuscode;
        }

        public int getFlagid() {
            return flagid;
        }

        public void setFlagid(int flagid) {
            this.flagid = flagid;
        }

        public boolean getIsdeleted() {
            return isdeleted;
        }

        public void setIsdeleted(boolean isdeleted) {
            this.isdeleted = isdeleted;
        }

        public boolean getIsactive() {
            return isactive;
        }

        public void setIsactive(boolean isactive) {
            this.isactive = isactive;
        }

        public int getDeletedby() {
            return deletedby;
        }

        public void setDeletedby(int deletedby) {
            this.deletedby = deletedby;
        }

        public int getModifiedby() {
            return modifiedby;
        }

        public void setModifiedby(int modifiedby) {
            this.modifiedby = modifiedby;
        }

        public int getCreatedby() {
            return createdby;
        }

        public void setCreatedby(int createdby) {
            this.createdby = createdby;
        }

        public boolean getStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public boolean getIs24x7() {
            return is24x7;
        }

        public void setIs24x7(boolean is24x7) {
            this.is24x7 = is24x7;
        }

        public String getBranchcommunications() {
            return branchcommunications;
        }

        public void setBranchcommunications(String branchcommunications) {
            this.branchcommunications = branchcommunications;
        }

        public String getMaroof() {
            return maroof;
        }

        public void setMaroof(String maroof) {
            this.maroof = maroof;
        }

        public String getYoutube() {
            return youtube;
        }

        public void setYoutube(String youtube) {
            this.youtube = youtube;
        }

        public String getSnapchat() {
            return snapchat;
        }

        public void setSnapchat(String snapchat) {
            this.snapchat = snapchat;
        }

        public String getInstagram() {
            return instagram;
        }

        public void setInstagram(String instagram) {
            this.instagram = instagram;
        }

        public String getLinkedin() {
            return linkedin;
        }

        public void setLinkedin(String linkedin) {
            this.linkedin = linkedin;
        }

        public String getTwitterhandle() {
            return twitterhandle;
        }

        public void setTwitterhandle(String twitterhandle) {
            this.twitterhandle = twitterhandle;
        }

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getQrcode() {
            return qrcode;
        }

        public void setQrcode(String qrcode) {
            this.qrcode = qrcode;
        }

        public boolean getAvailabilitystatus() {
            return availabilitystatus;
        }

        public void setAvailabilitystatus(boolean availabilitystatus) {
            this.availabilitystatus = availabilitystatus;
        }

        public boolean getPlanstatus() {
            return planstatus;
        }

        public void setPlanstatus(boolean planstatus) {
            this.planstatus = planstatus;
        }

        public int getMembershipplan() {
            return membershipplan;
        }

        public void setMembershipplan(int membershipplan) {
            this.membershipplan = membershipplan;
        }

        public String getBranchcolor() {
            return branchcolor;
        }

        public void setBranchcolor(String branchcolor) {
            this.branchcolor = branchcolor;
        }

        public String getBranchlogoimage() {
            return branchlogoimage;
        }

        public void setBranchlogoimage(String branchlogoimage) {
            this.branchlogoimage = branchlogoimage;
        }

        public String getBranchlogoimageattr() {
            return branchlogoimageattr;
        }

        public void setBranchlogoimageattr(String branchlogoimageattr) {
            this.branchlogoimageattr = branchlogoimageattr;
        }

        public String getBranchlogoimagefile() {
            return branchlogoimagefile;
        }

        public void setBranchlogoimagefile(String branchlogoimagefile) {
            this.branchlogoimagefile = branchlogoimagefile;
        }

        public String getBackgroundimage() {
            return backgroundimage;
        }

        public void setBackgroundimage(String backgroundimage) {
            this.backgroundimage = backgroundimage;
        }

        public String getBackgroundimageattr() {
            return backgroundimageattr;
        }

        public void setBackgroundimageattr(String backgroundimageattr) {
            this.backgroundimageattr = backgroundimageattr;
        }

        public String getBackgroundimagefile() {
            return backgroundimagefile;
        }

        public void setBackgroundimagefile(String backgroundimagefile) {
            this.backgroundimagefile = backgroundimagefile;
        }

        public String getReturnpolicyar() {
            return returnpolicyar;
        }

        public void setReturnpolicyar(String returnpolicyar) {
            this.returnpolicyar = returnpolicyar;
        }

        public String getReturnpolicyen() {
            return returnpolicyen;
        }

        public void setReturnpolicyen(String returnpolicyen) {
            this.returnpolicyen = returnpolicyen;
        }

        public String getTermsandconditionsar() {
            return termsandconditionsar;
        }

        public void setTermsandconditionsar(String termsandconditionsar) {
            this.termsandconditionsar = termsandconditionsar;
        }

        public String getTermsandconditionsen() {
            return termsandconditionsen;
        }

        public void setTermsandconditionsen(String termsandconditionsen) {
            this.termsandconditionsen = termsandconditionsen;
        }

        public int getShippingprovider() {
            return shippingprovider;
        }

        public void setShippingprovider(int shippingprovider) {
            this.shippingprovider = shippingprovider;
        }

        public boolean getIsshippingworldwide() {
            return isshippingworldwide;
        }

        public void setIsshippingworldwide(boolean isshippingworldwide) {
            this.isshippingworldwide = isshippingworldwide;
        }

        public int getTransportationcharges() {
            return transportationcharges;
        }

        public void setTransportationcharges(int transportationcharges) {
            this.transportationcharges = transportationcharges;
        }

        public double getFreedeliveryabovevalue() {
            return freedeliveryabovevalue;
        }

        public void setFreedeliveryabovevalue(double freedeliveryabovevalue) {
            this.freedeliveryabovevalue = freedeliveryabovevalue;
        }

        public int getMinimumordervalue() {
            return minimumordervalue;
        }

        public void setMinimumordervalue(int minimumordervalue) {
            this.minimumordervalue = minimumordervalue;
        }

        public int getDeliverycharges() {
            return deliverycharges;
        }

        public void setDeliverycharges(int deliverycharges) {
            this.deliverycharges = deliverycharges;
        }

        public int getPrebookingdeliverytime() {
            return prebookingdeliverytime;
        }

        public void setPrebookingdeliverytime(int prebookingdeliverytime) {
            this.prebookingdeliverytime = prebookingdeliverytime;
        }

        public int getPrebookingdeliverytimeunit() {
            return prebookingdeliverytimeunit;
        }

        public void setPrebookingdeliverytimeunit(int prebookingdeliverytimeunit) {
            this.prebookingdeliverytimeunit = prebookingdeliverytimeunit;
        }

        public int getIsdeliverycompany() {
            return isdeliverycompany;
        }

        public void setIsdeliverycompany(int isdeliverycompany) {
            this.isdeliverycompany = isdeliverycompany;
        }

        public boolean getIsdelivery() {
            return isdelivery;
        }

        public void setIsdelivery(boolean isdelivery) {
            this.isdelivery = isdelivery;
        }

        public int getPrebookinginstoretime() {
            return prebookinginstoretime;
        }

        public void setPrebookinginstoretime(int prebookinginstoretime) {
            this.prebookinginstoretime = prebookinginstoretime;
        }

        public int getPrebookinginstoretimeunit() {
            return prebookinginstoretimeunit;
        }

        public void setPrebookinginstoretimeunit(int prebookinginstoretimeunit) {
            this.prebookinginstoretimeunit = prebookinginstoretimeunit;
        }

        public boolean getInstoreservice() {
            return instoreservice;
        }

        public void setInstoreservice(boolean instoreservice) {
            this.instoreservice = instoreservice;
        }

        public boolean getPickup() {
            return pickup;
        }

        public void setPickup(boolean pickup) {
            this.pickup = pickup;
        }

        public String getBusinesssince() {
            return businesssince;
        }

        public void setBusinesssince(String businesssince) {
            this.businesssince = businesssince;
        }

        public String getStrbusinesssince() {
            return strbusinesssince;
        }

        public void setStrbusinesssince(String strbusinesssince) {
            this.strbusinesssince = strbusinesssince;
        }

        public int getAddresstypeid() {
            return addresstypeid;
        }

        public void setAddresstypeid(int addresstypeid) {
            this.addresstypeid = addresstypeid;
        }

        public String getApartmentofficeno() {
            return apartmentofficeno;
        }

        public void setApartmentofficeno(String apartmentofficeno) {
            this.apartmentofficeno = apartmentofficeno;
        }

        public String getFloorno() {
            return floorno;
        }

        public void setFloorno(String floorno) {
            this.floorno = floorno;
        }

        public String getBuildingno() {
            return buildingno;
        }

        public void setBuildingno(String buildingno) {
            this.buildingno = buildingno;
        }

        public String getStreetno() {
            return streetno;
        }

        public void setStreetno(String streetno) {
            this.streetno = streetno;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getDistrictar() {
            return districtar;
        }

        public void setDistrictar(String districtar) {
            this.districtar = districtar;
        }

        public String getCityar() {
            return cityar;
        }

        public void setCityar(String cityar) {
            this.cityar = cityar;
        }

        public String getRegionar() {
            return regionar;
        }

        public void setRegionar(String regionar) {
            this.regionar = regionar;
        }

        public String getCountryar() {
            return countryar;
        }

        public void setCountryar(String countryar) {
            this.countryar = countryar;
        }

        public String getDistricten() {
            return districten;
        }

        public void setDistricten(String districten) {
            this.districten = districten;
        }

        public String getCityen() {
            return cityen;
        }

        public void setCityen(String cityen) {
            this.cityen = cityen;
        }

        public String getRegionen() {
            return regionen;
        }

        public void setRegionen(String regionen) {
            this.regionen = regionen;
        }

        public String getCountryen() {
            return countryen;
        }

        public void setCountryen(String countryen) {
            this.countryen = countryen;
        }

        public int getDistrictid() {
            return districtid;
        }

        public void setDistrictid(int districtid) {
            this.districtid = districtid;
        }

        public int getCityid() {
            return cityid;
        }

        public void setCityid(int cityid) {
            this.cityid = cityid;
        }

        public int getRegionid() {
            return regionid;
        }

        public void setRegionid(int regionid) {
            this.regionid = regionid;
        }

        public int getCountryid() {
            return countryid;
        }

        public void setCountryid(int countryid) {
            this.countryid = countryid;
        }

        public String getDescriptionar() {
            return descriptionar;
        }

        public void setDescriptionar(String descriptionar) {
            this.descriptionar = descriptionar;
        }

        public String getDescriptionen() {
            return descriptionen;
        }

        public void setDescriptionen(String descriptionen) {
            this.descriptionen = descriptionen;
        }

        public String getBranchnamear() {
            return branchnamear;
        }

        public void setBranchnamear(String branchnamear) {
            this.branchnamear = branchnamear;
        }

        public String getBranchnameen() {
            return branchnameen;
        }

        public void setBranchnameen(String branchnameen) {
            this.branchnameen = branchnameen;
        }

        public int getStoreid() {
            return storeid;
        }

        public void setStoreid(int storeid) {
            this.storeid = storeid;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getActionname() {
            return actionname;
        }

        public void setActionname(String actionname) {
            this.actionname = actionname;
        }

        public String getControllername() {
            return controllername;
        }

        public void setControllername(String controllername) {
            this.controllername = controllername;
        }
    }
}
