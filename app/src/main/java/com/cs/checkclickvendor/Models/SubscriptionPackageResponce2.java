package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;


public class SubscriptionPackageResponce2 {

    @Expose
    @SerializedName("Data")
    private ArrayList<DataEntity> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<DataEntity> getData() {
        return Data;
    }

    public void setData(ArrayList<DataEntity> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class DataEntity {
        @Expose
        @SerializedName("DiscountedPrice")
        private float DiscountedPrice;
        @Expose
        @SerializedName("DiscountPercent")
        private int DiscountPercent;
        @Expose
        @SerializedName("ApplyDiscount")
        private boolean ApplyDiscount;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("SlabName")
        private String SlabName;
        @Expose
        @SerializedName("PlanDuration")
        private int PlanDuration;
        @Expose
        @SerializedName("PlanPrice")
        private int PlanPrice;
        @Expose
        @SerializedName("CoupanNoOfRegions")
        private int CoupanNoOfRegions;
        @Expose
        @SerializedName("CoupanNoOfUsers")
        private int CoupanNoOfUsers;
        @Expose
        @SerializedName("CoupanDuration")
        private int CoupanDuration;
        @Expose
        @SerializedName("NoOfCoupans")
        private int NoOfCoupans;
        @Expose
        @SerializedName("AdvNoOfRegions")
        private int AdvNoOfRegions;
        @Expose
        @SerializedName("AdvDuration")
        private int AdvDuration;
        @Expose
        @SerializedName("AdvNoOfOffers")
        private int AdvNoOfOffers;
        @Expose
        @SerializedName("NoOfAccess")
        private int NoOfAccess;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public float getDiscountedPrice() {
            return DiscountedPrice;
        }

        public void setDiscountedPrice(float DiscountedPrice) {
            this.DiscountedPrice = DiscountedPrice;
        }

        public int getDiscountPercent() {
            return DiscountPercent;
        }

        public void setDiscountPercent(int DiscountPercent) {
            this.DiscountPercent = DiscountPercent;
        }

        public boolean getApplyDiscount() {
            return ApplyDiscount;
        }

        public void setApplyDiscount(boolean ApplyDiscount) {
            this.ApplyDiscount = ApplyDiscount;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public String getSlabName() {
            return SlabName;
        }

        public void setSlabName(String SlabName) {
            this.SlabName = SlabName;
        }

        public int getPlanDuration() {
            return PlanDuration;
        }

        public void setPlanDuration(int PlanDuration) {
            this.PlanDuration = PlanDuration;
        }

        public int getPlanPrice() {
            return PlanPrice;
        }

        public void setPlanPrice(int PlanPrice) {
            this.PlanPrice = PlanPrice;
        }

        public int getCoupanNoOfRegions() {
            return CoupanNoOfRegions;
        }

        public void setCoupanNoOfRegions(int CoupanNoOfRegions) {
            this.CoupanNoOfRegions = CoupanNoOfRegions;
        }

        public int getCoupanNoOfUsers() {
            return CoupanNoOfUsers;
        }

        public void setCoupanNoOfUsers(int CoupanNoOfUsers) {
            this.CoupanNoOfUsers = CoupanNoOfUsers;
        }

        public int getCoupanDuration() {
            return CoupanDuration;
        }

        public void setCoupanDuration(int CoupanDuration) {
            this.CoupanDuration = CoupanDuration;
        }

        public int getNoOfCoupans() {
            return NoOfCoupans;
        }

        public void setNoOfCoupans(int NoOfCoupans) {
            this.NoOfCoupans = NoOfCoupans;
        }

        public int getAdvNoOfRegions() {
            return AdvNoOfRegions;
        }

        public void setAdvNoOfRegions(int AdvNoOfRegions) {
            this.AdvNoOfRegions = AdvNoOfRegions;
        }

        public int getAdvDuration() {
            return AdvDuration;
        }

        public void setAdvDuration(int AdvDuration) {
            this.AdvDuration = AdvDuration;
        }

        public int getAdvNoOfOffers() {
            return AdvNoOfOffers;
        }

        public void setAdvNoOfOffers(int AdvNoOfOffers) {
            this.AdvNoOfOffers = AdvNoOfOffers;
        }

        public int getNoOfAccess() {
            return NoOfAccess;
        }

        public void setNoOfAccess(int NoOfAccess) {
            this.NoOfAccess = NoOfAccess;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
