package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OrderTypeList {

    @Expose
    @SerializedName("Data")
    private ArrayList<Data> data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("Recipients")
        private ArrayList<Recipients> recipients;
        @Expose
        @SerializedName("ServiceProviders")
        private ArrayList<ServiceProviders> serviceproviders;
        @Expose
        @SerializedName("TrackingDetails")
        private ArrayList<TrackingDetails> trackingdetails;
        @Expose
        @SerializedName("UserDetails")
        private ArrayList<UserDetails> userdetails;
        @Expose
        @SerializedName("Items")
        private ArrayList<Items> items;
        @Expose
        @SerializedName("ItemsCount")
        private int itemscount;
        @Expose
        @SerializedName("StoreType")
        private String storetype;
        @Expose
        @SerializedName("Discount")
        private double discount;
        @Expose
        @SerializedName("GrandTotal")
        private double grandtotal;
        @Expose
        @SerializedName("DeliveryFee")
        private double deliveryfee;
        @Expose
        @SerializedName("VAT")
        private double vat;
        @Expose
        @SerializedName("SubTotal")
        private double subtotal;
        @Expose
        @SerializedName("ExpectingDelivery")
        private String expectingdelivery;
        @Expose
        @SerializedName("EndTime")
        private String endtime;
        @Expose
        @SerializedName("StartTime")
        private String starttime;
        @Expose
        @SerializedName("TimeSlotId")
        private int timeslotid;
        @Expose
        @SerializedName("OrderType")
        private int ordertype;
        @Expose
        @SerializedName("AddressId")
        private int addressid;
        @Expose
        @SerializedName("OrderStatus")
        private int orderstatus;
        @Expose
        @SerializedName("OrderDate")
        private String orderdate;
        @Expose
        @SerializedName("awbNo")
        private String awbNo;
        @Expose
        @SerializedName("BranchNameAr")
        private String branchnamear;
        @Expose
        @SerializedName("BranchNameEn")
        private String branchnameen;
        @Expose
        @SerializedName("BranchId")
        private int branchid;
        @Expose
        @SerializedName("Comments")
        private String comments;
        @Expose
        @SerializedName("UserId")
        private int userid;
        @Expose
        @SerializedName("PaymentStatus")
        private boolean paymentstatus;
        @Expose
        @SerializedName("PaymentType")
        private String paymenttype;
        @Expose
        @SerializedName("PaymentMode")
        private String paymentmode;
        @Expose
        @SerializedName("OrderStatusEn")
        private String orderstatusen;
        @Expose
        @SerializedName("InvoiceNo")
        private String invoiceno;
        @Expose
        @SerializedName("OrderId")
        private int orderid;
        private String multipleimages;

        public String getAwbNo() {
            return awbNo;
        }

        public void setAwbNo(String awbNo) {
            this.awbNo = awbNo;
        }

        public ArrayList<Recipients> getRecipients() {
            return recipients;
        }

        public void setRecipients(ArrayList<Recipients> recipients) {
            this.recipients = recipients;
        }

        public ArrayList<ServiceProviders> getServiceproviders() {
            return serviceproviders;
        }

        public void setServiceproviders(ArrayList<ServiceProviders> serviceproviders) {
            this.serviceproviders = serviceproviders;
        }

        public ArrayList<TrackingDetails> getTrackingdetails() {
            return trackingdetails;
        }

        public void setTrackingdetails(ArrayList<TrackingDetails> trackingdetails) {
            this.trackingdetails = trackingdetails;
        }

        public ArrayList<UserDetails> getUserdetails() {
            return userdetails;
        }

        public void setUserdetails(ArrayList<UserDetails> userdetails) {
            this.userdetails = userdetails;
        }

        public ArrayList<Items> getItems() {
            return items;
        }

        public void setItems(ArrayList<Items> items) {
            this.items = items;
        }

        public int getItemscount() {
            return itemscount;
        }

        public void setItemscount(int itemscount) {
            this.itemscount = itemscount;
        }

        public String getStoretype() {
            return storetype;
        }

        public void setStoretype(String storetype) {
            this.storetype = storetype;
        }

        public double getDiscount() {
            return discount;
        }

        public void setDiscount(double discount) {
            this.discount = discount;
        }

        public double getGrandtotal() {
            return grandtotal;
        }

        public void setGrandtotal(double grandtotal) {
            this.grandtotal = grandtotal;
        }

        public double getDeliveryfee() {
            return deliveryfee;
        }

        public void setDeliveryfee(double deliveryfee) {
            this.deliveryfee = deliveryfee;
        }

        public double getVat() {
            return vat;
        }

        public void setVat(double vat) {
            this.vat = vat;
        }

        public double getSubtotal() {
            return subtotal;
        }

        public void setSubtotal(double subtotal) {
            this.subtotal = subtotal;
        }

        public String getExpectingdelivery() {
            return expectingdelivery;
        }

        public void setExpectingdelivery(String expectingdelivery) {
            this.expectingdelivery = expectingdelivery;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public int getTimeslotid() {
            return timeslotid;
        }

        public void setTimeslotid(int timeslotid) {
            this.timeslotid = timeslotid;
        }

        public int getOrdertype() {
            return ordertype;
        }

        public void setOrdertype(int ordertype) {
            this.ordertype = ordertype;
        }

        public int getAddressid() {
            return addressid;
        }

        public void setAddressid(int addressid) {
            this.addressid = addressid;
        }

        public int getOrderstatus() {
            return orderstatus;
        }

        public void setOrderstatus(int orderstatus) {
            this.orderstatus = orderstatus;
        }

        public String getOrderdate() {
            return orderdate;
        }

        public void setOrderdate(String orderdate) {
            this.orderdate = orderdate;
        }

        public String getBranchnamear() {
            return branchnamear;
        }

        public void setBranchnamear(String branchnamear) {
            this.branchnamear = branchnamear;
        }

        public String getBranchnameen() {
            return branchnameen;
        }

        public void setBranchnameen(String branchnameen) {
            this.branchnameen = branchnameen;
        }

        public int getBranchid() {
            return branchid;
        }

        public void setBranchid(int branchid) {
            this.branchid = branchid;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }

        public boolean getPaymentstatus() {
            return paymentstatus;
        }

        public void setPaymentstatus(boolean paymentstatus) {
            this.paymentstatus = paymentstatus;
        }

        public String getPaymenttype() {
            return paymenttype;
        }

        public void setPaymenttype(String paymenttype) {
            this.paymenttype = paymenttype;
        }

        public String getPaymentmode() {
            return paymentmode;
        }

        public void setPaymentmode(String paymentmode) {
            this.paymentmode = paymentmode;
        }

        public String getOrderstatusen() {
            return orderstatusen;
        }

        public void setOrderstatusen(String orderstatusen) {
            this.orderstatusen = orderstatusen;
        }

        public String getInvoiceno() {
            return invoiceno;
        }

        public void setInvoiceno(String invoiceno) {
            this.invoiceno = invoiceno;
        }

        public int getOrderid() {
            return orderid;
        }

        public void setOrderid(int orderid) {
            this.orderid = orderid;
        }

        public String getMultipleimages() {
            return multipleimages;
        }

        public void setMultipleimages(String multipleimages) {
            this.multipleimages = multipleimages;
        }
    }

    public static class Recipients  implements Serializable{
        @Expose
        @SerializedName("MobileNo")
        private String mobileno;
        @Expose
        @SerializedName("Name")
        private String name;
        @Expose
        @SerializedName("Id")
        private int id;

        public String getMobileno() {
            return mobileno;
        }

        public void setMobileno(String mobileno) {
            this.mobileno = mobileno;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class ServiceProviders  implements Serializable{
        @Expose
        @SerializedName("MobileNo")
        private String mobileno;
        @Expose
        @SerializedName("Name")
        private String name;
        @Expose
        @SerializedName("Id")
        private int id;

        public String getMobileno() {
            return mobileno;
        }

        public void setMobileno(String mobileno) {
            this.mobileno = mobileno;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class TrackingDetails  implements Serializable{
        @Expose
        @SerializedName("OrderStatusId")
        private int orderstatusid;
        @Expose
        @SerializedName("Message")
        private String message;
        @Expose
        @SerializedName("UserId")
        private int userid;
        @Expose
        @SerializedName("CreatedOn")
        private String createdon;
        @Expose
        @SerializedName("OrderStatus")
        private String orderstatus;

        public int getOrderstatusid() {
            return orderstatusid;
        }

        public void setOrderstatusid(int orderstatusid) {
            this.orderstatusid = orderstatusid;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }

        public String getCreatedon() {
            return createdon;
        }

        public void setCreatedon(String createdon) {
            this.createdon = createdon;
        }

        public String getOrderstatus() {
            return orderstatus;
        }

        public void setOrderstatus(String orderstatus) {
            this.orderstatus = orderstatus;
        }
    }

    public static class UserDetails  implements Serializable{
        @Expose
        @SerializedName("Address")
        private String address;
        @Expose
        @SerializedName("EmailId")
        private String emailid;
        @Expose
        @SerializedName("MobileNo")
        private String mobileno;
        @Expose
        @SerializedName("LastName")
        private String lastname;
        @Expose
        @SerializedName("FirstName")
        private String firstname;
        @Expose
        @SerializedName("UserId")
        private int userid;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEmailid() {
            return emailid;
        }

        public void setEmailid(String emailid) {
            this.emailid = emailid;
        }

        public String getMobileno() {
            return mobileno;
        }

        public void setMobileno(String mobileno) {
            this.mobileno = mobileno;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }
    }

    public static class Items  implements Serializable{
        @Expose
        @SerializedName("Variants")
        private ArrayList<Variants> variants;
        @Expose
        @SerializedName("SubCategoryAr")
        private String subcategoryar;
        @Expose
        @SerializedName("SubCategoryEn")
        private String subcategoryen;
        @Expose
        @SerializedName("Manufacturer")
        private String manufacturer;
        @Expose
        @SerializedName("IsAccepted")
        private boolean isaccepted;
        @Expose
        @SerializedName("GrandTotal")
        private double grandtotal;
        @Expose
        @SerializedName("VAT")
        private double vat;
        @Expose
        @SerializedName("Price")
        private double price;
        @Expose
        @SerializedName("Qty")
        private int qty;
        @Expose
        @SerializedName("SKUId")
        private String skuid;
        @Expose
        @SerializedName("OrderId")
        private int orderid;
        @Expose
        @SerializedName("Image")
        private String image;
        @Expose
        @SerializedName("NameAr")
        private String namear;
        @Expose
        @SerializedName("NameEn")
        private String nameen;
        @Expose
        @SerializedName("ItemId")
        private int itemid;
        @Expose
        @SerializedName("OrderItemId")
        private int orderitemid;
        private boolean accept = false;
        private boolean reject = false;
        private String variantsname;
        private String variantsname_ar;
        private String comment;

        public ArrayList<Variants> getVariants() {
            return variants;
        }

        public void setVariants(ArrayList<Variants> variants) {
            this.variants = variants;
        }

        public String getSubcategoryar() {
            return subcategoryar;
        }

        public void setSubcategoryar(String subcategoryar) {
            this.subcategoryar = subcategoryar;
        }

        public String getSubcategoryen() {
            return subcategoryen;
        }

        public void setSubcategoryen(String subcategoryen) {
            this.subcategoryen = subcategoryen;
        }

        public String getManufacturer() {
            return manufacturer;
        }

        public void setManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
        }

        public boolean getIsaccepted() {
            return isaccepted;
        }

        public void setIsaccepted(boolean isaccepted) {
            this.isaccepted = isaccepted;
        }

        public double getGrandtotal() {
            return grandtotal;
        }

        public void setGrandtotal(double grandtotal) {
            this.grandtotal = grandtotal;
        }

        public double getVat() {
            return vat;
        }

        public void setVat(double vat) {
            this.vat = vat;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public int getQty() {
            return qty;
        }

        public void setQty(int qty) {
            this.qty = qty;
        }

        public String getSkuid() {
            return skuid;
        }

        public void setSkuid(String skuid) {
            this.skuid = skuid;
        }

        public int getOrderid() {
            return orderid;
        }

        public void setOrderid(int orderid) {
            this.orderid = orderid;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getNamear() {
            return namear;
        }

        public void setNamear(String namear) {
            this.namear = namear;
        }

        public String getNameen() {
            return nameen;
        }

        public void setNameen(String nameen) {
            this.nameen = nameen;
        }

        public int getItemid() {
            return itemid;
        }

        public void setItemid(int itemid) {
            this.itemid = itemid;
        }

        public int getOrderitemid() {
            return orderitemid;
        }

        public void setOrderitemid(int orderitemid) {
            this.orderitemid = orderitemid;
        }

        public boolean isAccept() {
            return accept;
        }

        public void setAccept(boolean accept) {
            this.accept = accept;
        }

        public boolean isReject() {
            return reject;
        }

        public void setReject(boolean reject) {
            this.reject = reject;
        }

        public String getVariantsname() {
            return variantsname;
        }

        public void setVariantsname(String variantsname) {
            this.variantsname = variantsname;
        }

        public String getVariantsname_ar() {
            return variantsname_ar;
        }

        public void setVariantsname_ar(String variantsname_ar) {
            this.variantsname_ar = variantsname_ar;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }
    }

    public static class Variants  implements Serializable{
        @Expose
        @SerializedName("OptionValueAr")
        private String optionvaluear;
        @Expose
        @SerializedName("OptionValueEn")
        private String optionvalueen;
        @Expose
        @SerializedName("OptionAr")
        private String optionar;
        @Expose
        @SerializedName("OptionEn")
        private String optionen;

        public String getOptionvaluear() {
            return optionvaluear;
        }

        public void setOptionvaluear(String optionvaluear) {
            this.optionvaluear = optionvaluear;
        }

        public String getOptionvalueen() {
            return optionvalueen;
        }

        public void setOptionvalueen(String optionvalueen) {
            this.optionvalueen = optionvalueen;
        }

        public String getOptionar() {
            return optionar;
        }

        public void setOptionar(String optionar) {
            this.optionar = optionar;
        }

        public String getOptionen() {
            return optionen;
        }

        public void setOptionen(String optionen) {
            this.optionen = optionen;
        }
    }

}
