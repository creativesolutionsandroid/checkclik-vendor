package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class SubscriptionPackageResponce implements Serializable {

    @Expose
    @SerializedName("Data")
    private ArrayList<DataEntity> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<DataEntity> getData() {
        return Data;
    }

    public void setData(ArrayList<DataEntity> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class DataEntity {
        @Expose
        @SerializedName("Days")
        private String Days;
        @Expose
        @SerializedName("NoDays")
        private String NoDays;
        @Expose
        @SerializedName("Expiry")
        private String Expiry;
        @Expose
        @SerializedName("AccessId")
        private int AccessId;
        @Expose
        @SerializedName("PlanExpiry")
        private String PlanExpiry;
        @Expose
        @SerializedName("EndDate")
        private String EndDate;
        @Expose
        @SerializedName("StartDate")
        private String StartDate;
        @Expose
        @SerializedName("PlanNameAr")
        private String PlanNameAr;
        @Expose
        @SerializedName("PlanNameEn")
        private String PlanNameEn;
        @Expose
        @SerializedName("PlanId")
        private int PlanId;
        @Expose
        @SerializedName("ID")
        private int ID;

        public String getDays() {
            return Days;
        }

        public void setDays(String Days) {
            this.Days = Days;
        }

        public String getNoDays() {
            return NoDays;
        }

        public void setNoDays(String NoDays) {
            this.NoDays = NoDays;
        }

        public String getExpiry() {
            return Expiry;
        }

        public void setExpiry(String Expiry) {
            this.Expiry = Expiry;
        }

        public int getAccessId() {
            return AccessId;
        }

        public void setAccessId(int AccessId) {
            this.AccessId = AccessId;
        }

        public String getPlanExpiry() {
            return PlanExpiry;
        }

        public void setPlanExpiry(String PlanExpiry) {
            this.PlanExpiry = PlanExpiry;
        }

        public String getEndDate() {
            return EndDate;
        }

        public void setEndDate(String EndDate) {
            this.EndDate = EndDate;
        }

        public String getStartDate() {
            return StartDate;
        }

        public void setStartDate(String StartDate) {
            this.StartDate = StartDate;
        }

        public String getPlanNameAr() {
            return PlanNameAr;
        }

        public void setPlanNameAr(String PlanNameAr) {
            this.PlanNameAr = PlanNameAr;
        }

        public String getPlanNameEn() {
            return PlanNameEn;
        }

        public void setPlanNameEn(String PlanNameEn) {
            this.PlanNameEn = PlanNameEn;
        }

        public int getPlanId() {
            return PlanId;
        }

        public void setPlanId(int PlanId) {
            this.PlanId = PlanId;
        }

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }
    }
}
