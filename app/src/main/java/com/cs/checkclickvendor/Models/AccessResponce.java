package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

public  class AccessResponce implements Serializable {

    @Expose
    @SerializedName("Data")
    private ArrayList<DataEntity> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<DataEntity> getData() {
        return Data;
    }

    public void setData(ArrayList<DataEntity> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class DataEntity implements Serializable {
        @Expose
        @SerializedName("ListOfAccessUsers")
        private ArrayList<ListOfAccessUsersEntity> ListOfAccessUsers;
        @Expose
        @SerializedName("NoOfBranches")
        private int NoOfBranches;
        @Expose
        @SerializedName("CategoryListAr")
        private String CategoryListAr;
        @Expose
        @SerializedName("CategoryListEn")
        private String CategoryListEn;
        @Expose
        @SerializedName("EntityNameAr")
        private String EntityNameAr;
        @Expose
        @SerializedName("EntityNameEn")
        private String EntityNameEn;
        @Expose
        @SerializedName("StoreNameEn")
        private String StoreNameEn;
        @Expose
        @SerializedName("StoreNameAr")
        private String StoreNameAr;
        @Expose
        @SerializedName("StoreType")
        private int StoreType;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;

        public ArrayList<ListOfAccessUsersEntity> getListOfAccessUsers() {
            return ListOfAccessUsers;
        }

        public void setListOfAccessUsers(ArrayList<ListOfAccessUsersEntity> ListOfAccessUsers) {
            this.ListOfAccessUsers = ListOfAccessUsers;
        }

        public int getNoOfBranches() {
            return NoOfBranches;
        }

        public void setNoOfBranches(int NoOfBranches) {
            this.NoOfBranches = NoOfBranches;
        }

        public String getCategoryListAr() {
            return CategoryListAr;
        }

        public void setCategoryListAr(String CategoryListAr) {
            this.CategoryListAr = CategoryListAr;
        }

        public String getCategoryListEn() {
            return CategoryListEn;
        }

        public void setCategoryListEn(String CategoryListEn) {
            this.CategoryListEn = CategoryListEn;
        }
        public String getEntityNameAr() {
            return EntityNameAr;
        }

        public void setEntityNameAr(String EntityNameAr) {
            this.EntityNameAr = EntityNameAr;
        }

        public String getEntityNameEn() {
            return EntityNameEn;
        }

        public void setEntityNameEn(String EntityNameEn) {
            this.EntityNameEn = EntityNameEn;
        }

        public String getStoreNameEn() {
            return StoreNameEn;
        }

        public void setStoreNameEn(String StoreNameEn) {
            this.StoreNameEn = StoreNameEn;
        }

        public String getStoreNameAr() {
            return StoreNameAr;
        }

        public void setStoreNameAr(String StoreNameAr) {
            this.StoreNameAr = StoreNameAr;
        }

        public int getStoreType() {
            return StoreType;
        }

        public void setStoreType(int StoreType) {
            this.StoreType = StoreType;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }
    }
    public static class ListOfAccessUsersEntity implements Serializable {
        @Expose
        @SerializedName("MenusJson")
        private ArrayList<MenusJsonEntity> MenusJson;
        @Expose
        @SerializedName("AccessImage")
        private String AccessImage;
        @Expose
        @SerializedName("PosiotionNameAr")
        private String PosiotionNameAr;
        @Expose
        @SerializedName("PosiotionNameEn")
        private String PosiotionNameEn;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("AccessId")
        private int AccessId;

        public ArrayList<MenusJsonEntity> getMenusJson() {
            return MenusJson;
        }

        public void setMenusJson(ArrayList<MenusJsonEntity> MenusJson) {
            this.MenusJson = MenusJson;
        }

        public String getAccessImage() {
            return AccessImage;
        }

        public void setAccessImage(String AccessImage) {
            this.AccessImage = AccessImage;
        }

        public String getPosiotionNameAr() {
            return PosiotionNameAr;
        }

        public void setPosiotionNameAr(String PosiotionNameAr) {
            this.PosiotionNameAr = PosiotionNameAr;
        }

        public String getPosiotionNameEn() {
            return PosiotionNameEn;
        }

        public void setPosiotionNameEn(String PosiotionNameEn) {
            this.PosiotionNameEn = PosiotionNameEn;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public int getAccessId() {
            return AccessId;
        }

        public void setAccessId(int AccessId) {
            this.AccessId = AccessId;
        }
    }

    public static class MenusJsonEntity implements Serializable {
        @Expose
        @SerializedName("Branches")
        private ArrayList<BranchesEntity> Branches;
        @Expose
        @SerializedName("MenuId")
        private int MenuId;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;

        public ArrayList<BranchesEntity> getBranches() {
            return Branches;
        }

        public void setBranches(ArrayList<BranchesEntity> Branches) {
            this.Branches = Branches;
        }

        public int getMenuId() {
            return MenuId;
        }

        public void setMenuId(int MenuId) {
            this.MenuId = MenuId;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }
    }

    public static class BranchesEntity implements Serializable {
        @Expose
        @SerializedName("BranchNameEn")
        private String BranchNameEn;
        @Expose
        @SerializedName("BranchNameAr")
        private String BranchNameAr;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;

        public String getBranchNameEn() {
            return BranchNameEn;
        }

        public void setBranchNameEn(String BranchNameEn) {
            this.BranchNameEn = BranchNameEn;
        }

        public String getBranchNameAr() {
            return BranchNameAr;
        }

        public void setBranchNameAr(String BranchNameAr) {
            this.BranchNameAr = BranchNameAr;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }
    }
}
