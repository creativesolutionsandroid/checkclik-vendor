package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.SerializedName;

public class DataEntity {
    @SerializedName("UserId")
    private int UserId;
    @SerializedName("Language")
    private String Language;
    @SerializedName("MobileNo")
    private String MobileNo;

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String Language) {
        this.Language = Language;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String MobileNo) {
        this.MobileNo = MobileNo;
    }
}
