package com.cs.checkclickvendor.Models;

import java.util.ArrayList;

public class TimeSlot_Invoice_no {

    String mTimeSlot;
    int exp1;
    int order_id1;
    ArrayList<TimeTable_Invoiceno> invoicenos = new ArrayList<>();

    public String getmTimeSlot() {
        return mTimeSlot;
    }

    public void setmTimeSlot(String mTimeSlot) {
        this.mTimeSlot = mTimeSlot;
    }

    public int getExp1() {
        return exp1;
    }

    public void setExp1(int exp1) {
        this.exp1 = exp1;
    }

    public int getOrder_id1() {
        return order_id1;
    }

    public void setOrder_id1(int order_id1) {
        this.order_id1 = order_id1;
    }

    public ArrayList<TimeTable_Invoiceno> getInvoicenos() {
        return invoicenos;
    }

    public void setInvoicenos(ArrayList<TimeTable_Invoiceno> invoicenos) {
        this.invoicenos = invoicenos;
    }
}

