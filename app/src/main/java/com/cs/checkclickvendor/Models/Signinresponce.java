package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class Signinresponce {
    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("MadeChatFriends")
        private boolean MadeChatFriends;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("JobPositionId")
        private int JobPositionId;
        @Expose
        @SerializedName("FlagId")
        private int FlagId;
        @Expose
        @SerializedName("StoreCount")
        private int StoreCount;
        @Expose
        @SerializedName("Message")
        private String Message;
        @Expose
        @SerializedName("SelectedStoreId")
        private int SelectedStoreId;
        @Expose
        @SerializedName("AccessType")
        private int AccessType;
        @Expose
        @SerializedName("VendorType")
        private int VendorType;
        @Expose
        @SerializedName("UserType")
        private int UserType;
        @Expose
        @SerializedName("ChatUserId")
        private String ChatUserId;
        @Expose
        @SerializedName("Language")
        private String Language;
        @Expose
        @SerializedName("Phone")
        private String Phone;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("FullName")
        private String FullName;
        @Expose
        @SerializedName("UserId")
        private String UserId;

        public boolean getMadeChatFriends() {
            return MadeChatFriends;
        }

        public void setMadeChatFriends(boolean MadeChatFriends) {
            this.MadeChatFriends = MadeChatFriends;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getJobPositionId() {
            return JobPositionId;
        }

        public void setJobPositionId(int JobPositionId) {
            this.JobPositionId = JobPositionId;
        }

        public int getFlagId() {
            return FlagId;
        }

        public void setFlagId(int FlagId) {
            this.FlagId = FlagId;
        }

        public int getStoreCount() {
            return StoreCount;
        }

        public void setStoreCount(int StoreCount) {
            this.StoreCount = StoreCount;
        }

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getSelectedStoreId() {
            return SelectedStoreId;
        }

        public void setSelectedStoreId(int SelectedStoreId) {
            this.SelectedStoreId = SelectedStoreId;
        }

        public int getAccessType() {
            return AccessType;
        }

        public void setAccessType(int AccessType) {
            this.AccessType = AccessType;
        }

        public int getVendorType() {
            return VendorType;
        }

        public void setVendorType(int VendorType) {
            this.VendorType = VendorType;
        }

        public int getUserType() {
            return UserType;
        }

        public void setUserType(int UserType) {
            this.UserType = UserType;
        }

        public String getChatUserId() {
            return ChatUserId;
        }

        public void setChatUserId(String ChatUserId) {
            this.ChatUserId = ChatUserId;
        }

        public String getLanguage() {
            return Language;
        }

        public void setLanguage(String Language) {
            this.Language = Language;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public String getUserId() {
            return UserId;
        }

        public void setUserId(String UserId) {
            this.UserId = UserId;
        }
    }
}
