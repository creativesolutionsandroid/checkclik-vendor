package com.cs.checkclickvendor.Models;

public class TimeSlots {

    String timeslot, timeslot_id;

    public String getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(String timeslot) {
        this.timeslot = timeslot;
    }

    public String getTimeslot_id() {
        return timeslot_id;
    }

    public void setTimeslot_id(String timeslot_id) {
        this.timeslot_id = timeslot_id;
    }
}
