package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RescheduleTimeSlotelist {

    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("TimeSlotList")
        private ArrayList<TimeSlotList> timeslotlist;

        public ArrayList<TimeSlotList> getTimeslotlist() {
            return timeslotlist;
        }

        public void setTimeslotlist(ArrayList<TimeSlotList> timeslotlist) {
            this.timeslotlist = timeslotlist;
        }
    }

    public static class TimeSlotList {
        @Expose
        @SerializedName("TotalAcceptOrders")
        private int totalacceptorders;
        @Expose
        @SerializedName("Acceptedorders")
        private int acceptedorders;
        @Expose
        @SerializedName("EndTime")
        private String endtime;
        @Expose
        @SerializedName("StartTime")
        private String starttime;
        @Expose
        @SerializedName("TimeSlot")
        private String timeslot;
        @Expose
        @SerializedName("SlotId")
        private int slotid;
        @Expose
        @SerializedName("WeekDayId")
        private int weekdayid;
        @Expose
        @SerializedName("OrderAcceptedPerHour")
        private int orderacceptedperhour;
        @Expose
        @SerializedName("TimingType")
        private String timingtype;
        @Expose
        @SerializedName("ProductManagementId")
        private int productmanagementid;
        @Expose
        @SerializedName("Day")
        private String day;

        public int getTotalacceptorders() {
            return totalacceptorders;
        }

        public void setTotalacceptorders(int totalacceptorders) {
            this.totalacceptorders = totalacceptorders;
        }

        public int getAcceptedorders() {
            return acceptedorders;
        }

        public void setAcceptedorders(int acceptedorders) {
            this.acceptedorders = acceptedorders;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getTimeslot() {
            return timeslot;
        }

        public void setTimeslot(String timeslot) {
            this.timeslot = timeslot;
        }

        public int getSlotid() {
            return slotid;
        }

        public void setSlotid(int slotid) {
            this.slotid = slotid;
        }

        public int getWeekdayid() {
            return weekdayid;
        }

        public void setWeekdayid(int weekdayid) {
            this.weekdayid = weekdayid;
        }

        public int getOrderacceptedperhour() {
            return orderacceptedperhour;
        }

        public void setOrderacceptedperhour(int orderacceptedperhour) {
            this.orderacceptedperhour = orderacceptedperhour;
        }

        public String getTimingtype() {
            return timingtype;
        }

        public void setTimingtype(String timingtype) {
            this.timingtype = timingtype;
        }

        public int getProductmanagementid() {
            return productmanagementid;
        }

        public void setProductmanagementid(int productmanagementid) {
            this.productmanagementid = productmanagementid;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }
    }
}
