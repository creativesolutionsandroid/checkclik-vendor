package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Notifications {

    @Expose
    @SerializedName("Data")
    private List<Data> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public List<Data> getData() {
        return Data;
    }

    public void setData(List<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("NotificationType")
        private int NotificationType;
        @Expose
        @SerializedName("SentDate")
        private String SentDate;
        @Expose
        @SerializedName("MessageAr")
        private String MessageAr;
        @Expose
        @SerializedName("MessageEn")
        private String MessageEn;
        @Expose
        @SerializedName("IsRead")
        private boolean IsRead;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getNotificationType() {
            return NotificationType;
        }

        public void setNotificationType(int NotificationType) {
            this.NotificationType = NotificationType;
        }

        public String getSentDate() {
            return SentDate;
        }

        public void setSentDate(String SentDate) {
            this.SentDate = SentDate;
        }

        public String getMessageAr() {
            return MessageAr;
        }

        public void setMessageAr(String MessageAr) {
            this.MessageAr = MessageAr;
        }

        public String getMessageEn() {
            return MessageEn;
        }

        public void setMessageEn(String MessageEn) {
            this.MessageEn = MessageEn;
        }

        public boolean getIsRead() {
            return IsRead;
        }

        public void setIsRead(boolean IsRead) {
            this.IsRead = IsRead;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
