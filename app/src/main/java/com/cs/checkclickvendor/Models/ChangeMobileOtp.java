package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.SerializedName;

public class ChangeMobileOtp {

    @SerializedName("Data")
    private DataEntity Data;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Status")
    private boolean Status;

    public DataEntity getData() {
        return Data;
    }

    public void setData(DataEntity Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class DataEntity {
        @SerializedName("StatusMessage")
        private String StatusMessage;
        @SerializedName("StatusCode")
        private String StatusCode;
        @SerializedName("FlagId")
        private int FlagId;
        @SerializedName("ValidUntil")
        private String ValidUntil;
        @SerializedName("OTPCode")
        private String OTPCode;
        @SerializedName("Message")
        private String Message;
        @SerializedName("MobileNo")
        private String MobileNo;
        @SerializedName("Id")
        private int Id;

        public String getStatusMessage() {
            return StatusMessage;
        }

        public void setStatusMessage(String StatusMessage) {
            this.StatusMessage = StatusMessage;
        }

        public String getStatusCode() {
            return StatusCode;
        }

        public void setStatusCode(String StatusCode) {
            this.StatusCode = StatusCode;
        }

        public int getFlagId() {
            return FlagId;
        }

        public void setFlagId(int FlagId) {
            this.FlagId = FlagId;
        }

        public String getValidUntil() {
            return ValidUntil;
        }

        public void setValidUntil(String ValidUntil) {
            this.ValidUntil = ValidUntil;
        }

        public String getOTPCode() {
            return OTPCode;
        }

        public void setOTPCode(String OTPCode) {
            this.OTPCode = OTPCode;
        }

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
