package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DashBoardList {


    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("MessageAr")
    private String messagear;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessagear() {
        return messagear;
    }

    public void setMessagear(String messagear) {
        this.messagear = messagear;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("CountrySale")
        private ArrayList<CountrySale> countrysale;
        @Expose
        @SerializedName("CitySale")
        private ArrayList<CitySale> citysale;
        @Expose
        @SerializedName("MapData")
        private ArrayList<MapData> mapdata;
        @Expose
        @SerializedName("Dashboard")
        private ArrayList<Dashboard> dashboard;

        public ArrayList<CountrySale> getCountrysale() {
            return countrysale;
        }

        public void setCountrysale(ArrayList<CountrySale> countrysale) {
            this.countrysale = countrysale;
        }

        public ArrayList<CitySale> getCitysale() {
            return citysale;
        }

        public void setCitysale(ArrayList<CitySale> citysale) {
            this.citysale = citysale;
        }

        public ArrayList<MapData> getMapdata() {
            return mapdata;
        }

        public void setMapdata(ArrayList<MapData> mapdata) {
            this.mapdata = mapdata;
        }

        public ArrayList<Dashboard> getDashboard() {
            return dashboard;
        }

        public void setDashboard(ArrayList<Dashboard> dashboard) {
            this.dashboard = dashboard;
        }
    }

    public static class CountrySale {
        @Expose
        @SerializedName("CountryOrdersCount")
        private int countryorderscount;
        @Expose
        @SerializedName("CountryNameAr")
        private String countrynamear;
        @Expose
        @SerializedName("CountryNameEn")
        private String countrynameen;
        @Expose
        @SerializedName("Id")
        private int id;

        public int getCountryorderscount() {
            return countryorderscount;
        }

        public void setCountryorderscount(int countryorderscount) {
            this.countryorderscount = countryorderscount;
        }

        public String getCountrynamear() {
            return countrynamear;
        }

        public void setCountrynamear(String countrynamear) {
            this.countrynamear = countrynamear;
        }

        public String getCountrynameen() {
            return countrynameen;
        }

        public void setCountrynameen(String countrynameen) {
            this.countrynameen = countrynameen;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class CitySale {
        @Expose
        @SerializedName("Longitude")
        private String longitude;
        @Expose
        @SerializedName("Latitude")
        private String latitude;
        @Expose
        @SerializedName("OrdersCount")
        private int orderscount;
        @Expose
        @SerializedName("CityNameAr")
        private String citynamear;
        @Expose
        @SerializedName("CityNameEn")
        private String citynameen;
        @Expose
        @SerializedName("CityId")
        private int cityid;

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public int getOrderscount() {
            return orderscount;
        }

        public void setOrderscount(int orderscount) {
            this.orderscount = orderscount;
        }

        public String getCitynamear() {
            return citynamear;
        }

        public void setCitynamear(String citynamear) {
            this.citynamear = citynamear;
        }

        public String getCitynameen() {
            return citynameen;
        }

        public void setCitynameen(String citynameen) {
            this.citynameen = citynameen;
        }

        public int getCityid() {
            return cityid;
        }

        public void setCityid(int cityid) {
            this.cityid = cityid;
        }
    }

    public static class MapData {
        @Expose
        @SerializedName("StoreNameAr")
        private String storenamear;
        @Expose
        @SerializedName("StoreNameEn")
        private String storenameen;
        @Expose
        @SerializedName("StoreId")
        private int storeid;
        @Expose
        @SerializedName("Longitude")
        private String longitude;
        @Expose
        @SerializedName("Latitude")
        private String latitude;
        @Expose
        @SerializedName("BranchNameAr")
        private String branchnamear;
        @Expose
        @SerializedName("BranchNameEn")
        private String branchnameen;
        @Expose
        @SerializedName("BranchId")
        private int branchid;

        public String getStorenamear() {
            return storenamear;
        }

        public void setStorenamear(String storenamear) {
            this.storenamear = storenamear;
        }

        public String getStorenameen() {
            return storenameen;
        }

        public void setStorenameen(String storenameen) {
            this.storenameen = storenameen;
        }

        public int getStoreid() {
            return storeid;
        }

        public void setStoreid(int storeid) {
            this.storeid = storeid;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getBranchnamear() {
            return branchnamear;
        }

        public void setBranchnamear(String branchnamear) {
            this.branchnamear = branchnamear;
        }

        public String getBranchnameen() {
            return branchnameen;
        }

        public void setBranchnameen(String branchnameen) {
            this.branchnameen = branchnameen;
        }

        public int getBranchid() {
            return branchid;
        }

        public void setBranchid(int branchid) {
            this.branchid = branchid;
        }
    }

    public static class Dashboard {
        @Expose
        @SerializedName("JScheduledBranches")
        private ArrayList<JScheduledBranches> jscheduledbranches;
        @Expose
        @SerializedName("JPendingBranches")
        private ArrayList<JPendingBranches> jpendingbranches;
        @Expose
        @SerializedName("ScheduledBranches")
        private String scheduledbranches;
        @Expose
        @SerializedName("PendingBranches")
        private String pendingbranches;
        @Expose
        @SerializedName("TodayRevenue")
        private double todayrevenue;
        @Expose
        @SerializedName("TotalRevenue")
        private double totalrevenue;
        @Expose
        @SerializedName("TotalScheduled")
        private int totalscheduled;
        @Expose
        @SerializedName("Closed")
        private int closed;
        @Expose
        @SerializedName("Scheduled")
        private int scheduled;
        @Expose
        @SerializedName("TotalPending")
        private int totalpending;
        @Expose
        @SerializedName("Accepted")
        private int accepted;
        @Expose
        @SerializedName("Pending")
        private int pending;

        public ArrayList<JScheduledBranches> getJscheduledbranches() {
            return jscheduledbranches;
        }

        public void setJscheduledbranches(ArrayList<JScheduledBranches> jscheduledbranches) {
            this.jscheduledbranches = jscheduledbranches;
        }

        public ArrayList<JPendingBranches> getJpendingbranches() {
            return jpendingbranches;
        }

        public void setJpendingbranches(ArrayList<JPendingBranches> jpendingbranches) {
            this.jpendingbranches = jpendingbranches;
        }

        public String getScheduledbranches() {
            return scheduledbranches;
        }

        public void setScheduledbranches(String scheduledbranches) {
            this.scheduledbranches = scheduledbranches;
        }

        public String getPendingbranches() {
            return pendingbranches;
        }

        public void setPendingbranches(String pendingbranches) {
            this.pendingbranches = pendingbranches;
        }

        public double getTodayrevenue() {
            return todayrevenue;
        }

        public void setTodayrevenue(double todayrevenue) {
            this.todayrevenue = todayrevenue;
        }

        public double getTotalrevenue() {
            return totalrevenue;
        }

        public void setTotalrevenue(double totalrevenue) {
            this.totalrevenue = totalrevenue;
        }

        public int getTotalscheduled() {
            return totalscheduled;
        }

        public void setTotalscheduled(int totalscheduled) {
            this.totalscheduled = totalscheduled;
        }

        public int getClosed() {
            return closed;
        }

        public void setClosed(int closed) {
            this.closed = closed;
        }

        public int getScheduled() {
            return scheduled;
        }

        public void setScheduled(int scheduled) {
            this.scheduled = scheduled;
        }

        public int getTotalpending() {
            return totalpending;
        }

        public void setTotalpending(int totalpending) {
            this.totalpending = totalpending;
        }

        public int getAccepted() {
            return accepted;
        }

        public void setAccepted(int accepted) {
            this.accepted = accepted;
        }

        public int getPending() {
            return pending;
        }

        public void setPending(int pending) {
            this.pending = pending;
        }
    }

    public static class JScheduledBranches {
        @Expose
        @SerializedName("TotalRevenue")
        private double totalrevenue;
        @Expose
        @SerializedName("Total")
        private int total;
        @Expose
        @SerializedName("TotalPending")
        private int totalpending;
        @Expose
        @SerializedName("PlanExpiry")
        private String planexpiry;
        @Expose
        @SerializedName("Address")
        private String address;
        @Expose
        @SerializedName("BranchNameAr")
        private String branchnamear;
        @Expose
        @SerializedName("BranchNameEn")
        private String branchnameen;
        @Expose
        @SerializedName("Id")
        private int id;

        public double getTotalrevenue() {
            return totalrevenue;
        }

        public void setTotalrevenue(double totalrevenue) {
            this.totalrevenue = totalrevenue;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getTotalpending() {
            return totalpending;
        }

        public void setTotalpending(int totalpending) {
            this.totalpending = totalpending;
        }

        public String getPlanexpiry() {
            return planexpiry;
        }

        public void setPlanexpiry(String planexpiry) {
            this.planexpiry = planexpiry;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getBranchnamear() {
            return branchnamear;
        }

        public void setBranchnamear(String branchnamear) {
            this.branchnamear = branchnamear;
        }

        public String getBranchnameen() {
            return branchnameen;
        }

        public void setBranchnameen(String branchnameen) {
            this.branchnameen = branchnameen;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class JPendingBranches {
        @Expose
        @SerializedName("TotalRevenue")
        private double totalrevenue;
        @Expose
        @SerializedName("Total")
        private int total;
        @Expose
        @SerializedName("TotalPending")
        private int totalpending;
        @Expose
        @SerializedName("PlanExpiry")
        private String planexpiry;
        @Expose
        @SerializedName("Address")
        private String address;
        @Expose
        @SerializedName("BranchNameAr")
        private String branchnamear;
        @Expose
        @SerializedName("BranchNameEn")
        private String branchnameen;
        @Expose
        @SerializedName("Id")
        private int id;

        public double getTotalrevenue() {
            return totalrevenue;
        }

        public void setTotalrevenue(double totalrevenue) {
            this.totalrevenue = totalrevenue;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getTotalpending() {
            return totalpending;
        }

        public void setTotalpending(int totalpending) {
            this.totalpending = totalpending;
        }

        public String getPlanexpiry() {
            return planexpiry;
        }

        public void setPlanexpiry(String planexpiry) {
            this.planexpiry = planexpiry;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getBranchnamear() {
            return branchnamear;
        }

        public void setBranchnamear(String branchnamear) {
            this.branchnamear = branchnamear;
        }

        public String getBranchnameen() {
            return branchnameen;
        }

        public void setBranchnameen(String branchnameen) {
            this.branchnameen = branchnameen;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
