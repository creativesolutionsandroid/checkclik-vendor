package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductStoreReviews {


    @Expose
    @SerializedName("Data")
    private List<Data> data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("CreatedOn")
        private String createdon;
        @Expose
        @SerializedName("OrderId")
        private int orderid;
        @Expose
        @SerializedName("BranchId")
        private int branchid;
        @Expose
        @SerializedName("ProfilePhoto")
        private String profilephoto;
        @Expose
        @SerializedName("FirstName")
        private String firstname;
        @Expose
        @SerializedName("Comments")
        private String comments;
        @Expose
        @SerializedName("Rating")
        private float rating;
        @Expose
        @SerializedName("NameAr")
        private String namear;
        @Expose
        @SerializedName("NameEn")
        private String nameen;
        @Expose
        @SerializedName("Id")
        private int id;

        public String getCreatedon() {
            return createdon;
        }

        public void setCreatedon(String createdon) {
            this.createdon = createdon;
        }

        public int getOrderid() {
            return orderid;
        }

        public void setOrderid(int orderid) {
            this.orderid = orderid;
        }

        public int getBranchid() {
            return branchid;
        }

        public void setBranchid(int branchid) {
            this.branchid = branchid;
        }

        public String getProfilephoto() {
            return profilephoto;
        }

        public void setProfilephoto(String profilephoto) {
            this.profilephoto = profilephoto;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public float getRating() {
            return rating;
        }

        public void setRating(float rating) {
            this.rating = rating;
        }

        public String getNamear() {
            return namear;
        }

        public void setNamear(String namear) {
            this.namear = namear;
        }

        public String getNameen() {
            return nameen;
        }

        public void setNameen(String nameen) {
            this.nameen = nameen;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
