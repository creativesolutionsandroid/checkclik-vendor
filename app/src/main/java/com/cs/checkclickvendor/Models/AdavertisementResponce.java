package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class AdavertisementResponce {

    @SerializedName("Data")
    private ArrayList<DataEntity> Data;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<DataEntity> getData() {
        return Data;
    }

    public void setData(ArrayList<DataEntity> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class DataEntity implements Serializable {
        @SerializedName("JProductDetails")
        private ArrayList<JProductDetailsEntity> JProductDetails;
        @SerializedName("NoOfUses")
        private int NoOfUses;
        @SerializedName("ProductDetails")
        private String ProductDetails;
        @SerializedName("Price")
        private int Price;
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @SerializedName("PaymentStatus")
        private boolean PaymentStatus;
        @SerializedName("Regions")
        private String Regions;
        @SerializedName("Position")
        private int Position;
        @SerializedName("StoreId")
        private int StoreId;
        @SerializedName("WeekNo")
        private int WeekNo;
        @SerializedName("IsUptoApplicable")
        private boolean IsUptoApplicable;
        @SerializedName("CountryId")
        private int CountryId;
        @SerializedName("EndDate")
        private String EndDate;
        @SerializedName("StartDate")
        private String StartDate;
        @SerializedName("ProductId")
        private int ProductId;
        @SerializedName("Percentage")
        private int Percentage;
        @SerializedName("BannerImageArName")
        private String BannerImageArName;
        @SerializedName("BannerImageEnName")
        private String BannerImageEnName;
        @SerializedName("OfferType")
        private int OfferType;
        @SerializedName("NameAr")
        private String NameAr;
        @SerializedName("NameEn")
        private String NameEn;
        @SerializedName("Id")
        private int Id;

        public ArrayList<JProductDetailsEntity> getJProductDetails() {
            return JProductDetails;
        }

        public void setJProductDetails(ArrayList<JProductDetailsEntity> JProductDetails) {
            this.JProductDetails = JProductDetails;
        }

        public int getNoOfUses() {
            return NoOfUses;
        }

        public void setNoOfUses(int NoOfUses) {
            this.NoOfUses = NoOfUses;
        }

        public String getProductDetails() {
            return ProductDetails;
        }

        public void setProductDetails(String ProductDetails) {
            this.ProductDetails = ProductDetails;
        }

        public int getPrice() {
            return Price;
        }

        public void setPrice(int Price) {
            this.Price = Price;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public boolean getPaymentStatus() {
            return PaymentStatus;
        }

        public void setPaymentStatus(boolean PaymentStatus) {
            this.PaymentStatus = PaymentStatus;
        }

        public String getRegions() {
            return Regions;
        }

        public void setRegions(String Regions) {
            this.Regions = Regions;
        }

        public int getPosition() {
            return Position;
        }

        public void setPosition(int Position) {
            this.Position = Position;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public int getWeekNo() {
            return WeekNo;
        }

        public void setWeekNo(int WeekNo) {
            this.WeekNo = WeekNo;
        }

        public boolean getIsUptoApplicable() {
            return IsUptoApplicable;
        }

        public void setIsUptoApplicable(boolean IsUptoApplicable) {
            this.IsUptoApplicable = IsUptoApplicable;
        }

        public int getCountryId() {
            return CountryId;
        }

        public void setCountryId(int CountryId) {
            this.CountryId = CountryId;
        }

        public String getEndDate() {
            return EndDate;
        }

        public void setEndDate(String EndDate) {
            this.EndDate = EndDate;
        }

        public String getStartDate() {
            return StartDate;
        }

        public void setStartDate(String StartDate) {
            this.StartDate = StartDate;
        }

        public int getProductId() {
            return ProductId;
        }

        public void setProductId(int ProductId) {
            this.ProductId = ProductId;
        }

        public int getPercentage() {
            return Percentage;
        }

        public void setPercentage(int Percentage) {
            this.Percentage = Percentage;
        }

        public String getBannerImageArName() {
            return BannerImageArName;
        }

        public void setBannerImageArName(String BannerImageArName) {
            this.BannerImageArName = BannerImageArName;
        }

        public String getBannerImageEnName() {
            return BannerImageEnName;
        }

        public void setBannerImageEnName(String BannerImageEnName) {
            this.BannerImageEnName = BannerImageEnName;
        }

        public int getOfferType() {
            return OfferType;
        }

        public void setOfferType(int OfferType) {
            this.OfferType = OfferType;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class JProductDetailsEntity implements Serializable {
        @SerializedName("Variant")
        private ArrayList<VariantEntity> Variant;
        @SerializedName("AfterPrice")
        private float AfterPrice;
        @SerializedName("skuId")
        private String skuId;
        @SerializedName("BeforePrice")
        private double BeforePrice;
        @SerializedName("NameAr")
        private String NameAr;
        @SerializedName("NameEn")
        private String NameEn;

        public ArrayList<VariantEntity> getVariant() {
            return Variant;
        }

        public void setVariant(ArrayList<VariantEntity> Variant) {
            this.Variant = Variant;
        }

        public float getAfterPrice() {
            return AfterPrice;
        }

        public void setAfterPrice(int AfterPrice) {
            this.AfterPrice = AfterPrice;
        }

        public String getSkuId() {
            return skuId;
        }

        public void setSkuId(String skuId) {
            this.skuId = skuId;
        }

        public double getBeforePrice() {
            return BeforePrice;
        }

        public void setBeforePrice(double BeforePrice) {
            this.BeforePrice = BeforePrice;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }
    }

    public static class VariantEntity implements Serializable {
        @SerializedName("OptionValueAr")
        private String OptionValueAr;
        @SerializedName("OptionValueEn")
        private String OptionValueEn;
        @SerializedName("OptionAr")
        private String OptionAr;
        @SerializedName("OptionEn")
        private String OptionEn;

        public String getOptionValueAr() {
            return OptionValueAr;
        }

        public void setOptionValueAr(String OptionValueAr) {
            this.OptionValueAr = OptionValueAr;
        }

        public String getOptionValueEn() {
            return OptionValueEn;
        }

        public void setOptionValueEn(String OptionValueEn) {
            this.OptionValueEn = OptionValueEn;
        }

        public String getOptionAr() {
            return OptionAr;
        }

        public void setOptionAr(String OptionAr) {
            this.OptionAr = OptionAr;
        }

        public String getOptionEn() {
            return OptionEn;
        }

        public void setOptionEn(String OptionEn) {
            this.OptionEn = OptionEn;
        }
    }
}
