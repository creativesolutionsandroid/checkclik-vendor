package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderManagmentList {


    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("AutoAcceptConfig")
        private ArrayList<AutoAcceptConfig> autoacceptconfig;
        @Expose
        @SerializedName("ProcessOrders")
        private ArrayList<ProcessOrders> processorders;
        @Expose
        @SerializedName("Scheduled")
        private ArrayList<Scheduled> scheduled;
        @Expose
        @SerializedName("Pending")
        private ArrayList<Pending> pending;
        @Expose
        @SerializedName("DashboardCount")
        private ArrayList<DashboardCount> dashboardcount;
        @Expose
        @SerializedName("BranchAr")
        private String branchar;
        @Expose
        @SerializedName("BranchEn")
        private String branchen;
        @Expose
        @SerializedName("BranchId")
        private int branchid;

        public ArrayList<AutoAcceptConfig> getAutoacceptconfig() {
            return autoacceptconfig;
        }

        public void setAutoacceptconfig(ArrayList<AutoAcceptConfig> autoacceptconfig) {
            this.autoacceptconfig = autoacceptconfig;
        }

        public ArrayList<ProcessOrders> getProcessorders() {
            return processorders;
        }

        public void setProcessorders(ArrayList<ProcessOrders> processorders) {
            this.processorders = processorders;
        }

        public ArrayList<Scheduled> getScheduled() {
            return scheduled;
        }

        public void setScheduled(ArrayList<Scheduled> scheduled) {
            this.scheduled = scheduled;
        }

        public ArrayList<Pending> getPending() {
            return pending;
        }

        public void setPending(ArrayList<Pending> pending) {
            this.pending = pending;
        }

        public ArrayList<DashboardCount> getDashboardcount() {
            return dashboardcount;
        }

        public void setDashboardcount(ArrayList<DashboardCount> dashboardcount) {
            this.dashboardcount = dashboardcount;
        }

        public String getBranchar() {
            return branchar;
        }

        public void setBranchar(String branchar) {
            this.branchar = branchar;
        }

        public String getBranchen() {
            return branchen;
        }

        public void setBranchen(String branchen) {
            this.branchen = branchen;
        }

        public int getBranchid() {
            return branchid;
        }

        public void setBranchid(int branchid) {
            this.branchid = branchid;
        }
    }

    public static class AutoAcceptConfig {
        @Expose
        @SerializedName("IsPayLater")
        private boolean ispaylater;
        @Expose
        @SerializedName("IsPayNow")
        private boolean ispaynow;
        @Expose
        @SerializedName("IsCash")
        private boolean iscash;
        @Expose
        @SerializedName("BranchId")
        private int branchid;
        @Expose
        @SerializedName("Id")
        private int id;

        public boolean getIspaylater() {
            return ispaylater;
        }

        public void setIspaylater(boolean ispaylater) {
            this.ispaylater = ispaylater;
        }

        public boolean getIspaynow() {
            return ispaynow;
        }

        public void setIspaynow(boolean ispaynow) {
            this.ispaynow = ispaynow;
        }

        public boolean getIscash() {
            return iscash;
        }

        public void setIscash(boolean iscash) {
            this.iscash = iscash;
        }

        public int getBranchid() {
            return branchid;
        }

        public void setBranchid(int branchid) {
            this.branchid = branchid;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class ProcessOrders {
        @Expose
        @SerializedName("ReturnTransferOrders")
        private int returntransferorders;
        @Expose
        @SerializedName("ClosedOrders")
        private int closedorders;
        @Expose
        @SerializedName("CancelledOrders")
        private int cancelledorders;
        @Expose
        @SerializedName("ReturnOrders")
        private int returnorders;
        @Expose
        @SerializedName("DelayOrders")
        private int delayorders;
        @Expose
        @SerializedName("TransferOrders")
        private int transferorders;
        @Expose
        @SerializedName("ReturnTransferOrderStatus")
        private int returntransferorderstatus;
        @Expose
        @SerializedName("ClosedOrderStatus")
        private int closedorderstatus;
        @Expose
        @SerializedName("CancelledOrderStatus")
        private int cancelledorderstatus;
        @Expose
        @SerializedName("ReturnOrderStatus")
        private int returnorderstatus;
        @Expose
        @SerializedName("DelayOrderStatus")
        private int delayorderstatus;
        @Expose
        @SerializedName("TransferOrderStatus")
        private int transferorderstatus;

        public int getReturntransferorders() {
            return returntransferorders;
        }

        public void setReturntransferorders(int returntransferorders) {
            this.returntransferorders = returntransferorders;
        }

        public int getClosedorders() {
            return closedorders;
        }

        public void setClosedorders(int closedorders) {
            this.closedorders = closedorders;
        }

        public int getCancelledorders() {
            return cancelledorders;
        }

        public void setCancelledorders(int cancelledorders) {
            this.cancelledorders = cancelledorders;
        }

        public int getReturnorders() {
            return returnorders;
        }

        public void setReturnorders(int returnorders) {
            this.returnorders = returnorders;
        }

        public int getDelayorders() {
            return delayorders;
        }

        public void setDelayorders(int delayorders) {
            this.delayorders = delayorders;
        }

        public int getTransferorders() {
            return transferorders;
        }

        public void setTransferorders(int transferorders) {
            this.transferorders = transferorders;
        }

        public int getReturntransferorderstatus() {
            return returntransferorderstatus;
        }

        public void setReturntransferorderstatus(int returntransferorderstatus) {
            this.returntransferorderstatus = returntransferorderstatus;
        }

        public int getClosedorderstatus() {
            return closedorderstatus;
        }

        public void setClosedorderstatus(int closedorderstatus) {
            this.closedorderstatus = closedorderstatus;
        }

        public int getCancelledorderstatus() {
            return cancelledorderstatus;
        }

        public void setCancelledorderstatus(int cancelledorderstatus) {
            this.cancelledorderstatus = cancelledorderstatus;
        }

        public int getReturnorderstatus() {
            return returnorderstatus;
        }

        public void setReturnorderstatus(int returnorderstatus) {
            this.returnorderstatus = returnorderstatus;
        }

        public int getDelayorderstatus() {
            return delayorderstatus;
        }

        public void setDelayorderstatus(int delayorderstatus) {
            this.delayorderstatus = delayorderstatus;
        }

        public int getTransferorderstatus() {
            return transferorderstatus;
        }

        public void setTransferorderstatus(int transferorderstatus) {
            this.transferorderstatus = transferorderstatus;
        }
    }

    public static class Scheduled {
        @Expose
        @SerializedName("ScheduledRemaining")
        private int scheduledremaining;
        @Expose
        @SerializedName("ScheduledTomorrow")
        private int scheduledtomorrow;
        @Expose
        @SerializedName("ScheduledToday")
        private int scheduledtoday;
        @Expose
        @SerializedName("ScheduledRemainingStatus")
        private int scheduledremainingstatus;
        @Expose
        @SerializedName("ScheduledTomorrowStatus")
        private int scheduledtomorrowstatus;
        @Expose
        @SerializedName("ScheduledTodayStatus")
        private int scheduledtodaystatus;

        public int getScheduledremaining() {
            return scheduledremaining;
        }

        public void setScheduledremaining(int scheduledremaining) {
            this.scheduledremaining = scheduledremaining;
        }

        public int getScheduledtomorrow() {
            return scheduledtomorrow;
        }

        public void setScheduledtomorrow(int scheduledtomorrow) {
            this.scheduledtomorrow = scheduledtomorrow;
        }

        public int getScheduledtoday() {
            return scheduledtoday;
        }

        public void setScheduledtoday(int scheduledtoday) {
            this.scheduledtoday = scheduledtoday;
        }

        public int getScheduledremainingstatus() {
            return scheduledremainingstatus;
        }

        public void setScheduledremainingstatus(int scheduledremainingstatus) {
            this.scheduledremainingstatus = scheduledremainingstatus;
        }

        public int getScheduledtomorrowstatus() {
            return scheduledtomorrowstatus;
        }

        public void setScheduledtomorrowstatus(int scheduledtomorrowstatus) {
            this.scheduledtomorrowstatus = scheduledtomorrowstatus;
        }

        public int getScheduledtodaystatus() {
            return scheduledtodaystatus;
        }

        public void setScheduledtodaystatus(int scheduledtodaystatus) {
            this.scheduledtodaystatus = scheduledtodaystatus;
        }
    }

    public static class Pending {
        @Expose
        @SerializedName("PendingNonUrgent")
        private int pendingnonurgent;
        @Expose
        @SerializedName("PendingUrgent")
        private int pendingurgent;
        @Expose
        @SerializedName("PendingNonUrgentStatus")
        private int pendingnonurgentstatus;
        @Expose
        @SerializedName("PendingUrgentStatus")
        private int pendingurgentstatus;

        public int getPendingnonurgent() {
            return pendingnonurgent;
        }

        public void setPendingnonurgent(int pendingnonurgent) {
            this.pendingnonurgent = pendingnonurgent;
        }

        public int getPendingurgent() {
            return pendingurgent;
        }

        public void setPendingurgent(int pendingurgent) {
            this.pendingurgent = pendingurgent;
        }

        public int getPendingnonurgentstatus() {
            return pendingnonurgentstatus;
        }

        public void setPendingnonurgentstatus(int pendingnonurgentstatus) {
            this.pendingnonurgentstatus = pendingnonurgentstatus;
        }

        public int getPendingurgentstatus() {
            return pendingurgentstatus;
        }

        public void setPendingurgentstatus(int pendingurgentstatus) {
            this.pendingurgentstatus = pendingurgentstatus;
        }
    }

    public static class DashboardCount {
        @Expose
        @SerializedName("ReturnCancelCount")
        private int returncancelcount;
        @Expose
        @SerializedName("PendingCount")
        private int pendingcount;
        @Expose
        @SerializedName("ScheduledCount")
        private int scheduledcount;
        @Expose
        @SerializedName("TotalRevenue")
        private double totalrevenue;
        @Expose
        @SerializedName("DeliveredCount")
        private int deliveredcount;

        public int getReturncancelcount() {
            return returncancelcount;
        }

        public void setReturncancelcount(int returncancelcount) {
            this.returncancelcount = returncancelcount;
        }

        public int getPendingcount() {
            return pendingcount;
        }

        public void setPendingcount(int pendingcount) {
            this.pendingcount = pendingcount;
        }

        public int getScheduledcount() {
            return scheduledcount;
        }

        public void setScheduledcount(int scheduledcount) {
            this.scheduledcount = scheduledcount;
        }

        public double getTotalrevenue() {
            return totalrevenue;
        }

        public void setTotalrevenue(double totalrevenue) {
            this.totalrevenue = totalrevenue;
        }

        public int getDeliveredcount() {
            return deliveredcount;
        }

        public void setDeliveredcount(int deliveredcount) {
            this.deliveredcount = deliveredcount;
        }
    }
}
