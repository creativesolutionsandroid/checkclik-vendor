package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class VerifyMobileResponse {
    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("StatusMessage")
        private String StatusMessage;
        @Expose
        @SerializedName("StatusCode")
        private String StatusCode;
        @Expose
        @SerializedName("FlagId")
        private int FlagId;
        @Expose
        @SerializedName("ValidUntil")
        private String ValidUntil;
        @Expose
        @SerializedName("OTPCode")
        private String OTPCode;
        @Expose
        @SerializedName("Message")
        private String Message;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getStatusMessage() {
            return StatusMessage;
        }

        public void setStatusMessage(String StatusMessage) {
            this.StatusMessage = StatusMessage;
        }

        public String getStatusCode() {
            return StatusCode;
        }

        public void setStatusCode(String StatusCode) {
            this.StatusCode = StatusCode;
        }

        public int getFlagId() {
            return FlagId;
        }

        public void setFlagId(int FlagId) {
            this.FlagId = FlagId;
        }

        public String getValidUntil() {
            return ValidUntil;
        }

        public void setValidUntil(String ValidUntil) {
            this.ValidUntil = ValidUntil;
        }

        public String getOTPCode() {
            return OTPCode;
        }

        public void setOTPCode(String OTPCode) {
            this.OTPCode = OTPCode;
        }

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
