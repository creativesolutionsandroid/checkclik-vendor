package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public  class ProductManagementResponce {


    @Expose
    @SerializedName("filterId")
    private int filterId;
    @Expose
    @SerializedName("EndDate")
    private String EndDate;
    @Expose
    @SerializedName("StartDate")
    private String StartDate;
    @Expose
    @SerializedName("TimeSlotId")
    private int TimeSlotId;
    @Expose
    @SerializedName("RequestDate")
    private String RequestDate;
    @Expose
    @SerializedName("ServiceProviderId")
    private int ServiceProviderId;
    @Expose
    @SerializedName("ServiceTypeId")
    private int ServiceTypeId;
    @Expose
    @SerializedName("OrderType")
    private int OrderType;
    @Expose
    @SerializedName("ServiceId")
    private int ServiceId;
    @Expose
    @SerializedName("TotalAcceptOrders")
    private int TotalAcceptOrders;
    @Expose
    @SerializedName("Acceptedorders")
    private int Acceptedorders;
    @Expose
    @SerializedName("SlotId")
    private int SlotId;
    @Expose
    @SerializedName("EndTime")
    private double EndTime;
    @Expose
    @SerializedName("StartTime")
    private double StartTime;
    @Expose
    @SerializedName("OrderAcceptedPerHour")
    private int OrderAcceptedPerHour;
    @Expose
    @SerializedName("ProductManagementId")
    private int ProductManagementId;
    @Expose
    @SerializedName("FlagId")
    private int FlagId;
    @Expose
    @SerializedName("CartId")
    private int CartId;
    @Expose
    @SerializedName("SortId")
    private int SortId;
    @Expose
    @SerializedName("BranchSubCategoryId")
    private int BranchSubCategoryId;
    @Expose
    @SerializedName("Date")
    private String Date;
    @Expose
    @SerializedName("CreatedOn")
    private String CreatedOn;
    @Expose
    @SerializedName("sellngPrice")
    private double sellngPrice;
    @Expose
    @SerializedName("ProductId")
    private int ProductId;
    @Expose
    @SerializedName("Returnable")
    private boolean Returnable;
    @Expose
    @SerializedName("LowQnty")
    private int LowQnty;
    @Expose
    @SerializedName("DisplayStockQuantity")
    private boolean DisplayStockQuantity;
    @Expose
    @SerializedName("NotReturnable")
    private boolean NotReturnable;
    @Expose
    @SerializedName("MaxQnty")
    private int MaxQnty;
    @Expose
    @SerializedName("MinQnty")
    private int MinQnty;
    @Expose
    @SerializedName("AvailabilityRange")
    private int AvailabilityRange;
    @Expose
    @SerializedName("StockQuantity")
    private int StockQuantity;
    @Expose
    @SerializedName("InventoryId")
    private int InventoryId;
    @Expose
    @SerializedName("IsActive")
    private boolean IsActive;
    @Expose
    @SerializedName("ManuFactureId")
    private int ManuFactureId;
    @Expose
    @SerializedName("SubCategoryId")
    private int SubCategoryId;
    @Expose
    @SerializedName("CategoryId")
    private int CategoryId;
    @Expose
    @SerializedName("Publised")
    private int Publised;
    @Expose
    @SerializedName("ProductName")
    private String ProductName;
    @Expose
    @SerializedName("ToDate")
    private String ToDate;
    @Expose
    @SerializedName("FromDate")
    private String FromDate;
    @Expose
    @SerializedName("PageSize")
    private int PageSize;
    @Expose
    @SerializedName("TotalRecords")
    private int TotalRecords;
    @Expose
    @SerializedName("PageNumber")
    private int PageNumber;
    @Expose
    @SerializedName("pagingNumber")
    private int pagingNumber;
    @Expose
    @SerializedName("CreatedBy")
    private int CreatedBy;
    @Expose
    @SerializedName("Id")
    private int Id;
    @Expose
    @SerializedName("Id_datatable")
    private int Id_datatable;
    @Expose
    @SerializedName("LowStock")
    private int LowStock;
    @Expose
    @SerializedName("SkuId")
    private String SkuId;
    @Expose
    @SerializedName("StoreId")
    private int StoreId;
    @Expose
    @SerializedName("BranchId")
    private int BranchId;
    @Expose
    @SerializedName("Data")
    private DataEntity Data;
    @Expose
    @SerializedName("ProductInventoryId")
    private int ProductInventoryId;
    @Expose
    @SerializedName("UserId")
    private int UserId;
    @Expose
    @SerializedName("WeekDayId")
    private int WeekDayId;
    @Expose
    @SerializedName("OrderId")
    private int OrderId;

    public int getFilterId() {
        return filterId;
    }

    public void setFilterId(int filterId) {
        this.filterId = filterId;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String EndDate) {
        this.EndDate = EndDate;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String StartDate) {
        this.StartDate = StartDate;
    }

    public int getTimeSlotId() {
        return TimeSlotId;
    }

    public void setTimeSlotId(int TimeSlotId) {
        this.TimeSlotId = TimeSlotId;
    }

    public String getRequestDate() {
        return RequestDate;
    }

    public void setRequestDate(String RequestDate) {
        this.RequestDate = RequestDate;
    }

    public int getServiceProviderId() {
        return ServiceProviderId;
    }

    public void setServiceProviderId(int ServiceProviderId) {
        this.ServiceProviderId = ServiceProviderId;
    }

    public int getServiceTypeId() {
        return ServiceTypeId;
    }

    public void setServiceTypeId(int ServiceTypeId) {
        this.ServiceTypeId = ServiceTypeId;
    }

    public int getOrderType() {
        return OrderType;
    }

    public void setOrderType(int OrderType) {
        this.OrderType = OrderType;
    }

    public int getServiceId() {
        return ServiceId;
    }

    public void setServiceId(int ServiceId) {
        this.ServiceId = ServiceId;
    }

    public int getTotalAcceptOrders() {
        return TotalAcceptOrders;
    }

    public void setTotalAcceptOrders(int TotalAcceptOrders) {
        this.TotalAcceptOrders = TotalAcceptOrders;
    }

    public int getAcceptedorders() {
        return Acceptedorders;
    }

    public void setAcceptedorders(int Acceptedorders) {
        this.Acceptedorders = Acceptedorders;
    }

    public int getSlotId() {
        return SlotId;
    }

    public void setSlotId(int SlotId) {
        this.SlotId = SlotId;
    }

    public double getEndTime() {
        return EndTime;
    }

    public void setEndTime(double EndTime) {
        this.EndTime = EndTime;
    }

    public double getStartTime() {
        return StartTime;
    }

    public void setStartTime(double StartTime) {
        this.StartTime = StartTime;
    }

    public int getOrderAcceptedPerHour() {
        return OrderAcceptedPerHour;
    }

    public void setOrderAcceptedPerHour(int OrderAcceptedPerHour) {
        this.OrderAcceptedPerHour = OrderAcceptedPerHour;
    }

    public int getProductManagementId() {
        return ProductManagementId;
    }

    public void setProductManagementId(int ProductManagementId) {
        this.ProductManagementId = ProductManagementId;
    }

    public int getFlagId() {
        return FlagId;
    }

    public void setFlagId(int FlagId) {
        this.FlagId = FlagId;
    }

    public int getCartId() {
        return CartId;
    }

    public void setCartId(int CartId) {
        this.CartId = CartId;
    }

    public int getSortId() {
        return SortId;
    }

    public void setSortId(int SortId) {
        this.SortId = SortId;
    }

    public int getBranchSubCategoryId() {
        return BranchSubCategoryId;
    }

    public void setBranchSubCategoryId(int BranchSubCategoryId) {
        this.BranchSubCategoryId = BranchSubCategoryId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String CreatedOn) {
        this.CreatedOn = CreatedOn;
    }

    public double getSellngPrice() {
        return sellngPrice;
    }

    public void setSellngPrice(double sellngPrice) {
        this.sellngPrice = sellngPrice;
    }

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int ProductId) {
        this.ProductId = ProductId;
    }

    public boolean getReturnable() {
        return Returnable;
    }

    public void setReturnable(boolean Returnable) {
        this.Returnable = Returnable;
    }

    public int getLowQnty() {
        return LowQnty;
    }

    public void setLowQnty(int LowQnty) {
        this.LowQnty = LowQnty;
    }

    public boolean getDisplayStockQuantity() {
        return DisplayStockQuantity;
    }

    public void setDisplayStockQuantity(boolean DisplayStockQuantity) {
        this.DisplayStockQuantity = DisplayStockQuantity;
    }

    public boolean getNotReturnable() {
        return NotReturnable;
    }

    public void setNotReturnable(boolean NotReturnable) {
        this.NotReturnable = NotReturnable;
    }

    public int getMaxQnty() {
        return MaxQnty;
    }

    public void setMaxQnty(int MaxQnty) {
        this.MaxQnty = MaxQnty;
    }

    public int getMinQnty() {
        return MinQnty;
    }

    public void setMinQnty(int MinQnty) {
        this.MinQnty = MinQnty;
    }

    public int getAvailabilityRange() {
        return AvailabilityRange;
    }

    public void setAvailabilityRange(int AvailabilityRange) {
        this.AvailabilityRange = AvailabilityRange;
    }

    public int getStockQuantity() {
        return StockQuantity;
    }

    public void setStockQuantity(int StockQuantity) {
        this.StockQuantity = StockQuantity;
    }

    public int getInventoryId() {
        return InventoryId;
    }

    public void setInventoryId(int InventoryId) {
        this.InventoryId = InventoryId;
    }

    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean IsActive) {
        this.IsActive = IsActive;
    }

    public int getManuFactureId() {
        return ManuFactureId;
    }

    public void setManuFactureId(int ManuFactureId) {
        this.ManuFactureId = ManuFactureId;
    }

    public int getSubCategoryId() {
        return SubCategoryId;
    }

    public void setSubCategoryId(int SubCategoryId) {
        this.SubCategoryId = SubCategoryId;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int CategoryId) {
        this.CategoryId = CategoryId;
    }

    public int getPublised() {
        return Publised;
    }

    public void setPublised(int Publised) {
        this.Publised = Publised;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public String getToDate() {
        return ToDate;
    }

    public void setToDate(String ToDate) {
        this.ToDate = ToDate;
    }

    public String getFromDate() {
        return FromDate;
    }

    public void setFromDate(String FromDate) {
        this.FromDate = FromDate;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int PageSize) {
        this.PageSize = PageSize;
    }

    public int getTotalRecords() {
        return TotalRecords;
    }

    public void setTotalRecords(int TotalRecords) {
        this.TotalRecords = TotalRecords;
    }

    public int getPageNumber() {
        return PageNumber;
    }

    public void setPageNumber(int PageNumber) {
        this.PageNumber = PageNumber;
    }

    public int getPagingNumber() {
        return pagingNumber;
    }

    public void setPagingNumber(int pagingNumber) {
        this.pagingNumber = pagingNumber;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getId_datatable() {
        return Id_datatable;
    }

    public void setId_datatable(int Id_datatable) {
        this.Id_datatable = Id_datatable;
    }

    public int getLowStock() {
        return LowStock;
    }

    public void setLowStock(int LowStock) {
        this.LowStock = LowStock;
    }

    public String getSkuId() {
        return SkuId;
    }

    public void setSkuId(String SkuId) {
        this.SkuId = SkuId;
    }

    public int getStoreId() {
        return StoreId;
    }

    public void setStoreId(int StoreId) {
        this.StoreId = StoreId;
    }

    public int getBranchId() {
        return BranchId;
    }

    public void setBranchId(int BranchId) {
        this.BranchId = BranchId;
    }

    public DataEntity getData() {
        return Data;
    }

    public void setData(DataEntity Data) {
        this.Data = Data;
    }

    public int getProductInventoryId() {
        return ProductInventoryId;
    }

    public void setProductInventoryId(int ProductInventoryId) {
        this.ProductInventoryId = ProductInventoryId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public int getWeekDayId() {
        return WeekDayId;
    }

    public void setWeekDayId(int WeekDayId) {
        this.WeekDayId = WeekDayId;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int OrderId) {
        this.OrderId = OrderId;
    }

    public static class DataEntity {
        @Expose
        @SerializedName("TotalRecords")
        private ArrayList<TotalRecordsEntity> TotalRecords;
        @Expose
        @SerializedName("ProductList")
        private ArrayList<ProductListEntity> ProductList;

        public ArrayList<TotalRecordsEntity> getTotalRecords() {
            return TotalRecords;
        }

        public void setTotalRecords(ArrayList<TotalRecordsEntity> TotalRecords) {
            this.TotalRecords = TotalRecords;
        }

        public ArrayList<ProductListEntity> getProductList() {
            return ProductList;
        }

        public void setProductList(ArrayList<ProductListEntity> ProductList) {
            this.ProductList = ProductList;
        }
    }

    public static class TotalRecordsEntity {
        @Expose
        @SerializedName("TotalRecords")
        private int TotalRecords;

        public int getTotalRecords() {
            return TotalRecords;
        }

        public void setTotalRecords(int TotalRecords) {
            this.TotalRecords = TotalRecords;
        }
    }

    public static class ProductListEntity {
        @Expose
        @SerializedName("InventoryMethod")
        private int InventoryMethod;
        @Expose
        @SerializedName("InventoryId")
        private int InventoryId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("ManufactureId")
        private int ManufactureId;
        @Expose
        @SerializedName("BranchSubCategoryId")
        private int BranchSubCategoryId;
        @Expose
        @SerializedName("BranchCategoryId")
        private int BranchCategoryId;
        @Expose
        @SerializedName("CreatedOn")
        private String CreatedOn;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("MainImage")
        private String MainImage;
        @Expose
        @SerializedName("SkuId")
        private String SkuId;
        @Expose
        @SerializedName("variant")
        private String variant;
        @Expose
        @SerializedName("BranchNameEn")
        private String BranchNameEn;
        @Expose
        @SerializedName("BranchNameAr")
        private String BranchNameAr;
        @Expose
        @SerializedName("ReservedQuantity")
        private int ReservedQuantity;
        @Expose
        @SerializedName("StockQuantity")
        private int StockQuantity;
        @Expose
        @SerializedName("sellngPrice")
        private double sellngPrice;
        @Expose
        @SerializedName("UPCBarcode")
        private String UPCBarcode;
        @Expose
        @SerializedName("ProductNameAr")
        private String ProductNameAr;
        @Expose
        @SerializedName("ProductNameEn")
        private String ProductNameEn;
        @Expose
        @SerializedName("ProductId")
        private int ProductId;
        @Expose
        @SerializedName("rowNum")
        private int rowNum;

        public int getInventoryMethod() {
            return InventoryMethod;
        }

        public void setInventoryMethod(int InventoryMethod) {
            this.InventoryMethod = InventoryMethod;
        }

        public int getInventoryId() {
            return InventoryId;
        }

        public void setInventoryId(int InventoryId) {
            this.InventoryId = InventoryId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getManufactureId() {
            return ManufactureId;
        }

        public void setManufactureId(int ManufactureId) {
            this.ManufactureId = ManufactureId;
        }

        public int getBranchSubCategoryId() {
            return BranchSubCategoryId;
        }

        public void setBranchSubCategoryId(int BranchSubCategoryId) {
            this.BranchSubCategoryId = BranchSubCategoryId;
        }

        public int getBranchCategoryId() {
            return BranchCategoryId;
        }

        public void setBranchCategoryId(int BranchCategoryId) {
            this.BranchCategoryId = BranchCategoryId;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String CreatedOn) {
            this.CreatedOn = CreatedOn;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public String getMainImage() {
            return MainImage;
        }

        public void setMainImage(String MainImage) {
            this.MainImage = MainImage;
        }

        public String getSkuId() {
            return SkuId;
        }

        public void setSkuId(String SkuId) {
            this.SkuId = SkuId;
        }

        public String getVariant() {
            return variant;
        }

        public void setVariant(String variant) {
            this.variant = variant;
        }

        public String getBranchNameEn() {
            return BranchNameEn;
        }

        public void setBranchNameEn(String BranchNameEn) {
            this.BranchNameEn = BranchNameEn;
        }

        public String getBranchNameAr() {
            return BranchNameAr;
        }

        public void setBranchNameAr(String BranchNameAr) {
            this.BranchNameAr = BranchNameAr;
        }

        public int getReservedQuantity() {
            return ReservedQuantity;
        }

        public void setReservedQuantity(int ReservedQuantity) {
            this.ReservedQuantity = ReservedQuantity;
        }

        public int getStockQuantity() {
            return StockQuantity;
        }

        public void setStockQuantity(int StockQuantity) {
            this.StockQuantity = StockQuantity;
        }

        public double getSellngPrice() {
            return sellngPrice;
        }

        public void setSellngPrice(double sellngPrice) {
            this.sellngPrice = sellngPrice;
        }

        public String getUPCBarcode() {
            return UPCBarcode;
        }

        public void setUPCBarcode(String UPCBarcode) {
            this.UPCBarcode = UPCBarcode;
        }

        public String getProductNameAr() {
            return ProductNameAr;
        }

        public void setProductNameAr(String ProductNameAr) {
            this.ProductNameAr = ProductNameAr;
        }

        public String getProductNameEn() {
            return ProductNameEn;
        }

        public void setProductNameEn(String ProductNameEn) {
            this.ProductNameEn = ProductNameEn;
        }

        public int getProductId() {
            return ProductId;
        }

        public void setProductId(int ProductId) {
            this.ProductId = ProductId;
        }

        public int getRowNum() {
            return rowNum;
        }

        public void setRowNum(int rowNum) {
            this.rowNum = rowNum;
        }
    }
}
