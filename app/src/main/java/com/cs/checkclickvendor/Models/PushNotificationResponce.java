package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public  class PushNotificationResponce {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("PNConfig")
        private ArrayList<PNConfig> PNConfig;
        @Expose
        @SerializedName("UserId")
        private int UserId;

        public ArrayList<PNConfig> getPNConfig() {
            return PNConfig;
        }

        public void setPNConfig(ArrayList<PNConfig> PNConfig) {
            this.PNConfig = PNConfig;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }
    }

    public static class PNConfig {
        @Expose
        @SerializedName("Status")
        private boolean Status;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("PNTypeId")
        private int PNTypeId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public boolean getStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getPNTypeId() {
            return PNTypeId;
        }

        public void setPNTypeId(int PNTypeId) {
            this.PNTypeId = PNTypeId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
