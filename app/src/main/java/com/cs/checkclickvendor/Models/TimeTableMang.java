package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TimeTableMang {

    @Expose
    @SerializedName("ServiceProvidersJson")
    private String ServiceProvidersJson;
    @Expose
    @SerializedName("ImagePath")
    private String ImagePath;
    @Expose
    @SerializedName("_URLLocation")
    private String _URLLocation;
    @Expose
    @SerializedName("Bytes")
    private String Bytes;
    @Expose
    @SerializedName("FileUploadLocation")
    private String FileUploadLocation;
    @Expose
    @SerializedName("FileName")
    private String FileName;
    @Expose
    @SerializedName("base64")
    private String base64;
    @Expose
    @SerializedName("Images_dt")
    private String Images_dt;
    @Expose
    @SerializedName("TimingType")
    private String TimingType;
    @Expose
    @SerializedName("ProductManagementId")
    private int ProductManagementId;
    @Expose
    @SerializedName("ProductShiftTimingsJson")
    private String ProductShiftTimingsJson;
    @Expose
    @SerializedName("ProductManagementJson")
    private String ProductManagementJson;
    @Expose
    @SerializedName("BranchId")
    private int BranchId;
    @Expose
    @SerializedName("StoreId")
    private int StoreId;
    @Expose
    @SerializedName("UserType")
    private int UserType;
    @Expose
    @SerializedName("UserId")
    private int UserId;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("datasetxml")
    private String datasetxml;
    @Expose
    @SerializedName("Data")
    private DataEntity Data;
    @Expose
    @SerializedName("ActionName")
    private String ActionName;
    @Expose
    @SerializedName("ControllerName")
    private String ControllerName;

    public String getServiceProvidersJson() {
        return ServiceProvidersJson;
    }

    public void setServiceProvidersJson(String ServiceProvidersJson) {
        this.ServiceProvidersJson = ServiceProvidersJson;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String ImagePath) {
        this.ImagePath = ImagePath;
    }

    public String get_URLLocation() {
        return _URLLocation;
    }

    public void set_URLLocation(String _URLLocation) {
        this._URLLocation = _URLLocation;
    }

    public String getBytes() {
        return Bytes;
    }

    public void setBytes(String Bytes) {
        this.Bytes = Bytes;
    }

    public String getFileUploadLocation() {
        return FileUploadLocation;
    }

    public void setFileUploadLocation(String FileUploadLocation) {
        this.FileUploadLocation = FileUploadLocation;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String FileName) {
        this.FileName = FileName;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public String getImages_dt() {
        return Images_dt;
    }

    public void setImages_dt(String Images_dt) {
        this.Images_dt = Images_dt;
    }

    public String getTimingType() {
        return TimingType;
    }

    public void setTimingType(String TimingType) {
        this.TimingType = TimingType;
    }

    public int getProductManagementId() {
        return ProductManagementId;
    }

    public void setProductManagementId(int ProductManagementId) {
        this.ProductManagementId = ProductManagementId;
    }

    public String getProductShiftTimingsJson() {
        return ProductShiftTimingsJson;
    }

    public void setProductShiftTimingsJson(String ProductShiftTimingsJson) {
        this.ProductShiftTimingsJson = ProductShiftTimingsJson;
    }

    public String getProductManagementJson() {
        return ProductManagementJson;
    }

    public void setProductManagementJson(String ProductManagementJson) {
        this.ProductManagementJson = ProductManagementJson;
    }

    public int getBranchId() {
        return BranchId;
    }

    public void setBranchId(int BranchId) {
        this.BranchId = BranchId;
    }

    public int getStoreId() {
        return StoreId;
    }

    public void setStoreId(int StoreId) {
        this.StoreId = StoreId;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int UserType) {
        this.UserType = UserType;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDatasetxml() {
        return datasetxml;
    }

    public void setDatasetxml(String datasetxml) {
        this.datasetxml = datasetxml;
    }

    public DataEntity getData() {
        return Data;
    }

    public void setData(DataEntity Data) {
        this.Data = Data;
    }

    public String getActionName() {
        return ActionName;
    }

    public void setActionName(String ActionName) {
        this.ActionName = ActionName;
    }

    public String getControllerName() {
        return ControllerName;
    }

    public void setControllerName(String ControllerName) {
        this.ControllerName = ControllerName;
    }

    public static class DataEntity implements Serializable {
        @Expose
        @SerializedName("ScheduledTitles")
        private ArrayList<ScheduledTitlesEntity> ScheduledTitles;
        @Expose
        @SerializedName("ScheduledOrders")
        private ArrayList<ScheduledOrdersEntity> ScheduledOrders;

        public ArrayList<ScheduledTitlesEntity> getScheduledTitles() {
            return ScheduledTitles;
        }

        public void setScheduledTitles(ArrayList<ScheduledTitlesEntity> ScheduledTitles) {
            this.ScheduledTitles = ScheduledTitles;
        }

        public ArrayList<ScheduledOrdersEntity> getScheduledOrders() {
            return ScheduledOrders;
        }

        public void setScheduledOrders(ArrayList<ScheduledOrdersEntity> ScheduledOrders) {
            this.ScheduledOrders = ScheduledOrders;
        }
    }

    public static class ScheduledTitlesEntity implements Serializable {
        @Expose
        @SerializedName("color")
        private String color;
        @Expose
        @SerializedName("OrderType")
        private int OrderType;
        @Expose
        @SerializedName("start")
        private String start;
        @Expose
        @SerializedName("title")
        private String title;

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public int getOrderType() {
            return OrderType;
        }

        public void setOrderType(int OrderType) {
            this.OrderType = OrderType;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class ScheduledOrdersEntity implements Serializable {
        @Expose
        @SerializedName("ServiceProviderName")
        private String ServiceProviderName;
        @Expose
        @SerializedName("ServiceProviderId")
        private int ServiceProviderId;
        @Expose
        @SerializedName("Expired")
        private int Expired;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;
        @Expose
        @SerializedName("TimeSlot")
        private String TimeSlot;
        @Expose
        @SerializedName("OrderType")
        private int OrderType;
        @Expose
        @SerializedName("start")
        private String start;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;

        public String getServiceProviderName() {
            return ServiceProviderName;
        }

        public void setServiceProviderName(String ServiceProviderName) {
            this.ServiceProviderName = ServiceProviderName;
        }

        public int getServiceProviderId() {
            return ServiceProviderId;
        }

        public void setServiceProviderId(int ServiceProviderId) {
            this.ServiceProviderId = ServiceProviderId;
        }

        public int getExpired() {
            return Expired;
        }

        public void setExpired(int Expired) {
            this.Expired = Expired;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public String getTimeSlot() {
            return TimeSlot;
        }

        public void setTimeSlot(String TimeSlot) {
            this.TimeSlot = TimeSlot;
        }

        public int getOrderType() {
            return OrderType;
        }

        public void setOrderType(int OrderType) {
            this.OrderType = OrderType;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }
    }
}
