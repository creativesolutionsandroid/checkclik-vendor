package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetOrderDetails implements Serializable {

    @Expose
    @SerializedName("Data")
    private List<Data> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public List<Data> getData() {
        return Data;
    }

    public void setData(List<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("Recipients")
        private List<Recipients> Recipients;
        @Expose
        @SerializedName("ServiceProviders")
        private List<ServiceProviders> ServiceProviders;
        @Expose
        @SerializedName("OrderPayments")
        private List<String> OrderPayments;
        @Expose
        @SerializedName("TrackingDetails")
        private List<TrackingDetails> TrackingDetails;
        @Expose
        @SerializedName("UserDetails")
        private List<UserDetails> UserDetails;
        @Expose
        @SerializedName("Items")
        private List<Items> Items;
        @Expose
        @SerializedName("ItemsCount")
        private int ItemsCount;
        @Expose
        @SerializedName("StoreType")
        private String StoreType;
        @Expose
        @SerializedName("awbNo")
        private String awbNo;
        @Expose
        @SerializedName("Discount")
        private int Discount;
        @Expose
        @SerializedName("GrandTotal")
        private double GrandTotal;
        @Expose
        @SerializedName("DeliveryFee")
        private int DeliveryFee;
        @Expose
        @SerializedName("VAT")
        private double VAT;
        @Expose
        @SerializedName("SubTotal")
        private double SubTotal;
        @Expose
        @SerializedName("ExpectingDelivery")
        private String ExpectingDelivery;
        @Expose
        @SerializedName("TimeSlotId")
        private int TimeSlotId;
        @Expose
        @SerializedName("OrderType")
        private int OrderType;
        @Expose
        @SerializedName("AddressId")
        private int AddressId;
        @Expose
        @SerializedName("OrderStatus")
        private int OrderStatus;
        @Expose
        @SerializedName("OrderDate")
        private String OrderDate;
        @Expose
        @SerializedName("BranchNameAr")
        private String BranchNameAr;
        @Expose
        @SerializedName("BranchNameEn")
        private String BranchNameEn;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("Comments")
        private String Comments;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("PaymentRegistrationId")
        private String PaymentRegistrationId;
        @Expose
        @SerializedName("PaymentStatus")
        private boolean PaymentStatus;
        @Expose
        @SerializedName("PaymentType")
        private String PaymentType;
        @Expose
        @SerializedName("PaymentMode")
        private String PaymentMode;
        @Expose
        @SerializedName("OrderStatusEn")
        private String OrderStatusEn;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;

        public List<Recipients> getRecipients() {
            return Recipients;
        }

        public void setRecipients(List<Recipients> Recipients) {
            this.Recipients = Recipients;
        }

        public List<ServiceProviders> getServiceProviders() {
            return ServiceProviders;
        }

        public void setServiceProviders(List<ServiceProviders> ServiceProviders) {
            this.ServiceProviders = ServiceProviders;
        }

        public List<String> getOrderPayments() {
            return OrderPayments;
        }

        public void setOrderPayments(List<String> OrderPayments) {
            this.OrderPayments = OrderPayments;
        }

        public List<TrackingDetails> getTrackingDetails() {
            return TrackingDetails;
        }

        public void setTrackingDetails(List<TrackingDetails> TrackingDetails) {
            this.TrackingDetails = TrackingDetails;
        }

        public List<UserDetails> getUserDetails() {
            return UserDetails;
        }

        public void setUserDetails(List<UserDetails> UserDetails) {
            this.UserDetails = UserDetails;
        }

        public List<Items> getItems() {
            return Items;
        }

        public void setItems(List<Items> Items) {
            this.Items = Items;
        }

        public int getItemsCount() {
            return ItemsCount;
        }

        public void setItemsCount(int ItemsCount) {
            this.ItemsCount = ItemsCount;
        }

        public String getStoreType() {
            return StoreType;
        }

        public void setStoreType(String StoreType) {
            this.StoreType = StoreType;
        }

        public String getAwbNo() {
            return awbNo;
        }

        public void setAwbNo(String awbNo) {
            this.awbNo = awbNo;
        }

        public int getDiscount() {
            return Discount;
        }

        public void setDiscount(int Discount) {
            this.Discount = Discount;
        }

        public double getGrandTotal() {
            return GrandTotal;
        }

        public void setGrandTotal(double GrandTotal) {
            this.GrandTotal = GrandTotal;
        }

        public int getDeliveryFee() {
            return DeliveryFee;
        }

        public void setDeliveryFee(int DeliveryFee) {
            this.DeliveryFee = DeliveryFee;
        }

        public double getVAT() {
            return VAT;
        }

        public void setVAT(double VAT) {
            this.VAT = VAT;
        }

        public double getSubTotal() {
            return SubTotal;
        }

        public void setSubTotal(double SubTotal) {
            this.SubTotal = SubTotal;
        }

        public String getExpectingDelivery() {
            return ExpectingDelivery;
        }

        public void setExpectingDelivery(String ExpectingDelivery) {
            this.ExpectingDelivery = ExpectingDelivery;
        }

        public int getTimeSlotId() {
            return TimeSlotId;
        }

        public void setTimeSlotId(int TimeSlotId) {
            this.TimeSlotId = TimeSlotId;
        }

        public int getOrderType() {
            return OrderType;
        }

        public void setOrderType(int OrderType) {
            this.OrderType = OrderType;
        }

        public int getAddressId() {
            return AddressId;
        }

        public void setAddressId(int AddressId) {
            this.AddressId = AddressId;
        }

        public int getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(int OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getOrderDate() {
            return OrderDate;
        }

        public void setOrderDate(String OrderDate) {
            this.OrderDate = OrderDate;
        }

        public String getBranchNameAr() {
            return BranchNameAr;
        }

        public void setBranchNameAr(String BranchNameAr) {
            this.BranchNameAr = BranchNameAr;
        }

        public String getBranchNameEn() {
            return BranchNameEn;
        }

        public void setBranchNameEn(String BranchNameEn) {
            this.BranchNameEn = BranchNameEn;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public String getComments() {
            return Comments;
        }

        public void setComments(String Comments) {
            this.Comments = Comments;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getPaymentRegistrationId() {
            return PaymentRegistrationId;
        }

        public void setPaymentRegistrationId(String PaymentRegistrationId) {
            this.PaymentRegistrationId = PaymentRegistrationId;
        }

        public boolean getPaymentStatus() {
            return PaymentStatus;
        }

        public void setPaymentStatus(boolean PaymentStatus) {
            this.PaymentStatus = PaymentStatus;
        }

        public String getPaymentType() {
            return PaymentType;
        }

        public void setPaymentType(String PaymentType) {
            this.PaymentType = PaymentType;
        }

        public String getPaymentMode() {
            return PaymentMode;
        }

        public void setPaymentMode(String PaymentMode) {
            this.PaymentMode = PaymentMode;
        }

        public String getOrderStatusEn() {
            return OrderStatusEn;
        }

        public void setOrderStatusEn(String OrderStatusEn) {
            this.OrderStatusEn = OrderStatusEn;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }
    }

    public static class Recipients implements Serializable{
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class ServiceProviders implements Serializable{
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class TrackingDetails implements Serializable{
        @Expose
        @SerializedName("OrderStatusId")
        private int OrderStatusId;
        @Expose
        @SerializedName("Message")
        private String Message;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("CreatedOn")
        private String CreatedOn;
        @Expose
        @SerializedName("OrderStatus")
        private String OrderStatus;

        public int getOrderStatusId() {
            return OrderStatusId;
        }

        public void setOrderStatusId(int OrderStatusId) {
            this.OrderStatusId = OrderStatusId;
        }

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String CreatedOn) {
            this.CreatedOn = CreatedOn;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }
    }

    public static class UserDetails implements Serializable{
        @Expose
        @SerializedName("Mobile2")
        private String Mobile2;
        @Expose
        @SerializedName("Address2")
        private String Address2;
        @Expose
        @SerializedName("Address1")
        private String Address1;
        @Expose
        @SerializedName("ZipCode")
        private String ZipCode;
        @Expose
        @SerializedName("CityName")
        private String CityName;
        @Expose
        @SerializedName("CountryName")
        private String CountryName;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("BranchNameAr")
        private String BranchNameAr;
        @Expose
        @SerializedName("BranchNameEn")
        private String BranchNameEn;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("UserId")
        private int UserId;

        public String getMobile2() {
            return Mobile2;
        }

        public void setMobile2(String Mobile2) {
            this.Mobile2 = Mobile2;
        }

        public String getAddress2() {
            return Address2;
        }

        public void setAddress2(String Address2) {
            this.Address2 = Address2;
        }

        public String getAddress1() {
            return Address1;
        }

        public void setAddress1(String Address1) {
            this.Address1 = Address1;
        }

        public String getZipCode() {
            return ZipCode;
        }

        public void setZipCode(String ZipCode) {
            this.ZipCode = ZipCode;
        }

        public String getCityName() {
            return CityName;
        }

        public void setCityName(String CityName) {
            this.CityName = CityName;
        }

        public String getCountryName() {
            return CountryName;
        }

        public void setCountryName(String CountryName) {
            this.CountryName = CountryName;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getBranchNameAr() {
            return BranchNameAr;
        }

        public void setBranchNameAr(String BranchNameAr) {
            this.BranchNameAr = BranchNameAr;
        }

        public String getBranchNameEn() {
            return BranchNameEn;
        }

        public void setBranchNameEn(String BranchNameEn) {
            this.BranchNameEn = BranchNameEn;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }
    }

    public static class Items implements Serializable{
        @Expose
        @SerializedName("Variants")
        private List<Variants> Variants;
        @Expose
        @SerializedName("SubCategoryAr")
        private String SubCategoryAr;
        @Expose
        @SerializedName("SubCategoryEn")
        private String SubCategoryEn;
        @Expose
        @SerializedName("Manufacturer")
        private String Manufacturer;
        @Expose
        @SerializedName("IsAccepted")
        private boolean IsAccepted;
        @Expose
        @SerializedName("GrandTotal")
        private double GrandTotal;
        @Expose
        @SerializedName("VAT")
        private double VAT;
        @Expose
        @SerializedName("Price")
        private double Price;
        @Expose
        @SerializedName("Qty")
        private int Qty;
        @Expose
        @SerializedName("SKUId")
        private String SKUId;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("Weight")
        private float Weight;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("OrderItemId")
        private int OrderItemId;

        public List<Variants> getVariants() {
            return Variants;
        }

        public void setVariants(List<Variants> Variants) {
            this.Variants = Variants;
        }

        public String getSubCategoryAr() {
            return SubCategoryAr;
        }

        public void setSubCategoryAr(String SubCategoryAr) {
            this.SubCategoryAr = SubCategoryAr;
        }

        public String getSubCategoryEn() {
            return SubCategoryEn;
        }

        public void setSubCategoryEn(String SubCategoryEn) {
            this.SubCategoryEn = SubCategoryEn;
        }

        public String getManufacturer() {
            return Manufacturer;
        }

        public void setManufacturer(String Manufacturer) {
            this.Manufacturer = Manufacturer;
        }

        public boolean getIsAccepted() {
            return IsAccepted;
        }

        public void setIsAccepted(boolean IsAccepted) {
            this.IsAccepted = IsAccepted;
        }

        public double getGrandTotal() {
            return GrandTotal;
        }

        public void setGrandTotal(double GrandTotal) {
            this.GrandTotal = GrandTotal;
        }

        public double getVAT() {
            return VAT;
        }

        public void setVAT(double VAT) {
            this.VAT = VAT;
        }

        public double getPrice() {
            return Price;
        }

        public void setPrice(double Price) {
            this.Price = Price;
        }

        public int getQty() {
            return Qty;
        }

        public void setQty(int Qty) {
            this.Qty = Qty;
        }

        public String getSKUId() {
            return SKUId;
        }

        public void setSKUId(String SKUId) {
            this.SKUId = SKUId;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public float getWeight() {
            return Weight;
        }

        public void setWeight(float Weight) {
            this.Weight = Weight;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public int getOrderItemId() {
            return OrderItemId;
        }

        public void setOrderItemId(int OrderItemId) {
            this.OrderItemId = OrderItemId;
        }
    }

    public static class Variants implements Serializable{
        @Expose
        @SerializedName("OptionValueAr")
        private String OptionValueAr;
        @Expose
        @SerializedName("OptionValueEn")
        private String OptionValueEn;
        @Expose
        @SerializedName("OptionAr")
        private String OptionAr;
        @Expose
        @SerializedName("OptionEn")
        private String OptionEn;

        public String getOptionValueAr() {
            return OptionValueAr;
        }

        public void setOptionValueAr(String OptionValueAr) {
            this.OptionValueAr = OptionValueAr;
        }

        public String getOptionValueEn() {
            return OptionValueEn;
        }

        public void setOptionValueEn(String OptionValueEn) {
            this.OptionValueEn = OptionValueEn;
        }

        public String getOptionAr() {
            return OptionAr;
        }

        public void setOptionAr(String OptionAr) {
            this.OptionAr = OptionAr;
        }

        public String getOptionEn() {
            return OptionEn;
        }

        public void setOptionEn(String OptionEn) {
            this.OptionEn = OptionEn;
        }
    }
}
