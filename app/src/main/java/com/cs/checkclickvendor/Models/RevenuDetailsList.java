package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RevenuDetailsList {

    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("EarningSummery")
        private ArrayList<EarningSummery> earningsummery;
        @Expose
        @SerializedName("DetailSummery")
        private ArrayList<DetailSummery> detailsummery;

        public ArrayList<EarningSummery> getEarningsummery() {
            return earningsummery;
        }

        public void setEarningsummery(ArrayList<EarningSummery> earningsummery) {
            this.earningsummery = earningsummery;
        }

        public ArrayList<DetailSummery> getDetailsummery() {
            return detailsummery;
        }

        public void setDetailsummery(ArrayList<DetailSummery> detailsummery) {
            this.detailsummery = detailsummery;
        }
    }

    public static class EarningSummery {
        @Expose
        @SerializedName("NetEarning")
        private double netearning;
        @Expose
        @SerializedName("DeliveryCharges")
        private int deliverycharges;
        @Expose
        @SerializedName("VAT")
        private double vat;
        @Expose
        @SerializedName("OperatingExpense")
        private double operatingexpense;
        @Expose
        @SerializedName("SalesRefund")
        private int salesrefund;
        @Expose
        @SerializedName("TotalEarning")
        private double totalearning;

        public double getNetearning() {
            return netearning;
        }

        public void setNetearning(double netearning) {
            this.netearning = netearning;
        }

        public int getDeliverycharges() {
            return deliverycharges;
        }

        public void setDeliverycharges(int deliverycharges) {
            this.deliverycharges = deliverycharges;
        }

        public double getVat() {
            return vat;
        }

        public void setVat(double vat) {
            this.vat = vat;
        }

        public double getOperatingexpense() {
            return operatingexpense;
        }

        public void setOperatingexpense(double operatingexpense) {
            this.operatingexpense = operatingexpense;
        }

        public int getSalesrefund() {
            return salesrefund;
        }

        public void setSalesrefund(int salesrefund) {
            this.salesrefund = salesrefund;
        }

        public double getTotalearning() {
            return totalearning;
        }

        public void setTotalearning(double totalearning) {
            this.totalearning = totalearning;
        }
    }

    public static class DetailSummery {
        @Expose
        @SerializedName("OrderCount")
        private int ordercount;
        @Expose
        @SerializedName("CurrentAmount")
        private double currentamount;
        @Expose
        @SerializedName("Days")
        private String days;

        public int getOrdercount() {
            return ordercount;
        }

        public void setOrdercount(int ordercount) {
            this.ordercount = ordercount;
        }

        public double getCurrentamount() {
            return currentamount;
        }

        public void setCurrentamount(double currentamount) {
            this.currentamount = currentamount;
        }

        public String getDays() {
            return days;
        }

        public void setDays(String days) {
            this.days = days;
        }
    }
}
