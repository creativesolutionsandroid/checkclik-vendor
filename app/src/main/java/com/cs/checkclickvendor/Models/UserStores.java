package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserStores {

    @Expose
    @SerializedName("MadeChatFriends")
    private boolean MadeChatFriends;
    @Expose
    @SerializedName("CreatedBy")
    private int CreatedBy;
    @Expose
    @SerializedName("JobPositionId")
    private int JobPositionId;
    @Expose
    @SerializedName("FlagId")
    private int FlagId;
    @Expose
    @SerializedName("Data")
    private DataEntity Data;
    @Expose
    @SerializedName("StoreCount")
    private int StoreCount;
    @Expose
    @SerializedName("SelectedStoreId")
    private int SelectedStoreId;
    @Expose
    @SerializedName("AccessType")
    private int AccessType;
    @Expose
    @SerializedName("VendorType")
    private int VendorType;
    @Expose
    @SerializedName("UserType")
    private int UserType;
    @Expose
    @SerializedName("UserId")
    private int UserId;

    public boolean getMadeChatFriends() {
        return MadeChatFriends;
    }

    public void setMadeChatFriends(boolean MadeChatFriends) {
        this.MadeChatFriends = MadeChatFriends;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    public int getJobPositionId() {
        return JobPositionId;
    }

    public void setJobPositionId(int JobPositionId) {
        this.JobPositionId = JobPositionId;
    }

    public int getFlagId() {
        return FlagId;
    }

    public void setFlagId(int FlagId) {
        this.FlagId = FlagId;
    }

    public DataEntity getData() {
        return Data;
    }

    public void setData(DataEntity Data) {
        this.Data = Data;
    }

    public int getStoreCount() {
        return StoreCount;
    }

    public void setStoreCount(int StoreCount) {
        this.StoreCount = StoreCount;
    }

    public int getSelectedStoreId() {
        return SelectedStoreId;
    }

    public void setSelectedStoreId(int SelectedStoreId) {
        this.SelectedStoreId = SelectedStoreId;
    }

    public int getAccessType() {
        return AccessType;
    }

    public void setAccessType(int AccessType) {
        this.AccessType = AccessType;
    }

    public int getVendorType() {
        return VendorType;
    }

    public void setVendorType(int VendorType) {
        this.VendorType = VendorType;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int UserType) {
        this.UserType = UserType;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public static class DataEntity {
        @Expose
        @SerializedName("SubMenuList")
        private ArrayList<SubMenuListEntity> SubMenuList;
        @Expose
        @SerializedName("MenuList")
        private ArrayList<MenuListEntity> MenuList;
        @Expose
        @SerializedName("StoreList")
        private ArrayList<StoreListEntity> StoreList;

        public ArrayList<SubMenuListEntity> getSubMenuList() {
            return SubMenuList;
        }

        public void setSubMenuList(ArrayList<SubMenuListEntity> SubMenuList) {
            this.SubMenuList = SubMenuList;
        }

        public ArrayList<MenuListEntity> getMenuList() {
            return MenuList;
        }

        public void setMenuList(ArrayList<MenuListEntity> MenuList) {
            this.MenuList = MenuList;
        }

        public ArrayList<StoreListEntity> getStoreList() {
            return StoreList;
        }

        public void setStoreList(ArrayList<StoreListEntity> StoreList) {
            this.StoreList = StoreList;
        }
    }

    public static class SubMenuListEntity {
        @Expose
        @SerializedName("AppMenu")
        private boolean AppMenu;
        @Expose
        @SerializedName("SubMenu")
        private boolean SubMenu;
        @Expose
        @SerializedName("ParentId")
        private int ParentId;
        @Expose
        @SerializedName("ActionName")
        private String ActionName;
        @Expose
        @SerializedName("ControllerName")
        private String ControllerName;
        @Expose
        @SerializedName("IconClass")
        private String IconClass;
        @Expose
        @SerializedName("MenuNameAr")
        private String MenuNameAr;
        @Expose
        @SerializedName("MenuNameEn")
        private String MenuNameEn;
        @Expose
        @SerializedName("MenuId")
        private int MenuId;

        public boolean getAppMenu() {
            return AppMenu;
        }

        public void setAppMenu(boolean AppMenu) {
            this.AppMenu = AppMenu;
        }

        public boolean getSubMenu() {
            return SubMenu;
        }

        public void setSubMenu(boolean SubMenu) {
            this.SubMenu = SubMenu;
        }

        public int getParentId() {
            return ParentId;
        }

        public void setParentId(int ParentId) {
            this.ParentId = ParentId;
        }

        public String getActionName() {
            return ActionName;
        }

        public void setActionName(String ActionName) {
            this.ActionName = ActionName;
        }

        public String getControllerName() {
            return ControllerName;
        }

        public void setControllerName(String ControllerName) {
            this.ControllerName = ControllerName;
        }

        public String getIconClass() {
            return IconClass;
        }

        public void setIconClass(String IconClass) {
            this.IconClass = IconClass;
        }

        public String getMenuNameAr() {
            return MenuNameAr;
        }

        public void setMenuNameAr(String MenuNameAr) {
            this.MenuNameAr = MenuNameAr;
        }

        public String getMenuNameEn() {
            return MenuNameEn;
        }

        public void setMenuNameEn(String MenuNameEn) {
            this.MenuNameEn = MenuNameEn;
        }

        public int getMenuId() {
            return MenuId;
        }

        public void setMenuId(int MenuId) {
            this.MenuId = MenuId;
        }
    }

    public static class MenuListEntity {
        @Expose
        @SerializedName("AppMenu")
        private boolean AppMenu;
        @Expose
        @SerializedName("SubMenu")
        private boolean SubMenu;
        @Expose
        @SerializedName("ParentId")
        private int ParentId;
        @Expose
        @SerializedName("ActionName")
        private String ActionName;
        @Expose
        @SerializedName("ControllerName")
        private String ControllerName;
        @Expose
        @SerializedName("IconClass")
        private String IconClass;
        @Expose
        @SerializedName("MenuNameAr")
        private String MenuNameAr;
        @Expose
        @SerializedName("MenuNameEn")
        private String MenuNameEn;
        @Expose
        @SerializedName("MenuId")
        private int MenuId;

        public boolean getAppMenu() {
            return AppMenu;
        }

        public void setAppMenu(boolean AppMenu) {
            this.AppMenu = AppMenu;
        }

        public boolean getSubMenu() {
            return SubMenu;
        }

        public void setSubMenu(boolean SubMenu) {
            this.SubMenu = SubMenu;
        }

        public int getParentId() {
            return ParentId;
        }

        public void setParentId(int ParentId) {
            this.ParentId = ParentId;
        }

        public String getActionName() {
            return ActionName;
        }

        public void setActionName(String ActionName) {
            this.ActionName = ActionName;
        }

        public String getControllerName() {
            return ControllerName;
        }

        public void setControllerName(String ControllerName) {
            this.ControllerName = ControllerName;
        }

        public String getIconClass() {
            return IconClass;
        }

        public void setIconClass(String IconClass) {
            this.IconClass = IconClass;
        }

        public String getMenuNameAr() {
            return MenuNameAr;
        }

        public void setMenuNameAr(String MenuNameAr) {
            this.MenuNameAr = MenuNameAr;
        }

        public String getMenuNameEn() {
            return MenuNameEn;
        }

        public void setMenuNameEn(String MenuNameEn) {
            this.MenuNameEn = MenuNameEn;
        }

        public int getMenuId() {
            return MenuId;
        }

        public void setMenuId(int MenuId) {
            this.MenuId = MenuId;
        }
    }

    public static class StoreListEntity {
        @Expose
        @SerializedName("PanelColor")
        private String PanelColor;
        @Expose
        @SerializedName("ChatIds")
        private String ChatIds;
        @Expose
        @SerializedName("BranchCount")
        private int BranchCount;
        @Expose
        @SerializedName("LogoCopy")
        private String LogoCopy;
        @Expose
        @SerializedName("BackgroundCopy")
        private String BackgroundCopy;
        @Expose
        @SerializedName("CategoriesAr")
        private String CategoriesAr;
        @Expose
        @SerializedName("CategoriesEn")
        private String CategoriesEn;
        @Expose
        @SerializedName("EntityNameAr")
        private String EntityNameAr;
        @Expose
        @SerializedName("EntityNameEn")
        private String EntityNameEn;
        @Expose
        @SerializedName("StoreNameAr")
        private String StoreNameAr;
        @Expose
        @SerializedName("StoreNameEn")
        private String StoreNameEn;
        @Expose
        @SerializedName("StoreType")
        private int StoreType;
        @Expose
        @SerializedName("VendorId")
        private int VendorId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getPanelColor() {
            return PanelColor;
        }

        public void setPanelColor(String panelColor) {
            PanelColor = panelColor;
        }

        public String getChatIds() {
            return ChatIds;
        }

        public void setChatIds(String ChatIds) {
            this.ChatIds = ChatIds;
        }

        public int getBranchCount() {
            return BranchCount;
        }

        public void setBranchCount(int BranchCount) {
            this.BranchCount = BranchCount;
        }

        public String getLogoCopy() {
            return LogoCopy;
        }

        public void setLogoCopy(String LogoCopy) {
            this.LogoCopy = LogoCopy;
        }

        public String getBackgroundCopy() {
            return BackgroundCopy;
        }

        public void setBackgroundCopy(String BackgroundCopy) {
            this.BackgroundCopy = BackgroundCopy;
        }

        public String getCategoriesAr() {
            return CategoriesAr;
        }

        public void setCategoriesAr(String CategoriesAr) {
            this.CategoriesAr = CategoriesAr;
        }

        public String getCategoriesEn() {
            return CategoriesEn;
        }

        public void setCategoriesEn(String CategoriesEn) {
            this.CategoriesEn = CategoriesEn;
        }

        public String getEntityNameAr() {
            return EntityNameAr;
        }

        public void setEntityNameAr(String EntityNameAr) {
            this.EntityNameAr = EntityNameAr;
        }

        public String getEntityNameEn() {
            return EntityNameEn;
        }

        public void setEntityNameEn(String EntityNameEn) {
            this.EntityNameEn = EntityNameEn;
        }

        public String getStoreNameAr() {
            return StoreNameAr;
        }

        public void setStoreNameAr(String StoreNameAr) {
            this.StoreNameAr = StoreNameAr;
        }

        public String getStoreNameEn() {
            return StoreNameEn;
        }

        public void setStoreNameEn(String StoreNameEn) {
            this.StoreNameEn = StoreNameEn;
        }

        public int getStoreType() {
            return StoreType;
        }

        public void setStoreType(int StoreType) {
            this.StoreType = StoreType;
        }

        public int getVendorId() {
            return VendorId;
        }

        public void setVendorId(int VendorId) {
            this.VendorId = VendorId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
