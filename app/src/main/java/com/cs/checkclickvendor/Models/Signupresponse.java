package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Signupresponse {
    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("oginText")
        private String oginText;
        @Expose
        @SerializedName("StatusMessage")
        private String StatusMessage;
        @Expose
        @SerializedName("StatusCode")
        private String StatusCode;
        @Expose
        @SerializedName("FlagId")
        private int FlagId;
        @Expose
        @SerializedName("IsDeleted")
        private boolean IsDeleted;
        @Expose
        @SerializedName("DeletedBy")
        private int DeletedBy;
        @Expose
        @SerializedName("ModifiedBy")
        private int ModifiedBy;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("Status")
        private boolean Status;
        @Expose
        @SerializedName("VerifyOTP")
        private String VerifyOTP;
        @Expose
        @SerializedName("IsEmailVerified")
        private boolean IsEmailVerified;
        @Expose
        @SerializedName("IsMobileNoVerified")
        private boolean IsMobileNoVerified;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("CofirmPassword")
        private String CofirmPassword;
        @Expose
        @SerializedName("Password")
        private String Password;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("DOB")
        private String DOB;
        @Expose
        @SerializedName("LastName")
        private String LastName;
        @Expose
        @SerializedName("MiddleName")
        private String MiddleName;
        @Expose
        @SerializedName("FirstName")
        private String FirstName;
        @Expose
        @SerializedName("UserType")
        private int UserType;
        @Expose
        @SerializedName("VendorType")
        private int VendorType;
        @Expose
        @SerializedName("Id")
        private String Id;

        public String getOginText() {
            return oginText;
        }

        public void setOginText(String oginText) {
            this.oginText = oginText;
        }

        public String getStatusMessage() {
            return StatusMessage;
        }

        public void setStatusMessage(String StatusMessage) {
            this.StatusMessage = StatusMessage;
        }

        public String getStatusCode() {
            return StatusCode;
        }

        public void setStatusCode(String StatusCode) {
            this.StatusCode = StatusCode;
        }

        public int getFlagId() {
            return FlagId;
        }

        public void setFlagId(int FlagId) {
            this.FlagId = FlagId;
        }

        public boolean getIsDeleted() {
            return IsDeleted;
        }

        public void setIsDeleted(boolean IsDeleted) {
            this.IsDeleted = IsDeleted;
        }

        public int getDeletedBy() {
            return DeletedBy;
        }

        public void setDeletedBy(int DeletedBy) {
            this.DeletedBy = DeletedBy;
        }

        public int getModifiedBy() {
            return ModifiedBy;
        }

        public void setModifiedBy(int ModifiedBy) {
            this.ModifiedBy = ModifiedBy;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public boolean getStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public String getVerifyOTP() {
            return VerifyOTP;
        }

        public void setVerifyOTP(String VerifyOTP) {
            this.VerifyOTP = VerifyOTP;
        }

        public boolean getIsEmailVerified() {
            return IsEmailVerified;
        }

        public void setIsEmailVerified(boolean IsEmailVerified) {
            this.IsEmailVerified = IsEmailVerified;
        }

        public boolean getIsMobileNoVerified() {
            return IsMobileNoVerified;
        }

        public void setIsMobileNoVerified(boolean IsMobileNoVerified) {
            this.IsMobileNoVerified = IsMobileNoVerified;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getCofirmPassword() {
            return CofirmPassword;
        }

        public void setCofirmPassword(String CofirmPassword) {
            this.CofirmPassword = CofirmPassword;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String Password) {
            this.Password = Password;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getDOB() {
            return DOB;
        }

        public void setDOB(String DOB) {
            this.DOB = DOB;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getMiddleName() {
            return MiddleName;
        }

        public void setMiddleName(String MiddleName) {
            this.MiddleName = MiddleName;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public int getUserType() {
            return UserType;
        }

        public void setUserType(int UserType) {
            this.UserType = UserType;
        }

        public int getVendorType() {
            return VendorType;
        }

        public void setVendorType(int VendorType) {
            this.VendorType = VendorType;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }
    }
}
