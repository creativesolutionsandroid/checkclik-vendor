package com.cs.checkclickvendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RescheduleDateslist {


    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("valtwo")
    private int valtwo;
    @Expose
    @SerializedName("DeliveryHomeService")
    private String deliveryhomeservice;
    @Expose
    @SerializedName("PreBookingDeliveryTimeUnit")
    private int prebookingdeliverytimeunit;
    @Expose
    @SerializedName("valfirst")
    private int valfirst;
    @Expose
    @SerializedName("PickUpInStoreType")
    private String pickupinstoretype;
    @Expose
    @SerializedName("PreBookingInStoreTimeUnit")
    private int prebookinginstoretimeunit;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getValtwo() {
        return valtwo;
    }

    public void setValtwo(int valtwo) {
        this.valtwo = valtwo;
    }

    public String getDeliveryhomeservice() {
        return deliveryhomeservice;
    }

    public void setDeliveryhomeservice(String deliveryhomeservice) {
        this.deliveryhomeservice = deliveryhomeservice;
    }

    public int getPrebookingdeliverytimeunit() {
        return prebookingdeliverytimeunit;
    }

    public void setPrebookingdeliverytimeunit(int prebookingdeliverytimeunit) {
        this.prebookingdeliverytimeunit = prebookingdeliverytimeunit;
    }

    public int getValfirst() {
        return valfirst;
    }

    public void setValfirst(int valfirst) {
        this.valfirst = valfirst;
    }

    public String getPickupinstoretype() {
        return pickupinstoretype;
    }

    public void setPickupinstoretype(String pickupinstoretype) {
        this.pickupinstoretype = pickupinstoretype;
    }

    public int getPrebookinginstoretimeunit() {
        return prebookinginstoretimeunit;
    }

    public void setPrebookinginstoretimeunit(int prebookinginstoretimeunit) {
        this.prebookinginstoretimeunit = prebookinginstoretimeunit;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("DeliveryHomeWeekdays")
        private ArrayList<DeliveryHomeWeekdays> deliveryhomeweekdays;
        @Expose
        @SerializedName("PickupInServiceWeekdays")
        private ArrayList<PickupInServiceWeekdays> pickupinserviceweekdays;

        public ArrayList<DeliveryHomeWeekdays> getDeliveryhomeweekdays() {
            return deliveryhomeweekdays;
        }

        public void setDeliveryhomeweekdays(ArrayList<DeliveryHomeWeekdays> deliveryhomeweekdays) {
            this.deliveryhomeweekdays = deliveryhomeweekdays;
        }

        public ArrayList<PickupInServiceWeekdays> getPickupinserviceweekdays() {
            return pickupinserviceweekdays;
        }

        public void setPickupinserviceweekdays(ArrayList<PickupInServiceWeekdays> pickupinserviceweekdays) {
            this.pickupinserviceweekdays = pickupinserviceweekdays;
        }
    }

    public static class DeliveryHomeWeekdays {
        @Expose
        @SerializedName("Weekday")
        private String weekday;
        @Expose
        @SerializedName("WeekdayId")
        private int weekdayid;

        public String getWeekday() {
            return weekday;
        }

        public void setWeekday(String weekday) {
            this.weekday = weekday;
        }

        public int getWeekdayid() {
            return weekdayid;
        }

        public void setWeekdayid(int weekdayid) {
            this.weekdayid = weekdayid;
        }
    }

    public static class PickupInServiceWeekdays {
        @Expose
        @SerializedName("Weekday")
        private String weekday;
        @Expose
        @SerializedName("WeekdayId")
        private int weekdayid;

        public String getWeekday() {
            return weekday;
        }

        public void setWeekday(String weekday) {
            this.weekday = weekday;
        }

        public int getWeekdayid() {
            return weekdayid;
        }

        public void setWeekdayid(int weekdayid) {
            this.weekdayid = weekdayid;
        }
    }
}
