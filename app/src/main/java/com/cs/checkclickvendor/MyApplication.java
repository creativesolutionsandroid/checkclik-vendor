package com.cs.checkclickvendor;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cs.checkclickvendor.Contracts.StringContract;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static com.cs.checkclickvendor.Adapters.AccessAdapter.TAG;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/GothamBook.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        CometChat.init(this, StringContract.AppDetails.APP_ID,new CometChat.CallbackListener<String>() {

            @Override
            public void onSuccess(String s) {
//                Toast.makeText(MyApplication.this, "SetUp Complete", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(CometChatException e) {
                Log.d(TAG, "onError: "+e.getMessage());
            }

        });
    }
}
