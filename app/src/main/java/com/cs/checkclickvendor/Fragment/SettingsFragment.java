package com.cs.checkclickvendor.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cs.checkclickvendor.Activity.ChangePasswordActivity;
import com.cs.checkclickvendor.Activity.EditProfileActivity;
import com.cs.checkclickvendor.Activity.MainActivity;
import com.cs.checkclickvendor.Activity.PushNotificationActivity;
import com.cs.checkclickvendor.Activity.StartScreenActivity;
import com.cs.checkclickvendor.R;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;
import static com.cs.checkclickvendor.Utils.Utils.Constants.customDialog;
import static com.facebook.react.common.ReactConstants.TAG;

public class SettingsFragment extends Fragment {
    View rootView;
    TextView changepassword, changephonenumber, changeemail, changelanguage, pushnotification, terms, logout;
    ImageView chamgepasswordicon, changephoneicon, changeemailcon, changelanguagecon, pushnotifactionicon, termsicon, logouticon;
    ImageView menubtn;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.settingsfragment, container, false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        changepassword = (TextView) rootView.findViewById(R.id.changepassword);
        changephonenumber = (TextView) rootView.findViewById(R.id.changephone);
        changeemail = (TextView) rootView.findViewById(R.id.chngeemail);
        changelanguage = (TextView) rootView.findViewById(R.id.changelanguage);
        pushnotification = (TextView) rootView.findViewById(R.id.pushnotifaction);
        terms = (TextView) rootView.findViewById(R.id.terms);
        logout = (TextView) rootView.findViewById(R.id.logout);

        chamgepasswordicon = (ImageView) rootView.findViewById(R.id.chamgepasswordicon);
        changephoneicon = (ImageView) rootView.findViewById(R.id.changephoneicon);
        changeemailcon = (ImageView) rootView.findViewById(R.id.changeemailcon);
        changelanguagecon = (ImageView) rootView.findViewById(R.id.changelanguagecon);
        pushnotifactionicon = (ImageView) rootView.findViewById(R.id.pushnotifactionicon);
        termsicon = (ImageView) rootView.findViewById(R.id.termsicon);
        logouticon = (ImageView) rootView.findViewById(R.id.logouticon);

        menubtn = (ImageView) rootView.findViewById(R.id.back_btn);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_changepass = getResources().getIdentifier("changepassword2x_" + appColor, "drawable", getContext().getPackageName());
                int ic_changephone = getResources().getIdentifier("changephone2x_" + appColor, "drawable", getContext().getPackageName());
                int ic_changeemail = getResources().getIdentifier("contact2x_" + appColor, "drawable", getContext().getPackageName());
                int ic_changelang = getResources().getIdentifier("changelanguage2x_" + appColor, "drawable", getContext().getPackageName());
                int ic_notification = getResources().getIdentifier("notification_icon2x_" + appColor, "drawable", getContext().getPackageName());
                int ic_term_condition = getResources().getIdentifier("termsandconditions2x_" + appColor, "drawable", getContext().getPackageName());
                int ic_logout = getResources().getIdentifier("logout2x_" + appColor, "drawable", getContext().getPackageName());
                int ic_sidemenu = getResources().getIdentifier("sidemenu2x_" + appColor, "drawable", getContext().getPackageName());

                chamgepasswordicon.setImageDrawable(getResources().getDrawable(ic_changepass));
                changephoneicon.setImageDrawable(getResources().getDrawable(ic_changephone));
                changeemailcon.setImageDrawable(getResources().getDrawable(ic_changeemail));
                changelanguagecon.setImageDrawable(getResources().getDrawable(ic_changelang));
                pushnotifactionicon.setImageDrawable(getResources().getDrawable(ic_notification));
                termsicon.setImageDrawable(getResources().getDrawable(ic_term_condition));
                logouticon.setImageDrawable(getResources().getDrawable(ic_logout));
                menubtn.setImageDrawable(getResources().getDrawable(ic_sidemenu));

            }
        }
        changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity().getApplication(), ChangePasswordActivity.class);
                startActivity(intent);
            }
        });

        menubtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showtwoButtonsAlertDialog();
            }
        });

        changeemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                intent.putExtra("changename", "Email");
                startActivity(intent);
            }
        });

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                intent.putExtra("changename", "Terms");
                startActivity(intent);
            }
        });

        changephonenumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                intent.putExtra("changename", "Mobile");
                startActivity(intent);
            }
        });

        changelanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                intent.putExtra("changename", "lang");
                startActivity(intent);
            }
        });

        pushnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity().getApplication(), PushNotificationActivity.class);
                startActivity(intent);
            }
        });

        return rootView;
    }

    public void showtwoButtonsAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final TextView title = (TextView) dialogView.findViewById(R.id.title);
        final TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        final TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        final TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


//            title.setText("CkeckClik");
//            yes.setText(getResources().getString(R.string.yes));
//            no.setText(getResources().getString(R.string.no));
//            desc.setText("Do you want to logout?");

        desc.setText("  Do you want to  LOG OUT");


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPrefsEditor.clear();
                userPrefsEditor.commit();
                customDialog.dismiss();

                CometChat.logout(new CometChat.CallbackListener<String>() {
                    @Override
                    public void onSuccess(String successMessage) {
                        Intent i = new Intent(getActivity(), StartScreenActivity.class);
                        i.putExtra("type", "1");
                        startActivity(i);
                        getActivity().finish();
                    }

                    @Override
                    public void onError(CometChatException e) {
                        Intent i = new Intent(getActivity(), StartScreenActivity.class);
                        i.putExtra("type", "1");
                        startActivity(i);
                        getActivity().finish();
                    }
                });
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();

            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}
