package com.cs.checkclickvendor.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.cs.checkclickvendor.Activity.MainActivity;
import com.cs.checkclickvendor.Adapters.BranchListAdapter;
import com.cs.checkclickvendor.Models.BranchList;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class OrderFragment extends Fragment {

    RecyclerView branch_list;

    BranchListAdapter mAdapter;

    ArrayList<BranchList.Branches> orderLists = new ArrayList<>();
    SharedPreferences userPrefs;

    View rootView;

    private TextView select_branch;
    private View noDataFoundLayout;
    private ImageView nothingFoundImage;
    private Button tryAgainButton;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.orders_fragment, container, false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        branch_list = rootView.findViewById(R.id.branch_list);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        branch_list.setLayoutManager(mLayoutManager);

        ImageView menu_btn = (ImageView) rootView.findViewById(R.id.menu_btn);

        select_branch = (TextView) rootView.findViewById(R.id.select_branch);
        tryAgainButton = (Button) rootView.findViewById(R.id.try_again);
        nothingFoundImage = (ImageView) rootView.findViewById(R.id.nothing_found);
        noDataFoundLayout = (View) rootView.findViewById(R.id.no_data_found_layout);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_sidemenu = getResources().getIdentifier("sidemenu3x_" + appColor, "drawable", getContext().getPackageName());
                menu_btn.setImageDrawable(getResources().getDrawable(ic_sidemenu));
            }
        }

        tryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new BranchListapi().execute();
            }
        });

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        new BranchListapi().execute();

        return rootView;
    }

    private String prepareBranchListJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Id", 0);
            parentObj.put("StoreId", userPrefs.getInt("storeId", 0));
            parentObj.put("FlagId", 1);
            parentObj.put("CreatedBy", userPrefs.getString("userId", null));
            parentObj.put("ControllerName", "Branches");
            parentObj.put("ActionName", "Index");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    private class BranchListapi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareBranchListJson();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<BranchList> call = apiService.getbranchlist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BranchList>() {
                @Override
                public void onResponse(Call<BranchList> call, Response<BranchList> response) {
                    if (response.isSuccessful()) {
                        BranchList Response = response.body();

                        orderLists = Response.getData().getBranches();

                        if (orderLists != null && orderLists.size() > 0) {

                            mAdapter = new BranchListAdapter(getActivity(), orderLists);
                            branch_list.setAdapter(mAdapter);

                            noDataFoundLayout.setVisibility(View.GONE);
                            branch_list.setVisibility(View.VISIBLE);
                            nothingFoundImage.setVisibility(View.GONE);
                        } else {
                            noDataFoundLayout.setVisibility(View.VISIBLE);
                            branch_list.setVisibility(View.GONE);
                            nothingFoundImage.setVisibility(View.GONE);
                        }
                        Log.i("TAG", "onResponse: " + Response.getMessage());

                    } else {
                        noDataFoundLayout.setVisibility(View.VISIBLE);
                        select_branch.setVisibility(View.GONE);
                        nothingFoundImage.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: ");
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BranchList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onFailure: " + t);
                    }
                    Constants.closeLoadingDialog();
                    noDataFoundLayout.setVisibility(View.GONE);
                    select_branch.setVisibility(View.GONE);
                    nothingFoundImage.setVisibility(View.VISIBLE);
                }
            });
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Constants.closeLoadingDialog();
    }
}
