package com.cs.checkclickvendor.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.cs.checkclickvendor.Activity.MainActivity;
import com.cs.checkclickvendor.Models.DashBoardList;
import com.cs.checkclickvendor.Models.UserStores;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.BranchColor;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;


public class DashBoardFragment extends Fragment {

    ArrayList<DashBoardList.JPendingBranches> dashboards = new ArrayList<>();
//    ACProgressFlower dialog;

    ImageView up_btn, down_btn;
    TextView branch_name, dash_board_count, address, total_orders, total_revenue, sub_status;
    View rootView;
    int dash_pos = 0;
    SharedPreferences userPrefs;
    ArrayList<UserStores.StoreListEntity> allStoresList = new ArrayList<>();
    SharedPreferences.Editor userPrefsEditor;

    private RelativeLayout dashBoardLayout;
    private View noDataFoundLayout;
    private ImageView nothingFoundImage;
    private Button tryAgainButton;

    UserStores.StoreListEntity storeListEntities;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.dashboard_activity, container, false);
        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        up_btn = rootView.findViewById(R.id.up_btn);
        down_btn = rootView.findViewById(R.id.down_btn);

        branch_name = rootView.findViewById(R.id.branch_name);
        dash_board_count = rootView.findViewById(R.id.dashboard_count);
        address = rootView.findViewById(R.id.address);
        total_orders = rootView.findViewById(R.id.total_orders);
        total_revenue = rootView.findViewById(R.id.total_revenue);
        sub_status = rootView.findViewById(R.id.sub_status);
        ImageView menu_btn = (ImageView) rootView.findViewById(R.id.menu_btn);

        dashBoardLayout = (RelativeLayout) rootView.findViewById(R.id.dashboard_layout);
        tryAgainButton = (Button) rootView.findViewById(R.id.try_again);
        nothingFoundImage = (ImageView) rootView.findViewById(R.id.nothing_found);
        noDataFoundLayout = (View) rootView.findViewById(R.id.no_data_found_layout);

//        navigation.getMenu().findItem(R.id.navigation_dashbord).setChecked(true);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_sidemenu = getResources().getIdentifier("sidemenu3x_" + appColor, "drawable", getContext().getPackageName());
                menu_btn.setImageDrawable(getResources().getDrawable(ic_sidemenu));
            }
        }

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        if (userPrefs.getInt("storeId", 0) == 0) {
            new getStoresApi().execute();
        } else {
            new DashboardListapi().execute();
        }

        tryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DashboardListapi().execute();
            }
        });

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_dashbroad_downarrow = getResources().getIdentifier("dashbroad_downarrow_" + appColor, "drawable", getContext().getPackageName());
                down_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_downarrow));
            }
        }

        up_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (dash_pos > 0) {
                    dash_pos = dash_pos - 1;
                    branch_name.setText("" + dashboards.get(dash_pos).getBranchnameen());
                    address.setText("" + dashboards.get(dash_pos).getAddress());
                    dash_board_count.setText("B" + (dash_pos + 1));
                    total_orders.setText("" + dashboards.get(dash_pos).getTotalpending() + " out of " + dashboards.get(dash_pos).getTotal());
                    total_revenue.setText("" + Constants.priceFormat1.format(dashboards.get(dash_pos).getTotalrevenue()) + " SAR");
                    sub_status.setText("" + dashboards.get(dash_pos).getPlanexpiry());

                    if (dash_pos == 0) {
                        if (appColor != null) {
                            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                                int ic_dashbroad_downarrow = getResources().getIdentifier("dashbroad_downarrow_" + appColor, "drawable", getContext().getPackageName());
                                down_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_downarrow));
                                up_btn.setImageDrawable(getResources().getDrawable(R.drawable.dashboard_mins));
                            }
                        } else {
                            int ic_dashbroad_downarrow = getResources().getIdentifier("dashbroad_downarrow", "drawable", getContext().getPackageName());
                            down_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_downarrow));
                            up_btn.setImageDrawable(getResources().getDrawable(R.drawable.dashboard_mins));
                        }
                    } else {
                        if (appColor != null) {
                            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                                int ic_dashbroad_downarrow = getResources().getIdentifier("dashbroad_downarrow_" + appColor, "drawable", getContext().getPackageName());
                                up_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_downarrow));
                                int ic_dashbroad_uparrow = getResources().getIdentifier("dashbroad_uparrow_" + appColor, "drawable", getContext().getPackageName());
                                down_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_uparrow));
                            }
                        } else {
                            int ic_dashbroad_downarrow = getResources().getIdentifier("dashbroad_downarrow", "drawable", getContext().getPackageName());
                            up_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_downarrow));
                            int ic_dashbroad_uparrow = getResources().getIdentifier("dashbroad_uparrow", "drawable", getContext().getPackageName());
                            down_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_uparrow));
                        }

                    }

                }
            }
        });

        down_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ((dash_pos + 1) < dashboards.size()) {
                    dash_pos = dash_pos + 1;
                    branch_name.setText("" + dashboards.get(dash_pos).getBranchnameen());
                    address.setText("" + dashboards.get(dash_pos).getAddress());
                    dash_board_count.setText("B" + (dash_pos + 1));
                    total_orders.setText("" + dashboards.get(dash_pos).getTotalpending() + " out of " + dashboards.get(dash_pos).getTotal());
                    total_revenue.setText("" + Constants.priceFormat1.format(dashboards.get(dash_pos).getTotalrevenue()) + " SAR");
                    sub_status.setText("" + dashboards.get(dash_pos).getPlanexpiry());

                    if (dash_pos == dashboards.size() - 1) {
                        if (appColor != null) {
                            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                                int ic_dashbroad_uparrow = getResources().getIdentifier("dashbroad_uparrow_" + appColor, "drawable", getContext().getPackageName());
                                up_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_uparrow));
                                down_btn.setImageDrawable(getResources().getDrawable(R.drawable.dashboard_mins));
                            }
                        } else {
                            int ic_dashbroad_uparrow = getResources().getIdentifier("dashbroad_uparrow", "drawable", getContext().getPackageName());
                            up_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_uparrow));
                            down_btn.setImageDrawable(getResources().getDrawable(R.drawable.dashboard_mins));
                        }

                    } else {
                        if (appColor != null) {
                            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                                int ic_dashbroad_uparrow = getResources().getIdentifier("dashbroad_uparrow_" + appColor, "drawable", getContext().getPackageName());
                                up_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_uparrow));
                                int ic_dashbroad_downarrow = getResources().getIdentifier("dashbroad_downarrow_" + appColor, "drawable", getContext().getPackageName());
                                down_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_downarrow));
                            }
                        } else {
                            int ic_dashbroad_uparrow = getResources().getIdentifier("dashbroad_uparrow", "drawable", getContext().getPackageName());
                            up_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_uparrow));
                            int ic_dashbroad_downarrow = getResources().getIdentifier("dashbroad_downarrow", "drawable", getContext().getPackageName());
                            down_btn.setImageDrawable(getResources().getDrawable(ic_dashbroad_downarrow));
                        }
                    }
                }
            }
        });

        return rootView;
    }

    private String prepareDashboardJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("StoreId", userPrefs.getInt("storeId", 0));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("TAG", "prepareDashboardJson: " + parentObj.toString());

        return parentObj.toString();
    }

    private class DashboardListapi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareDashboardJson();

            Constants.showLoadingDialog(getActivity());
//            showloaderAlertDialog();

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<DashBoardList> call = apiService.getdashboard(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<DashBoardList>() {
                @Override
                public void onResponse(Call<DashBoardList> call, Response<DashBoardList> response) {
                    if (response.isSuccessful()) {
                        DashBoardList Response = response.body();

                        dashboards = Response.getData().getDashboard().get(0).getJpendingbranches();

                        if (dashboards != null && dashboards.size() > 0) {
                            branch_name.setText("" + dashboards.get(0).getBranchnameen());
                            address.setText("" + dashboards.get(0).getAddress());
                            dash_board_count.setText("B" + (dash_pos + 1));
                            total_orders.setText("" + dashboards.get(0).getTotalpending() + " out of " + dashboards.get(0).getTotal());
                            total_revenue.setText("" + Constants.priceFormat1.format(dashboards.get(0).getTotalrevenue()) + " SAR");
                            sub_status.setText("" + dashboards.get(0).getPlanexpiry());

                            noDataFoundLayout.setVisibility(View.GONE);
                            dashBoardLayout.setVisibility(View.VISIBLE);
                            nothingFoundImage.setVisibility(View.GONE);
                        } else {
                            noDataFoundLayout.setVisibility(View.VISIBLE);
                            dashBoardLayout.setVisibility(View.GONE);
                            nothingFoundImage.setVisibility(View.GONE);
                        }

                        Log.i("TAG", "onResponse: " + Response.getMessage());

                    } else {
                        noDataFoundLayout.setVisibility(View.VISIBLE);
                        dashBoardLayout.setVisibility(View.GONE);
                        nothingFoundImage.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: ");
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<DashBoardList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onFailure: " + t);
                    }
                    Constants.closeLoadingDialog();
                    noDataFoundLayout.setVisibility(View.GONE);
                    dashBoardLayout.setVisibility(View.GONE);
                    nothingFoundImage.setVisibility(View.VISIBLE);
                }
            });
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Constants.closeLoadingDialog();
    }

    private class getStoresApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UserStores> call = apiService.getUserStores(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UserStores>() {
                @Override
                public void onResponse(Call<UserStores> call, Response<UserStores> response) {
                    if (response.isSuccessful()) {
                        Constants.closeLoadingDialog();
                        UserStores VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getData().getStoreList().size() > 0) {
                                allStoresList = VerifyMobileResponse.getData().getStoreList();
                                userPrefsEditor.putInt("storeId", allStoresList.get(0).getId());

                                Gson gson = new Gson();
                                String json = gson.toJson(allStoresList.get(0));

                                userPrefsEditor.putString("store", json);
                                userPrefsEditor.commit();

                                Gson gson1 = new Gson();
                                String json1 = userPrefs.getString("store", null);
                                Type type = new TypeToken<UserStores.StoreListEntity>() {
                                }.getType();
                                storeListEntities = gson1.fromJson(json1, type);

                                BranchColor branchColor = new BranchColor();
                                appColor = branchColor.BranchColor(storeListEntities.getPanelColor());
                                Log.i("TAG", "onResponse: " + appColor);

                                new DashboardListapi().execute();
                            } else {
                                noDataFoundLayout.setVisibility(View.VISIBLE);
                                dashBoardLayout.setVisibility(View.GONE);
                                nothingFoundImage.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            noDataFoundLayout.setVisibility(View.VISIBLE);
                            dashBoardLayout.setVisibility(View.GONE);
                            nothingFoundImage.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserStores> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                    noDataFoundLayout.setVisibility(View.GONE);
                    dashBoardLayout.setVisibility(View.GONE);
                    nothingFoundImage.setVisibility(View.VISIBLE);
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {

        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userPrefs.getString("userId", ""));
            parentObj.put("UserType", userPrefs.getInt("userType", 1));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareVerifyMobileJson: " + parentObj.toString());

        return parentObj.toString();
    }
}