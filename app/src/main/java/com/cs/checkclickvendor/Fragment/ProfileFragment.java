package com.cs.checkclickvendor.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickvendor.Activity.MainActivity;
import com.cs.checkclickvendor.Activity.NotificationsActivity;
import com.cs.checkclickvendor.Adapters.AccessAdapter;
import com.cs.checkclickvendor.Adapters.ProfileBranchesAdapter;
import com.cs.checkclickvendor.Adapters.StoresSideMenuAdapter;
import com.cs.checkclickvendor.Models.BranchList;
import com.cs.checkclickvendor.Models.UserStores;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.BranchColor;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class ProfileFragment extends Fragment {

    ImageView branchimage, menu_btn, store_menu_btn, notification, store_icon;
    TextView number_of_branches, storeName, cat;
    RecyclerView branchlist;
    View rootView;
    ArrayList<String> images = new ArrayList<>();
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;
    private LinearLayout branchDetailLayout;
    public DrawerLayout drawer;
    private ListView sideMenuListView;
    private StoresSideMenuAdapter mAdapter;
    private ArrayList<String> storesList = new ArrayList<>();
    private ArrayList<UserStores> userStores = new ArrayList<>();
    public static int itemSelectedPostion;
    ArrayList<UserStores.StoreListEntity> allStoresList = new ArrayList<>();
    ArrayList<BranchList.Branches> branchLists = new ArrayList<>();
    private ProfileBranchesAdapter madapter;

    UserStores.StoreListEntity storeListEntities;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        menu_btn = (ImageView) rootView.findViewById(R.id.menu_btn);
        store_menu_btn = (ImageView) rootView.findViewById(R.id.stores_menu_icon);
        branchlist = (RecyclerView) rootView.findViewById(R.id.branchlist);
        branchimage = (ImageView) rootView.findViewById(R.id.branchimage);
        storeName = (TextView) rootView.findViewById(R.id.branchname);
        number_of_branches = (TextView) rootView.findViewById(R.id.numberofbranches);
        cat = (TextView) rootView.findViewById(R.id.cat);
        drawer = (DrawerLayout) rootView.findViewById(R.id.drawer_layout);
        branchDetailLayout = (LinearLayout) rootView.findViewById(R.id.layout);
        notification = (ImageView) rootView.findViewById(R.id.notification);
        store_icon = (ImageView) rootView.findViewById(R.id.store_icon);

        sideMenuListView = (ListView) rootView.findViewById(R.id.side_menu_list_view);

        sideMenuListView.setOnItemClickListener(new DrawerItemClickListener());

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {

                int ic_pickup = getResources().getIdentifier("dots_menu_" + appColor, "drawable", getContext().getPackageName());
                store_menu_btn.setImageDrawable(getResources().getDrawable(ic_pickup));

                int ic_notify = getResources().getIdentifier("notification_icon2x_" + appColor, "drawable", getContext().getPackageName());
                notification.setImageDrawable(getResources().getDrawable(ic_notify));

                int ic_sidemenu = getResources().getIdentifier("sidemenu3x_" + appColor, "drawable", getContext().getPackageName());
                menu_btn.setImageDrawable(getResources().getDrawable(ic_sidemenu));

                int ic_store_icon = getResources().getIdentifier("store_icon_" + appColor, "drawable", getContext().getPackageName());
                menu_btn.setImageDrawable(getResources().getDrawable(ic_store_icon));

            }
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), NotificationsActivity.class);
                startActivity(intent);

            }
        });

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        store_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuClick();
            }
        });

        String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new getStoresApi().execute();
        } else {
            Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
        }

        return rootView;
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            itemSelectedPostion = position;
//            mAdapter = new StoresSideMenuAdapter(getActivity(), storesList,itemSelectedPostion );
//            sideMenuListView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            sideMenuListView.smoothScrollToPosition(position);

            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            }

            Gson gson = new Gson();
            String json = gson.toJson(allStoresList.get(position));

            userPrefsEditor.putInt("storeId", allStoresList.get(position).getId());
            userPrefsEditor.putString("store", json);
            userPrefsEditor.putInt("pos", position);
            userPrefsEditor.commit();

            Gson gson1 = new Gson();
            String json1 = userPrefs.getString("store", null);
            Type type = new TypeToken<UserStores.StoreListEntity>() {
            }.getType();
            storeListEntities = gson1.fromJson(json1, type);

            BranchColor branchColor = new BranchColor();
            appColor = branchColor.BranchColor(storeListEntities.getPanelColor());
            Log.i("TAG", "onResponse: " + appColor);

            getBranchData();

            Intent a = new Intent(getActivity(), MainActivity.class);
            startActivity(a);

        }
    }

    @SuppressLint("RestrictedApi")
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setPadding(0, 15, 0, 0);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    public void menuClick() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);

        } else {
            drawer.openDrawer(GravityCompat.END);
        }
    }

    private class getStoresApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UserStores> call = apiService.getUserStores(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UserStores>() {
                @Override
                public void onResponse(Call<UserStores> call, Response<UserStores> response) {
                    if (response.isSuccessful()) {
                        Constants.closeLoadingDialog();
                        UserStores VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getData().getStoreList().size() > 0) {
                                allStoresList = VerifyMobileResponse.getData().getStoreList();
                                filterStores(allStoresList);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserStores> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userPrefs.getString("userId", "0"));
            parentObj.put("UserType", userPrefs.getInt("userType", 1));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    private class getBranchesApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareBranchesJson();
            Constants.showLoadingDialog(getActivity());
            branchLists.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<BranchList> call = apiService.getbranchs(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BranchList>() {
                @Override
                public void onResponse(Call<BranchList> call, Response<BranchList> response) {
                    Log.d("TAG", "onResponse: " + response.toString());
                    if (response.isSuccessful()) {
                        BranchList VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getData().getBranches().size() > 0) {
                                branchLists = VerifyMobileResponse.getData().getBranches();

                                if (branchLists.size() > 0) {
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                    branchlist.setLayoutManager(mLayoutManager);
                                    madapter = new ProfileBranchesAdapter(getContext(), branchLists);
                                    branchlist.setAdapter(madapter);
                                } else {
                                    Constants.showOneButtonAlertDialog("No Branches found", getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BranchList> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareBranchesJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id", "0");
            parentObj.put("StoreId", allStoresList.get(itemSelectedPostion).getId());
            parentObj.put("FlagId", 1);
            parentObj.put("CreatedBy", userPrefs.getString("userId", "14"));
            parentObj.put("ControllerName", "Branches");
            parentObj.put("ActionName", "Index");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    private void filterStores(ArrayList<UserStores.StoreListEntity> stores) {
        mAdapter = new StoresSideMenuAdapter(getActivity(), stores);
        sideMenuListView.setAdapter(mAdapter);
        getBranchData();
    }

    private void getBranchData() {
        branchDetailLayout.setVisibility(View.VISIBLE);
        storeName.setText(allStoresList.get(itemSelectedPostion).getStoreNameEn());
        cat.setText(allStoresList.get(itemSelectedPostion).getCategoriesEn());
        Glide.with(getContext()).load(Constants.STORE_IMAGE_URL + allStoresList.get(itemSelectedPostion).getLogoCopy())
                .into(branchimage);
        number_of_branches.setText("Number of branches(" + allStoresList.get(itemSelectedPostion).getBranchCount() + ")");
        new getBranchesApi().execute();
    }
}
