package com.cs.checkclickvendor.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cs.checkclickvendor.Activity.MainActivity;
import com.cs.checkclickvendor.Adapters.VoucherAdapter;
import com.cs.checkclickvendor.Models.VoucherResponce;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class VoucherFragment extends Fragment {

    View rootView;
    ImageView menu_btn;
    RecyclerView couponlist;
    VoucherAdapter mCouponAdapter;
    public static final String TAG = "TAG";
    ArrayList<VoucherResponce.DataEntity> couponArrayList = new ArrayList<>();
    SharedPreferences userPrefs;

    private View noDataFoundLayout;
    private ImageView nothingFoundImage;
    private Button tryAgainButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.voucher_fragment, container, false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        menu_btn = (ImageView) rootView.findViewById(R.id.menu_btn);
        couponlist = (RecyclerView) rootView.findViewById(R.id.couponslist);

        tryAgainButton = (Button) rootView.findViewById(R.id.try_again);
        nothingFoundImage = (ImageView) rootView.findViewById(R.id.nothing_found);
        noDataFoundLayout = (View) rootView.findViewById(R.id.no_data_found_layout);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_sidemenu = getResources().getIdentifier("sidemenu2x_" + appColor, "drawable", getContext().getPackageName());
                menu_btn.setImageDrawable(getResources().getDrawable(ic_sidemenu));
            }
        }

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        tryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new categoryApi().execute();
            }
        });

        new categoryApi().execute();

        return rootView;
    }

    private class categoryApi extends AsyncTask<String, String, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparecatJson();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);
            Call<VoucherResponce> call = apiService.getvoucher(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VoucherResponce>() {
                @Override
                public void onResponse(Call<VoucherResponce> call, Response<VoucherResponce> response) {
                    Log.d(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {
                        VoucherResponce CouponResponce = response.body();
                        try {
                            if (response.isSuccessful()) {
                                VoucherResponce coupons = response.body();

                                if (coupons.getStatus()) {
                                    couponArrayList = coupons.getData();
                                    Log.d(TAG, "coupon arry size: " + couponArrayList.size());
                                    if (couponArrayList != null && couponArrayList.size() > 0) {

                                        mCouponAdapter = new VoucherAdapter(getContext(), couponArrayList, getActivity());
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                                        couponlist.setLayoutManager(new GridLayoutManager(getContext(), 1));
                                        couponlist.setAdapter(mCouponAdapter);

                                        noDataFoundLayout.setVisibility(View.GONE);
                                        nothingFoundImage.setVisibility(View.GONE);

                                    } else {

                                        noDataFoundLayout.setVisibility(View.VISIBLE);
                                        nothingFoundImage.setVisibility(View.GONE);

                                    }

                                } else {
                                    noDataFoundLayout.setVisibility(View.VISIBLE);
                                    nothingFoundImage.setVisibility(View.GONE);
                                    String failureResponse = CouponResponce.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                            } else {

                                noDataFoundLayout.setVisibility(View.VISIBLE);
                                nothingFoundImage.setVisibility(View.GONE);

                                String failureResponse = CouponResponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), getActivity());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        noDataFoundLayout.setVisibility(View.VISIBLE);
                        nothingFoundImage.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<VoucherResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());

                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                    noDataFoundLayout.setVisibility(View.GONE);
                    nothingFoundImage.setVisibility(View.VISIBLE);

                }
            });
            return null;
        }
    }

    private String preparecatJson() {
        JSONObject parentObj = new JSONObject();
        try {

            parentObj.put("StoreId", userPrefs.getInt("storeId", 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

}
