package com.cs.checkclickvendor.Fragment;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.cs.checkclickvendor.Activity.MainActivity;
import com.cs.checkclickvendor.Adapters.ViewPagerAdapter;
import com.cs.checkclickvendor.Contracts.CometChatActivityContract;
import com.cs.checkclickvendor.Contracts.StringContract;
import com.cs.checkclickvendor.Fragment.ContactsFragment;
import com.cs.checkclickvendor.Fragment.GroupListFragment;
import com.cs.checkclickvendor.Presenters.CometChatActivityPresenter;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.FabIconAnimator;
import com.cs.checkclickvendor.Utils.Utils.ScrollHelper;

import java.util.HashMap;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;


public class CometChatFragment extends Fragment implements CometChatActivityContract.CometChatActivityView {

    private ViewPager mViewPager; //view pager
    private TabLayout tabs;
    private Context context;
    private CoordinatorLayout mainContent;
    private CometChatActivityContract.CometChatActivityPresenter cometChatActivityPresenter;
    private ViewPagerAdapter adapter;
    private static final String TAG = "CometChatFragment";
    public static HashMap<String, Integer> countMap;
    private MenuItem searchItem;
    private SearchView searchView;
    private int pageNumber = 0;
    View rootView;
    SharedPreferences userPrefs;
    ImageView menu_btn, stores_menu_icon;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_comet_chat, container, false);
        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        context = getContext();
        cometChatActivityPresenter = new CometChatActivityPresenter();
        cometChatActivityPresenter.attach(this);
//        initViewComponents();

//        new Thread(() -> cometChatActivityPresenter.getBlockedUser(getContext())).start();

        tabs = rootView.findViewById(R.id.tabs);

        mViewPager = (ViewPager) rootView.findViewById(R.id.container);
        menu_btn = (ImageView) rootView.findViewById(R.id.menu_btn);

        stores_menu_icon = (ImageView) rootView.findViewById(R.id.stores_menu_icon);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_pickup = getResources().getIdentifier("dots_menu_" + appColor, "drawable", getContext().getPackageName());
                stores_menu_icon.setImageDrawable(getResources().getDrawable(ic_pickup));

                int ic_sidemenu = getResources().getIdentifier("sidemenu3x_" + appColor, "drawable", getContext().getPackageName());
                menu_btn.setImageDrawable(getResources().getDrawable(ic_sidemenu));
            }
        }

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        mViewPager.setOffscreenPageLimit(2);

        // checking cometchat login user
        if (CometChat.getLoggedInUser() == null) {
            if (!userPrefs.getString("chatUserId", "").equals("")) {
                CometChat.login(userPrefs.getString("chatUserId", ""), StringContract.AppDetails.API_KEY, new CometChat.CallbackListener<User>() {
                    @Override
                    public void onSuccess(User user) {
                        setViewPager();
                    }

                    @Override
                    public void onError(CometChatException e) {
                        e.printStackTrace();
                    }

                });
            } else {
//               CometChat.
            }
        } else {
            setViewPager();
        }

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                adapter.notifyDataSetChanged();
                pageNumber = i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        cometChatActivityPresenter.addCallEventListener(context, TAG);
        Log.d(TAG, "onResume: ");
        cometChatActivityPresenter.addMessageListener(getContext(), TAG);
    }


    @Override
    public void onPause() {
        super.onPause();
        cometChatActivityPresenter.removeMessageListener(TAG);
        cometChatActivityPresenter.removeCallEventListener(TAG);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        new GroupListFragment().onActivityResult(requestCode, resultCode, data);
//        mViewPager.setCurrentItem(2);
//    }

    private void setViewPager() {

        adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new ContactsFragment(), getString(R.string.contacts));
        adapter.addFragment(new GroupListFragment(), getString(R.string.group));
        mViewPager.setAdapter(adapter);
        tabs.setupWithViewPager(mViewPager);

    }

}



