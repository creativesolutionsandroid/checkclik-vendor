package com.cs.checkclickvendor.Fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickvendor.Activity.DatePickerActivity;
import com.cs.checkclickvendor.Activity.MainActivity;
import com.cs.checkclickvendor.Adapters.RevenueBranchesListAdapter;
import com.cs.checkclickvendor.Models.BranchList;
import com.cs.checkclickvendor.Models.RevenueGraphList;
import com.cs.checkclickvendor.Models.UserStores;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hrules.charter.CharterBar;
import com.hrules.charter.CharterLine;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v7.app.AppCompatActivity.RESULT_CANCELED;
import static android.support.v7.app.AppCompatActivity.RESULT_OK;
import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class RevenueFragment extends Fragment {

    View rootView;

    private static final int DEFAULT_ITEMS_COUNT = 12;
    private static final int DEFAULT_RANDOM_VALUE_MIN = 10;
    private static final int DEFAULT_RANDOM_VALUE_MAX = 100;
    private float[] values;
    private float[] values1;
    float current_amount, past_amount;

    String userId = "32";
    CharterBar charterBar, charterBar1;
    CharterLine charterLine;

    ArrayList<RevenueGraphList.FilteredGraph> filteredGraphs = new ArrayList<>();
    ArrayList<RevenueGraphList.CurrentYearGraph> currentYearGraphs = new ArrayList<>();
    ArrayList<BranchList.Branches> orderLists = new ArrayList<>();

    private static final int CUSTOMRANGE = 1;

    ImageView mimg;
    TextView brand_name, no_of_branches, categaries;
    TextView mselect_date, current_price, past_price, current_date, past_date, current_price1, past_price1, difference_price, total_income, total_expences;
    ViewPager mbranches_list;
    RevenueBranchesListAdapter mAdapter;


    LinearLayout total_orders_layout;
    private TextView today, yesterday, last_7_days, last_30_days, this_month, last_month, custom_rang, apply, cancel;

    String start_date = "0", end_date = "0";
    int mYear, mMonth, mDay;
    boolean isToday;
    String start = "", end = "";

    RelativeLayout charts_layout;

    String select_date_txt = "Today";
    SharedPreferences userPrefs;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.revenue_fragment, container, false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        charterBar = rootView.findViewById(R.id.charter_bar);
        charterBar1 = rootView.findViewById(R.id.charter_bar1);
        charterLine = rootView.findViewById(R.id.charter_line);

        mimg = rootView.findViewById(R.id.img);
        brand_name = rootView.findViewById(R.id.brand_name);
        no_of_branches = rootView.findViewById(R.id.no_of_branches);
        categaries = rootView.findViewById(R.id.categaries);
        mselect_date = rootView.findViewById(R.id.select_date);
        current_price = rootView.findViewById(R.id.currrent_price);
        past_price = rootView.findViewById(R.id.past_price);
        current_date = rootView.findViewById(R.id.current_date);
        past_date = rootView.findViewById(R.id.past_date);
        current_price1 = rootView.findViewById(R.id.current_price1);
        past_price1 = rootView.findViewById(R.id.past_price1);
        difference_price = rootView.findViewById(R.id.differents_price);
        total_income = rootView.findViewById(R.id.total_income);
        total_expences = rootView.findViewById(R.id.total_expences);
        mbranches_list = rootView.findViewById(R.id.branches_list);

        total_orders_layout = rootView.findViewById(R.id.total_orders_layout);

        today = (TextView) rootView.findViewById(R.id.today);
        yesterday = (TextView) rootView.findViewById(R.id.yesterday);
        last_7_days = (TextView) rootView.findViewById(R.id.last_seven_days);
        last_30_days = (TextView) rootView.findViewById(R.id.last_thirty_days);
        this_month = (TextView) rootView.findViewById(R.id.current_mouth);
        last_month = (TextView) rootView.findViewById(R.id.last_mouth);
        custom_rang = (TextView) rootView.findViewById(R.id.custom_range);
        apply = (TextView) rootView.findViewById(R.id.apply);
        cancel = (TextView) rootView.findViewById(R.id.cancel);

        charts_layout = (RelativeLayout) rootView.findViewById(R.id.charts_layout);

        Gson gson = new Gson();
        String json = userPrefs.getString("store", null);
        Type type = new TypeToken<UserStores.StoreListEntity>() {
        }.getType();
        UserStores.StoreListEntity storeList = gson.fromJson(json, type);

        if (storeList != null) {
            Log.i("TAG", "onCreateView: " + storeList.getCategoriesEn());
            brand_name.setText(storeList.getStoreNameEn());
            categaries.setText(storeList.getCategoriesEn());
            Glide.with(getContext()).load(Constants.STORE_IMAGE_URL + storeList.getLogoCopy())
                    .into(mimg);
            no_of_branches.setText("Number of branches(" + storeList.getBranchCount() + ")");
        }

        ImageView menu_btn = (ImageView) rootView.findViewById(R.id.menu_btn);
        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_sidemenu = getResources().getIdentifier("sidemenu3x_" + appColor, "drawable", getContext().getPackageName());
                menu_btn.setImageDrawable(getResources().getDrawable(ic_sidemenu));

                current_price1.setTextColor(Color.parseColor("#" + Constants.appColor));
                int color = getResources().getIdentifier("C50" + appColor, "color", getContext().getPackageName());
                charts_layout.setBackgroundColor(getResources().getColor(color));
            }
        }

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        total_orders_layout.setVisibility(View.GONE);
        mselect_date.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                if (total_orders_layout.getVisibility() == View.VISIBLE) {
                    total_orders_layout.setVisibility(View.GONE);
                } else {
                    total_orders_layout.setVisibility(View.VISIBLE);
                }

            }
        });

        String currentDate, currentTime;

        final Calendar c = Calendar.getInstance();

        SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        currentDate = df3.format(c.getTime());

        start_date = currentDate;
        end_date = currentDate;

        today.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, currentTime;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                currentDate = df3.format(c.getTime());

                start_date = currentDate;
                end_date = currentDate;
                select_date_txt = "Today";

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.white));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });

        yesterday.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, yesterdayDate;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                currentDate = df3.format(c.getTime());

                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("TAG", "before: " + currentDate);

                c.setTime(date);
                c.add(Calendar.DAY_OF_MONTH, -1);

                yesterdayDate = df3.format(c.getTime());
                Log.i("TAG", "after: " + yesterdayDate);


                start_date = yesterdayDate;
                end_date = currentDate;
                select_date_txt = "Yesterday";

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.white));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });

        last_7_days.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, startdate;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat selected_dateformat = new SimpleDateFormat("dd MMM''yy", Locale.US);

                currentDate = df3.format(c.getTime());

                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("TAG", "before: " + currentDate);

                c.setTime(date);
                c.add(Calendar.DAY_OF_MONTH, -7);

                startdate = df3.format(c.getTime());
                Log.i("TAG", "after: " + startdate);


                start_date = startdate;
                end_date = currentDate;

                Date select_start_date = null, select_end_date = null;

                try {
                    select_start_date = df3.parse(startdate);
                    select_end_date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                select_date_txt = selected_dateformat.format(select_start_date) + " - " + selected_dateformat.format(select_end_date);

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.white));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });

        last_30_days.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, startdate;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat selected_dateformat = new SimpleDateFormat("dd MMM''yy", Locale.US);

                currentDate = df3.format(c.getTime());

                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("TAG", "before: " + currentDate);

                c.setTime(date);
                c.add(Calendar.DAY_OF_MONTH, -30);

                startdate = df3.format(c.getTime());
                Log.i("TAG", "after: " + startdate);


                start_date = startdate;
                end_date = currentDate;

                Date select_start_date = null, select_end_date = null;

                try {
                    select_start_date = df3.parse(startdate);
                    select_end_date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                select_date_txt = selected_dateformat.format(select_start_date) + " - " + selected_dateformat.format(select_end_date);

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.white));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });

        this_month.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, startdate, smonth, syear;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat month = new SimpleDateFormat("MM", Locale.US);
                SimpleDateFormat year = new SimpleDateFormat("yyyy", Locale.US);
                SimpleDateFormat selected_dateformat = new SimpleDateFormat("dd MMM''yy", Locale.US);

                currentDate = df3.format(c.getTime());
                smonth = month.format(c.getTime());
                syear = year.format(c.getTime());

                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("TAG", "before: " + currentDate);


                startdate = syear + "-" + smonth + "-" + "01";
                Log.i("TAG", "after: " + startdate);


                start_date = startdate;
                end_date = currentDate;

                Date select_start_date = null, select_end_date = null;

                try {
                    select_start_date = df3.parse(startdate);
                    select_end_date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                select_date_txt = selected_dateformat.format(select_start_date) + " - " + selected_dateformat.format(select_end_date);

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.white));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });

        last_month.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, startdate, smonth, syear, firstday, lastday;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat month = new SimpleDateFormat("MM", Locale.US);
                SimpleDateFormat year = new SimpleDateFormat("yyyy", Locale.US);
                SimpleDateFormat day = new SimpleDateFormat("dd", Locale.US);
                SimpleDateFormat selected_dateformat = new SimpleDateFormat("dd MMM''yy", Locale.US);

                currentDate = df3.format(c.getTime());


                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                Calendar nextNotifTime = Calendar.getInstance();
                nextNotifTime.add(Calendar.MONTH, 1);
                nextNotifTime.set(Calendar.DATE, 1);
                nextNotifTime.add(Calendar.DATE, -1);

                c.setTime(date);
                c.add(Calendar.MONTH, -1);
                c.getActualMaximum(Calendar.DAY_OF_MONTH);
                lastday = day.format(nextNotifTime.getTime());
                Log.i("TAG", "lastday: " + lastday);

                smonth = month.format(c.getTime());
                syear = year.format(c.getTime());
                firstday = "01";

                startdate = syear + "-" + smonth + "-" + firstday;
                Log.i("TAG", "after: " + startdate);
                currentDate = syear + "-" + smonth + "-" + lastday;
                Log.i("TAG", "before: " + currentDate);


                start_date = startdate;
                end_date = currentDate;

                Date select_start_date = null, select_end_date = null;

                try {
                    select_start_date = df3.parse(startdate);
                    select_end_date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                select_date_txt = selected_dateformat.format(select_start_date) + " - " + selected_dateformat.format(select_end_date);

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.white));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });


        custom_rang.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                Intent a = new Intent(getActivity(), DatePickerActivity.class);
                startActivityForResult(a, CUSTOMRANGE);

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.white));
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                start_date = "0";
                end_date = "0";
                total_orders_layout.setVisibility(View.GONE);


            }
        });

        apply.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

//
                if (start_date.equalsIgnoreCase("0") && end_date.equalsIgnoreCase("0")) {
                    String currentDate, currentTime;

                    final Calendar c = Calendar.getInstance();

                    SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                    currentDate = df3.format(c.getTime());

                    start_date = currentDate;
                    end_date = currentDate;

                    today.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                    yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                    today.setTextColor(getResources().getColor(R.color.white));
                    yesterday.setTextColor(getResources().getColor(R.color.black));
                    last_7_days.setTextColor(getResources().getColor(R.color.black));
                    last_30_days.setTextColor(getResources().getColor(R.color.black));
                    this_month.setTextColor(getResources().getColor(R.color.black));
                    last_month.setTextColor(getResources().getColor(R.color.black));
                    custom_rang.setTextColor(getResources().getColor(R.color.black));


                }
                mselect_date.setText(select_date_txt);
                new RevenuGraphApi().execute();

                String currentDate, currentTime;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                currentDate = df3.format(c.getTime());


                Log.i("TAG", "start end dates: " + start_date + " " + end_date);
                total_orders_layout.setVisibility(View.GONE);


            }
        });

        new BranchListapi().execute();


        mbranches_list.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


//        values =
//                fillRandomValues(DEFAULT_ITEMS_COUNT, DEFAULT_RANDOM_VALUE_MAX, DEFAULT_RANDOM_VALUE_MIN);


        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CUSTOMRANGE && resultCode == RESULT_OK) {
            Log.d("TAG", "onActivityResult: " + data.getStringExtra("startdate"));
            start_date = data.getStringExtra("startdate");
            end_date = data.getStringExtra("enddate");

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            SimpleDateFormat selected_dateformat = new SimpleDateFormat("dd MMM''yy", Locale.US);

            Date datestart = null, dateend = null;

            Log.i("TAG", "start end: " + start_date + " " + end_date);

            try {
                datestart = dateFormat.parse(start_date);
                dateend = dateFormat.parse(end_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.i("TAG", "datestart dateend: " + datestart + " " + dateend);

            if (datestart.before(dateend)) {

                start_date = dateFormat.format(datestart);
                end_date = dateFormat.format(dateend);

            } else {

                start_date = dateFormat.format(dateend);
                end_date = dateFormat.format(datestart);

            }

            Log.i("TAG", "startdate enddate: " + start_date + " " + end_date);

            Date select_start_date = null, select_end_date = null;

            try {
                select_start_date = dateFormat.parse(start_date);
                select_end_date = dateFormat.parse(end_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            select_date_txt = selected_dateformat.format(select_start_date) + " - " + selected_dateformat.format(select_end_date);

        } else if (requestCode == CUSTOMRANGE && resultCode == RESULT_CANCELED) {
            start_date = "0";
            end_date = "0";
        }

    }


    private float[] fillRandomValues(int length, int max, int min) {
        Random random = new Random();

        float[] newRandomValues = new float[length];
        for (int i = 0; i < newRandomValues.length; i++) {
            newRandomValues[i] = random.nextInt(max - min + 1) - min;
        }
        return newRandomValues;
    }


    private String prepareRevenuGraphJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId", userPrefs.getString("userId", null));
            parentObj.put("StartDate", start_date);
            parentObj.put("EndDate", end_date);
            parentObj.put("filterId", 5);
            parentObj.put("StoreId", userPrefs.getInt("storeId", 0));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj.toString());

        return parentObj.toString();
    }

    private class RevenuGraphApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareRevenuGraphJson();
//            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<RevenueGraphList> call = apiService.getRevenuegraph(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<RevenueGraphList>() {
                @Override
                public void onResponse(Call<RevenueGraphList> call, Response<RevenueGraphList> response) {
                    if (response.isSuccessful()) {
                        RevenueGraphList updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            filteredGraphs = updateorderList.getData().getFilteredgraph();
                            currentYearGraphs = updateorderList.getData().getCurrentyeargraph();

                            String current = null;

                            values = new float[filteredGraphs.size()];
                            for (int i = 0; i < filteredGraphs.size(); i++) {
                                values[i] = filteredGraphs.get(i).getCurrentamount();
                                if (current != null && current.length() > 0) {

                                    current = current + "," + filteredGraphs.get(i).getCurrentamount();
                                    current_amount = current_amount + filteredGraphs.get(i).getCurrentamount();
                                    past_amount = past_amount + filteredGraphs.get(i).getPastamount();

                                } else {

                                    current = String.valueOf(filteredGraphs.get(i).getCurrentamount());
                                    current_amount = filteredGraphs.get(i).getCurrentamount();
                                    past_amount = filteredGraphs.get(i).getPastamount();
                                }
                            }

                            current_price.setText("" + Constants.priceFormat1.format(current_amount));
                            past_price.setText("" + Constants.priceFormat1.format(past_amount));

                            current_price1.setText("SAR " + Constants.priceFormat1.format(updateorderList.getData().getCurrentamount()));
                            past_price1.setText("SAR " + Constants.priceFormat1.format(updateorderList.getData().getPastamount()));
                            difference_price.setText("SAR " + Constants.priceFormat1.format(updateorderList.getData().getDifferenceamount()));

                            total_income.setText("" + Constants.priceFormat1.format(updateorderList.getData().getTotalincome()) + " SAR");
                            total_expences.setText("" + Constants.priceFormat1.format(updateorderList.getData().getTotalexpense()) + " SAR");

                            if (updateorderList.getData().getTotalincome() > 0) {

//                            Log.i("TAG", "onResponse: " + values[0]);

                                values1 = new float[currentYearGraphs.size()];
                                for (int i = 0; i < currentYearGraphs.size(); i++) {
                                    values1[i] = currentYearGraphs.get(i).getAmount();
                                }

//                            Log.i("TAG", "onResponse: " + values1[0]);

                                charterLine.setValues(values);
                                charterLine.setAnimInterpolator(new BounceInterpolator());


                                charterLine.setLineColor(getResources().getColor(R.color.white));
                                charterLine.setIndicatorColor(getResources().getColor(R.color.white));
                                charterLine.setChartFillColor(Color.TRANSPARENT);
                                charterLine.setIndicatorSize(6);
                                charterLine.setStrokeSize(4);
                                charterLine.setLineColor(Color.TRANSPARENT);

                                Resources res = getResources();
                                int[] barColors = new int[]{
                                        res.getColor(R.color.bargraph), res.getColor(R.color.bargraph),
                                        res.getColor(R.color.bargraph)
                                };

                                charterBar1.setValues(values);
                                charterBar1.setColors(barColors);
                                charterBar1.setColorsBackground(new int[]{Color.TRANSPARENT});

                                if (appColor != null) {
                                    if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                                        int color = getResources().getIdentifier("C" + appColor, "color", getContext().getPackageName());
                                        Resources res1 = getResources();
                                        int[] barColors1 = new int[]{
                                                res1.getColor(color), res1.getColor(color),
                                                res1.getColor(color)
                                        };
                                        charterBar.setValues(values1);
                                        charterBar.setColors(barColors1);
                                    }
                                } else {
//                                    int color = getResources().getIdentifier("C" + appColor, "color", getContext().getPackageName());
                                    Resources res1 = getResources();
                                    int[] barColors1 = new int[]{
                                            res1.getColor(R.color.colorPrimary), res1.getColor(R.color.colorPrimary),
                                            res1.getColor(R.color.colorPrimary)
                                    };
                                    charterBar.setValues(values1);
                                    charterBar.setColors(barColors1);
                                }
                            }

                        } else {
                            String failureResponse = updateorderList.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    "Ok", (AppCompatActivity) getActivity());
                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<RevenueGraphList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Log.i("TAG", "onFailure: " + t);
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareChangePasswordJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id", 0);
            parentObj.put("StoreId", userPrefs.getInt("storeId", 0));
            parentObj.put("FlagId", 1);
            parentObj.put("CreatedBy", 32);
            parentObj.put("ControllerName", "Branches");
            parentObj.put("ActionName", "Index");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    private class BranchListapi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareChangePasswordJson();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<BranchList> call = apiService.getbranchlist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BranchList>() {
                @Override
                public void onResponse(Call<BranchList> call, Response<BranchList> response) {

                    if (response.isSuccessful()) {
                        BranchList Response = response.body();

                        orderLists = Response.getData().getBranches();

                        mAdapter = new RevenueBranchesListAdapter(getActivity(), orderLists);
                        mbranches_list.setAdapter(mAdapter);

                        Log.i("TAG", "RevenueBranchesListAdapter: " + Response.getMessage());
                        new RevenuGraphApi().execute();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: ");
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BranchList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    Constants.closeLoadingDialog();
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onFailure: " + t);
                    }
                }
            });
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Constants.closeLoadingDialog();
    }


    public void StartDatePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if (view.isShown()) {

                            if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                isToday = true;
                            } else {
                                isToday = false;
                            }
                            mYear = year;
                            mDay = dayOfMonth;
                            mMonth = monthOfYear;


                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                            Date date1 = null;

                            try {
                                date1 = df.parse(date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            start = df.format(date1);

                            Log.i("TAG", "onDateSet start: " + start);

                            EndDatePicker();
                        }


                    }
                }, mYear, mMonth, mDay);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String date;
        c.add(Calendar.YEAR, -1);
        date = dateFormat.format(c.getTimeInMillis());
        Log.i("TAG", "StartDatePicker: " + date);

        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//        int max_day = 0;
//        int max_days = max_day;
//        long max = TimeUnit.DAYS.toMillis(Long.parseLong(String.valueOf(max_days)));
//        Log.i("TAG", "maxdays+1: " + max_days);
        Calendar calendar = Calendar.getInstance();

        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.show();

    }

    public void EndDatePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if (view.isShown()) {

                            if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                isToday = true;
                            } else {
                                isToday = false;
                            }
                            mYear = year;
                            mDay = dayOfMonth;
                            mMonth = monthOfYear;


                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                            Date date1 = null;

                            try {
                                date1 = df.parse(date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            end = df.format(date1);

                            Log.i("TAG", "onDateSet end: " + end);

                            if (!start.equalsIgnoreCase("") || !end.equalsIgnoreCase("")) {

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                                SimpleDateFormat selected_dateformat = new SimpleDateFormat("dd MMM''yy", Locale.US);

                                Date datestart = null, dateend = null;

                                Log.i("TAG", "start end: " + start + " " + end);

                                try {
                                    datestart = dateFormat.parse(start);
                                    dateend = dateFormat.parse(end);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                Log.i("TAG", "datestart dateend: " + datestart + " " + dateend);

                                if (datestart.before(dateend)) {

                                    start_date = dateFormat.format(datestart);
                                    end_date = dateFormat.format(dateend);

                                } else {

                                    start_date = dateFormat.format(dateend);
                                    end_date = dateFormat.format(datestart);

                                }

                                Log.i("TAG", "startdate enddate: " + start_date + " " + end_date);

                                Date select_start_date = null, select_end_date = null;

                                try {
                                    select_start_date = dateFormat.parse(start_date);
                                    select_end_date = dateFormat.parse(end_date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                select_date_txt = selected_dateformat.format(select_start_date) + " - " + selected_dateformat.format(select_end_date);


                            } else {


                            }

                        }


                    }
                }, mYear, mMonth, mDay);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String date;
        c.add(Calendar.YEAR, -1);
        date = dateFormat.format(c.getTimeInMillis());
        Log.i("TAG", "StartDatePicker: " + date);

        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//        int max_day = 0;
//        int max_days = max_day;
//        long max = TimeUnit.DAYS.toMillis(Long.parseLong(String.valueOf(max_days)));
//        Log.i("TAG", "maxdays+1: " + max_days);
        Calendar calendar = Calendar.getInstance();
        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.show();

    }

}
