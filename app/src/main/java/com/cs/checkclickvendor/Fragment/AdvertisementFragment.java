package com.cs.checkclickvendor.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Activity.MainActivity;
import com.cs.checkclickvendor.Activity.NotificationsActivity;
import com.cs.checkclickvendor.Adapters.AdvertisementAdapter;
import com.cs.checkclickvendor.Models.AdavertisementResponce;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickvendor.Adapters.ProfileBranchesAdapter.TAG;
import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;


public class AdvertisementFragment extends Fragment {

    View rootView;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;
    ImageView menu_btn, notification;
    TextView adveactivetext, advefuturetext, adveexpiredtext, offeractivetext, offerfuturetext, offerexpiredtext;
    ViewPager advelistviewpager, offerlistviewpager;
    RelativeLayout adveActive, adveFuture, adveExpired, offerActive, offerFuture, offerExpired;
    View adveActiveview, adveFutureview, adveExpiredview, offerActiveview, offerFutureview, offerExpiredview;
    AdvertisementAdapter madapter;

    ArrayList<AdavertisementResponce.DataEntity> adveMainArryList = new ArrayList<>();
    ArrayList<AdavertisementResponce.DataEntity> adveactivelist = new ArrayList<>();
    ArrayList<AdavertisementResponce.DataEntity> advefuturelist = new ArrayList<>();
    ArrayList<AdavertisementResponce.DataEntity> adveexpiredlist = new ArrayList<>();

    ArrayList<AdavertisementResponce.DataEntity> offeractivelist = new ArrayList<>();
    ArrayList<AdavertisementResponce.DataEntity> offerfuturelist = new ArrayList<>();
    ArrayList<AdavertisementResponce.DataEntity> offerexpiredlist = new ArrayList<>();


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.advertisement_fragment, container, false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        new advestApi().execute();

        adveActive = (RelativeLayout) rootView.findViewById(R.id.adveactivelayout);
        adveActiveview = (View) rootView.findViewById(R.id.adveactiveview);
        adveFuture = (RelativeLayout) rootView.findViewById(R.id.advefutre);
        adveFutureview = (View) rootView.findViewById(R.id.advefutureview);
        adveExpired = (RelativeLayout) rootView.findViewById(R.id.adveexpired);
        adveExpiredview = (View) rootView.findViewById(R.id.adveExpiredview);

        offerActive = (RelativeLayout) rootView.findViewById(R.id.offeractivie);
        offerActiveview = (View) rootView.findViewById(R.id.offeractivieview);
        offerFuture = (RelativeLayout) rootView.findViewById(R.id.offerfuture);
        offerFutureview = (View) rootView.findViewById(R.id.offerfutureview);
        offerExpired = (RelativeLayout) rootView.findViewById(R.id.offerexpired);
        offerExpiredview = (View) rootView.findViewById(R.id.offerexpiredview);
        notification = (ImageView) rootView.findViewById(R.id.notification);

        menu_btn = (ImageView) rootView.findViewById(R.id.menu_btn);
        advelistviewpager = (ViewPager) rootView.findViewById(R.id.adve_list);
        offerlistviewpager = (ViewPager) rootView.findViewById(R.id.offer_list);
        offerlistviewpager.setClipToPadding(false);

        offerlistviewpager.setPadding(10, 0, 10, 0);
        offerlistviewpager.setPageMargin(10);
        advelistviewpager.setPadding(10, 0, 10, 0);
        advelistviewpager.setPageMargin(10);

        adveactivetext = (TextView) rootView.findViewById(R.id.adveactivetext);
        advefuturetext = (TextView) rootView.findViewById(R.id.advefuturetext);
        adveexpiredtext = (TextView) rootView.findViewById(R.id.adveexpiredtext);
        offeractivetext = (TextView) rootView.findViewById(R.id.offeractivetext);
        offerfuturetext = (TextView) rootView.findViewById(R.id.offerfuturetext);
        offerexpiredtext = (TextView) rootView.findViewById(R.id.offerexpiredtext);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_notify = getResources().getIdentifier("notification_icon2x_" + appColor, "drawable", getActivity().getPackageName());
                notification.setImageDrawable(getResources().getDrawable(ic_notify));

                int ic_sidemenu = getResources().getIdentifier("sidemenu3x_" + appColor, "drawable", getContext().getPackageName());
                menu_btn.setImageDrawable(getResources().getDrawable(ic_sidemenu));

                adveActiveview.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                adveFutureview.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                adveExpiredview.setBackgroundColor(Color.parseColor("#" + Constants.appColor));

                offerActiveview.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                offerFutureview.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                offerExpiredview.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
            }
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NotificationsActivity.class);
                startActivity(intent);
            }
        });

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();

            }
        });

        adveActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adveactivelist != null && adveactivelist.size() > 0) {
                    adveFutureview.setVisibility(View.INVISIBLE);
                    adveActiveview.setVisibility(View.VISIBLE);
                    adveExpiredview.setVisibility(View.INVISIBLE);

                    if (appColor != null) {
                        if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                            adveactivetext.setTextColor(Color.parseColor("#" + Constants.appColor));
                        }
                    } else {
                        adveactivetext.setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                    advefuturetext.setTextColor(getResources().getColor(R.color.black));
                    adveexpiredtext.setTextColor(getResources().getColor(R.color.black));

                    madapter = new AdvertisementAdapter(getContext(), adveactivelist, false);
                    advelistviewpager.setAdapter(madapter);
                }
            }
        });


        adveFuture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (advefuturelist != null && advefuturelist.size() > 0) {
                    adveFutureview.setVisibility(View.VISIBLE);
                    adveActiveview.setVisibility(View.INVISIBLE);
                    adveExpiredview.setVisibility(View.INVISIBLE);
                    if (appColor != null) {
                        if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                            advefuturetext.setTextColor(Color.parseColor("#" + Constants.appColor));
                        }
                    } else {
                        advefuturetext.setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                    adveactivetext.setTextColor(getResources().getColor(R.color.black));
                    adveexpiredtext.setTextColor(getResources().getColor(R.color.black));

                    madapter = new AdvertisementAdapter(getContext(), advefuturelist, false);
                    advelistviewpager.setAdapter(madapter);
                }

            }
        });

        adveExpired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adveexpiredlist != null && adveexpiredlist.size() > 0) {
                    adveExpiredview.setVisibility(View.VISIBLE);
                    adveFutureview.setVisibility(View.INVISIBLE);
                    adveActiveview.setVisibility(View.INVISIBLE);
                    if (appColor != null) {
                        if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                            adveexpiredtext.setTextColor(Color.parseColor("#" + Constants.appColor));
                        }
                    } else {
                        adveexpiredtext.setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                    adveactivetext.setTextColor(getResources().getColor(R.color.black));
                    advefuturetext.setTextColor(getResources().getColor(R.color.black));

                    madapter = new AdvertisementAdapter(getContext(), adveexpiredlist, false);
                    advelistviewpager.setAdapter(madapter);
                }

            }
        });

        offerActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (offeractivelist != null && offeractivelist.size() > 0) {
                    offerActiveview.setVisibility(View.VISIBLE);
                    offerFutureview.setVisibility(View.INVISIBLE);
                    offerFutureview.setVisibility(View.INVISIBLE);
                    if (appColor != null) {
                        if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                            offeractivetext.setTextColor(Color.parseColor("#" + Constants.appColor));
                        }
                    } else {
                        offeractivetext.setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                    offerfuturetext.setTextColor(getResources().getColor(R.color.black));
                    offerexpiredtext.setTextColor(getResources().getColor(R.color.black));
                    madapter = new AdvertisementAdapter(getContext(), offeractivelist, true);
                    offerlistviewpager.setAdapter(madapter);
                }

            }
        });

        offerFuture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (offerfuturelist != null && offerfuturelist.size() > 0) {
                    offerFutureview.setVisibility(View.VISIBLE);
                    offerExpiredview.setVisibility(View.INVISIBLE);
                    offerActiveview.setVisibility(View.INVISIBLE);
                    offeractivetext.setTextColor(getResources().getColor(R.color.black));
                    if (appColor != null) {
                        if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                            offerfuturetext.setTextColor(Color.parseColor("#" + Constants.appColor));
                        }
                    } else {
                        offerfuturetext.setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                    offerexpiredtext.setTextColor(getResources().getColor(R.color.black));

                    madapter = new AdvertisementAdapter(getContext(), offerfuturelist, true);
                    offerlistviewpager.setAdapter(madapter);
                }
            }
        });

        offerExpired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (offerexpiredlist != null && offerexpiredlist.size() > 0) {
                    offerExpiredview.setVisibility(View.VISIBLE);
                    offerFutureview.setVisibility(View.INVISIBLE);
                    offerActiveview.setVisibility(View.INVISIBLE);
                    offeractivetext.setTextColor(getResources().getColor(R.color.black));
                    offerfuturetext.setTextColor(getResources().getColor(R.color.black));
                    if (appColor != null) {
                        if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                            offerexpiredtext.setTextColor(Color.parseColor("#" + Constants.appColor));
                        }
                    } else {

                        offerexpiredtext.setTextColor(getResources().getColor(R.color.colorPrimary));

                    }
                    madapter = new AdvertisementAdapter(getContext(), offerexpiredlist, true);
                    offerlistviewpager.setAdapter(madapter);
                }
            }
        });

        return rootView;
    }

    private class advestApi extends AsyncTask<String, String, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareAdveJson();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<AdavertisementResponce> call = apiService.getadavertise(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<AdavertisementResponce>() {
                @Override
                public void onResponse(Call<AdavertisementResponce> call, Response<AdavertisementResponce> response) {
                    if (response.isSuccessful()) {
                        AdavertisementResponce AdveResponce = response.body();
                        try {
                            if (AdveResponce.getStatus()) {
                                AdavertisementResponce Response = response.body();
                                adveMainArryList = Response.getData();
                                Log.d(TAG, "onResponsedatasize: " + adveMainArryList.size());
                                setadvelist();

                            } else {
                                String failureResponse = AdveResponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), getActivity());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<AdavertisementResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareAdveJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id", 0);
            parentObj.put("StoreId", userPrefs.getInt("storeId", 0));
            parentObj.put("FlagId", 4);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareAdveJson: " + parentObj);
        return parentObj.toString();
    }

    public void setadvelist() {

        for (int i = 0; i < adveMainArryList.size(); i++) {

            Date Startdate = null;
            Date Enddate = null;

            Calendar todaydate = Calendar.getInstance();

            String sdate = adveMainArryList.get(i).getStartDate();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            try {
                Startdate = format1.parse(sdate);

            } catch (ParseException e) {
                e.printStackTrace();

            }

            String edate = adveMainArryList.get(i).getEndDate();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            try {
                Enddate = format.parse(edate);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (adveMainArryList.get(i).getOfferType() == 1) {
                if (Startdate.before(todaydate.getTime()) && Enddate.after(todaydate.getTime())) {
                    adveactivelist.add(adveMainArryList.get(i));
                } else if (Enddate.before(todaydate.getTime())) {
                    adveexpiredlist.add(adveMainArryList.get(i));
                } else if (Startdate.after(todaydate.getTime())) {
                    advefuturelist.add(adveMainArryList.get(i));
                }

            } else {
                if (Startdate.before(todaydate.getTime()) && Enddate.after(todaydate.getTime())) {
                    offeractivelist.add(adveMainArryList.get(i));
                } else if (Enddate.before(todaydate.getTime())) {
                    offerexpiredlist.add(adveMainArryList.get(i));
                } else if (Startdate.after(todaydate.getTime())) {

                    offerfuturelist.add(adveMainArryList.get(i));
//                    Log.d(TAG, "setadvelistoffer: "+offerfuturelist.size());
                }

            }
        }

        adveactivetext.setText("Active (" + adveactivelist.size() + ")");
        advefuturetext.setText("Future (" + advefuturelist.size() + ")");
        adveexpiredtext.setText("Expired (" + adveexpiredlist.size() + ")");
        offeractivetext.setText("Active (" + offeractivelist.size() + ")");
        offerfuturetext.setText("Future (" + offerfuturelist.size() + ")");
        offerexpiredtext.setText("Expired (" + offerexpiredlist.size() + ")");

        if (adveactivelist.size() > 0) {
            adveActive.performClick();
        } else if (advefuturelist.size() > 0) {
            adveFuture.performClick();
        } else {
            adveExpired.performClick();
        }

        if (offeractivelist.size() > 0) {
            offerActive.performClick();
        } else if (offerfuturelist.size() > 0) {
            offerFuture.performClick();
        } else {
            offerExpired.performClick();
        }
    }
}
