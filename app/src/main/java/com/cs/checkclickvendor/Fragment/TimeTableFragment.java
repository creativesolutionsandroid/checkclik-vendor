package com.cs.checkclickvendor.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Activity.MainActivity;
import com.cs.checkclickvendor.Activity.NotificationsActivity;
import com.cs.checkclickvendor.Adapters.TimeTableAdapter;
import com.cs.checkclickvendor.Models.BranchList;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickvendor.Adapters.ProfileBranchesAdapter.TAG;
import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;


public class TimeTableFragment extends Fragment {
    View rootView;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;
    RecyclerView branchlist;
    ImageView menu_btn, notification;
    TimeTableAdapter madapter;
    ArrayList<BranchList.Branches> orderLists = new ArrayList<>();

    private TextView select_branch;
    private View noDataFoundLayout;
    private ImageView nothingFoundImage;
    private Button tryAgainButton;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_timetable, container, false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        menu_btn = (ImageView) rootView.findViewById(R.id.menu_btn);
        branchlist = (RecyclerView) rootView.findViewById(R.id.timetablelist);
        notification = (ImageView) rootView.findViewById(R.id.notification);

        select_branch = (TextView) rootView.findViewById(R.id.select_branch);
        tryAgainButton = (Button) rootView.findViewById(R.id.try_again);
        nothingFoundImage = (ImageView) rootView.findViewById(R.id.nothing_found);
        noDataFoundLayout = (View) rootView.findViewById(R.id.no_data_found_layout);
        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_notify = getResources().getIdentifier("notification_icon2x_" + appColor, "drawable", getActivity().getPackageName());
                notification.setImageDrawable(getResources().getDrawable(ic_notify));

                int ic_sidemenu = getResources().getIdentifier("sidemenu3x_" + appColor, "drawable", getContext().getPackageName());
                menu_btn.setImageDrawable(getResources().getDrawable(ic_sidemenu));
            }
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NotificationsActivity.class);
                startActivity(intent);
            }
        });

        tryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new branchlistApi().execute();
            }
        });

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        new branchlistApi().execute();

        return rootView;
    }

    private class branchlistApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparecatJson();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<BranchList> call = apiService.getbranchs(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BranchList>() {
                @Override
                public void onResponse(Call<BranchList> call, Response<BranchList> response) {
                    if (response.isSuccessful()) {
                        BranchList Catresponce = response.body();
                        try {
                            if (Catresponce.getStatus()) {
                                BranchList Response = response.body();
                                orderLists = Response.getData().getBranches();
                                if (orderLists != null && orderLists.size() > 0) {

                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                    branchlist.setLayoutManager(mLayoutManager);
                                    madapter = new TimeTableAdapter(getContext(), orderLists, "timetable");
                                    branchlist.setAdapter(madapter);

                                    noDataFoundLayout.setVisibility(View.GONE);
                                    select_branch.setVisibility(View.VISIBLE);
                                    nothingFoundImage.setVisibility(View.GONE);

                                } else {

                                    noDataFoundLayout.setVisibility(View.VISIBLE);
                                    select_branch.setVisibility(View.GONE);
                                    nothingFoundImage.setVisibility(View.GONE);

                                }

                            } else {

                                noDataFoundLayout.setVisibility(View.VISIBLE);
                                select_branch.setVisibility(View.GONE);
                                nothingFoundImage.setVisibility(View.GONE);
                                String failureResponse = Catresponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), getActivity());

                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                        }
                    } else {

                        noDataFoundLayout.setVisibility(View.VISIBLE);
                        select_branch.setVisibility(View.GONE);
                        nothingFoundImage.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BranchList> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                    noDataFoundLayout.setVisibility(View.GONE);
                    select_branch.setVisibility(View.GONE);
                    nothingFoundImage.setVisibility(View.VISIBLE);
                }
            });
            return null;
        }
    }

    private String preparecatJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id", 0);
            parentObj.put("StoreId", userPrefs.getInt("storeId", 0));
            parentObj.put("FlagId", 1);
            parentObj.put("CreatedBy", userPrefs.getString("userId", null));
            parentObj.put("ControllerName", "Branches");
            parentObj.put("ActionName", "Index");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }
}
