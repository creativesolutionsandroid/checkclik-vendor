package com.cs.checkclickvendor.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickvendor.Activity.MainActivity;
import com.cs.checkclickvendor.Activity.NotificationsActivity;
import com.cs.checkclickvendor.Adapters.AccessAdapter;
import com.cs.checkclickvendor.Models.AccessResponce;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickvendor.Adapters.ProfileBranchesAdapter.TAG;
import static com.cs.checkclickvendor.Utils.Utils.Constants.ADS_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;


public class AccessManagementFragment extends Fragment {

    View rootView;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;
    ImageView menu_btn, notification;
    RecyclerView accessnameslist;
    ImageView branchlogo;
    AccessAdapter madapter;
    String userId;
    TextView storename, numberofbranch, cat;
    ArrayList<AccessResponce.DataEntity> accessarralist = new ArrayList<>();
    CamomileSpinner spinner;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_acessmanagement, container, false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        userId = userPrefs.getString("userId", null);

        menu_btn = (ImageView) rootView.findViewById(R.id.menu_btn);
        notification = (ImageView) rootView.findViewById(R.id.notification);
        accessnameslist = (RecyclerView) rootView.findViewById(R.id.accesslist);

        numberofbranch = (TextView) rootView.findViewById(R.id.numberofbranches);
        storename = (TextView) rootView.findViewById(R.id.storename);
        cat = (TextView) rootView.findViewById(R.id.cat);
        branchlogo = (ImageView) rootView.findViewById(R.id.storeimage);
        spinner = (CamomileSpinner) rootView.findViewById(R.id.spinner);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_notify = getResources().getIdentifier("notification_icon2x_" + appColor, "drawable", getActivity().getPackageName());
                notification.setImageDrawable(getResources().getDrawable(ic_notify));

                int ic_sidemenu = getResources().getIdentifier("sidemenu3x_" + appColor, "drawable", getContext().getPackageName());
                menu_btn.setImageDrawable(getResources().getDrawable(ic_sidemenu));
            }
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NotificationsActivity.class);
                startActivity(intent);
            }
        });

        new accessapi().execute();

//        spinner.start();
//
//        spinner.recreateWithParams(
//                getContext(),
//                DialogUtils.getColor(getContext(), R.color.black),
//                120,
//                true
//        );


        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });
        return rootView;
    }

    private class accessapi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareAdveJson();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<AccessResponce> call = apiService.getaccesslist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<AccessResponce>() {
                @Override
                public void onResponse(Call<AccessResponce> call, Response<AccessResponce> response) {
                    Log.d(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {
                        AccessResponce AccessResponce = response.body();
                        try {
                            if (AccessResponce.getStatus()) {
                                AccessResponce Response = response.body();
                                accessarralist = Response.getData();

                                int storePos = 0;
                                for (int i = 0; i < accessarralist.size(); i++) {
                                    if (userPrefs.getInt("storeId", 0) == accessarralist.get(i).getStoreId()) {
                                        storePos = i;
                                    }
                                }
                                storename.setText(accessarralist.get(storePos).getStoreNameEn());
                                numberofbranch.setText("Number of branches (" + accessarralist.get(storePos).getNoOfBranches() + ")");
                                cat.setText(accessarralist.get(storePos).getCategoryListEn());
                                cat.setVisibility(View.VISIBLE);

//                                Glide.with(getContext())
//                                        .load(ADS_IMAGE_URL+accessarralist.get(storePos).gets)
//                                        .listener(new RequestListener<Drawable>() {
//                                            @Override
//                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                                                spinner.setVisibility(View.GONE);
//                                                return false;
//                                            }
//                                            @Override
//                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                                                spinner.setVisibility(View.GONE);
//                                                return false;
//                                            }
//                                        })
//                                        .into(branchlogo);


                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                accessnameslist.setLayoutManager(mLayoutManager);
                                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(accessnameslist.getContext(),
                                        ((LinearLayoutManager) mLayoutManager).getOrientation());
                                accessnameslist.addItemDecoration(dividerItemDecoration);

                                madapter = new AccessAdapter(getContext(), accessarralist.get(storePos).getListOfAccessUsers());
                                accessnameslist.setAdapter(madapter);


                            } else {
                                String failureResponse = AccessResponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), getActivity());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<AccessResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareAdveJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("VendorId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareAcceJson: " + parentObj);
        return parentObj.toString();
    }
}
