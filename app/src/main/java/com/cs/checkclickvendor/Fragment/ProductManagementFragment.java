package com.cs.checkclickvendor.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Activity.MainActivity;
import com.cs.checkclickvendor.Activity.NotificationsActivity;
import com.cs.checkclickvendor.Adapters.ProductAvailableAdapter;
import com.cs.checkclickvendor.Models.ProductManagementResponce;
import com.cs.checkclickvendor.Models.ProductMangPagination;
import com.cs.checkclickvendor.Models.ProductMangServiceResponse;
import com.cs.checkclickvendor.Models.UserStores;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickvendor.Activity.ForgotPasswordActivity.TAG;
import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class ProductManagementFragment extends Fragment {

    View rootView;
    RelativeLayout outofstocklayout, instocklayout;
    View outofstockview, instockview;
    ImageView menu_btn, notification;
    TextView outofstocktext, instocktext;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;
    RecyclerView listview;
    ProductAvailableAdapter mavableadapter;
    ProductManagementResponce.DataEntity productArryList;
    ArrayList<ProductManagementResponce.ProductListEntity> productArryList1 = new ArrayList<>();
    ProductMangServiceResponse.DataEntity serviceArryList;
    ArrayList<ProductMangServiceResponse.ServiceListEntity> serviceArryList1 = new ArrayList<>();
    boolean isOutStockSelected = false;

    int page_no = 1, page_size = 10;
    int lastVisibleItem, no_of_rows, lastvisibleposition = 8;

    ArrayList<ProductMangPagination> productMangPaginations = new ArrayList<>();
    UserStores.StoreListEntity storeListEntities;
    String Json;
    int pos;

    private LinearLayout layout;
    private View noDataFoundLayout;
    private ImageView nothingFoundImage;
    private Button tryAgainButton;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_product_management, container, false);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        pos = userPrefs.getInt("pos", 0);
        Gson gson = new Gson();
        String json = userPrefs.getString("store", null);
        Type type = new TypeToken<UserStores.StoreListEntity>() {
        }.getType();
        storeListEntities = gson.fromJson(json, type);

//        Log.i("TAG", "onCreateView: " + storeListEntities.size());

        productMangPaginations.clear();

        outofstocklayout = (RelativeLayout) rootView.findViewById(R.id.outofstocklayout);
        instocklayout = (RelativeLayout) rootView.findViewById(R.id.Availablelayout);
        outofstockview = (View) rootView.findViewById(R.id.adveactiveview);
        instockview = (View) rootView.findViewById(R.id.instockview);
        notification = (ImageView) rootView.findViewById(R.id.notification);
        menu_btn = (ImageView) rootView.findViewById(R.id.menu_btn);
        listview = (RecyclerView) rootView.findViewById(R.id.listview);
        outofstocktext = (TextView) rootView.findViewById(R.id.outofstocktext);
        instocktext = (TextView) rootView.findViewById(R.id.instocktext);

        layout = (LinearLayout) rootView.findViewById(R.id.layout);
        tryAgainButton = (Button) rootView.findViewById(R.id.try_again);
        nothingFoundImage = (ImageView) rootView.findViewById(R.id.nothing_found);
        noDataFoundLayout = (View) rootView.findViewById(R.id.no_data_found_layout);

        if (storeListEntities.getStoreType() == 1) {
            new productmanagementApi().execute();
        } else if (storeListEntities.getStoreType() == 2) {
            new productmanagementServiceApi().execute();
        }
        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_notify = getResources().getIdentifier("notification_icon2x_" + appColor, "drawable", getActivity().getPackageName());
                notification.setImageDrawable(getResources().getDrawable(ic_notify));

                int ic_sidemenu = getResources().getIdentifier("sidemenu3x_" + appColor, "drawable", getContext().getPackageName());
                menu_btn.setImageDrawable(getResources().getDrawable(ic_sidemenu));
            }
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NotificationsActivity.class);
                startActivity(intent);
            }
        });

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        tryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (storeListEntities.getStoreType() == 1) {
                    new productmanagementApi().execute();
                } else if (storeListEntities.getStoreType() == 2) {
                    new productmanagementServiceApi().execute();
                }

            }
        });
        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                instockview.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                outofstockview.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                instocktext.setTextColor(Color.parseColor("#" + Constants.appColor));
            }
        }

        instocklayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isOutStockSelected = false;
                instockview.setVisibility(View.VISIBLE);
                outofstockview.setVisibility(View.GONE);

                if (appColor != null) {
                    if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                        instockview.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                        instocktext.setTextColor(Color.parseColor("#" + Constants.appColor));
                    }
                } else {
                    instockview.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    instocktext.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
                outofstocktext.setTextColor(getResources().getColor(R.color.black));

                if (storeListEntities.getStoreType() == 1) {
                    new productmanagementApi().execute();
                } else if (storeListEntities.getStoreType() == 2) {
                    new productmanagementServiceApi().execute();
                }
            }
        });

        outofstocklayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isOutStockSelected = true;
                outofstockview.setVisibility(View.VISIBLE);
                instockview.setVisibility(View.GONE);
                if (appColor != null) {
                    if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                        outofstockview.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                        outofstocktext.setTextColor(Color.parseColor("#" + Constants.appColor));
                    }
                } else {
                    outofstockview.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    outofstocktext.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
                instocktext.setTextColor(getResources().getColor(R.color.black));

                if (storeListEntities.getStoreType() == 1) {
                    new productmanagementApi().execute();
                } else if (storeListEntities.getStoreType() == 2) {
                    new productmanagementServiceApi().execute();
                }
            }
        });
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        listview.setLayoutManager(linearLayoutManager);

        listview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                lastVisibleItem = linearLayoutManager
                        .findLastVisibleItemPosition();
                Log.i("TAG", "onScrolled: " + lastVisibleItem);

                if (lastvisibleposition < lastVisibleItem && lastVisibleItem == (productMangPaginations.size() - 1)) {

                    if ((page_no * page_size) <= no_of_rows) {
//                        if () {

//                        page_size = page_size + 10;
                        page_no = page_no + 1;
                        if (storeListEntities.getStoreType() == 1) {
                            new productmanagementApi().execute();
                        } else if (storeListEntities.getStoreType() == 2) {
                            new productmanagementServiceApi().execute();
                        }
                        lastvisibleposition = lastVisibleItem;


//                        }
                    }
                }
            }
        });

        return rootView;
    }

    private class productmanagementApi extends AsyncTask<String, String, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareAdveJson();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ProductManagementResponce> call = apiService.getproductmanagement(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductManagementResponce>() {
                @Override
                public void onResponse(Call<ProductManagementResponce> call, Response<ProductManagementResponce> response) {
                    if (response.isSuccessful()) {
                        try {
                            ProductManagementResponce Response = response.body();
                            productArryList = Response.getData();
                            productArryList1 = Response.getData().getProductList();

                            if (productArryList1 != null && productArryList1.size() > 0) {

                                no_of_rows = productArryList.getTotalRecords().get(0).getTotalRecords();

                                for (int i = 0; i < productArryList1.size(); i++) {

                                    ProductMangPagination productMangPagination = new ProductMangPagination();

                                    productMangPagination.setProduct_name_ar(productArryList1.get(i).getProductNameAr());
                                    productMangPagination.setProduct_name_en(productArryList1.get(i).getProductNameEn());
                                    productMangPagination.setVariant(productArryList1.get(i).getVariant());
                                    productMangPagination.setSelling_price(productArryList1.get(i).getSellngPrice());
                                    productMangPagination.setStock_qty(productArryList1.get(i).getStockQuantity());
                                    productMangPagination.setImage(productArryList1.get(i).getMainImage());
                                    productMangPagination.setInventery(productArryList1.get(i).getInventoryId());
                                    if (storeListEntities != null) {
                                        productMangPagination.setStore_type(storeListEntities.getStoreType());
                                    }

                                    productMangPaginations.add(productMangPagination);

                                }

                                Log.i("TAG", "pagination: " + (productMangPaginations.size()));
                                listview.getLayoutManager().scrollToPosition(productMangPaginations.size() - 10);

                                mavableadapter = new ProductAvailableAdapter(getContext(), productMangPaginations, isOutStockSelected);
                                listview.setAdapter(mavableadapter);

                                noDataFoundLayout.setVisibility(View.GONE);
                                layout.setVisibility(View.VISIBLE);
                                nothingFoundImage.setVisibility(View.GONE);

                            } else {

                                noDataFoundLayout.setVisibility(View.VISIBLE);
                                layout.setVisibility(View.GONE);
                                nothingFoundImage.setVisibility(View.GONE);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        noDataFoundLayout.setVisibility(View.VISIBLE);
                        layout.setVisibility(View.GONE);
                        nothingFoundImage.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ProductManagementResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                    noDataFoundLayout.setVisibility(View.GONE);
                    layout.setVisibility(View.GONE);
                    nothingFoundImage.setVisibility(View.VISIBLE);

                }
            });
            return null;
        }
    }

    private String prepareAdveJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("PageNumber", page_no);
            parentObj.put("PageSize", page_size);
            parentObj.put("FromDate", "");
            parentObj.put("ToDate", "");
            parentObj.put("ProductName", "");
            parentObj.put("CategoryId", 0);
            parentObj.put("SubCategoryId", 0);
            parentObj.put("ManuFactureId", 0);
            parentObj.put("BranchId", 0);
            parentObj.put("Publised", 1);
            parentObj.put("SkuId", "");
            parentObj.put("StoreId", userPrefs.getInt("storeId", 0));
            if (isOutStockSelected) {
                parentObj.put("LowStock", 2);
            } else {
                parentObj.put("LowStock", 1);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareAdveJson: " + parentObj);
        return parentObj.toString();
    }

    private class productmanagementServiceApi extends AsyncTask<String, String, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareAdveServiceJson();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ProductMangServiceResponse> call = apiService.getproductmanagservice(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductMangServiceResponse>() {
                @Override
                public void onResponse(Call<ProductMangServiceResponse> call, Response<ProductMangServiceResponse> response) {
                    if (response.isSuccessful()) {
                        try {
                            ProductMangServiceResponse Response = response.body();
                            serviceArryList = Response.getData();
                            serviceArryList1 = Response.getData().getServiceList();

                            if (serviceArryList1 != null && serviceArryList1.size() > 0) {
                                no_of_rows = serviceArryList.getTotalRecords().get(0).getTotalRecords();

                                for (int i = 0; i < serviceArryList1.size(); i++) {

                                    ProductMangPagination productMangPagination = new ProductMangPagination();

                                    productMangPagination.setProduct_name_ar(serviceArryList1.get(i).getServiceNameAr());
                                    productMangPagination.setProduct_name_en(serviceArryList1.get(i).getServiceNameEn());
                                    productMangPagination.setVariant(serviceArryList1.get(i).getMeasureEn());
                                    productMangPagination.setSelling_price(serviceArryList1.get(i).getPrice());
                                    productMangPagination.setStock_qty(serviceArryList1.get(i).getRowNum());
                                    productMangPagination.setImage(serviceArryList1.get(i).getMainImage());
                                    productMangPagination.setInventery(serviceArryList1.get(i).getServiceId());
                                    if (storeListEntities != null) {
                                        productMangPagination.setStore_type(storeListEntities.getStoreType());
                                    }

                                    productMangPaginations.add(productMangPagination);

                                }

                                Log.i("TAG", "pagination: " + (productMangPaginations.size()));
                                listview.getLayoutManager().scrollToPosition(productMangPaginations.size() - 10);

                                mavableadapter = new ProductAvailableAdapter(getContext(), productMangPaginations, isOutStockSelected);
                                listview.setAdapter(mavableadapter);

                                noDataFoundLayout.setVisibility(View.GONE);
                                layout.setVisibility(View.VISIBLE);
                                nothingFoundImage.setVisibility(View.GONE);

                            } else {

                                noDataFoundLayout.setVisibility(View.VISIBLE);
                                layout.setVisibility(View.GONE);
                                nothingFoundImage.setVisibility(View.GONE);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        noDataFoundLayout.setVisibility(View.VISIBLE);
                        layout.setVisibility(View.GONE);
                        nothingFoundImage.setVisibility(View.GONE);

                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ProductMangServiceResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                    noDataFoundLayout.setVisibility(View.GONE);
                    layout.setVisibility(View.GONE);
                    nothingFoundImage.setVisibility(View.VISIBLE);

                }
            });
            return null;
        }
    }

    private String prepareAdveServiceJson() {

        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("PageNumber", page_no);
            parentObj.put("PageSize", page_size);
            parentObj.put("FromDate", "");
            parentObj.put("ToDate", "");
            parentObj.put("ServiceName", "");
            parentObj.put("CategoryId", 0);
            parentObj.put("SubCategoryId", 0);
            parentObj.put("ServiceProviderId", 0);
            parentObj.put("Publised", 1);
            parentObj.put("ServiceType", 0);
            parentObj.put("StoreId", userPrefs.getInt("storeId", 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "prepareAdveJson: " + parentObj);
        return parentObj.toString();
    }
}
