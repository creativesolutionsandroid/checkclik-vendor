package com.cs.checkclickvendor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.cs.checkclickvendor.Activity.MainActivity;
import com.cs.checkclickvendor.Activity.SignInActivity;
import com.cs.checkclickvendor.Activity.StartScreenActivity;
import com.cs.checkclickvendor.Firebase.Config;
import com.cs.checkclickvendor.Models.UserStores;
import com.cs.checkclickvendor.Utils.Utils.BranchColor;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class SplashScreen extends AppCompatActivity {

    BroadcastReceiver mRegistrationBroadcastReceiver;
    public static String regId = "";
    SharedPreferences userPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        FirebaseApp.initializeApp(this);

//        Gson gson1 = new Gson();
//        String json1 = userPrefs.getString("store", null);
//        Type type = new TypeToken<UserStores.StoreListEntity>() {
//        }.getType();
//        storeListEntities = gson1.fromJson(json1, type);
//
//        BranchColor branchColor = new BranchColor();
//        appColor = branchColor.BranchColor(storeListEntities.getPanelColor());
//        Log.i("TAG", "onResponse: " + appColor);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                    regId = pref.getString("regId", null);

                    Log.i("TAG", "Firebase reg id: " + regId);
                }
            }
        };

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TAG", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        regId = task.getResult().getToken();
                        Log.i("TAG", "reg id: " + regId);
                    }
                });

        new Handler().postDelayed(new Runnable() {
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {

                if (userPrefs.getString("userId","").equals("")) {

                    Intent i = new Intent(SplashScreen.this, StartScreenActivity.class);
                    startActivity(i);

                } else {

                    Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(intent);

                }
                finish();
            }
        }, 2000);
    }
}
