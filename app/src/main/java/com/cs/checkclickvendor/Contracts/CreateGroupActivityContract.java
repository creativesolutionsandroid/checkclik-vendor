package com.cs.checkclickvendor.Contracts;

import android.content.Context;

import com.cometchat.pro.models.Group;
import com.cs.checkclickvendor.Base.BasePresenter;
import com.cs.checkclickvendor.Base.BaseView;


public interface CreateGroupActivityContract {

    interface CreateGroupView extends BaseView {

    }

    interface CreateGroupPresenter extends BasePresenter<CreateGroupView> {

        void createGroup(Context context, Group group);

    }
}
