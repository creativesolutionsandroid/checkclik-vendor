package com.cs.checkclickvendor.Contracts;

import android.app.ProgressDialog;
import android.content.Context;

import com.cometchat.pro.models.Group;
import com.cs.checkclickvendor.Adapters.GroupListAdapter;
import com.cs.checkclickvendor.Base.BasePresenter;
import com.cs.checkclickvendor.Base.BaseView;

import java.util.List;

public interface GroupListContract {


    interface GroupView extends BaseView {

        void setGroupAdapter(List<Group> groupList);

        void groupjoinCallback(Group group);

        void setFilterGroup(List<Group> groups);
    }

    interface GroupPresenter extends BasePresenter<GroupView>
    {
        void initGroupView();

        void joinGroup(Context context, Group group, ProgressDialog progressDialog, GroupListAdapter groupListAdapter);

        void refresh();

        void searchGroup(String s);
    }
}
