package com.cs.checkclickvendor.Contracts;

import android.content.Context;

import com.cs.checkclickvendor.Base.BasePresenter;


public interface CallActivityContract {

    interface CallActivityView{

    }

    interface CallActivityPresenter extends BasePresenter<CallActivityView> {

        void removeCallListener(String listener);

        void addCallListener(Context context, String listener);

    }
}
