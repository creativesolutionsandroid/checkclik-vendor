package com.cs.checkclickvendor.Contracts;


import com.cs.checkclickvendor.Base.BasePresenter;

public interface LoginActivityContract {

    interface LoginActivityView {

        void startCometChatActivity();
    }

    interface LoginActivityPresenter extends BasePresenter<LoginActivityView> {

        void Login(String uid);

        void loginCheck();
    }
}
