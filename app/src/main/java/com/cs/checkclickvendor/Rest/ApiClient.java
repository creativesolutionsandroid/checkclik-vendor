package com.cs.checkclickvendor.Rest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static final String BASE_URL = "http://checkclikapi.csadms.com/";
//    public static final String BASE_URL = "http://csadms.com/CheckClickAPI/";
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit==null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                    .connectTimeout(3, TimeUnit.MINUTES)
                    .readTimeout(3, TimeUnit.MINUTES);
            // add your other interceptors …

            httpClient.addInterceptor(new Interceptor() {
                                          @Override
                                          public Response intercept(Interceptor.Chain chain) throws IOException {

                                         /*     //to disable menu toggle button while request is processing
                                              if (context instanceof MainActivity) {
                                                  MainActivity.enableToggle(false);
                                              }*/
                                              Request original = chain.request();

                                              Request request = original.newBuilder()
                                                      .header("api_key", "1233456")
                                                      .method(original.method(), original.body())
                                                      .build();
                                              Response response = chain.proceed(request);

                                            /*  //to enable menu toggle button after processing
                                              if (context instanceof MainActivity) {
                                                  MainActivity.enableToggle(true);
                                              }*/
                                              return response;

                                          }
                                      }
            );

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }
}
