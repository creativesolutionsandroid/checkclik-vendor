package com.cs.checkclickvendor.Rest;


import com.cs.checkclickvendor.Models.AccessResponce;
import com.cs.checkclickvendor.Models.AdavertisementResponce;
import com.cs.checkclickvendor.Models.BasicResponse;
import com.cs.checkclickvendor.Models.BranchList;
import com.cs.checkclickvendor.Models.BranchStatusUpdateResponse;
import com.cs.checkclickvendor.Models.CancelShipment;
import com.cs.checkclickvendor.Models.ChangeEmailResponce;
import com.cs.checkclickvendor.Models.ChangeMobileOtp;
import com.cs.checkclickvendor.Models.ChangePasswordResponce;
import com.cs.checkclickvendor.Models.ChangePhoneResponce;
import com.cs.checkclickvendor.Models.DashBoardList;
import com.cs.checkclickvendor.Models.GetOrderDetails;
import com.cs.checkclickvendor.Models.NewScheduleOrdersList;
import com.cs.checkclickvendor.Models.Notifications;
import com.cs.checkclickvendor.Models.OrderManagmentList;
import com.cs.checkclickvendor.Models.OrderParticallyAccepted;
import com.cs.checkclickvendor.Models.OrderTypeList;
import com.cs.checkclickvendor.Models.ProductManagementResponce;
import com.cs.checkclickvendor.Models.ProductMangServiceResponse;
import com.cs.checkclickvendor.Models.ProductStoreReviews;
import com.cs.checkclickvendor.Models.PushNotificationResponce;
import com.cs.checkclickvendor.Models.RescheduleDateslist;
import com.cs.checkclickvendor.Models.RescheduleOrderList;
import com.cs.checkclickvendor.Models.RescheduleTimeSlotelist;
import com.cs.checkclickvendor.Models.ResetPasswordResponce;
import com.cs.checkclickvendor.Models.RevenuDetailsList;
import com.cs.checkclickvendor.Models.RevenueGraphList;
import com.cs.checkclickvendor.Models.Signinresponce;
import com.cs.checkclickvendor.Models.Signupresponse;
import com.cs.checkclickvendor.Models.SubscriptionPackageResponce;
import com.cs.checkclickvendor.Models.SubscriptionPackageResponce2;
import com.cs.checkclickvendor.Models.TimeTableMang;
import com.cs.checkclickvendor.Models.TransferOrdersList;
import com.cs.checkclickvendor.Models.UpdateAcceptOrderStatus;
import com.cs.checkclickvendor.Models.UpdateOrderReject;
import com.cs.checkclickvendor.Models.UserStores;
import com.cs.checkclickvendor.Models.VerifyEmailResponse;
import com.cs.checkclickvendor.Models.VerifyMobileResponse;
import com.cs.checkclickvendor.Models.VoucherDeleteResponce;
import com.cs.checkclickvendor.Models.VoucherResponce;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {

    @POST("api/VendorAPI/NewAddUpdateVendorDetails")
    Call<Signupresponse> getsignup(@Body RequestBody body);

    @POST("api/MessageOTPAPI/NewAddUpdateMessageOTPDetails")
    Call<VerifyMobileResponse> verfiyMobileNumber(@Body RequestBody body);

    @POST("api/VendorAPI/ValidateEmail")
    Call<VerifyEmailResponse> getEmail(@Body RequestBody body);

    @POST("api/VendorAPI/NewVendorLogin")
    Call<Signinresponce> getsignin(@Body RequestBody body);

    @POST("api/VendorAPI/ResetPassword")
    Call<ResetPasswordResponce> getrestart(@Body RequestBody body);

    @POST("api/VendorAPI/ChangePassword")
    Call<ChangePasswordResponce> getchagepassword(@Body RequestBody body);

    @POST("api/VendorAPI/ValidateEmail")
    Call<ChangeEmailResponce> getchageemail(@Body RequestBody body);

    @POST("api/VendorAPI/GetUpdatePNConfiguration")
    Call<PushNotificationResponce> getpnotifaction(@Body RequestBody body);

    @POST("api/VendorAPI/UpdateMobileNumber")
    Call<ChangePhoneResponce> getphonechange(@Body RequestBody body);

    @POST("api/MessageOTPAPI/NewAddUpdateMessageOTPDetails")
    Call<ChangeMobileOtp> getphoneotp(@Body RequestBody body);

    @POST("api/BranchAPI/GetBranchDetails")
    Call<BranchList> getbranchs(@Body RequestBody body);

    @POST("api/VoucherManagementAPI/GetCoupons")
    Call<VoucherResponce> getvoucher(@Body RequestBody body);

    @POST("api/VoucherManagementAPI/DeActivateCoupon")
    Call<VoucherDeleteResponce> getvoucherdelet(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetProductsList")
    Call<ProductManagementResponce> gerservicemanage(@Body RequestBody body);

    @POST("api/VendorAdvertisementAPI/NewGetVendorAdvertisementDetails")
    Call<AdavertisementResponce> getadavertise(@Body RequestBody body);

    @POST("api/AccessManagementAPI/NewGetAccessManagementList")
    Call<AccessResponce> getaccesslist(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetProductsList")
    Call<ProductManagementResponce> getproductmanagement(@Body RequestBody body);

    @POST("api/ServiceManagementAPI/NewGetServicesList")
    Call<ProductMangServiceResponse> getproductmanagservice(@Body RequestBody body);

    @POST("api/StoreAPI/NewGetStoreDashboard")
    Call<DashBoardList> getdashboard(@Body RequestBody body);

    @POST("api/VendorAPI/NewGetLoginUserDetails")
    Call<UserStores> getUserStores(@Body RequestBody body);

    @POST("api/BranchAPI/GetBranchDetails")
    Call<BranchList> getbranchlist(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetRevenueDashboard")
    Call<RevenueGraphList> getRevenuegraph(@Body RequestBody body);

    @POST("api/VendorOrderManagementAPI/NewReScheduleOrder")
    Call<RescheduleOrderList> getReschedulerOrder(@Body RequestBody body);

    @POST("api/VendorOrderManagementAPI/NewTransferOrder")
    Call<TransferOrdersList> getTransferOrders(@Body RequestBody body);

    @POST("api/VendorOrderManagementAPI/NewScheduleOrder")
    Call<NewScheduleOrdersList> getNewScheduleOrders(@Body RequestBody body);

    @POST("api/VendorOrderManagementAPI/NewOrderPartialAccept")
    Call<OrderParticallyAccepted> getPartiallyAccepted(@Body RequestBody body);

    @POST("api/VendorOrderManagementAPI/NewOrderReject")
    Call<UpdateOrderReject> getupdaterejectorder(@Body RequestBody body);

    @POST("api/VendorOrderManagementAPI/NewOrderAccept")
    Call<UpdateAcceptOrderStatus> getupdateorder(@Body RequestBody body);

    @POST("api/VendorOrderManagementAPI/NewGetOrders")
    Call<OrderTypeList> getorderstatus(@Body RequestBody body);

    @POST("api/VendorOrderManagementAPI/NewGetBranchOrdersCount")
    Call<OrderManagmentList> orderstatus(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetTimeSlotsDayWise")
    Call<RescheduleTimeSlotelist> getRescheduletimeslote(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetBranchDetailsForReschedule")
    Call<RescheduleDateslist> getRescheduledates(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetBranchRevenueDashboardBreakdown")
    Call<RevenuDetailsList> getRevenuedetails(@Body RequestBody body);

    @POST("api/TimeTableAPI/NewGetSelectedBranchDetails")
    Call<TimeTableMang> getTimetablemang(@Body RequestBody body);

    @POST("api/OrdersAPI/NewAddGetUserReviews")
    Call<ProductStoreReviews> getReviews(@Body RequestBody body);

    @POST("api/SubscriptionManagementAPI/GetBranchMemberShipHistoryDetails")
    Call<SubscriptionPackageResponce> getsubpacks(@Body RequestBody body);

    @POST("api/MemberShipPlanAPI/GetMemberShipPlanList")
    Call<SubscriptionPackageResponce2> getsubDetailspacks(@Body RequestBody body);

    @POST("api/VendorOrderManagementAPI/NewGetOrderDetails")
    Call<GetOrderDetails> NewGetOrderDetails(@Body RequestBody body);

    @POST("api/SMSAAPI/AddShipment")
    Call<BasicResponse> AddShipment(@Body RequestBody body);

    @GET("api/SMSAAPI/getStatus")
    Call<CancelShipment> cancelShipment(@Query("AWBNO") String AWBNO);

    @POST("api/BranchAPI/NewAvailabilityStatus")
    Call<BranchStatusUpdateResponse> UpdateAvailabilityStatus(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetNotifications")
    Call<Notifications> getNotifications(@Body RequestBody body);
}
