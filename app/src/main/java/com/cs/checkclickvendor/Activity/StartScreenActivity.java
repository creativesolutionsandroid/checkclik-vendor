package com.cs.checkclickvendor.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.cs.checkclickvendor.R;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class StartScreenActivity extends Activity implements View.OnClickListener {

    private TextView termsText, selectYourRole;
    private LinearLayout bottomLayout;
    private Button signUp, signIn, vendor, access;
    private View closeBottom;
    private Context context;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_activity);
        context = this;

        bottomLayout = (LinearLayout) findViewById(R.id.bottom_layout);

        termsText = (TextView) findViewById(R.id.terms_text);
        selectYourRole = (TextView) findViewById(R.id.select_your_role);

        closeBottom = (View) findViewById(R.id.close_bottom);

        signIn = (Button) findViewById(R.id.btn_sign_in);
        signUp = (Button) findViewById(R.id.btn_sign_up);
        vendor = (Button) findViewById(R.id.vendor);
        access = (Button) findViewById(R.id.access);

//        String desc = "<p><span style=\"color: #D3D3D3;\">By tapping Sign Up, you agree to the </span><span style=\"color: #000000;\"> <a href=\"http://csadms.com/samkrabackend/About/About\">Terms Of Service</a></span></span><span style=\"color: #000000;\"> and </span><span style=\"color: #000000;\"> <a href=\"http://csadms.com/samkrabackend/About/About\">Privacy Policy</a></span></span></p>";
        String desc = "<p><span style=\"color: #D3D3D3;\">By tapping Sign Up, you agree to the </span><span style=\"color: #000000;\"> <a href=\"http://checkclik.csadms.com/home/TermsConditions\">Terms Of Service</a></span></span><span style=\"color: #000000;\"> and </span><span style=\"color: #000000;\"> <a href=\"about:blank\">Privacy Policy</a></span></span></p>";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            termsText.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_COMPACT));
        } else {
            termsText.setText(Html.fromHtml(desc));
        }
        termsText.setMovementMethod(LinkMovementMethod.getInstance());

        signUp.setOnClickListener(this);
        signIn.setOnClickListener(this);
        vendor.setOnClickListener(this);
        access.setOnClickListener(this);
        closeBottom.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_up:
                startActivity(new Intent(StartScreenActivity.this, SignUpActivity.class));
                break;
            case R.id.btn_sign_in:
                Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.bottom_down);
                bottomLayout.setVisibility(View.VISIBLE);
                bottomLayout.startAnimation(slideUp);
                break;
            case R.id.vendor:
                Intent intent = new Intent(StartScreenActivity.this, SignInActivity.class);
                intent.putExtra("userType", 1);
                startActivity(intent);
                break;
            case R.id.access:
                Intent intent1 = new Intent(StartScreenActivity.this, SignInActivity.class);
                intent1.putExtra("userType", 2);
                startActivity(intent1);
                break;
            case R.id.close_bottom:
                bottomLayout.setVisibility(View.GONE);
                break;
        }
    }
}