package com.cs.checkclickvendor.Activity;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.cs.checkclickvendor.Adapters.ProductReviewsAdapter;
import com.cs.checkclickvendor.Contracts.CallActivityContract;
import com.cs.checkclickvendor.Contracts.StringContract;
import com.cs.checkclickvendor.Models.BasicResponse;
import com.cs.checkclickvendor.Models.BranchList;
import com.cs.checkclickvendor.Models.BranchStatusUpdateResponse;
import com.cs.checkclickvendor.Models.ProductStoreReviews;
import com.cs.checkclickvendor.Presenters.CallActivityPresenter;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.Logger;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class ProfileManagementActivity extends AppCompatActivity implements CallActivityContract.CallActivityView {

    private ArrayList<BranchList.Branches> branchesArrayList = new ArrayList<>();
    private int branchPos;
    private CircleImageView branchImageView;
    private TextView branchName, branchAddress, branchStatus, branchStatusAvailable, branchStatusUnAvailable, reviewCount;
    private RatingBar ratingBar;
    private ImageView callIcon, back_btn;
    public List<ProductStoreReviews.Data> reviewsList = new ArrayList<>();
    ProductReviewsAdapter reviewsAdapter;
    private ListView reviewsListView;
    private int flagIDStatus = 0; //  Available : 8, UnAvailability : 9
    CamomileSpinner spinner;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;
    SharedPreferences userPrefs;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_management);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        branchPos = getIntent().getIntExtra("pos", 0);
        branchesArrayList = (ArrayList<BranchList.Branches>) getIntent().getSerializableExtra("array");

        back_btn = (ImageView) findViewById(R.id.back_btn);
        callIcon = (ImageView) findViewById(R.id.store_call);
        branchImageView = (CircleImageView) findViewById(R.id.branchimage);
        branchName = (TextView) findViewById(R.id.branchname);
        branchAddress = (TextView) findViewById(R.id.branch_address);
        branchStatus = (TextView) findViewById(R.id.store_status);
        reviewCount = (TextView) findViewById(R.id.reviews_count);
        branchStatusAvailable = (TextView) findViewById(R.id.status_available);
        branchStatusUnAvailable = (TextView) findViewById(R.id.status_unavailable);
        reviewsListView = (ListView) findViewById(R.id.reviews_list);

        ratingBar = (RatingBar) findViewById(R.id.ratingbar);
        spinner = (CamomileSpinner) findViewById(R.id.spinner);

        branchName.setText(branchesArrayList.get(branchPos).getBranchnameen());
        branchAddress.setText(branchesArrayList.get(branchPos).getAddress());
        ratingBar.setRating(branchesArrayList.get(branchPos).getReview());
        reviewCount.setText("(" + branchesArrayList.get(branchPos).getRatingCount() + " Reviews)");

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_pickup = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_pickup));

                int ic_callIcon = getResources().getIdentifier("ic_call_red_" + appColor, "drawable", getPackageName());
                callIcon.setImageDrawable(getResources().getDrawable(ic_callIcon));
            }
        }

        spinner.start();
        spinner.recreateWithParams(
                getApplicationContext(),
                DialogUtils.getColor(ProfileManagementActivity.this, R.color.black),
                120,
                true
        );

        Glide.with(this)
                .load(Constants.STORE_IMAGE_URL + branchesArrayList.get(branchPos).getBranchlogoimage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        spinner.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        spinner.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(branchImageView);

        branchImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String url = "http://www.example.com/store/1";
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                startActivity(i);

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "" + branchesArrayList.get(branchPos).getId());
                sendIntent.setType("text/plain");
                sendIntent.setComponent(new ComponentName("com.cs.checkclickuser", "com.cs.checkclickuser.Activites.MainActivity"));
                sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                // Verify it resolves
                PackageManager packageManager = getPackageManager();
                List<ResolveInfo> activities = packageManager.queryIntentActivities(sendIntent, 0);
                boolean isIntentSafe = activities.size() > 0;

                // Start an activity if it's safe
                if (isIntentSafe) {
                    startActivity(sendIntent);
                } else {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.checkclickuser")));
                }

//                Intent intent = new Intent(Intent.ACTION_MAIN);
//                intent.setComponent(new ComponentName("com.cs.checkclickuser","com.cs.checkclickuser.Activites.ProductStoresActivityStep1"));
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
            }
        });

        if (branchesArrayList.get(branchPos).getAvailabilitystatus()) {
            branchStatus.setText("Open Now");
            branchStatus.setTextColor(getResources().getColor(R.color.green));
            branchStatusAvailable.setTextColor(getResources().getColor(R.color.green));
            branchStatusAvailable.setBackground(getResources().getDrawable(R.drawable.shape_status_available));
            branchStatusUnAvailable.setTextColor(getResources().getColor(R.color.gray));
            branchStatusUnAvailable.setBackground(getResources().getDrawable(R.drawable.shape_langueage));
            flagIDStatus = 8;
        } else {
            branchStatus.setText("Now Closed");
            branchStatus.setTextColor(getResources().getColor(R.color.red));
            branchStatusUnAvailable.setTextColor(getResources().getColor(R.color.red));
            branchStatusUnAvailable.setBackground(getResources().getDrawable(R.drawable.shape_status_not_available));
            branchStatusAvailable.setTextColor(getResources().getColor(R.color.gray));
            branchStatusAvailable.setBackground(getResources().getDrawable(R.drawable.shape_langueage));
            flagIDStatus = 9;
        }

        new getReviewsApi().execute();

        callIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
//                        String mobile = branchesArrayList.get(branchPos).getTe();
//                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+mobile));
//                        startActivity(intent);
                    }
                } else {
//                    String mobile = carData.get(carPos).getBidlist().get(bidPos).getWks().get(0).getMobileno();
//                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+mobile));
//                    startActivity(intent);
                }
            }
        });

        branchStatusAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flagIDStatus == 9) {
                    flagIDStatus = 8;
                    new updateBranchStatusApi().execute();
                }
            }
        });

        branchStatusUnAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flagIDStatus == 8) {
                    flagIDStatus = 9;
                    new updateBranchStatusApi().execute();
                }
            }
        });
    }


    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(ProfileManagementActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {

                } else {
                    Toast.makeText(ProfileManagementActivity.this, "Call permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private class getReviewsApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetReviewsJSON();
            Constants.showLoadingDialog(ProfileManagementActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ProfileManagementActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            retrofit2.Call<ProductStoreReviews> call = apiService.getReviews(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductStoreReviews>() {
                @Override
                public void onResponse(retrofit2.Call<ProductStoreReviews> call, Response<ProductStoreReviews> response) {
                    if (response.isSuccessful()) {
                        ProductStoreReviews VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                reviewsList = VerifyMobileResponse.getData();
                                reviewsAdapter = new ProductReviewsAdapter(ProfileManagementActivity.this, VerifyMobileResponse.getData());
                                reviewsListView.setAdapter(reviewsAdapter);
                            } else {
                                String failureResponse = VerifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), ProfileManagementActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ProfileManagementActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ProfileManagementActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ProductStoreReviews> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ProfileManagementActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(ProfileManagementActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareGetReviewsJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("BranchId", branchesArrayList.get(branchPos).getId());
            parentObj.put("Type", 1);
            parentObj.put("StatusId", 1);
            parentObj.put("pagesize", 100);
            parentObj.put("pagenumber", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private class updateBranchStatusApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareUpdateStatusJSON();
            Constants.showLoadingDialog(ProfileManagementActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ProfileManagementActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            retrofit2.Call<BranchStatusUpdateResponse> call = apiService.UpdateAvailabilityStatus(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BranchStatusUpdateResponse>() {
                @Override
                public void onResponse(retrofit2.Call<BranchStatusUpdateResponse> call, Response<BranchStatusUpdateResponse> response) {
                    if (response.isSuccessful()) {
                        BranchStatusUpdateResponse VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                if (flagIDStatus == 8) {
                                    branchesArrayList.get(branchPos).setAvailabilitystatus(true);
                                    branchStatus.setText("Open Now");
                                    branchStatus.setTextColor(getResources().getColor(R.color.green));
                                    branchStatusAvailable.setTextColor(getResources().getColor(R.color.green));
                                    branchStatusAvailable.setBackground(getResources().getDrawable(R.drawable.shape_status_available));
                                    branchStatusUnAvailable.setTextColor(getResources().getColor(R.color.gray));
                                    branchStatusUnAvailable.setBackground(getResources().getDrawable(R.drawable.shape_langueage));
                                } else {
                                    branchesArrayList.get(branchPos).setAvailabilitystatus(false);
                                    branchStatus.setText("Now Closed");
                                    branchStatus.setTextColor(getResources().getColor(R.color.red));
                                    branchStatusUnAvailable.setTextColor(getResources().getColor(R.color.red));
                                    branchStatusUnAvailable.setBackground(getResources().getDrawable(R.drawable.shape_status_not_available));
                                    branchStatusAvailable.setTextColor(getResources().getColor(R.color.gray));
                                    branchStatusAvailable.setBackground(getResources().getDrawable(R.drawable.shape_langueage));
                                }

                            } else {
                                String failureResponse = VerifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), ProfileManagementActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ProfileManagementActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ProfileManagementActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BranchStatusUpdateResponse> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ProfileManagementActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(ProfileManagementActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareUpdateStatusJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id", branchesArrayList.get(branchPos).getId());
            parentObj.put("CreatedBy", userPrefs.getString("userId", ""));
            parentObj.put("FlagId", flagIDStatus);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }
}
