package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import com.cs.checkclickvendor.Adapters.TransferListAdapter;
import com.cs.checkclickvendor.Models.EmployeeList;
import com.cs.checkclickvendor.Models.OrderTypeList;
import com.cs.checkclickvendor.R;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TransferOrderActivity extends AppCompatActivity {

    TextView cancel, done;
    EditText search_text;
    RecyclerView employee_list;

    public static int employee_Id = 0;
    public static String employee_name;

    ArrayList<OrderTypeList.Recipients> recipients = new ArrayList<>();
    ArrayList<EmployeeList> transferlist = new ArrayList<>();

    TransferListAdapter mAdapter;
    String search;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transfer_orders);

        recipients = (ArrayList<OrderTypeList.Recipients>) getIntent().getSerializableExtra("transfer_array_list");
        transferlist.clear();

        cancel = findViewById(R.id.cancel);
        search_text = findViewById(R.id.search_text);
        employee_list = findViewById(R.id.employee_list);
        done = findViewById(R.id.done);

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(TransferOrderActivity.this);
        employee_list.setLayoutManager(layoutManager1);

        for (int i = 0; i < recipients.size(); i++) {

            EmployeeList employeeList = new EmployeeList();
            employeeList.setName(recipients.get(i).getName());
            employeeList.setEmployee_id(recipients.get(i).getId());

            transferlist.add(employeeList);
        }

        mAdapter = new TransferListAdapter(TransferOrderActivity.this, transferlist);
        employee_list.setAdapter(mAdapter);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        search_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int j, int i1, int i2) {
//                if (MenuFragment.mAdapter != null) {
//                    MenuFragment.mAdapter.getFilter().filter(charSequence);
//                }

                search = search_text.getText().toString().toLowerCase();

                transferlist.clear();
                for (int i = 0; i < recipients.size(); i++) {

                    if (!search.equals("")) {
                        if (recipients.get(i).getName().toLowerCase().contains(search)) {

                            EmployeeList employeeList1 = new EmployeeList();


                            employeeList1.setName(recipients.get(i).getName());
                            employeeList1.setEmployee_id(recipients.get(i).getId());

                            transferlist.add(employeeList1);


                        }
                    } else {

                        EmployeeList employeeList2 = new EmployeeList();

                        employeeList2.setName(recipients.get(i).getName());
                        employeeList2.setEmployee_id(recipients.get(i).getId());

                        transferlist.add(employeeList2);

                    }


                }

                mAdapter = new TransferListAdapter(TransferOrderActivity.this, transferlist);
                employee_list.setAdapter(mAdapter);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent("TransferServiceCall");
                intent.putExtra("employee_id", employee_Id);
                intent.putExtra("employeename", employee_name);
                LocalBroadcastManager.getInstance(TransferOrderActivity.this).sendBroadcast(intent);
                finish();

            }
        });

    }

}
