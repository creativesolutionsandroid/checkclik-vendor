package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Models.ChangeEmailResponce;
import com.cs.checkclickvendor.Models.ChangeMobileOtp;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class EditProfileActivity extends AppCompatActivity {

    TextView tapbarname, arabic, english, done;
    ImageView back_btn, notification;
    LinearLayout changename, changeemail, changephone, terms, changelanguage;
    private static final String TAG = "TAG";
    Button changedone, emaildone, changephonedone;
    EditText firstnameed, lastnameed, email, phone;
    String strFirstname, strLastname, stremail, strphone;
    private TextInputLayout inputLayoutfirstname, inputLayoutlastname, inputLayoutemail;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences userPrefs;
    String userId, fistname, lastname;
    ProgressBar progressBar;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profileactivity);

        tapbarname = (TextView) findViewById(R.id.tapbarname);
        changeemail = (LinearLayout) findViewById(R.id.changeemaillayout);
        changephone = (LinearLayout) findViewById(R.id.changephonellayout);
        email = (EditText) findViewById(R.id.signin_input_email);
        phone = (EditText) findViewById(R.id.signin_input_phone);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        notification = (ImageView) findViewById(R.id.notification);
        emaildone = (Button) findViewById(R.id.emaildone);
        changephonedone = (Button) findViewById(R.id.phonedone);
        arabic = (Button) findViewById(R.id.Arabic);
        english = (Button) findViewById(R.id.English);
        done = (Button) findViewById(R.id.done);
        changelanguage = (LinearLayout) findViewById(R.id.changelanguagelayout);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        fistname = userPrefs.getString("name", null);
        lastname = userPrefs.getString("lastname", null);

        inputLayoutemail = (TextInputLayout) findViewById(R.id.layout_email_input);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {

                int ic_notify = getResources().getIdentifier("notification_icon2x_" + appColor, "drawable", getPackageName());
                notification.setImageDrawable(getResources().getDrawable(ic_notify));

                int ic_color = getResources().getIdentifier("C" + appColor, "color", getPackageName());
                Drawable background = emaildone.getBackground();

                GradientDrawable shapeDrawable = (GradientDrawable) background;
                shapeDrawable.setColor(ContextCompat.getColor(EditProfileActivity.this, ic_color));

                Drawable background1 = changephonedone.getBackground();

                GradientDrawable shapeDrawable1 = (GradientDrawable) background1;
                shapeDrawable1.setColor(ContextCompat.getColor(EditProfileActivity.this, ic_color));

                Drawable background2 = done.getBackground();

                GradientDrawable shapeDrawable2 = (GradientDrawable) background2;
                shapeDrawable2.setColor(ContextCompat.getColor(EditProfileActivity.this, ic_color));
            }
        }

        arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arabic.setTextColor(getResources().getColor(R.color.colorPrimary));
                english.setTextColor(getResources().getColor(R.color.black));
            }
        });

        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                english.setTextColor(getResources().getColor(R.color.colorPrimary));
                arabic.setTextColor(getResources().getColor(R.color.black));
            }
        });

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_back_btn = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_back_btn));
            }
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent intent = getIntent();
        if (intent.getStringExtra("changename").equals("Email")) {
            tapbarname.setText("Change Email");
            changeemail.setVisibility(View.VISIBLE);
        }

        try {
            if (intent.getStringExtra("changename").equals("Mobile")) {
                tapbarname.setText("Change Phone Number");
                changephone.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (intent.getStringExtra("changename").equals("Terms")) {
            tapbarname.setText("Terms And Conditions");

            String registerTerms = "http://checkclik.csadms.com/home/TermsConditions";
            progressBar = (ProgressBar) findViewById(R.id.progressBar1);
            android.webkit.WebView wv = (android.webkit.WebView) findViewById(R.id.web_view);
            progressBar.setVisibility(View.VISIBLE);
            wv.setVisibility(View.VISIBLE);
            wv.setWebViewClient(new MyWebViewClient());
            wv.getSettings().setLoadsImagesAutomatically(true);
            wv.getSettings().setJavaScriptEnabled(true);
            wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

            wv.loadUrl(registerTerms);


        }
        if (intent.getStringExtra("changename").equals("lang")) {
            tapbarname.setText("Change Language");
            changelanguage.setVisibility(View.VISIBLE);

        }

        changephonedone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phoneValidation()) {
                    new ChangephoneResponse().execute();
                }
            }
        });

        emaildone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emailValidation()) {
                    new ChangeemailApi().execute();
                }
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(android.webkit.WebView view, String url) {
            view.loadUrl(url);
            return true;
        }


        @Override
        public void onPageFinished(android.webkit.WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(android.webkit.WebView view, String url, Bitmap favicon) {
            progressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }

    private boolean emailValidation() {
        stremail = email.getText().toString();
        if (stremail.length() == 0) {
            inputLayoutemail.setError(getResources().getString(R.string.signup_msg_enter_email));
            Constants.requestEditTextFocus(email, EditProfileActivity.this);
            return false;
        }
        return true;
    }

    private boolean phoneValidation() {
        strphone = phone.getText().toString();
        if (strphone.length() == 0) {
            inputLayoutemail.setError(getResources().getString(R.string.signup_msg_enter_phone));
            Constants.requestEditTextFocus(email, EditProfileActivity.this);
            return false;
        }
        return true;
    }

    private boolean nameValidations() {
        strFirstname = firstnameed.getText().toString();
        strLastname = lastnameed.getText().toString();

        if (strFirstname.length() == 0) {
            inputLayoutfirstname.setError(getResources().getString(R.string.signup_msg_enter_firstname));
            Constants.requestEditTextFocus(firstnameed, EditProfileActivity.this);
            return false;
        }
        if (strLastname.length() == 0) {
            inputLayoutlastname.setError(getResources().getString(R.string.signup_msg_enter_lastname));
            Constants.requestEditTextFocus(lastnameed, EditProfileActivity.this);
            return false;
        }

        return true;
    }


    private class ChangeemailApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparenameJson();
            Constants.showLoadingDialog(EditProfileActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangeEmailResponce> call = apiService.getchageemail(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangeEmailResponce>() {
                @Override
                public void onResponse(Call<ChangeEmailResponce> call, Response<ChangeEmailResponce> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        ChangeEmailResponce resetPasswordResponse = response.body();
                        try {
                            if (resetPasswordResponse.getStatus()) {

                                finish();
                                Toast.makeText(EditProfileActivity.this, R.string.reset_success_msg_email, Toast.LENGTH_SHORT).show();
                            } else {

                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), EditProfileActivity.this);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        Constants.closeLoadingDialog();
                    }
                }

                @Override
                public void onFailure(Call<ChangeEmailResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(EditProfileActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        Toast.makeText(EditProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                    }
                    Constants.showLoadingDialog(EditProfileActivity.this);

                }
            });
            return null;
        }
    }

    private String preparenameJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("EmailId", stremail);
            parentObj.put("Id", userId);
            parentObj.put("FlagId", 2);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "preparechangeemail: " + parentObj);
        return parentObj.toString();
    }

    private class ChangephoneResponse extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            Constants.showLoadingDialog(EditProfileActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangeMobileOtp> call = apiService.getphoneotp(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangeMobileOtp>() {
                @Override
                public void onResponse(Call<ChangeMobileOtp> call, Response<ChangeMobileOtp> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        ChangeMobileOtp resetPasswordResponse = response.body();
                        try {
                            if (resetPasswordResponse.getStatus()) {
                                Log.d(TAG, "OTP: " + resetPasswordResponse.getData().getOTPCode());
                                showVerifyDialog(resetPasswordResponse.getData().getOTPCode());
                            } else {
                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), EditProfileActivity.this);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                        }
                        Constants.closeLoadingDialog();
                    }
                }

                @Override
                public void onFailure(Call<ChangeMobileOtp> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(EditProfileActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(EditProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

            });
            return null;
        }
    }

    private String prepareResetPasswordJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("MobileNo", "966" + strphone);
            parentObj.put("FlagId", 3);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareResetPasswordJson: " + parentObj.toString());
        return parentObj.toString();
    }

    private void showVerifyDialog(String otp) {
        Intent intent = new Intent(EditProfileActivity.this, VerifyOTPActivity.class);
        intent.putExtra("screen", "changemobile");
        intent.putExtra("MobileNo2", "966" + strphone);
        intent.putExtra("OTP2", otp);
        startActivity(intent);
    }

}
