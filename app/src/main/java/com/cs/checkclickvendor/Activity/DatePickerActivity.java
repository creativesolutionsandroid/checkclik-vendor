package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class DatePickerActivity extends AppCompatActivity {

    CalendarView calendar;
    TextView start_date, start_date_txt, end_date, end_date_txt, cancel, done;
    LinearLayout start_date_layout, end_date_layout;
    boolean startboolean = true, endboolean = false;
    String startdate, enddate;
    ImageView back_btn;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
//        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
//        language = LanguagePrefs.getString("language", "En");
////        if (language.equalsIgnoreCase("En")) {
        setContentView(R.layout.date_time_picker);
//        } else {
//            setContentView(R.layout.order_details_arabic);
//        }

        back_btn = findViewById(R.id.back_btn);


        start_date = findViewById(R.id.start_date);
        start_date_txt = findViewById(R.id.start_date_txt);
        end_date = findViewById(R.id.end_date);
        end_date_txt = findViewById(R.id.end_date_txt);

        start_date_layout = findViewById(R.id.start_date_layout);
        end_date_layout = findViewById(R.id.end_date_layout);

        cancel = findViewById(R.id.cancel);
        done = findViewById(R.id.done);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_back_btn = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_back_btn));
            }
        }

        calendar = findViewById(R.id.calendar_view);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        end_date_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (start_date_txt.getText().toString().equalsIgnoreCase("-")) {

                    Constants.showOneButtonAlertDialog("Please select startdate", getResources().getString(R.string.app_name), "Ok", DatePickerActivity.this);

                } else {

                    endboolean = true;
                    startboolean = false;

                    start_date.setTextColor(getResources().getColor(R.color.black));
                    start_date_txt.setTextColor(getResources().getColor(R.color.black));
                    start_date_layout.setBackgroundColor(getResources().getColor(R.color.white));

                    end_date.setTextColor(getResources().getColor(R.color.white));
                    end_date_txt.setTextColor(getResources().getColor(R.color.white));
                    end_date_layout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                }

            }
        });

        start_date_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                endboolean = false;
                startboolean = true;

                start_date.setTextColor(getResources().getColor(R.color.white));
                start_date_txt.setTextColor(getResources().getColor(R.color.white));
                start_date_layout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                end_date.setTextColor(getResources().getColor(R.color.black));
                end_date_txt.setTextColor(getResources().getColor(R.color.black));
                end_date_layout.setBackgroundColor(getResources().getColor(R.color.white));


            }
        });

        if (start_date_txt.getText().toString().equalsIgnoreCase("-") || end_date_txt.getText().toString().equalsIgnoreCase("-")) {

            done.setBackground(getResources().getDrawable(R.drawable.total_order_apply_unclickable_bg));
            done.setClickable(false);
            done.setPadding(30,30,30,30);

        } else {

            done.setClickable(true);
            done.setBackground(getResources().getDrawable(R.drawable.total_order_apply_bg));
            done.setPadding(30,30,30,30);

        }

        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -1);

        Calendar calendar1 = Calendar.getInstance();

        calendar.setMaxDate(calendar1.getTimeInMillis());
        calendar.setMinDate(c.getTimeInMillis());

        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView calendarView, int year, int month, int day) {

                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd,yyyy", Locale.US);
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                String date, date2;
                Date date1 = null;

                date = year + "-" + (month+1) + "-" + day;

                try {
                    date1 = dateFormat1.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                date = dateFormat.format(date1);
                date2 = dateFormat1.format(date1);

                if (startboolean){

                    start_date_txt.setText("" +  date);
                    startdate = date2;


                } else if (endboolean){

                    end_date_txt.setText("" +  date);
                    enddate = date2;

                }

                if (start_date_txt.getText().toString().equalsIgnoreCase("-") || end_date_txt.getText().toString().equalsIgnoreCase("-")) {

                    done.setBackground(getResources().getDrawable(R.drawable.total_order_apply_unclickable_bg));
                    done.setClickable(false);
                    done.setPadding(30,30,30,30);

                } else {

                    done.setClickable(true);
                    done.setBackground(getResources().getDrawable(R.drawable.total_order_apply_bg));
                    done.setPadding(30,30,30,30);

                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setResult(RESULT_CANCELED);
                finish();

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.putExtra("startdate", startdate);
                intent.putExtra("enddate",  enddate);
                setResult(RESULT_OK, intent);
                finish();

            }
        });

    }


}
