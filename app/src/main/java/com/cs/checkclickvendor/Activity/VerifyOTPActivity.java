package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Models.ChangePhoneResponce;
import com.cs.checkclickvendor.Models.VerifyMobileResponse;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class VerifyOTPActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView description, buttonResend;
    private Button btnNext;
    CountDownTimer countDownTimer;
    ImageView back_btn;
    EditText inputotp;
    private String screen = "screen";
    private static String TAG = "TAG";
    private String otpEntered = "", strCofirmPassword, strEmail, strPassword, strnewmobile,strMobile,otp, otp1, changephoneotp, stroldmobile,userId;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences.Editor userPrefEditor;
    public static boolean isResetSuccessful = false;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();

        description = (TextView) findViewById(R.id.verify_desc);
        btnNext = (Button) findViewById(R.id.next_otp);
        buttonResend = (TextView) findViewById(R.id.resend);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        inputotp = (EditText) findViewById(R.id.input_otp);
        otp = getIntent().getStringExtra("OTP");
        otp1 = getIntent().getStringExtra("OTP1");
        changephoneotp = getIntent().getStringExtra("OTP2");
        strnewmobile=getIntent().getStringExtra("MobileNo2");
        userId = userPrefs.getString("userId","");
        stroldmobile = userPrefs.getString("mobile", "");

        screen= getIntent().getStringExtra("screen");

        description.setText(getResources().getString(R.string.verify_desc) + "+966 " +
                getIntent().getStringExtra("phone") + getResources().getString(R.string.verify_desc1));

//        int ic_pickup = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
//        back_btn.setImageDrawable(getResources().getDrawable(ic_pickup));

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VerifyOTPActivity.this, UpdateProfileActivity.class);
                startActivity(intent);
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        buttonResend.setEnabled(false);
        buttonResend.setAlpha(0.5f);

        setTimerForResend();
        btnNext.setOnClickListener(this);
        buttonResend.setOnClickListener(this);

    }

    private void setTimerForResend() {
        countDownTimer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                String timeRemaining = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));
                buttonResend.setText(getResources().getString(R.string.otp_msg_resend) + " in " + timeRemaining);
            }

            public void onFinish() {
                buttonResend.setText(getResources().getString(R.string.otp_msg_resend));
                buttonResend.setEnabled(true);
                buttonResend.setAlpha(1.0f);

            }

        }.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_otp:
                otpEntered = inputotp.getText().toString();
                if (otpEntered.length() != 4) {
                    Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1),
                            getResources().getString(R.string.alert_invalid_otp), getResources().getString(R.string.ok), VerifyOTPActivity.this);
                } else {
                    if (screen.equals("register")) {
                    if (otpEntered.equals(otp)) {
                        Intent intent = new Intent(VerifyOTPActivity.this,UpdateProfileActivity.class);
                        otp1 = getIntent().getStringExtra("OTP");
                        strEmail=getIntent().getStringExtra("EmailId");
                        strPassword=getIntent().getStringExtra("Password");
                        strMobile=getIntent().getStringExtra("MobileNo");
                        strCofirmPassword=getIntent().getStringExtra("CofirmPassword");

                        intent.putExtra("EmailId", strEmail);
                        intent.putExtra("MobileNo", strMobile);
                        intent.putExtra("Password", strPassword);
                        intent.putExtra("OTP", otp1);
                        intent.putExtra("CofirmPassword",strCofirmPassword);
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(VerifyOTPActivity.this, R.string.alert_invalid_otp, Toast.LENGTH_SHORT).show();

                    }
                    }

                    else if (screen.equals("forgot")) {

                        if (otpEntered.equals(otp1)){
                            isResetSuccessful = false;
                            Intent intent=new Intent(VerifyOTPActivity.this, ResetPasswordActivity.class);
                            strMobile=getIntent().getStringExtra("MobileNo1");
                            intent.putExtra("MobileNo1",  strMobile);
                            intent.putExtra("otp1", otp1);
                            startActivity(intent);
                        }
                        else {
                            Toast.makeText(VerifyOTPActivity.this, R.string.alert_invalid_otp, Toast.LENGTH_SHORT).show();
                        }
                    }

                    else if (screen.equals("changemobile")) {

                        if (otpEntered.equals(changephoneotp)){

                           new changephoneApi().execute();
                        }
                        else {
                            Toast.makeText(VerifyOTPActivity.this, R.string.alert_invalid_otp, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;

            case R.id.resend:
                String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOTPActivity.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new verifyMobileApi().execute();
                } else {
                    Toast.makeText(VerifyOTPActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(VerifyOTPActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOTPActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyMobileResponse VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                Log.i(TAG, "otp1: " + VerifyMobileResponse.getData().getOTPCode());
                            } else {
                                String failureResponse = VerifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), VerifyOTPActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(VerifyOTPActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(VerifyOTPActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                    buttonResend.setEnabled(false);
                    buttonResend.setAlpha(0.5f);
                    setTimerForResend();
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(VerifyOTPActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(VerifyOTPActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private class changephoneApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparechangeMobileJson();
            Constants.showLoadingDialog(VerifyOTPActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOTPActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ChangePhoneResponce> call = apiService.getphonechange(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangePhoneResponce>() {
                @Override
                public void onResponse(Call<ChangePhoneResponce> call, Response<ChangePhoneResponce> response) {
                    if (response.isSuccessful()) {
                        ChangePhoneResponce VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                Toast.makeText(VerifyOTPActivity.this, R.string.success, Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "checkphone:");
                               Intent intent= new Intent(VerifyOTPActivity.this,MainActivity.class);
                               intent.putExtra("setting",true);
                               startActivity(intent);

                            } else {
                                String failureResponse = VerifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), VerifyOTPActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(VerifyOTPActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(VerifyOTPActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                    buttonResend.setEnabled(false);
                    buttonResend.setAlpha(0.5f);
                    setTimerForResend();
                }

                @Override
                public void onFailure(Call<ChangePhoneResponce> call, Throwable t) {
                    Log.d(TAG, "onchangephoneFailure: "+t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(VerifyOTPActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(VerifyOTPActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("MobileNo", "966" + strMobile);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private String preparechangeMobileJson() {
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("Id", userId );
            mobileObj.put("MobileNo",  stroldmobile);
            mobileObj.put("NewMobileNo",  strnewmobile);
            mobileObj.put("VerifyOTP",changephoneotp);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG, "preparechangeMobileJson: "+mobileObj);
        }
        return mobileObj.toString();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @Override
    public void onBackPressed() {

    }


}


