package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Models.RescheduleDateslist;
import com.cs.checkclickvendor.Models.RescheduleTimeSlotelist;
import com.cs.checkclickvendor.Models.TimeSlots;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RescheduleOrdersActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    TextView select_date, select_time, done, cancel;
    EditText reason;
    String sreason = "null";
    int order_id, order_type;

    DatePickerDialog datePickerDialog;
    int Year, Month, Day, Hour, Minute;
    Calendar calendar;
    private String selectedDate = "", selectedTime = "";
    int dayOfWeek = 0;
    public static int timingsPos = 0;
    String userid;
    Spinner timingsSpinner;

    ArrayList<RescheduleDateslist.PickupInServiceWeekdays> rescheduleDateslists = new ArrayList<>();
    ArrayList<RescheduleDateslist.DeliveryHomeWeekdays> deliveryHomeWeekdays = new ArrayList<>();

    ArrayList<RescheduleTimeSlotelist.TimeSlotList> timingSlotsList = new ArrayList<>();
    private ArrayList<TimeSlots> timings = new ArrayList<>();
    private ArrayAdapter<TimeSlots> timingsAdapter;

    Date timespinnerdate;
    String timespinnerdate1;

    SharedPreferences userPrefs;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reschedule_orders);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userid = userPrefs.getString("userId", "");

        order_id = getIntent().getIntExtra("order_id", 0);
        order_type = getIntent().getIntExtra("order_type", 0);

        select_date = findViewById(R.id.select_date);
        select_time = findViewById(R.id.select_time);
        done = findViewById(R.id.done);
        cancel = findViewById(R.id.cancel);
        timingsSpinner = findViewById(R.id.timings_spinner);

        reason = findViewById(R.id.reason);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        new ReschduleDateOrderApi().execute();


        if (!sreason.equalsIgnoreCase("null")) {
            sreason = reason.getText().toString();
        }

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (selectedDate.equalsIgnoreCase("")){

                    Constants.showOneButtonAlertDialog("Please select Date", getResources().getString(R.string.app_name), "Ok", RescheduleOrdersActivity.this);

                } else if (timings.size() == 0) {

                    Constants.showOneButtonAlertDialog("Please select Time", getResources().getString(R.string.app_name), "Ok", RescheduleOrdersActivity.this);



                } else {

                    Intent intent = new Intent("RescheduleServiceCall");
                    intent.putExtra("RescheduleDate", selectedDate);
                    intent.putExtra("TimeSlotId", timings.get(timingsPos).getTimeslot_id());
                    intent.putExtra("Message", sreason);
                    LocalBroadcastManager.getInstance(RescheduleOrdersActivity.this).sendBroadcast(intent);
                    finish();

                }

            }
        });

        timingsSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (selectedDate.equalsIgnoreCase("")){

                    showCalendarData();

                } else {

                    new ReschduleTimeSlotApi().execute();
                    setTimingsSpinner(timespinnerdate, timespinnerdate1);

                }

                return false;
            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int Year, int Month, int Day) {

        String date = "", date1;
        Date formttedDate = null;
        if (Day <= 9 && Month < 9) {
            date = "0" + Day + "/0" + (Month + 1) + "/" + Year;
        } else if (Day <= 9) {
            date = "0" + Day + "/" + (Month + 1) + "/" + Year;
        } else if (Month < 9) {
            date = Day + "/0" + (Month + 1) + "/" + Year;
        }

        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
        SimpleDateFormat sdf2 = new SimpleDateFormat("MMM dd,yyyy", Locale.US);

        Log.i("TAG", "onDateSet: " + date);
        Log.i("TAG", "Date: " + date);

        try {
            formttedDate = sdf1.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(formttedDate);
        dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);



        date = sdf2.format(formttedDate);
        Log.i("TAG", "onDateSet: " + date);

        date1 = sdf3.format(formttedDate);
        select_date.setText("" + date);
        selectedDate = date1;
        Log.i("TAG", "onDateSet: " + selectedDate);
        new ReschduleTimeSlotApi().execute();
        setTimingsSpinner(formttedDate, date);

        timespinnerdate = formttedDate;
        timespinnerdate1 = date;

    }

    private void setTimingsSpinner(Date date, final String dateStr) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        timings.clear();
        for (int i = 0; i < timingSlotsList.size(); i++) {
            if (timingSlotsList.get(i).getWeekdayid() == calendar.get(Calendar.DAY_OF_WEEK)) {
                TimeSlots timeSlots = new TimeSlots();
                timeSlots.setTimeslot(timingSlotsList.get(i).getTimeslot());
                timeSlots.setTimeslot_id("" + timingSlotsList.get(i).getSlotid());
                timings.add(timeSlots);
            }
        }

        timingsAdapter = new ArrayAdapter<TimeSlots>(this, R.layout.list_spinner, timings) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setText(timings.get(position).getTimeslot());
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.LEFT);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        timingsSpinner.setAdapter(timingsAdapter);
        timingsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                timingsPos = i;
                select_time.setText(timings.get(i).getTimeslot());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private String prepareTimeSlotJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId", userid);
            parentObj.put("WeekDayId", dayOfWeek);
            parentObj.put("Date", selectedDate);
            parentObj.put("OrderType", order_type);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj);

        return parentObj.toString();
    }

    private class ReschduleTimeSlotApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareTimeSlotJson();
            Constants.showLoadingDialog(RescheduleOrdersActivity.this);
//            showloaderAlertDialog();

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<RescheduleTimeSlotelist> call = apiService.getRescheduletimeslote(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<RescheduleTimeSlotelist>() {
                @Override
                public void onResponse(Call<RescheduleTimeSlotelist> call, Response<RescheduleTimeSlotelist> response) {
                    if (response.isSuccessful()) {
                        RescheduleTimeSlotelist updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessage();
                            Log.i("TAG", "onResponse: " + message);

                            timingSlotsList = updateorderList.getData().getTimeslotlist();


                        } else {
                            String failureResponse = updateorderList.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    "Ok", RescheduleOrdersActivity.this);
                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RescheduleOrdersActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<RescheduleTimeSlotelist> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(RescheduleOrdersActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RescheduleOrdersActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RescheduleOrdersActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }


    private String prepareJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("OrderId", order_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj);

        return parentObj.toString();
    }

    private class ReschduleDateOrderApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
           Constants.showLoadingDialog(RescheduleOrdersActivity.this);
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<RescheduleDateslist> call = apiService.getRescheduledates(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<RescheduleDateslist>() {
                @Override
                public void onResponse(Call<RescheduleDateslist> call, Response<RescheduleDateslist> response) {
                    if (response.isSuccessful()) {
                        RescheduleDateslist updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessage();
                            Log.i("TAG", "onResponse: " + message);

                            rescheduleDateslists = updateorderList.getData().getPickupinserviceweekdays();
                            deliveryHomeWeekdays = updateorderList.getData().getDeliveryhomeweekdays();

                            select_date.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    showCalendarData();

                                }
                            });


                        } else {
//                            String failureResponse = changePasswordResponse.getMessage();
//                            showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
//                                    getResources().getString(R.string.ok), ChangePasswordActivity.this);
                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RescheduleOrdersActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                   Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<RescheduleDateslist> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(RescheduleOrdersActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RescheduleOrdersActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RescheduleOrdersActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private void showCalendarData() {

        datePickerDialog = DatePickerDialog.newInstance(RescheduleOrdersActivity.this, Year, Month, Day);
        datePickerDialog.setThemeDark(false);
        datePickerDialog.showYearPickerFirst(false);
        datePickerDialog.setTitle("Select Date");

        // Setting Min Date to today date
        Calendar min_date_c = Calendar.getInstance();
        datePickerDialog.setMinDate(min_date_c);

        // Setting Max Date to next 2 years
        Calendar max_date_c = Calendar.getInstance();
        max_date_c.add(Calendar.MONTH, 1);
        datePickerDialog.setMaxDate(max_date_c);

        if (order_type == 1) {

            for (int i = 0; i < rescheduleDateslists.size(); i++) {
                Calendar min = Calendar.getInstance();
                Calendar max = Calendar.getInstance();
                max.add(Calendar.MONTH, 1);
                max.add(Calendar.DATE, 1);
                Calendar loopdate = null;

                for (loopdate = min; min.before(max); min.add(Calendar.DATE, 1), loopdate = min) {
                    int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
                    if (dayOfWeek == rescheduleDateslists.get(i).getWeekdayid()) {
                        Calendar[] disabledDays = new Calendar[1];
                        disabledDays[0] = loopdate;
                        datePickerDialog.setDisabledDays(disabledDays);
                    }
                }
            }

        } else if (order_type == 2) {

            for (int i = 0; i < deliveryHomeWeekdays.size(); i++) {
                Calendar min = Calendar.getInstance();
                Calendar max = Calendar.getInstance();
                max.add(Calendar.MONTH, 1);
                max.add(Calendar.DATE, 1);
                Calendar loopdate = null;

                for (loopdate = min; min.before(max); min.add(Calendar.DATE, 1), loopdate = min) {
                    int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
                    if (dayOfWeek == deliveryHomeWeekdays.get(i).getWeekdayid()) {
                        Calendar[] disabledDays = new Calendar[1];
                        disabledDays[0] = loopdate;
                        datePickerDialog.setDisabledDays(disabledDays);
                    }
                }
            }
        }


        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        });

        datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
    }
}
