package com.cs.checkclickvendor.Activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Adapters.OrderDetailsAdapter;
import com.cs.checkclickvendor.Models.BasicResponse;
import com.cs.checkclickvendor.Models.CancelShipment;
import com.cs.checkclickvendor.Models.GetOrderDetails;
import com.cs.checkclickvendor.Models.NewScheduleOrdersList;
import com.cs.checkclickvendor.Models.OrderParticallyAccepted;
import com.cs.checkclickvendor.Models.OrderTypeList;
import com.cs.checkclickvendor.Models.RescheduleOrderList;
import com.cs.checkclickvendor.Models.TransferOrdersList;
import com.cs.checkclickvendor.Models.UpdateAcceptOrderStatus;
import com.cs.checkclickvendor.Models.UpdateOrderReject;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class OrderDetailsActivity extends AppCompatActivity {

    TextView order_details_total_price, service_type, item_qty, delivery_date_time;
    TextView payment_mode, total_amount, discount_amount, delivery_charger, vat, net_amount, day, month, user_name, address_txt, mobile_no;
    String mmobile_no, address, longitude;
    RecyclerView order_details_list;
    public static String mcomment = "", order_id = "", orderstatus = "", customer_name = "", customer_email = "";
    SharedPreferences userPrefs;
    TextView header_txt, msend;
    TextView mready, transfer, mreject, mreschedule;
    LinearLayout mreject_accept;
    OrderDetailsAdapter orderDetailsAdapter;
        ImageView back_btn;
    View view;
    String userId;
    int morder_id;
    int employeeid;
    String rescheduleDate = "", Message = "";
    int TimeSlotId = 0;
    String employee_name;
    public static ArrayList<OrderTypeList.Data> orderDetailsLists = new ArrayList<>();
    ArrayList<OrderTypeList.UserDetails> userDetails = new ArrayList<>();
    ArrayList<OrderTypeList.Recipients> recipients = new ArrayList<>();

    public static String header_title;
    public static int branchid, type, statusid;
    LinearLayout minvoice_layout;
    TextView invoice_no, order_status;
    ImageView address_img, call_img;

    String schedule_comment;
    int schedule_StatusId, track_statusid;

    ArrayList<Integer> rejectitemsId = new ArrayList<>();
    ArrayList<String> rejectcomments = new ArrayList<>();

    SharedPreferences LanguagePrefs;
//    Toolbar toolbar;
    String language;
    int pos;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int PHONE_REQUEST = 3;
    private static final int LOCATION_REQUEST = 4;

    LinearLayout call_layout, address_layout;
    int selectedPosition = 0;
    private String TAG = "TAG";
    private List<GetOrderDetails.Data> orderData = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
//        if (language.equalsIgnoreCase("En")) {
        setContentView(R.layout.order_details);
//        } else {
//            setContentView(R.layout.order_details_arabic);
//        }

        userId = userPrefs.getString("userId", "");
        customer_email = userPrefs.getString("email", "");
//        userId = userPrefs.getString("userId", "0");

        header_txt = findViewById(R.id.header);

        back_btn = findViewById(R.id.back_btn);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_back_btn = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_back_btn));
            }
        }

        header_txt.setText("" + getIntent().getStringExtra("header"));

        branchid = getIntent().getIntExtra("BranchId", 0);
        type = getIntent().getIntExtra("Type", 0);
        statusid = getIntent().getIntExtra("StatusId", 0);
        header_title = getIntent().getStringExtra("header");
        morder_id = getIntent().getIntExtra("orderid", 0);

        order_details_total_price = findViewById(R.id.order_details_total_amount);
        service_type = findViewById(R.id.service_type);
        item_qty = findViewById(R.id.item_qty);
        msend = findViewById(R.id.send);
//        reject = findViewById(R.id.reject);
//        accept = findViewById(R.id.accept);
        delivery_date_time = findViewById(R.id.delivery_date_time);
        payment_mode = findViewById(R.id.payment_option);
        total_amount = findViewById(R.id.total_amount);
        discount_amount = findViewById(R.id.discount_amount);
        delivery_charger = findViewById(R.id.delivery_charger);
        vat = findViewById(R.id.vat);
        net_amount = findViewById(R.id.net_amount);
//        note = findViewById(R.id.note);
        day = findViewById(R.id.day);
        month = findViewById(R.id.month);
        user_name = findViewById(R.id.user_name);
        address_txt = findViewById(R.id.address_txt);
        mobile_no = findViewById(R.id.mobile_number);
        order_details_list = findViewById(R.id.order_details_list);
        view = findViewById(R.id.view1);

        minvoice_layout = findViewById(R.id.invoice_layout);

        invoice_no = findViewById(R.id.invoice_no);
        order_status = findViewById(R.id.order_status);

        mreject_accept = findViewById(R.id.reject_accept);
        mready = findViewById(R.id.ready);
        transfer = findViewById(R.id.transfer);
        mreject = findViewById(R.id.reject);
        mreschedule = findViewById(R.id.reschedule);

        call_layout = findViewById(R.id.call_layout);
        address_layout = findViewById(R.id.address_layout);

        call_img = findViewById(R.id.call_img);
        address_img = findViewById(R.id.address_img);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OrderDetailsActivity.this);
        order_details_list.setLayoutManager(mLayoutManager);

        new OrderdetailsApi().execute();

        minvoice_layout.setVisibility(View.GONE);
        mreject_accept.setVisibility(View.GONE);

        Log.i("TAG", "userid: " + userId);
        Log.i("TAG", "orderid: " + morder_id);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                order_status.setTextColor(Color.parseColor("#" + Constants.appColor));
                address_txt.setTextColor(Color.parseColor("#" + Constants.appColor));

                int ic_call_img = getResources().getIdentifier("call_img_" + appColor, "drawable", getPackageName());
                call_img.setImageDrawable(getResources().getDrawable(ic_call_img));

                int ic_address_img = getResources().getIdentifier("address_img_" + appColor, "drawable", getPackageName());
                address_img.setImageDrawable(getResources().getDrawable(ic_address_img));

            }
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        msend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int rejectedOrders = 0, acceptedOrders = 0, rejectitemids = 0;
                String Comment = null;
                for (int i = 0; i < orderDetailsLists.get(selectedPosition).getItems().size(); i++) {

                    if (orderDetailsLists.get(selectedPosition).getItems().get(i).isReject()) {
                        rejectedOrders = rejectedOrders + 1;

                        rejectitemsId.add(orderDetailsLists.get(selectedPosition).getItems().get(i).getItemid());
                        rejectcomments.add(orderDetailsLists.get(selectedPosition).getItems().get(i).getComment());
                    }
                    else if (orderDetailsLists.get(selectedPosition).getItems().get(i).isAccept()) {
                        acceptedOrders = acceptedOrders + 1;
                    }

                    Log.d("TAG", "isReject: "+rejectedOrders);
                    Log.d("TAG", "isAccept: "+acceptedOrders);
                }

                if (rejectedOrders == 0 && acceptedOrders == 0) {

                    Constants.showOneButtonAlertDialog("please select product accept / reject", getResources().getString(R.string.app_name), "Ok", OrderDetailsActivity.this);

                } else if (rejectedOrders == orderDetailsLists.get(selectedPosition).getItems().size()) {

                    new UpdateRejectOrderApi().execute();

                } else if (acceptedOrders == orderDetailsLists.get(selectedPosition).getItems().size()) {

                    new UpdateOrderApi().execute("add_shipment");

                } else {

                    AlertDialog customDialog = null;
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetailsActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout;
//                                        if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.comment_alert_dialog;
//                                        }else {
//                                            layout = R.layout.comment_alert_dialog_arabic;
//                                        }
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView cancel = (TextView) dialogView.findViewById(R.id.cancel);
                    final EditText comment = (EditText) dialogView.findViewById(R.id.comment);
                    TextView done = (TextView) dialogView.findViewById(R.id.done);


                    customDialog = dialogBuilder.create();
                    customDialog.show();

                    final AlertDialog finalCustomDialog1 = customDialog;
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            finalCustomDialog1.dismiss();

                        }
                    });

                    final AlertDialog finalCustomDialog = customDialog;
                    done.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (comment.getText().toString().equals("")) {

//                                                    if (language.equalsIgnoreCase("En")) {
                                Constants.showOneButtonAlertDialog("Please enter reason", getResources().getString(R.string.app_name),
                                        "Ok", OrderDetailsActivity.this);
//                                                    }else {
//                                                        showOneButtonAlertDialog("الرجاء كتابة السبب", getResources().getString(R.string.app_name_ar),
//                                                                getResources().getString(R.string.ok_ar), OrderDetailsActivity.this);

//                                                    }

                            } else {

                                mcomment = comment.getText().toString();
//                                                    orderstatus = "Cancel";
//                                                    customer_name = orderDetailsLists.get(finalI1).getOrderMain().getUserName();
                                new PartionallyacceptedOrderApi().execute();

                                finalCustomDialog.dismiss();

                            }

                        }
                    });

                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);

                }

            }
        });

        LocalBroadcastManager.getInstance(OrderDetailsActivity.this).registerReceiver(
                mTransferOrder, new IntentFilter("TransferServiceCall"));

        LocalBroadcastManager.getInstance(OrderDetailsActivity.this).registerReceiver(
                mReschudelOrder, new IntentFilter("RescheduleServiceCall"));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OrderDetailsActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
//                    getGPSCoordinates();
                } else {
                    Toast.makeText(OrderDetailsActivity.this, "Location permission denied", Toast.LENGTH_LONG).show();
                }
                break;

            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mmobile_no));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                } else {
                    Toast.makeText(OrderDetailsActivity.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private BroadcastReceiver mTransferOrder = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            employeeid = intent.getIntExtra("employee_id", 0);
            employee_name = intent.getStringExtra("employeename");

            new TransferOrderApi().execute();

        }
    };

    private BroadcastReceiver mReschudelOrder = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            TimeSlotId = intent.getIntExtra("TimeSlotId", 0);
            rescheduleDate = intent.getStringExtra("RescheduleDate");
            Message = intent.getStringExtra("Message");

            new ReschudelOrderApi().execute();

        }
    };


    private String prepareRescheduleOrdersJson() {
        JSONObject parentObj = new JSONObject();

        try {

            parentObj.put("OrderId", morder_id);
            parentObj.put("RescheduleDate", rescheduleDate);
            parentObj.put("UserId", userId);
            parentObj.put("TimeSlotId", TimeSlotId);
            parentObj.put("Message", Message);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj.toString());

        return parentObj.toString();
    }

    private class ReschudelOrderApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareRescheduleOrdersJson();
            Constants.showLoadingDialog(OrderDetailsActivity.this);
//            showloaderAlertDialog();

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<RescheduleOrderList> call = apiService.getReschedulerOrder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<RescheduleOrderList>() {
                @Override
                public void onResponse(Call<RescheduleOrderList> call, Response<RescheduleOrderList> response) {
                    if (response.isSuccessful()) {
                        RescheduleOrderList updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessage();
                            Log.i("TAG", "onResponse: " + message);

                            finish();
                        } else {
                            String failureResponse = updateorderList.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    "Ok", OrderDetailsActivity.this);
                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                   Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<RescheduleOrderList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();

                }
            });
            return null;
        }
    }

    private String prepareTransferordesJson() {
        JSONObject parentObj = new JSONObject();

        try {

            parentObj.put("OrderId", morder_id);
            parentObj.put("TransferType", 1);
            parentObj.put("UserId", userId);
            parentObj.put("TransferId", employeeid);
            parentObj.put("ActionBy", userId);
            parentObj.put("Message", "Transfer Order to " + employee_name);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj.toString());

        return parentObj.toString();
    }

    private class TransferOrderApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareTransferordesJson();
            Constants.showLoadingDialog(OrderDetailsActivity.this);
//            showloaderAlertDialog();

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<TransferOrdersList> call = apiService.getTransferOrders(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<TransferOrdersList>() {
                @Override
                public void onResponse(Call<TransferOrdersList> call, Response<TransferOrdersList> response) {
                    if (response.isSuccessful()) {
                        TransferOrdersList updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessage();
                            Log.i("TAG", "onResponse: " + message);

                            finish();

                        } else {
                            String failureResponse = updateorderList.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    "Ok", OrderDetailsActivity.this);
                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<TransferOrdersList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }


    private String prepareNewSchduleordesJson() {
        JSONObject parentObj = new JSONObject();

        try {

            parentObj.put("UserId", userId);
            parentObj.put("StatusId", schedule_StatusId);
            parentObj.put("OrderId", morder_id);
            parentObj.put("Comments", schedule_comment);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj.toString());

        return parentObj.toString();
    }

    private class NewSchduleOrderApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareNewSchduleordesJson();
            Constants.showLoadingDialog(OrderDetailsActivity.this);
//            showloaderAlertDialog();

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<NewScheduleOrdersList> call = apiService.getNewScheduleOrders(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<NewScheduleOrdersList>() {
                @Override
                public void onResponse(Call<NewScheduleOrdersList> call, Response<NewScheduleOrdersList> response) {
                    if (response.isSuccessful()) {
                        NewScheduleOrdersList updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessage();
                            Log.i("TAG", "onResponse: " + message);

                            finish();

                        } else {
                            String failureResponse = updateorderList.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    "Ok", OrderDetailsActivity.this);
                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                   Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<NewScheduleOrdersList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();

                }
            });
            return null;
        }
    }


    private String preparePartionallyacceptJson() {
        JSONObject parentObj = new JSONObject();
        JSONArray itemArray = new JSONArray();

        try {
            parentObj.put("UserId", userId);
            parentObj.put("OrderId", morder_id);

            Log.i("TAG", "preparePartionallyacceptJson: " + rejectitemsId.size());


            for (int i = 0; i < rejectitemsId.size(); i++) {
                JSONObject itemObj = new JSONObject();
                itemObj.put("OrderItemId", rejectitemsId.get(i));
                itemObj.put("Reason", rejectcomments.get(i));
                itemArray.put(itemObj);
            }

            parentObj.put("Reason", mcomment);
            parentObj.put("jItems", itemArray.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj.toString());

        return parentObj.toString();
    }

    private class PartionallyacceptedOrderApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparePartionallyacceptJson();
            Constants.showLoadingDialog(OrderDetailsActivity.this);
//            showloaderAlertDialog();

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderParticallyAccepted> call = apiService.getPartiallyAccepted(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderParticallyAccepted>() {
                @Override
                public void onResponse(Call<OrderParticallyAccepted> call, Response<OrderParticallyAccepted> response) {
                    if (response.isSuccessful()) {
                        OrderParticallyAccepted updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessage();
                            Log.i("TAG", "onResponse: " + message);

                            finish();

                        } else {
                            String failureResponse = updateorderList.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    "Ok", OrderDetailsActivity.this);
                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderParticallyAccepted> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();

                }
            });
            return null;
        }
    }

    private String prepareRejectJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId", userId);
            parentObj.put("OrderId", morder_id);
            parentObj.put("Message", "Rejected Order");
//            parentObj.put("CustomerEmail", customer_email);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj);

        return parentObj.toString();
    }

    private class UpdateRejectOrderApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareRejectJson();
            Constants.showLoadingDialog(OrderDetailsActivity.this);
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<UpdateOrderReject> call = apiService.getupdaterejectorder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UpdateOrderReject>() {
                @Override
                public void onResponse(Call<UpdateOrderReject> call, Response<UpdateOrderReject> response) {
                    if (response.isSuccessful()) {
                        UpdateOrderReject updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessage();
                            Log.i("TAG", "onResponse: " + message);

                            finish();

                        } else {
                            String failureResponse = updateorderList.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    "Ok", OrderDetailsActivity.this);
                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<UpdateOrderReject> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId", userId);
            parentObj.put("OrderId", morder_id);
            parentObj.put("Message", "Accept Order");
//            parentObj.put("CustomerEmail", customer_email);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj);

        return parentObj.toString();
    }


    private class UpdateOrderApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            Constants.showLoadingDialog(OrderDetailsActivity.this);
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<UpdateAcceptOrderStatus> call = apiService.getupdateorder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UpdateAcceptOrderStatus>() {
                @Override
                public void onResponse(Call<UpdateAcceptOrderStatus> call, Response<UpdateAcceptOrderStatus> response) {
                    if (response.isSuccessful()) {
                        UpdateAcceptOrderStatus updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            if (strings[0].equalsIgnoreCase("add_shipment")) {
                                if (orderDetailsLists.get(selectedPosition).getOrdertype() == 3) {
                                    String message = updateorderList.getMessage();
                                    String orderId = updateorderList.getData().getOrderid();
                                    String invoiceNo = updateorderList.getData().getInvoiceno();
                                    new GetOrder().execute(orderId, invoiceNo);
                                }
                                else {
                                    finish();
                                }
                            }
                            else {
                                finish();
                            }
                        } else {
                            String failureResponse = updateorderList.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    "Ok", OrderDetailsActivity.this);
                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                   Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<UpdateAcceptOrderStatus> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();

                }
            });
            return null;
        }
    }

    private String prepareOrderDetailsJson() {
        JSONObject parentObj = new JSONObject();

        try {

            parentObj.put("BranchId", branchid);
            parentObj.put("Type", type);
            parentObj.put("StatusId", statusid);
            parentObj.put("OrderId", morder_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "orderid: " + morder_id);
        Log.i("TAG", "prepareorderdetailsJson: " + parentObj);

        return parentObj.toString();
    }

    private class OrderdetailsApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareOrderDetailsJson();
            Constants.showLoadingDialog(OrderDetailsActivity.this);
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderTypeList> call = apiService.getorderstatus(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderTypeList>() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onResponse(Call<OrderTypeList> call, Response<OrderTypeList> response) {
                    if (response.isSuccessful()) {
                        OrderTypeList orderdetailsResponse = response.body();
                        if (orderdetailsResponse.getStatus()) {

                            recipients.clear();
                            orderDetailsLists = orderdetailsResponse.getData();

                            for (int i = 0; i < orderdetailsResponse.getData().size(); i++) {

                                recipients = orderdetailsResponse.getData().get(i).getRecipients();

                            }

                            if (type == 1) {

                                minvoice_layout.setVisibility(View.GONE);
                                mreject_accept.setVisibility(View.GONE);
                                msend.setVisibility(View.VISIBLE);

                            } else if (type == 2) {

                                minvoice_layout.setVisibility(View.VISIBLE);
                                mreject_accept.setVisibility(View.VISIBLE);
                                msend.setVisibility(View.GONE);
                                if (orderDetailsLists.get(0).getOrdertype() == 1) {

                                    transfer.setVisibility(View.GONE);
                                    if (orderDetailsLists.get(0).getOrderstatus() == 7) {
                                        mreject.setVisibility(View.GONE);
                                        mready.setText("Completed");
                                    }

                                } else {
                                    for (int i = 0; i < orderDetailsLists.get(0).getTrackingdetails().size(); i++) {
                                        if (orderDetailsLists.get(0).getTrackingdetails().get(i).getOrderstatusid() == 8) {

                                            transfer.setText("Re_transfer");

                                        }
                                    }

                                    if (orderDetailsLists.get(0).getOrderstatus() == 7 || orderDetailsLists.get(0).getOrderstatus() == 8) {

                                        mready.setVisibility(View.GONE);
                                        mreject.setVisibility(View.GONE);

                                    }
                                }

                                for (int i = 0; i < orderDetailsLists.get(0).getTrackingdetails().size(); i++) {
                                    if (orderDetailsLists.get(0).getTrackingdetails().get(i).getOrderstatusid() == 18 || orderDetailsLists.get(0).getTrackingdetails().get(i).getOrderstatusid() == 19 || orderDetailsLists.get(0).getTrackingdetails().get(i).getOrderstatusid() == 20) {

                                        mreschedule.setVisibility(View.GONE);

                                        if (orderDetailsLists.get(0).getOrderstatus() == 18 || orderDetailsLists.get(0).getOrderstatus() == 19 || orderDetailsLists.get(0).getOrderstatus() == 20) {

                                            mready.setVisibility(View.GONE);
                                            mreschedule.setVisibility(View.GONE);
                                            transfer.setVisibility(View.GONE);
                                            mreject.setVisibility(View.GONE);

                                        }

                                    }

                                }


                                if (orderDetailsLists.get(0).getPaymenttype().equalsIgnoreCase("PayLater")) {

                                    mreschedule.setVisibility(View.GONE);

                                }


                            } else if (type == 3) {
                                minvoice_layout.setVisibility(View.VISIBLE);
                                mreject_accept.setVisibility(View.VISIBLE);
                                msend.setVisibility(View.GONE);

                                if (statusid == 1) {

                                    transfer.setVisibility(View.GONE);
                                    mreject.setVisibility(View.GONE);
                                    mreschedule.setVisibility(View.GONE);
                                    mreject_accept.setVisibility(View.VISIBLE);

                                    if (orderDetailsLists.get(0).getOrderstatus() == 9) {

                                        mready.setText("Delivered");

                                    } else {

                                        mready.setText("Pick Up");

                                    }

                                } else if (statusid == 2) {

                                    transfer.setVisibility(View.GONE);
                                    mready.setVisibility(View.GONE);
                                    mreject_accept.setVisibility(View.VISIBLE);

                                    for (int i = 0; i < orderDetailsLists.get(0).getTrackingdetails().size(); i++) {
                                        if (orderDetailsLists.get(0).getTrackingdetails().get(i).getOrderstatusid() == 18 || orderDetailsLists.get(0).getTrackingdetails().get(i).getOrderstatusid() == 19 || orderDetailsLists.get(0).getTrackingdetails().get(i).getOrderstatusid() == 20) {

                                            mreschedule.setVisibility(View.GONE);

                                        }

                                    }


                                } else if (statusid == 3) {

                                    mreject_accept.setVisibility(View.GONE);

                                } else if (statusid == 4) {

                                    mreject_accept.setVisibility(View.VISIBLE);

                                    mreschedule.setVisibility(View.GONE);
                                    mready.setVisibility(View.GONE);

                                    transfer.setText("Accept");

                                    transfer.setBackground(getResources().getDrawable(R.drawable.accept_bg));
                                    transfer.setTextColor(getResources().getColor(R.color.green));

                                    if (orderDetailsLists.get(0).getOrderstatus() == 3) {

                                        mreject_accept.setVisibility(View.GONE);

                                    }

                                } else if (statusid == 5) {

                                    mreject_accept.setVisibility(View.GONE);

                                }

                            }

                            mreject.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    new UpdateRejectOrderApi().execute();

                                }
                            });

                            mready.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    schedule_StatusId = 7;
                                    schedule_comment = "Order Ready";
                                    if (mready.getText().toString().equalsIgnoreCase("Compelete") || mready.getText().toString().equalsIgnoreCase("Delivered")) {
                                        schedule_StatusId = 11;
                                        schedule_comment = "Order Delivered";
                                    } else if (mready.getText().toString().equalsIgnoreCase("Pick Up")) {
                                        schedule_StatusId = 9;
                                        schedule_comment = "Order Picked";
                                    }

                                    new NewSchduleOrderApi().execute();

                                }
                            });

                            transfer.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    if (transfer.getText().toString().equalsIgnoreCase("Accept")) {

                                        new UpdateOrderApi().execute("");

                                    } else {

                                        Intent a = new Intent(OrderDetailsActivity.this, TransferOrderActivity.class);
                                        a.putExtra("transfer_array_list", recipients);
                                        startActivity(a);

                                    }
                                }
                            });

                            mreschedule.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Intent a = new Intent(OrderDetailsActivity.this, RescheduleOrdersActivity.class);
                                    a.putExtra("order_id", morder_id);
                                    a.putExtra("order_type", orderDetailsLists.get(0).getOrdertype());
                                    startActivity(a);

                                }
                            });


                            String varients = null, varients_ar = null;
                            for (int i = 0; i < orderDetailsLists.size(); i++) {

                                for (int j = 0; j < orderDetailsLists.get(i).getItems().size(); j++) {
                                    for (int k = 0; k < orderDetailsLists.get(i).getItems().get(j).getVariants().size(); k++) {

                                        if (k != 0) {

                                            varients = varients + " / " + orderDetailsLists.get(i).getItems().get(j).getVariants().get(k).getOptionen() + " : " + orderDetailsLists.get(i).getItems().get(j).getVariants().get(k).getOptionvalueen();
                                            varients_ar = varients_ar + " / " + orderDetailsLists.get(i).getItems().get(j).getVariants().get(k).getOptionar() + " : " + orderDetailsLists.get(i).getItems().get(j).getVariants().get(k).getOptionvaluear();

                                        } else {

                                            varients = orderDetailsLists.get(i).getItems().get(j).getVariants().get(k).getOptionen() + " : " + orderDetailsLists.get(i).getItems().get(j).getVariants().get(k).getOptionvalueen();
                                            varients_ar = orderDetailsLists.get(i).getItems().get(j).getVariants().get(k).getOptionar() + " : " + orderDetailsLists.get(i).getItems().get(j).getVariants().get(k).getOptionvaluear();

                                        }
                                    }
                                    orderDetailsLists.get(i).getItems().get(j).setVariantsname(varients);
                                    orderDetailsLists.get(i).getItems().get(j).setVariantsname_ar(varients_ar);
                                }

                                selectedPosition = i;
                                orderDetailsAdapter = new OrderDetailsAdapter(OrderDetailsActivity.this, i, language, type, OrderDetailsActivity.this);
                                order_details_list.setAdapter(orderDetailsAdapter);

//                                if (language.equalsIgnoreCase("En")) {
                                order_details_total_price.setText("" + Constants.priceFormat1.format(orderDetailsLists.get(i).getGrandtotal()) + " SAR");
                                if (orderDetailsLists.get(i).getOrdertype() == 1 || orderDetailsLists.get(i).getOrdertype() == 4) {
                                    service_type.setText("Pick Up");
                                } else if (orderDetailsLists.get(i).getOrdertype() == 2 || orderDetailsLists.get(i).getOrdertype() == 5) {
                                    service_type.setText("Delivery");
                                } else if (orderDetailsLists.get(i).getOrdertype() == 3) {
                                    service_type.setText("Shipping");
                                }
//                                } else {
//                                    order_details_total_price.setText("" + Constants.decimalFormat.format(orderDetailsLists.get(i).getOrderMain().getTotalPrice()) + " " + getResources().getString(R.string.price_format_ar));
//                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Take Away/CarryOut")) {
//                                        service_type.setText("طريقة التوصيل");
//                                    } else {
//                                        service_type.setText("" + orderDetailsLists.get(i).getOrderMain().getOrderMode_Ar());
//                                    }
//                                }
                                invoice_no.setText("#" + orderDetailsLists.get(i).getInvoiceno());
                                order_status.setText("" + orderDetailsLists.get(i).getOrderstatusen());

                                item_qty.setText("" + orderDetailsLists.get(i).getItems().size());


                                final int finalI1 = i;
//                                reject.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//
//                                        AlertDialog customDialog = null;
//                                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetailsActivity.this);
//                                        // ...Irrelevant code for customizing the buttons and title
//                                        LayoutInflater inflater = getLayoutInflater();
//                                        int layout;
////                                        if (language.equalsIgnoreCase("En")) {
//                                        layout = R.layout.comment_alert_dialog;
////                                        }else {
////                                            layout = R.layout.comment_alert_dialog_arabic;
////                                        }
//                                        View dialogView = inflater.inflate(layout, null);
//                                        dialogBuilder.setView(dialogView);
//                                        dialogBuilder.setCancelable(false);
//
//                                        TextView cancel = (TextView) dialogView.findViewById(R.id.cancel);
//                                        final EditText comment = (EditText) dialogView.findViewById(R.id.comment);
//                                        TextView done = (TextView) dialogView.findViewById(R.id.done);
//
//
//                                        final AlertDialog finalCustomDialog1 = customDialog;
//                                        cancel.setOnClickListener(new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View v) {
//
//                                                finalCustomDialog1.dismiss();
//
//                                            }
//                                        });
//
//                                        final AlertDialog finalCustomDialog = customDialog;
//                                        done.setOnClickListener(new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View v) {
//
//                                                if (comment.getText().toString().equals("")) {
//
////                                                    if (language.equalsIgnoreCase("En")) {
//                                                    Constants.showOneButtonAlertDialog("Please enter reason", getResources().getString(R.string.app_name),
//                                                            "Ok", OrderDetailsActivity.this);
////                                                    }else {
////                                                        showOneButtonAlertDialog("الرجاء كتابة السبب", getResources().getString(R.string.app_name_ar),
////                                                                getResources().getString(R.string.ok_ar), OrderDetailsActivity.this);
//
////                                                    }
//
//                                                } else {
//
//                                                    order_id = String.valueOf(orderDetailsLists.get(finalI1).getOrderid());
//                                                    mcomment = comment.getText().toString();
////                                                    orderstatus = "Cancel";
////                                                    customer_name = orderDetailsLists.get(finalI1).getOrderMain().getUserName();
//
//                                                    new UpdateOrderApi().execute();
//
//                                                    finalCustomDialog.dismiss();
//
//                                                }
//
//                                            }
//                                        });
//
//                                        customDialog = dialogBuilder.create();
//                                        customDialog.show();
//
//                                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                                        Window window = customDialog.getWindow();
//                                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                                        lp.copyFrom(window.getAttributes());
//                                        //This makes the dialog take up the full width
//                                        Display display = getWindowManager().getDefaultDisplay();
//                                        Point size = new Point();
//                                        display.getSize(size);
//                                        int screenWidth = size.x;
//
//                                        double d = screenWidth * 0.85;
//                                        lp.width = (int) d;
//                                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                                        window.setAttributes(lp);
//
//                                    }
//                                });

//                                if (orderDetailsLists.get(i).getOrderstatus().equals("New")) {
//                                    accept.setVisibility(View.VISIBLE);
//                                    reject.setVisibility(View.VISIBLE);
//                                    if (language.equalsIgnoreCase("En")) {
//                                        accept.setText("Accepted");
//                                    }else {
//                                        accept.setText("تم قبوله");
//                                    }
//                                } else if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("Accepted")) {
//                                    accept.setVisibility(View.VISIBLE);
//                                    reject.setVisibility(View.VISIBLE);
//                                    if (language.equalsIgnoreCase("En")) {
//                                        accept.setText("Ready");
//                                    }else {
//                                        accept.setText("جاهز");
//                                    }
//                                } else if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("Ready")) {
//                                    accept.setVisibility(View.VISIBLE);
//                                    reject.setVisibility(View.VISIBLE);
//                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Dine In")) {
//                                        if (language.equalsIgnoreCase("En")) {
//                                            accept.setText("Served");
//                                        } else {
//                                            accept.setText("يقدم");
//                                        }
//                                    }
//                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Take Away/CarryOut")) {
//                                        if (language.equalsIgnoreCase("En")) {
//                                            accept.setText("Served");
//                                        }else {
//                                            accept.setText("يقدم");
//                                        }
//                                    }
//                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Delivery")) {
//                                        if (language.equalsIgnoreCase("En")) {
//                                            accept.setText("Pickup");
//                                        } else {
//                                            accept.setText("طريقة التوصيل");
//                                        }
//                                    }
//                                } else if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("Close")) {
//                                    accept.setVisibility(View.GONE);
//                                    reject.setVisibility(View.GONE);
//                                    view.setVisibility(View.GONE);
//                                } else if (orderDetailsLists.get(i).getOrderMain().getOrderStatus().equals("Cancel")) {
//                                    accept.setVisibility(View.GONE);
//                                    reject.setVisibility(View.GONE);
//                                    view.setVisibility(View.GONE);
//                                }

                                final int finalI = i;
//                                accept.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//
//                                        mcomment = "";
//                                        order_id = String.valueOf(orderDetailsLists.get(finalI).getOrderid());
////                                        if (orderDetailsLists.get(finalI).getOrderMain().getOrderStatus().equals("New")) {
////                                            orderstatus = "Accepted";
////                                        } else if (orderDetailsLists.get(finalI).getOrderMain().getOrderStatus().equals("Accepted")) {
////                                            orderstatus = "Ready";
////                                        } else if (orderDetailsLists.get(finalI).getOrderMain().getOrderStatus().equals("Ready")) {
////                                            orderstatus = "Delivered";
////                                        }
////                                        customer_name = orderDetailsLists.get(finalI).getOrderMain().getUserName();
//                                        new UpdateOrderApi().execute();
//
//                                    }
//                                });

                                SimpleDateFormat current_date = new SimpleDateFormat("dd MMM yy hh:mm aa", Locale.US);
                                SimpleDateFormat dayFormat = new SimpleDateFormat("dd", Locale.US);
                                SimpleDateFormat mouthFormat = new SimpleDateFormat("MMMM", Locale.US);
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

                                Date delivery_date = null;
                                String delivery_date1;
                                try {
                                    delivery_date = dateFormat.parse(orderDetailsLists.get(i).getOrderdate());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                delivery_date1 = current_date.format(delivery_date);

                                delivery_date_time.setText(delivery_date1);
//                                if (language.equalsIgnoreCase("En")) {

                                if (orderDetailsLists.get(i).getPaymenttype().equalsIgnoreCase("payNow")) {
                                    payment_mode.setText("Online Now");
                                } else if (orderDetailsLists.get(i).getPaymenttype().equalsIgnoreCase("COD")) {
                                    payment_mode.setText("Cash");
                                } else {
                                    payment_mode.setText("" + orderDetailsLists.get(i).getPaymenttype());
                                }

                                total_amount.setText("" + Constants.priceFormat1.format(orderDetailsLists.get(i).getSubtotal()) + " SAR");
                                if (orderDetailsLists.get(i).getDiscount() == 0) {
                                    discount_amount.setText("- " + "0.00" + " SAR");
                                } else {
                                    discount_amount.setText("- " + Constants.priceFormat1.format(orderDetailsLists.get(i).getDiscount()) + " SAR");
                                }
                                if (orderDetailsLists.get(i).getDeliveryfee() == 0) {
                                    delivery_charger.setText("+ " + "0.00" + " SAR");
                                } else {
                                    delivery_charger.setText("+ " + Constants.priceFormat1.format(orderDetailsLists.get(i).getDeliveryfee()) + " SAR");
                                }
                                vat.setText("+ " + Constants.priceFormat1.format(orderDetailsLists.get(i).getVat()) + " SAR");
                                net_amount.setText("" + Constants.priceFormat1.format(orderDetailsLists.get(i).getGrandtotal()) + " SAR");

//                                } else {
//
//                                    payment_mode.setText("" + orderDetailsLists.get(i).getOrderMain().getPaymentMode());
//                                    total_amount.setText("" + Constants.priceFormat1.format(orderDetailsLists.get(i).getOrderMain().getTotalPrice()) + " " + getResources().getString(R.string.price_format_ar));
//                                    if (orderDetailsLists.get(i).getOrderMain().getCouponDiscount() == 0) {
//                                        discount_amount.setText("- " + "0.00" + " " + getResources().getString(R.string.price_format_ar));
//                                    } else {
//                                        discount_amount.setText("- " + Constants.priceFormat1.format(orderDetailsLists.get(i).getOrderMain().getCouponDiscount()) + " " + getResources().getString(R.string.price_format_ar));
//                                    }
//                                    if (orderDetailsLists.get(i).getOrderMain().getDeliveryCharges() == 0) {
//                                        delivery_charger.setText("+ " + "0.00" + " " + getResources().getString(R.string.price_format_ar));
//                                    } else {
//                                        delivery_charger.setText("- " + Constants.priceFormat1.format(orderDetailsLists.get(i).getOrderMain().getDeliveryCharges()) + " " + getResources().getString(R.string.price_format_ar));
//                                    }
//                                    vat.setText("+ " + Constants.priceFormat1.format(orderDetailsLists.get(i).getOrderMain().getVatCharges()) + " " + getResources().getString(R.string.price_format_ar));
//                                    net_amount.setText("" + Constants.priceFormat1.format(orderDetailsLists.get(i).getOrderMain().getTotalPrice()) + " " + getResources().getString(R.string.price_format_ar));
//
//                                }
//                                note.setText("Note:");
                                //order date
                                Date day1 = null;
                                String day2, month2;

                                try {
                                    day1 = dateFormat.parse(orderDetailsLists.get(i).getOrderdate());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                day2 = dayFormat.format(day1);
                                month2 = mouthFormat.format(day1);

                                day.setText(day2);

                                month.setText(month2);
                                userDetails = orderdetailsResponse.getData().get(i).getUserdetails();
                                for (int j = 0; j < orderDetailsLists.get(i).getUserdetails().size(); j++) {
                                    user_name.setText("" + orderDetailsLists.get(i).getUserdetails().get(j).getFirstname() + " " + orderDetailsLists.get(i).getUserdetails().get(j).getLastname());
//                                    if (orderDetailsLists.get(i).getOrderMain().getOrderMode_En().equals("Delivery")) {
                                    address_txt.setText("" + orderDetailsLists.get(i).getUserdetails().get(j).getAddress());
                                    mobile_no.setText("" + orderDetailsLists.get(i).getUserdetails().get(j).getMobileno());
//                                    } else {
//                                        address_txt.setText("" + orderDetailsLists.get(i).getOrderMain().getBranchAddress());
//                                        mobile_no.setText("" + orderDetailsLists.get(i).getOrderMain().getBranchMobileNo());
//                                    }
                                    mmobile_no = orderDetailsLists.get(i).getUserdetails().get(j).getMobileno();
                                    address = orderDetailsLists.get(i).getUserdetails().get(j).getAddress();

                                }

                                address_layout.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        String geoUri = "http://maps.google.com/maps?q=loc:" + address;
                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                                        startActivity(intent);
//                                        Log.i("TAG", "onClick: " + orderList.get(finalI).getBranchName());

                                    }
                                });

                                call_layout.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        int currentapiVersion = Build.VERSION.SDK_INT;
                                        if (currentapiVersion >= Build.VERSION_CODES.M) {
                                            if (!canAccessPhonecalls()) {
                                                requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                                            } else {
                                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mmobile_no));
                                                if (ActivityCompat.checkSelfPermission(OrderDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                                    // TODO: Consider calling
                                                    //    ActivityCompat#requestPermissions
                                                    // here to request the missing permissions, and then overriding
                                                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                                    //                                          int[] grantResults)
                                                    // to handle the case where the user grants the permission. See the documentation
                                                    // for ActivityCompat#requestPermissions for more details.
                                                    return;
                                                }
                                                startActivity(intent);
                                            }
                                        } else {
                                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mmobile_no));
                                            startActivity(intent);
                                        }

                                    }
                                });


                            }

                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                        Log.i("TAG", "onResponse: ");
                    }
                   Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderTypeList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                        Log.i("TAG", "onFailure: " + t);
                    }
                    Constants.closeLoadingDialog();

                }
            });
            return null;
        }
    }

    private class GetOrder extends AsyncTask<String, Integer, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(OrderDetailsActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);
            String inputStr = prepareGetOrderJson(strings[0], strings[1]);

            Call<GetOrderDetails> call = apiService.NewGetOrderDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<GetOrderDetails>() {
                @Override
                public void onResponse(Call<GetOrderDetails> call, Response<GetOrderDetails> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            Constants.closeLoadingDialog();
                            orderData = response.body().getData();
                            if(orderData.get(0).getOrderStatus() != 2) {
                                finish();
                            }
                            else {
                                new UpdateShipment().execute();
                            }
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), OrderDetailsActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetOrderDetails> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetOrderJson(String orderId, String invoiceId){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Type", 1);
            parentObj.put("StatusId", 1);
            parentObj.put("OrderId", orderId);
            parentObj.put("InvoiceNo", invoiceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class UpdateShipment extends AsyncTask<String, Integer, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareUpdateShipmentJson();
            Constants.showLoadingDialog(OrderDetailsActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<BasicResponse> call = apiService.AddShipment(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            Constants.closeLoadingDialog();
                            finish();
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), OrderDetailsActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<BasicResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareUpdateShipmentJson(){
        int qty = 0;
        float weight = 0;
        String itemDescription = "";

        for (int i = 0; i < orderData.get(0).getItems().size(); i++) {
            qty = qty + orderData.get(0).getItems().get(i).getQty();
            weight = weight + (orderData.get(0).getItems().get(i).getWeight() *
                    orderData.get(0).getItems().get(i).getQty());

            String itemVariantsDescription = "";
            for (int j = 0; j < orderData.get(0).getItems().get(i).getVariants().size(); j++) {
                if (j == 0){
                    itemVariantsDescription = orderData.get(0).getItems().get(i).getVariants().get(j).getOptionValueEn();
                }
                else {
                    itemVariantsDescription = itemVariantsDescription + "/" +orderData.get(0).getItems().get(i).getVariants().get(j).getOptionValueEn();
                }
            }
            if (i == 0){
                itemDescription = orderData.get(0).getItems().get(i).getNameEn() + "(" + itemVariantsDescription + "), Qty:" +
                        orderData.get(0).getItems().get(i).getQty();
            }
            else {
                itemDescription = itemDescription + ", " + orderData.get(0).getItems().get(i).getNameEn() + "(" + itemVariantsDescription + "), Qty:" +
                        orderData.get(0).getItems().get(i).getQty();
            }
        }

        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("passKey", "Testing0");
            parentObj.put("refNo", orderData.get(0).getInvoiceNo());
            parentObj.put("sentDate", getOrderDate(orderData.get(0).getOrderDate()));
            parentObj.put("idNo", orderData.get(0).getInvoiceNo());
            parentObj.put("cName", orderData.get(0).getUserDetails().get(0).getFirstName() + " " +
                    orderData.get(0).getUserDetails().get(0).getLastName());
            parentObj.put("Cntry", orderData.get(0).getUserDetails().get(0).getCountryName());
            parentObj.put("cCity", orderData.get(0).getUserDetails().get(0).getCityName());
            parentObj.put("cZip", orderData.get(0).getUserDetails().get(0).getZipCode());
            parentObj.put("cPOBox", "");
            parentObj.put("cMobile", orderData.get(0).getUserDetails().get(0).getMobileNo());
            parentObj.put("cTel1", orderData.get(0).getUserDetails().get(0).getMobile2());
            parentObj.put("cTel2", "");
            parentObj.put("cAddr1", orderData.get(0).getUserDetails().get(0).getAddress1());
            parentObj.put("cAddr2", orderData.get(0).getUserDetails().get(0).getAddress2());
            parentObj.put("shipType", "DLV");
            parentObj.put("PCs", qty);
            parentObj.put("cEmail", orderData.get(0).getUserDetails().get(0).getEmailId());
            parentObj.put("codAmt", orderData.get(0).getGrandTotal());
            parentObj.put("weight", weight);
            parentObj.put("itemDesc", itemDescription);
            parentObj.put("prefDelvDate", getOrderDate(orderData.get(0).getExpectingDelivery()));
            parentObj.put("carrValue", "");
            parentObj.put("carrCurr", "");
            parentObj.put("custCurr", "");
            parentObj.put("custVal", "");
            parentObj.put("insrCurr", "");
            parentObj.put("insrAmt", "");
            parentObj.put("sName", "");
            parentObj.put("sContact", "");
            parentObj.put("sAddr1", "");
            parentObj.put("sAddr2", "");
            parentObj.put("sCity", "");
            parentObj.put("sPhone", "");
            parentObj.put("sCntry", "");
            parentObj.put("gpsPoints", "");
            parentObj.put("awbNo", "");
            parentObj.put("reason", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private String getOrderDate(String date){
        String orderDate = null;

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.US);

        try {
            Date formattedDate = sdf1.parse(date);
            orderDate = sdf2.format(formattedDate);
            Log.d(TAG, "getOrderDate: "+orderDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return orderDate;
    }

    private class CancelShipmentApi extends AsyncTask<String, Integer, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(OrderDetailsActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<CancelShipment> call = apiService.cancelShipment(orderDetailsLists.get(0).getAwbNo());
            call.enqueue(new Callback<CancelShipment>() {
                @Override
                public void onResponse(Call<CancelShipment> call, Response<CancelShipment> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            Constants.closeLoadingDialog();
                           finish();
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server), getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), OrderDetailsActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<CancelShipment> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
}
