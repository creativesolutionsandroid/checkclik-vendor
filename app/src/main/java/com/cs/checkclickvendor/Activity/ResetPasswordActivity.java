package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cs.checkclickvendor.Models.ResetPasswordResponce;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private int currentStage = 1;
    TextInputLayout inputLayoutphone, inputLayoutConfirmPassword, inputLayoutnewPassword;
    EditText inputPassword, inputConfirmPassword;
    String strPassword, strConfirmPassword, otp, strMobile;
    Button buttonSend;
    private static String TAG = "TAG";
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_reset_password);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();


        inputPassword = (EditText) findViewById(R.id.reset_input_password);
        inputConfirmPassword = (EditText) findViewById(R.id.reset_input_retype_password);
        inputConfirmPassword.addTextChangedListener(new TextWatcher(inputConfirmPassword));
        buttonSend = (Button) findViewById(R.id.reset_submit_button);

        strMobile = getIntent().getStringExtra("MobileNo1");
        otp = getIntent().getStringExtra("otp1");

        buttonSend.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                if (currentStage == 1) {
                    finish();
                }

            case R.id.reset_submit_button:
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ResetPasswordActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new ResetPasswordApi().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private boolean validations() {
        strPassword = inputPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();

        if (strPassword.length() == 0) {

            inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            return false;
        } else if (strPassword.length() < 4 || strPassword.length() > 20) {
            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            return false;
        } else if (strConfirmPassword.length() == 0) {
            inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            return false;
        } else if (strConfirmPassword.length() < 4 || strConfirmPassword.length() > 20) {
            inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            return false;
        } else if (!strPassword.equals(strConfirmPassword)) {
            inputConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match));
            return false;
        }
        return true;
    }

    private void clearErrors() {
        inputLayoutphone.setErrorEnabled(false);
        inputLayoutphone.setErrorEnabled(false);
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_password:
                    clearErrors();
                    if (editable.length() > 20) {
                        inputLayoutnewPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
                case R.id.input_confirm_password:
                    clearErrors();
                    if (editable.length() > 20) {
                        inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
            }
        }
    }

    private String prepareResetPasswordJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Password", strPassword);
            parentObj.put("VerifyOTP", otp);
            if (strMobile.contains("966")) {
                parentObj.put("MobileNo", strMobile.replace(" ", ""));
                parentObj.put("FlagId", 1);
            } else {
                parentObj.put("MobileNo", strMobile);
                parentObj.put("FlagId", 2);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "prepareResetPasswordJson: " + parentObj.toString());
        return parentObj.toString();
    }

    private class ResetPasswordApi extends AsyncTask<String, Integer, String> {


        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            Constants.showLoadingDialog(ResetPasswordActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ResetPasswordResponce> call = apiService.getrestart(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ResetPasswordResponce>() {
                @Override
                public void onResponse(Call<ResetPasswordResponce> call, Response<ResetPasswordResponce> response) {
                    Log.d(TAG, "resetonResponse: " + response.message());
                    if (response.isSuccessful()) {
                        ResetPasswordResponce resetPasswordResponse = response.body();
                        try {
                            if (resetPasswordResponse.getStatus()) {
//                                status true case
                                String userId = resetPasswordResponse.getData().getUserId();
                                userPrefsEditor.putString("userId", userId);
                                if (resetPasswordResponse.getData().getMobileNo().contains("966")) {
                                    userPrefsEditor.putString("MobileNo", resetPasswordResponse.getData().getMobileNo());
                                } else {
                                    userPrefsEditor.putString("EmailId", resetPasswordResponse.getData().getMobileNo());
                                }
                                userPrefsEditor.commit();
                                Toast.makeText(ResetPasswordActivity.this, R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
                                ForgotPasswordActivity.isResetSuccessful = true;
                                Intent intent = new Intent(ResetPasswordActivity.this, MainActivity.class);
                                startActivity(intent);
                                Constants.closeLoadingDialog();
                            } else {
//                                status false case
                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), ResetPasswordActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ResetPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ResetPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ResetPasswordResponce> call, Throwable t) {
                    Log.d(TAG, "resetonFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ResetPasswordActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ResetPasswordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ResetPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}

