package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.cs.checkclickvendor.Models.ChangePasswordResponce;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class ChangePasswordActivity extends AppCompatActivity {

    private TextInputLayout inputLayoutcurrentpassword, inputLayoutnewpassword, inputLayoutconfirampassword;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences userPrefs;
    String response, userId, strPhone, strPassword, strNewpassword;
    Button submit;
    String TAG;
    ImageView back_btn;
    EditText oldPassword, newPassword, confirmPassword;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();

        userId = userPrefs.getString("userId", null);
        strPhone = userPrefs.getString("mobile", null);

        oldPassword = (EditText) findViewById(R.id.currentpassword);
        newPassword = (EditText) findViewById(R.id.newpassword);
        confirmPassword = (EditText) findViewById(R.id.confirampassword);
        submit = (Button) findViewById(R.id.submit);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_close = getResources().getIdentifier("close2x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_close));

                int ic_color = getResources().getIdentifier("C" + appColor, "color", getPackageName());
                Drawable background = submit.getBackground();

                GradientDrawable shapeDrawable = (GradientDrawable) background;
                shapeDrawable.setColor(ContextCompat.getColor(ChangePasswordActivity.this, ic_color));
            }
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String oldPwd = oldPassword.getText().toString();
                String newPwd = newPassword.getText().toString();
                String confirmPwd = confirmPassword.getText().toString();
                strPassword = oldPwd;
                strNewpassword = newPwd;
                if (oldPwd.length() == 0) {
                    oldPassword.setError("Please enter old password");
                } else if (newPwd.length() == 0) {
                    newPassword.setError("Please enter new password");

                } else if (confirmPwd.length() == 0) {
                    confirmPassword.setError("Please retype password");
                } else if (!newPwd.equals(confirmPwd)) {
                    confirmPassword.setError("Passwords not match, please retype");
                } else {
                    new ChangePasswordResponse().execute(userId, oldPwd, newPwd);
                }
            }
        });
    }

    private class ChangePasswordResponse extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            Constants.showLoadingDialog(ChangePasswordActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangePasswordResponce> call = apiService.getchagepassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangePasswordResponce>() {
                @Override
                public void onResponse(Call<ChangePasswordResponce> call, Response<ChangePasswordResponce> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        ChangePasswordResponce resetPasswordResponse = response.body();
                        try {
                            if (resetPasswordResponse.getStatus()) {
//                                status true case
                                String userId = resetPasswordResponse.getData().getUserId();
                                userPrefEditor.putString("userId", userId);
                                userPrefEditor.commit();
                                Log.d(TAG, "changeuserid: " + userId);
                                finish();
                                Toast.makeText(ChangePasswordActivity.this, R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
                            } else {

                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), ChangePasswordActivity.this);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                        }
                        Constants.closeLoadingDialog();
                    }
                }

                @Override
                public void onFailure(Call<ChangePasswordResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ChangePasswordActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ChangePasswordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareResetPasswordJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id", userId);
            parentObj.put("Password", strPassword);
            parentObj.put("CofirmPassword", strNewpassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareResetPasswordJson: " + parentObj.toString());
        return parentObj.toString();
    }

}
