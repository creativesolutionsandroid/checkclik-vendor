package com.cs.checkclickvendor.Activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.cs.checkclickvendor.Adapters.PendingListAdapter;
import com.cs.checkclickvendor.Models.OrderTypeList;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class OrderTypeActivity extends AppCompatActivity {

    LinearLayout pending;
    LinearLayout scheduled, order;
    TextView pending_txt;
    TextView scheduled_txt, order_txt;
    View pending_view;
    View scheduled_view, order_view;
    String language, userId = "1";
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    boolean pending_boolean = false;
    boolean scheduled_boolean = false, order_boolean = false;

    ImageView back_btn;

    ArrayList<OrderTypeList.Data> orderLists = new ArrayList<>();
    ArrayList<OrderTypeList.Items> itemsLists = new ArrayList<>();

    ArrayList<OrderTypeList.Data> dine_inorderLists = new ArrayList<>();
    ArrayList<OrderTypeList.Data> pickuporderLists = new ArrayList<>();
    ArrayList<OrderTypeList.Data> deliveryorderLists = new ArrayList<>();

    String[] images;
    public static String header_title;

    RecyclerView mpending_list;
    PendingListAdapter pendingListAdapter;
    public static int branchid, type, statusid;

    TextView header_txt;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_types);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "1");

        header_txt = findViewById(R.id.header);

        back_btn = findViewById(R.id.back_btn);

        header_title = getIntent().getStringExtra("header");
        header_txt.setText("" + getIntent().getStringExtra("header"));
        branchid = getIntent().getIntExtra("BranchId", 0);
        type = getIntent().getIntExtra("Type", 0);
        statusid = getIntent().getIntExtra("StatusId", 0);

        pending = findViewById(R.id.pending);
        scheduled = findViewById(R.id.schedule);
        order = findViewById(R.id.order);

        pending_txt = findViewById(R.id.pending_txt);
        scheduled_txt = findViewById(R.id.schedule_txt);
        order_txt = findViewById(R.id.order_txt);

        pending_view = findViewById(R.id.pending_view);
        scheduled_view = findViewById(R.id.schedule_view);
        order_view = findViewById(R.id.order_view);

        mpending_list = findViewById(R.id.pendinglist);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_back_btn = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_back_btn));
            }
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                pending_txt.setTextColor(Color.parseColor("#" + Constants.appColor));
                pending_view.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                scheduled_view.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                order_view.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
            }
        }

        pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pending_boolean = true;
                scheduled_boolean = false;
                order_boolean = false;
                if (appColor != null) {
                    if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                        pending_txt.setTextColor(Color.parseColor("#" + Constants.appColor));
                    }
                } else {
                    pending_txt.setTextColor(getResources().getColor(R.color.colorPrimary));

                }
                scheduled_txt.setTextColor(getResources().getColor(R.color.black));
                order_txt.setTextColor(getResources().getColor(R.color.black));

                pending_view.setVisibility(View.VISIBLE);
                scheduled_view.setVisibility(View.GONE);
                order_view.setVisibility(View.GONE);

                new Orderstatusapi().execute();

            }
        });

        scheduled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pending_boolean = false;
                scheduled_boolean = true;
                order_boolean = false;

                pending_txt.setTextColor(getResources().getColor(R.color.black));
                if (appColor != null) {
                    if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                        scheduled_txt.setTextColor(Color.parseColor("#" + Constants.appColor));
                    }
                } else {

                    scheduled_txt.setTextColor(getResources().getColor(R.color.colorPrimary));
                }

                order_txt.setTextColor(getResources().getColor(R.color.black));
                if (appColor != null) {
                    if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                        scheduled_view.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                    }
                }
                pending_view.setVisibility(View.GONE);
                scheduled_view.setVisibility(View.VISIBLE);
                order_view.setVisibility(View.GONE);

                new Orderstatusapi().execute();

            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pending_boolean = false;
                scheduled_boolean = false;
                order_boolean = true;

                pending_txt.setTextColor(getResources().getColor(R.color.black));
                scheduled_txt.setTextColor(getResources().getColor(R.color.black));
                if (appColor != null) {
                    if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                        order_txt.setTextColor(Color.parseColor("#" + Constants.appColor));
                    }
                } else {
                    order_txt.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
                if (appColor != null) {
                    if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                        order_view.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                    }
                }
                pending_view.setVisibility(View.GONE);
                scheduled_view.setVisibility(View.GONE);
                order_view.setVisibility(View.VISIBLE);

                new Orderstatusapi().execute();

            }
        });


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OrderTypeActivity.this);
        mpending_list.setLayoutManager(mLayoutManager);

//        new Orderstatusapi().execute();

        LocalBroadcastManager.getInstance(OrderTypeActivity.this).registerReceiver(
                mupdate_order, new IntentFilter("UpdateOrder"));
    }

    private BroadcastReceiver mupdate_order = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("TAG", "onBroadcastReceive: UpdateOrder");
            new Orderstatusapi().execute();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        new Orderstatusapi().execute();
    }

    private String prepareChangePasswordJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("BranchId", branchid);
            parentObj.put("Type", type);
            parentObj.put("StatusId", statusid);
            parentObj.put("OrderId", 0);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareChangePasswordJson: " + parentObj);

        return parentObj.toString();
    }

    private class Orderstatusapi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareChangePasswordJson();

            if (!((Activity) OrderTypeActivity.this).isFinishing()) {
                Constants.showLoadingDialog(OrderTypeActivity.this);
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderTypeList> call = apiService.getorderstatus(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderTypeList>() {
                @Override
                public void onResponse(Call<OrderTypeList> call, Response<OrderTypeList> response) {

                    Log.i("TAG", "onResponse: " + response.isSuccessful());
                    if (response.isSuccessful()) {
                        OrderTypeList Response = response.body();

                        orderLists = Response.getData();

                        dine_inorderLists.clear();
                        pickuporderLists.clear();
                        deliveryorderLists.clear();
                        Log.i("TAG", "count: " + orderLists.size());

                        if (type == 1) {
                            for (int i = 0; i < orderLists.size(); i++) {
                                if (orderLists.get(i).getPaymenttype().equals("COD")) {
                                    if (orderLists.get(i).getItems() != null) {
                                        String multipleimages = null;
                                        Log.i("TAG", "itemcount: " + orderLists.get(i).getItems().size());
                                        for (int j = 0; j < orderLists.get(i).getItems().size(); j++) {

                                            if (multipleimages != null && multipleimages.length() > 0) {

                                                multipleimages = multipleimages + "," + orderLists.get(i).getItems().get(j).getImage();

                                            } else {

                                                multipleimages = orderLists.get(i).getItems().get(j).getImage();

                                            }
                                        }
                                        orderLists.get(i).setMultipleimages(multipleimages);
                                        dine_inorderLists.add(orderLists.get(i));
                                    }
                                }
                                Log.i("TAG", "Order type: " + dine_inorderLists.size());

                                if (orderLists.get(i).getPaymenttype().equals("PayNow")) {
                                    String multipleimages = null;
                                    for (int j = 0; j < orderLists.get(i).getItems().size(); j++) {

                                        if (multipleimages != null && multipleimages.length() > 0) {

                                            multipleimages = multipleimages + "," + orderLists.get(i).getItems().get(j).getImage();

                                        } else {

                                            multipleimages = orderLists.get(i).getItems().get(j).getImage();
                                        }

                                    }
                                    orderLists.get(i).setMultipleimages(multipleimages);
                                    pickuporderLists.add(orderLists.get(i));
                                }
                                if (orderLists.get(i).getPaymenttype().equals("PayLater")) {
                                    String multipleimages = null;
                                    for (int j = 0; j < orderLists.get(i).getItems().size(); j++) {

                                        if (multipleimages != null && multipleimages.length() > 0) {

                                            multipleimages = multipleimages + "," + orderLists.get(i).getItems().get(j).getImage();

                                        } else {

                                            multipleimages = orderLists.get(i).getItems().get(j).getImage();

                                        }

                                    }
                                    orderLists.get(i).setMultipleimages(multipleimages);
                                    deliveryorderLists.add(orderLists.get(i));
                                }
                            }

                            pending_txt.setText("Cash (" + dine_inorderLists.size() + ")");

                            scheduled_txt.setText("Online Now (" + pickuporderLists.size() + ")");

                            order_txt.setText("Online Later (" + deliveryorderLists.size() + ")");

                            pending.setVisibility(View.VISIBLE);
                            scheduled.setVisibility(View.VISIBLE);
                            order.setVisibility(View.VISIBLE);

                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, dine_inorderLists, userId, language);
                            mpending_list.setAdapter(pendingListAdapter);

                            if (pending_boolean) {

                                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, dine_inorderLists, userId, language);
                                mpending_list.setAdapter(pendingListAdapter);

                            } else if (scheduled_boolean) {

                                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
                                mpending_list.setAdapter(pendingListAdapter);

                            } else if (order_boolean) {

                                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, deliveryorderLists, userId, language);
                                mpending_list.setAdapter(pendingListAdapter);

                            }

                        } else if (type == 2) {

                            for (int i = 0; i < orderLists.size(); i++) {
                                if (orderLists.get(i).getOrdertype() == 1 || orderLists.get(i).getOrdertype() == 4) {
                                    String multipleimages = null;
                                    for (int j = 0; j < orderLists.get(i).getItems().size(); j++) {

                                        if (multipleimages != null && multipleimages.length() > 0) {

                                            multipleimages = multipleimages + "," + orderLists.get(i).getItems().get(j).getImage();

                                        } else {

                                            multipleimages = orderLists.get(i).getItems().get(j).getImage();

                                        }

                                    }
                                    orderLists.get(i).setMultipleimages(multipleimages);
                                    dine_inorderLists.add(orderLists.get(i));
                                }
//                                Log.i("TAG", "Order type: " + orderLists.get(i).getOrderMode_En());

                                if (orderLists.get(i).getOrdertype() == 2 || orderLists.get(i).getOrdertype() == 5) {
                                    String multipleimages = null;
                                    for (int j = 0; j < orderLists.get(i).getItems().size(); j++) {

                                        if (multipleimages != null && multipleimages.length() > 0) {

                                            multipleimages = multipleimages + "," + orderLists.get(i).getItems().get(j).getImage();

                                        } else {

                                            multipleimages = orderLists.get(i).getItems().get(j).getImage();
                                        }

                                    }
                                    orderLists.get(i).setMultipleimages(multipleimages);
                                    pickuporderLists.add(orderLists.get(i));
                                }
                                if (orderLists.get(i).getOrdertype() == 3) {
                                    String multipleimages = null;
                                    for (int j = 0; j < orderLists.get(i).getItems().size(); j++) {

                                        if (multipleimages != null && multipleimages.length() > 0) {

                                            multipleimages = multipleimages + "," + orderLists.get(i).getItems().get(j).getImage();

                                        } else {

                                            multipleimages = orderLists.get(i).getItems().get(j).getImage();

                                        }

                                    }
                                    orderLists.get(i).setMultipleimages(multipleimages);
                                    deliveryorderLists.add(orderLists.get(i));
                                }
                            }
                            Constants.closeLoadingDialog();

                            pending_txt.setText("Pick Up (" + dine_inorderLists.size() + ")");

                            scheduled_txt.setText("Delivery (" + pickuporderLists.size() + ")");

                            order_txt.setText("Shipping (" + deliveryorderLists.size() + ")");

                            pending.setVisibility(View.VISIBLE);
                            scheduled.setVisibility(View.VISIBLE);
                            order.setVisibility(View.VISIBLE);

                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, dine_inorderLists, userId, language);
                            mpending_list.setAdapter(pendingListAdapter);

                            if (pending_boolean) {

                                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, dine_inorderLists, userId, language);
                                mpending_list.setAdapter(pendingListAdapter);

                            } else if (scheduled_boolean) {

                                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
                                mpending_list.setAdapter(pendingListAdapter);

                            } else if (order_boolean) {

                                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, deliveryorderLists, userId, language);
                                mpending_list.setAdapter(pendingListAdapter);
                            }

                        } else if (type == 3 && statusid == 1) {

                            String currentDate, expected_date;
                            Date date = null, current_date = null;

                            final Calendar c = Calendar.getInstance();
                            SimpleDateFormat expected = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
                            SimpleDateFormat df3 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                            SimpleDateFormat df4 = new SimpleDateFormat("hh:mm a", Locale.US);
                            SimpleDateFormat df5 = new SimpleDateFormat("HH:mm", Locale.US);
                            currentDate = expected.format(c.getTime());

                            for (int i = 0; i < orderLists.size(); i++) {

                                try {
                                    date = expected.parse(orderLists.get(i).getExpectingdelivery());
                                    current_date = expected.parse(currentDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                if (date.compareTo(current_date) == 0 || date.before(current_date)) {

                                    String multipleimages = null;
                                    for (int j = 0; j < orderLists.get(i).getItems().size(); j++) {

                                        if (multipleimages != null && multipleimages.length() > 0) {

                                            multipleimages = multipleimages + "," + orderLists.get(i).getItems().get(j).getImage();

                                        } else {

                                            multipleimages = orderLists.get(i).getItems().get(j).getImage();

                                        }

                                    }
                                    orderLists.get(i).setMultipleimages(multipleimages);
                                    dine_inorderLists.add(orderLists.get(i));

                                } else {
                                    String multipleimages = null;
                                    for (int j = 0; j < orderLists.get(i).getItems().size(); j++) {

                                        if (multipleimages != null && multipleimages.length() > 0) {

                                            multipleimages = multipleimages + "," + orderLists.get(i).getItems().get(j).getImage();

                                        } else {

                                            multipleimages = orderLists.get(i).getItems().get(j).getImage();

                                        }

                                    }
                                    orderLists.get(i).setMultipleimages(multipleimages);
                                    pickuporderLists.add(orderLists.get(i));

                                }
                                Log.i("TAG", "onResponse: " + orderLists.get(i).getExpectingdelivery());
                            }

                            pending_txt.setText("Todays Orders (" + dine_inorderLists.size() + ")");

                            scheduled_txt.setText("Schedule Orders (" + pickuporderLists.size() + ")");

                            order.setVisibility(View.GONE);

                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, dine_inorderLists, userId, language);
                            mpending_list.setAdapter(pendingListAdapter);

                            if (pending_boolean) {

                                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, dine_inorderLists, userId, language);
                                mpending_list.setAdapter(pendingListAdapter);

                            } else if (scheduled_boolean) {

                                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
                                mpending_list.setAdapter(pendingListAdapter);

                            }

                        } else if (type == 3) {

                            for (int i = 0; i < orderLists.size(); i++) {
                                String multipleimages = null;
                                for (int j = 0; j < orderLists.get(i).getItems().size(); j++) {

                                    if (multipleimages != null && multipleimages.length() > 0) {

                                        multipleimages = multipleimages + "," + orderLists.get(i).getItems().get(j).getImage();

                                    } else {

                                        multipleimages = orderLists.get(i).getItems().get(j).getImage();

                                    }

                                }
                                orderLists.get(i).setMultipleimages(multipleimages);
                            }

                            order.setVisibility(View.GONE);
                            scheduled.setVisibility(View.GONE);
                            pending.setVisibility(View.GONE);

                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, orderLists, userId, language);
                            mpending_list.setAdapter(pendingListAdapter);

                        }
                        Log.i("TAG", "onResponse: " + Response.getMessage());

                    } else {
                        Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderTypeList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderTypeActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onFailure: " + t);
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}
