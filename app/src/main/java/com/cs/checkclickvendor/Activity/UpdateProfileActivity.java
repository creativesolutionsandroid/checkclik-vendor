package com.cs.checkclickvendor.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Models.Signupresponse;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;
import com.lovejjfg.shadowcircle.CircleImageView;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class UpdateProfileActivity extends Activity implements View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    private EditText inputFirstName, inputMiddleName, inputLastName;
    private com.lovejjfg.shadowcircle.CircleImageView profilepic;
    private FrameLayout profilePicLayout;
    private AlertDialog customDialog;
    private static String TAG = "TAG";
    private int mYear, mMonth, mDay;
    TextView inputdate;
    Button next_phone;

    private static final int PICK_IMAGE_FROM_GALLERY = 1;
    private static final int STORAGE_REQUEST = 2;
    private static final int CAMERA_REQUEST = 3;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    boolean isCamera = false;
    Bitmap thumbnail;
    Boolean isImageChanged = false;
    String pictureName = "";
    Boolean isToday = false;
    ImageView back_btn;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    SharedPreferences.Editor userPrefsEditor;
    private String strFirstname, strLastname, strMiddlename, strEmail, strPassword, strMobile, strotp, strCofirmPassword;
    private TextInputLayout inputLayoutfirstname, inputLayoutlastname;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        profilepic = (com.lovejjfg.shadowcircle.CircleImageView) findViewById(R.id.profile_pic);
        profilePicLayout = (FrameLayout) findViewById(R.id.profile_add_layout);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        inputFirstName = (EditText) findViewById(R.id.input_first_name);
        inputMiddleName = (EditText) findViewById(R.id.input_middle_name);
        inputLastName = (EditText) findViewById(R.id.input_last_name);
        inputdate = (TextView) findViewById(R.id.input_date);
        next_phone = (Button) findViewById(R.id.next_phone);
        strMobile = getIntent().getStringExtra("MobileNo");
        strEmail = getIntent().getStringExtra("EmailId");
        strotp = getIntent().getStringExtra("OTP");
        strPassword = getIntent().getStringExtra("Password");
        strCofirmPassword = getIntent().getStringExtra("CofirmPassword");

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_pickup = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_pickup));

                int ic_default_user3x = getResources().getIdentifier("default_user3x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_default_user3x));
            }
        }

        profilePicLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImageFrom();
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        inputdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(UpdateProfileActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                    isToday = true;
                                } else {
                                    isToday = false;
                                }
                                mYear = year;
                                mDay = dayOfMonth;
                                mMonth = monthOfYear;
                                if (mMonth < 9) {
                                    inputdate.setText("" + mYear + "/" + "0" + (mMonth + 1) + "/" + mDay);
                                } else if (mDay < 9) {
                                    inputdate.setText("" + mYear + "/" + (mMonth + 1) + "/" + "0" + mDay);
                                } else if (mMonth < 9) {
                                    inputdate.setText("" + mYear + "/" + "0" + (mMonth + 1) + "/" + mDay);
                                } else {
                                    inputdate.setText("" + mYear + "/" + mMonth + "/" + mDay);
                                }

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.setTitle("Select Date");
                datePickerDialog.show();
            }
        });
        next_phone.setOnClickListener(this);

    }

    public void pickImageFrom() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            } else if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
            }
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(UpdateProfileActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog_camera;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        TextView cancel = (TextView) dialogView.findViewById(R.id.cancel_btn);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
                customDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                customDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, perm));
    }

    public void openGallery() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {

            if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                isCamera = false;
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
            }
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
        }
    }


    public void openCamera() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                isCamera = true;
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            } else {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            thumbnail = (Bitmap) data.getExtras().get("data");
            convertPixel();
        }

        if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
            convertPixel();
        }
    }


    private void convertPixel() {
        thumbnail = Bitmap.createScaledBitmap(thumbnail, 900, 800,
                false);
        thumbnail = codec(thumbnail, Bitmap.CompressFormat.JPEG, 100);
        profilepic.setImageBitmap(thumbnail);
        isImageChanged = true;
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }

    private boolean nameValidations() {
        strFirstname = inputFirstName.getText().toString();
        strLastname = inputLastName.getText().toString();
        strMiddlename = inputMiddleName.getText().toString();

        if (strFirstname.length() == 0) {
            inputFirstName.setError(getResources().getString(R.string.signup_msg_enter_firstname));
            Constants.requestEditTextFocus(inputFirstName, UpdateProfileActivity.this);
            return false;
        }
        if (strLastname.length() == 0) {
            inputLastName.setError(getResources().getString(R.string.signup_msg_enter_lastname));
            Constants.requestEditTextFocus(inputLastName, UpdateProfileActivity.this);
            return false;
        }
        if (strMiddlename.length() == 0) {
            inputMiddleName.setError(getResources().getString(R.string.signup_msg_enter_middle));
            Constants.requestEditTextFocus(inputMiddleName, UpdateProfileActivity.this);
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_phone:
                nameValidations();
            {
                new userRegistrationApi().execute();
            }
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

    }

    private class userRegistrationApi extends AsyncTask<String, String, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignUpJson();
            Constants.showLoadingDialog(UpdateProfileActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(UpdateProfileActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<Signupresponse> call = apiService.getsignup(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Signupresponse>() {
                @Override
                public void onResponse(Call<Signupresponse> call, Response<Signupresponse> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        Signupresponse registrationResponse = response.body();
                        if (registrationResponse.getStatus()) {
//                          status true case
                            String userId = registrationResponse.getData().getId();
                            userPrefsEditor.putString("userId", userId);
                            userPrefsEditor.putString("FirstName", registrationResponse.getData().getFirstName());
                            userPrefsEditor.putString("LastName", registrationResponse.getData().getFirstName());
                            userPrefsEditor.putString("EmailId", registrationResponse.getData().getEmailId());
                            userPrefsEditor.putString("MobileNo", registrationResponse.getData().getMobileNo());
                            userPrefsEditor.putInt("userType", registrationResponse.getData().getUserType());
//                            userPrefsEditor.putString("Password", registrationResponse.getData().getPhone());
//                            userPrefsEditor.putString("pic", registrationResponse.getData().getProfilephoto());
                            userPrefsEditor.commit();
                            Intent intent = new Intent(UpdateProfileActivity.this, MainActivity.class);
                            intent.putExtra("type", "1");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
//                          status false case
                            String failureResponse = registrationResponse.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), UpdateProfileActivity.this);
                        }
                    } else {
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            Toast.makeText(UpdateProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(UpdateProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<Signupresponse> call, Throwable t) {
                    Log.d(TAG, "updateonFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(UpdateProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(UpdateProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();

                }
            });
            return null;
        }
    }

    private String prepareSignUpJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id", 0);
            parentObj.put("VendorType", 1);
            parentObj.put("FirstName", strFirstname);
            parentObj.put("MiddleName", strMiddlename);
            parentObj.put("LastName", strLastname);
            if (mMonth < 9) {
                inputdate.setText("" + mYear + "-" + "0" + (mMonth + 1) + "-" + mDay);
                parentObj.put("DOB", "" + mYear + "-" + "0" + (mMonth + 1) + "-" + mDay);
            } else if (mDay < 9) {
                inputdate.setText("" + "0" + mDay + "-" + (mMonth + 1) + "-" + mDay);
                parentObj.put("DOB", "" + mYear + "-" + (mMonth + 1) + "-" + "0" + mDay);
            } else {
                inputdate.setText("" + mDay + "-" + mMonth + "-" + mDay);
                parentObj.put("DOB", "" + mYear + "-" + mMonth + "-" + mDay);
            }

            parentObj.put("EmailId", strEmail);
            parentObj.put("Password", strPassword);
            parentObj.put("CofirmPassword", strCofirmPassword);
            parentObj.put("MobileNo", "+966" + strMobile);
            parentObj.put("IsMobileNoVerified", true);
            parentObj.put("IsEmailVerified", false);
            parentObj.put("VerifyOTP", strotp);
            parentObj.put("Status", true);
            parentObj.put("CreatedBy", 1);
            parentObj.put("ModifiedBy", 0);
            parentObj.put("DeletedBy", 0);
            parentObj.put("IsDeleted", true);
            parentObj.put("FlagId", 1);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareSignUpJson: " + parentObj);
        return parentObj.toString();
    }


}
