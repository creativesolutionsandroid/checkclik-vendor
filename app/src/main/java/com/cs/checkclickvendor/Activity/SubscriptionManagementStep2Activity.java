package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.cs.checkclickvendor.Adapters.SubscriptionAdapter1;
import com.cs.checkclickvendor.Adapters.SubscriptionAdapter2;
import com.cs.checkclickvendor.Models.SubscriptionPackageResponce2;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Activity.ForgotPasswordActivity.TAG;
import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

//*********Puli********//
public class SubscriptionManagementStep2Activity extends AppCompatActivity {

    ImageView backbtn;
    RecyclerView listview;
    SubscriptionAdapter2 mAdapter;
    ArrayList<SubscriptionPackageResponce2.DataEntity> subDetailslist = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subscriptonstep2_activity);

        backbtn=(ImageView)findViewById(R.id.back_btn);
        listview=(RecyclerView)findViewById(R.id.listview);
        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_back_btn = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
                backbtn.setImageDrawable(getResources().getDrawable(ic_back_btn));
            }
        }

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        new subpackageApi().execute();
    }

    private class subpackageApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparecatJson();
            Constants.showLoadingDialog(SubscriptionManagementStep2Activity.this);
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SubscriptionManagementStep2Activity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<SubscriptionPackageResponce2> call = apiService.getsubDetailspacks(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SubscriptionPackageResponce2>() {
                @Override
                public void onResponse(Call<SubscriptionPackageResponce2> call, Response<SubscriptionPackageResponce2> response) {
                    if (response.isSuccessful()) {
                        SubscriptionPackageResponce2 Catresponce = response.body();
                        try {
                            if (Catresponce.getStatus()) {
                                SubscriptionPackageResponce2 Response = response.body();
                                subDetailslist = Response.getData();
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SubscriptionManagementStep2Activity.this);
                                listview.setLayoutManager(mLayoutManager);
                                mAdapter = new SubscriptionAdapter2(SubscriptionManagementStep2Activity.this,subDetailslist);
                                listview.setAdapter(mAdapter);
                            }
                            else {
                                String failureResponse = Catresponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), SubscriptionManagementStep2Activity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SubscriptionManagementStep2Activity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SubscriptionManagementStep2Activity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
                @Override
                public void onFailure(Call<SubscriptionPackageResponce2> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(SubscriptionManagementStep2Activity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SubscriptionManagementStep2Activity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String preparecatJson() {
        JSONObject parentObj = new JSONObject();

        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }

}
