package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Models.Signinresponce;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.SplashScreen;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class SignInActivity extends AppCompatActivity {
    ImageView backbtn;
    EditText inputMobile, inputPassword;
    TextView forgot;
    Button signbtn;
    String strMobile, strPassword;
    private static final String TAG = "TAG";
    private static int SIGNUP_REQUEST = 1;
    private static int FORGOT_PASSWORD_REQUEST = 2;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signinactivity);
        backbtn = (ImageView) findViewById(R.id.back_btn);
        inputMobile = (EditText) findViewById(R.id.signin_input_phone);
        inputPassword = (EditText) findViewById(R.id.signin_input_password);
        forgot = (TextView) findViewById(R.id.forgot);
        signbtn = (Button) findViewById(R.id.bt_continue);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

//        int ic_back_btn = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
//        backbtn.setImageDrawable(getResources().getDrawable(ic_back_btn));
//
//        int ic_color = getResources().getIdentifier("C" + appColor, "color", getPackageName());
//        Drawable background = signbtn.getBackground();
//
//        GradientDrawable shapeDrawable = (GradientDrawable) background;
//        shapeDrawable.setColor(ContextCompat.getColor(SignInActivity.this, ic_color));

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        signbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        userLoginApi();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        inputMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                if (text.matches("[0-9]+") && !text.contains("+966") && text.length() >= 1 && text.length() <= 9) {
                    inputMobile.removeTextChangedListener(this);
                    inputMobile.setText("+966 " + text);
                    inputMobile.setSelection(inputMobile.length());
                    inputMobile.addTextChangedListener(this);
                } else {
                    String textCopy = text;
                    textCopy = textCopy.replace("+966 ", "");
                    if (textCopy.matches("[0-9]+") && textCopy.length() <= 9) {

                    } else {
                        inputMobile.removeTextChangedListener(this);
                        text = text.replace("+966 ", "");
                        inputMobile.setText(text);
                        inputMobile.setSelection(inputMobile.length());
                        inputMobile.addTextChangedListener(this);
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGNUP_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (requestCode == FORGOT_PASSWORD_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    private boolean validations() {
        strMobile = inputMobile.getText().toString();
        strPassword = inputPassword.getText().toString();

        if (strMobile.length() == 0) {
            inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            Constants.requestEditTextFocus(inputMobile, SignInActivity.this);
            return false;
        } else if (strPassword.length() == 0) {
            inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));

            Constants.requestEditTextFocus(inputPassword, SignInActivity.this);
            return false;
        } else if (strPassword.length() < 4 || strPassword.length() > 20) {
            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));

            Constants.requestEditTextFocus(inputPassword, SignInActivity.this);
            return false;
        }
        return true;
    }

    private void userLoginApi() {
        String inputStr = prepareLoginJson();
        Constants.showLoadingDialog(SignInActivity.this);
        final String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<Signinresponce> call = apiService.getsignin(
                RequestBody.create(MediaType.parse("application/json"), inputStr));
        call.enqueue(new Callback<Signinresponce>() {

            public void onResponse(Call<Signinresponce> call, Response<Signinresponce> response) {
                if (response.isSuccessful()) {
                    Signinresponce registrationResponse = response.body();
                    try {
                        if (registrationResponse.getStatus()) {
                            //status true case
                            String userId = registrationResponse.getData().getUserId();
                            Log.i("TAG", "onResponse: " + userId);
                            userPrefsEditor.putString("userId", userId);
                            userPrefsEditor.putString("name", registrationResponse.getData().getFullName());
                            userPrefsEditor.putString("EmailId", registrationResponse.getData().getEmail());
                            userPrefsEditor.putString("MobileNo", registrationResponse.getData().getPhone());
                            userPrefsEditor.putInt("userType", registrationResponse.getData().getUserType());
                            userPrefsEditor.putString("chatUserId", registrationResponse.getData().getChatUserId());
                            userPrefsEditor.commit();
                            setResult(RESULT_OK);
                            Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            //status false case
                            String failureResponse = registrationResponse.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), SignInActivity.this);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d(TAG, "onResponse: " + response.errorBody());
                    Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                Constants.closeLoadingDialog();

            }

            public void onFailure(Call<Signinresponce> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(SignInActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                Constants.closeLoadingDialog();

            }
        });
    }

    private String prepareLoginJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("LoginText", strMobile);
            parentObj.put("Password", strPassword);
            parentObj.put("DeviceToken", SplashScreen.regId);
            parentObj.put("Language", "en");
            parentObj.put("DeviceType", "Android");
            parentObj.put("DeviceVersion", "andr v");
            parentObj.put("UserType", getIntent().getIntExtra("userType", 1));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "SigninResponce: " + parentObj.toString());
        return parentObj.toString();
    }
}
