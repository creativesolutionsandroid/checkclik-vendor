package com.cs.checkclickvendor.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickvendor.Models.AdavertisementResponce;
import com.cs.checkclickvendor.Models.VoucherDeleteResponce;
import com.cs.checkclickvendor.Models.VoucherResponce;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;
import static com.cs.checkclickvendor.Utils.Utils.Constants.customDialog;


public class AdvertisementDetailsActivity extends AppCompatActivity {

    TextView adv_name, expiery_date, startdate, enddate, numberofdays, numberofuses, target, paid, details_txt;
    View advise_view;
    ImageView coupondelete, back_btn, adv_img;
    int pos;
    String TAG, userId;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences userPrefs;
    public ArrayList<AdavertisementResponce.DataEntity> couponArrayList = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.advertisment_details);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        couponArrayList = (ArrayList<AdavertisementResponce.DataEntity>) getIntent().getSerializableExtra("advertiesarry");
        pos = getIntent().getIntExtra("pos", 0);

        adv_img = (ImageView) findViewById(R.id.img);
        adv_name = (TextView) findViewById(R.id.adv_name);
        expiery_date = (TextView) findViewById(R.id.expiery_date);
        startdate = (TextView) findViewById(R.id.startdate);
        enddate = (TextView) findViewById(R.id.enddate);
        numberofdays = (TextView) findViewById(R.id.numberofdays);
        numberofuses = (TextView) findViewById(R.id.numberofuses);
        target = (TextView) findViewById(R.id.target);
        paid = (TextView) findViewById(R.id.paid);
        coupondelete = (ImageView) findViewById(R.id.delete);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        details_txt =(TextView) findViewById(R.id.details_txt);
        advise_view = (View) findViewById(R.id.adveactiveview);

        int ic_delete = getResources().getIdentifier("delete2x_" + appColor, "drawable", getPackageName());
        coupondelete.setImageDrawable(getResources().getDrawable(ic_delete));

        int ic_back_btn = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
        back_btn.setImageDrawable(getResources().getDrawable(ic_back_btn));

        details_txt.setTextColor(Color.parseColor("#"+Constants.appColor));
        advise_view.setBackgroundColor(Color.parseColor("#"+Constants.appColor));

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        coupondelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showtwoButtonsAlertDialog();
            }
        });

        Glide.with(AdvertisementDetailsActivity.this)
                .load(Constants.ADS_IMAGE_URL + couponArrayList.get(pos).getBannerImageEnName());
        adv_name.setText("" + couponArrayList.get(pos).getPercentage() + "% off on " + couponArrayList.get(pos).getNameEn());
        String date1 = couponArrayList.get(pos).getEndDate();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-ddHH'T'mm:ss", Locale.US);
        SimpleDateFormat sdf3 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        date1.replace(date1, "yyyy-MM-dd");
        try {
            Date datetime = format1.parse(date1);
            date1 = sdf3.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        expiery_date.setText("Expiry - " + date1);
        numberofdays.setText(couponArrayList.get(pos).getNoOfUses() + " Days");
        numberofuses.setText("" + couponArrayList.get(pos).getNoOfUses());
        target.setText(couponArrayList.get(pos).getRegions());
        paid.setText("" + Constants.priceFormat1.format(couponArrayList.get(pos).getPrice()) + " SAR");

        String date = couponArrayList.get(pos).getEndDate();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);

        try {
            Date datetime = format.parse(date);
            date = sdf1.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        enddate.setText(date);

        String date2 = couponArrayList.get(pos).getStartDate();
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        SimpleDateFormat sdf4 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat sdf5 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        ;

        try {
            Date datetime = format2.parse(date2);
            date2 = sdf3.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        startdate.setText(date2);

    }

    public void showtwoButtonsAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AdvertisementDetailsActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final TextView title = (TextView) dialogView.findViewById(R.id.title);
        final TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        final TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        final TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        desc.setText("  Do you want to  Delete the Advertisement ?");

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new deletvoucheapi().execute();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();

            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private class deletvoucheapi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            Constants.showLoadingDialog(AdvertisementDetailsActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<VoucherDeleteResponce> call = apiService.getvoucherdelet(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VoucherDeleteResponce>() {
                @Override
                public void onResponse(Call<VoucherDeleteResponce> call, Response<VoucherDeleteResponce> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        VoucherDeleteResponce resetPasswordResponse = response.body();
                        try {
                            if (resetPasswordResponse.getStatus()) {
                                finish();
                                Toast.makeText(AdvertisementDetailsActivity.this, R.string.success, Toast.LENGTH_SHORT).show();
                            } else {
                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), AdvertisementDetailsActivity.this);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(AdvertisementDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                        }
                        Constants.closeLoadingDialog();
                    }
                }

                @Override
                public void onFailure(Call<VoucherDeleteResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(AdvertisementDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(AdvertisementDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AdvertisementDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

            });
            return null;
        }
    }

    private String prepareResetPasswordJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id", userId);
            parentObj.put("StoreId", couponArrayList.get(pos).getStoreId());
            parentObj.put("FlagId", 4);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareResetPasswordJson: " + parentObj.toString());
        return parentObj.toString();
    }


}
