package com.cs.checkclickvendor.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Models.VerifyEmailResponse;
import com.cs.checkclickvendor.Models.VerifyMobileResponse;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.SplashScreen;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.content.ContentValues.TAG;
import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class SignUpActivity extends Activity implements View.OnClickListener{

    private int currentStage = 1;
    private ImageView backBtn, entityRB, individualRB;
    private RelativeLayout entityLayout, individualLayout;
    private RelativeLayout typeLayout, emailLayout, phoneLayout;
    private EditText inputEmail, inputPassword, inputConfirm, inputPhone;
    private TextInputLayout inputLayoutEmail, inputLayoutPassword, inputLayoutConfirm;
    private Boolean isEntity = true;
    private Button btnTypeNext, btnEmailNext, btnContinue;
    private Context context;
    private String  OTP,strEmail, strPassword,strCofirmPassword, strPhone;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        context = this;

        backBtn = (ImageView) findViewById(R.id.back_btn);
        entityRB = (ImageView) findViewById(R.id.rb_entity);
        individualRB = (ImageView) findViewById(R.id.rb_individual);

        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);
        inputConfirm = (EditText) findViewById(R.id.input_confirm_password);
        inputPhone = (EditText) findViewById(R.id.input_phone);

        inputLayoutEmail = (TextInputLayout) findViewById(R.id.layout_email);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.layout_password);
        inputLayoutConfirm = (TextInputLayout) findViewById(R.id.layout_confirm_password);

        typeLayout = (RelativeLayout) findViewById(R.id.type_layout);
        emailLayout =   (RelativeLayout) findViewById(R.id.email_layout);
        phoneLayout = (RelativeLayout) findViewById(R.id.phone_layout);
        entityLayout = (RelativeLayout) findViewById(R.id.entity_layout);
        individualLayout = (RelativeLayout) findViewById(R.id.individual_layout);

        btnTypeNext = (Button) findViewById(R.id.next_user_type);
        btnEmailNext = (Button) findViewById(R.id.next_email);
        btnContinue = (Button) findViewById(R.id.next_phone);



//        int ic_pickup = context.getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
//        Log.i("TAG", "onCreate: " + ic_pickup);
//        backBtn.setImageDrawable(context.getResources().getDrawable(ic_pickup));

        setTypeface();
        backBtn.setOnClickListener(this);
        btnTypeNext.setOnClickListener(this);
        btnEmailNext.setOnClickListener(this);
        entityLayout.setOnClickListener(this);
        individualLayout.setOnClickListener(this);
        btnContinue.setOnClickListener(this);

        inputEmail.addTextChangedListener(new TextWatcher(inputEmail));
        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));
        inputConfirm.addTextChangedListener(new TextWatcher(inputConfirm));
    }

    private void setTypeface(){
//        btnTypeNext.setTypeface(Constants.getBookTypeFace(context));
//        ((TextView)findViewById(R.id.entity_title)).setTypeface(Constants.getMediumTypeFace(context));
//        ((TextView)findViewById(R.id.entity_desc)).setTypeface(Constants.getBookTypeFace(context));
//        ((TextView)findViewById(R.id.individual_title)).setTypeface(Constants.getMediumTypeFace(context));
//        ((TextView)findViewById(R.id.individual_desc)).setTypeface(Constants.getBookTypeFace(context));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.entity_layout:
                entityRB.setImageDrawable(getResources().getDrawable(R.drawable.ovalgroup_3x));
                individualRB.setImageDrawable(getResources().getDrawable(R.drawable.oval_3x));
                isEntity = true;
                break;

            case R.id.individual_layout:
                individualRB.setImageDrawable(getResources().getDrawable(R.drawable.ovalgroup_3x));
                entityRB.setImageDrawable(getResources().getDrawable(R.drawable.oval_3x));
                isEntity = false;
                break;

            case R.id.back_btn:
                if(currentStage == 1){
                    finish();
                }
                else if(currentStage == 2){
                    currentStage = 1;
                    ((TextView)findViewById(R.id.signup_title)).setVisibility(View.VISIBLE);
                    emailLayout.setVisibility(View.GONE);
                    Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);
                    typeLayout.setVisibility(View.VISIBLE);
                    typeLayout.startAnimation(slideUp);
                }
                else if(currentStage == 3){
                    currentStage = 2;
                    phoneLayout.setVisibility(View.GONE);
                    Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);
                    emailLayout.setVisibility(View.VISIBLE);
                    emailLayout.startAnimation(slideUp);
                }
                break;

            case R.id.next_user_type:
                currentStage = 2;
                ((TextView)findViewById(R.id.signup_title)).setVisibility(View.GONE);
                typeLayout.setVisibility(View.GONE);
                Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.enter_from_right);
                emailLayout.setVisibility(View.VISIBLE);
                emailLayout.startAnimation(slideUp);
                break;

            case R.id.next_email:
                if(emailValidations()){
                    currentStage = 3;
//                    emailLayout.setVisibility(View.GONE);
//                    Animation slideUp1 = AnimationUtils.loadAnimation(this, R.anim.enter_from_right);
//                    phoneLayout.setVisibility(View.VISIBLE);
//                    phoneLayout.startAnimation(slideUp1);
                    new verifyEmailApi().execute();
                }
                break;

            case R.id.next_phone:
                if(mobileValidations()){
                    new verifyMobileApi().execute();
//                    Intent intent = new Intent(SignUpActivity.this, VerifyOTPActivity.class);
//                    intent.putExtra("phone", strPhone);
//                    startActivity(intent);
                }
                break;
        }
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_email:
                    clearErrors();
                    break;
                case R.id.input_password:
                    clearErrors();
                    if(editable.length() > 20){
                        inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
                case R.id.input_confirm_password:
                    clearErrors();
                    if(editable.length() > 20){
                        inputLayoutConfirm.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
            }
        }
    }

    private void clearErrors() {
        inputLayoutEmail.setErrorEnabled(false);
        inputLayoutPassword.setErrorEnabled(false);
        inputLayoutConfirm.setErrorEnabled(false);
    }

    private boolean mobileValidations(){
        strPhone = inputPhone.getText().toString();
        if(strPhone.length() == 0){
            inputPhone.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            Constants.requestEditTextFocus(inputPhone, SignUpActivity.this);
            return false;
        }
        else if(strPhone.length() != 9){
            inputPhone.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
            Constants.requestEditTextFocus(inputPhone, SignUpActivity.this);
            return false;
        }
        return true;
    }

    private boolean emailValidations(){
        strEmail = inputEmail.getText().toString();
        strPassword = inputPassword.getText().toString();
         strCofirmPassword = inputConfirm.getText().toString();

        if(strEmail.length() == 0){
            inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            Constants.requestEditTextFocus(inputEmail, SignUpActivity.this);
            return false;
        }
        else if(!Constants.isValidEmail(strEmail)){
            inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_invalid_email));
            Constants.requestEditTextFocus(inputEmail, SignUpActivity.this);
            return false;
        }
        else if(strPassword.length() == 0){
            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            Constants.requestEditTextFocus(inputPassword, SignUpActivity.this);
            return false;
        }
        else if(strPassword.length() < 4){
            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            Constants.requestEditTextFocus(inputPassword, SignUpActivity.this);
            return false;
        }
        else if(strCofirmPassword.length() == 0){
            inputLayoutConfirm.setError(getResources().getString(R.string.signup_msg_enter_password));
            Constants.requestEditTextFocus(inputConfirm, SignUpActivity.this);
            return false;
        }
        else if(!strCofirmPassword.equals(strPassword)){
            inputLayoutConfirm.setError(getResources().getString(R.string.signup_msg_valid_password));
            Constants.requestEditTextFocus(inputConfirm, SignUpActivity.this);
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(currentStage == 1){
            finish();
        }
        else if(currentStage == 2){
            currentStage = 1;
            ((TextView)findViewById(R.id.signup_title)).setVisibility(View.VISIBLE);
            emailLayout.setVisibility(View.GONE);
            Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);
            typeLayout.setVisibility(View.VISIBLE);
            typeLayout.startAnimation(slideUp);
        }
        else if(currentStage == 3){
            currentStage = 2;
            phoneLayout.setVisibility(View.GONE);
            Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);
            emailLayout.setVisibility(View.VISIBLE);
            emailLayout.startAnimation(slideUp);
        }
    }

    private class verifyEmailApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyEmailJson();
            Constants.showLoadingDialog(SignUpActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyEmailResponse> call = apiService.getEmail(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyEmailResponse>() {
                @Override
                public void onResponse(Call<VerifyEmailResponse> call, Response<VerifyEmailResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyEmailResponse VerifyEmailResponse = response.body();
                        try {
                            if (VerifyEmailResponse.getStatus()) {
                                Log.d(TAG, "onResponse: " + VerifyEmailResponse.getData().getEmailId());
                                emailLayout.setVisibility(View.GONE);
                                Animation slideUp1 = AnimationUtils.loadAnimation(SignUpActivity.this, R.anim.enter_from_right);
                                phoneLayout.setVisibility(View.VISIBLE);
                                phoneLayout.startAnimation(slideUp1);
                            } else {
                                String failureResponse = VerifyEmailResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), SignUpActivity.this);
                            }
                            Constants.closeLoadingDialog();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<VerifyEmailResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(SignUpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                }
            });
            return null;
        }
    }
    private String prepareVerifyEmailJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("EmailId", strEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(SignUpActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyMobileResponse VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                Log.d(TAG, "onResponse: " + VerifyMobileResponse.getData().getOTPCode());
                                showVerifyDialog(VerifyMobileResponse.getData().getOTPCode());
                            } else {
                                String failureResponse = VerifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), SignUpActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(SignUpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("MobileNo", "966" + strPhone);
            parentObj.put("FlagId", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }

    private void showVerifyDialog(String otp) {
        Intent intent = new Intent(SignUpActivity.this, VerifyOTPActivity.class);
        intent.putExtra("EmailId", strEmail);
        intent.putExtra("MobileNo", strPhone);
        intent.putExtra("Password", strPassword);
        intent.putExtra("CofirmPassword", strCofirmPassword);
        intent.putExtra("OTP",otp);
        intent.putExtra("Language", "en");
        intent.putExtra("DeviceToken", -1);
        intent.putExtra("DeviceVersion", SplashScreen.regId);
        intent.putExtra("DeviceType", "ander");
        intent.putExtra("screen", "register");
        startActivity(intent);
    }




}
