package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarUtils;
import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.DatePicker;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.builders.DatePickerBuilder;
import com.applandeo.materialcalendarview.extensions.CalendarViewPager;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.applandeo.materialcalendarview.listeners.OnSelectDateListener;
import com.cs.checkclickvendor.Adapters.TimeTable_TimeslotAdapter;
import com.cs.checkclickvendor.Models.ChangePasswordResponce;
import com.cs.checkclickvendor.Models.TimeSlot_Invoice_no;
import com.cs.checkclickvendor.Models.TimeTableMang;
import com.cs.checkclickvendor.Models.TimeTable_Invoiceno;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.CustomListView;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class TimeTableMangActivity extends AppCompatActivity {

    ImageView back_btn;
    CalendarView calendarView;
    CustomListView time_table_list;
    ArrayList<TimeTableMang.ScheduledOrdersEntity> scheduledOrdersEntities = new ArrayList<>();
    ArrayList<TimeSlot_Invoice_no> timeSlot_invoice_nos = new ArrayList<>();

    ArrayList<String> timeslot = new ArrayList<>();
    ArrayList<String> startdates = new ArrayList<>();
    TimeTable_TimeslotAdapter mAdapter;
    public static int branch_id;
    ImageView mimg, pick_up_service, delivery_service, shiping_serivce;
    RelativeLayout pick_up_layout, delivery_layout, shiping_layout;
    TextView mservice_name;
    int order_type = 2;
    Calendar clickedDayCalendar;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timetablemanagment);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        calendarView = findViewById(R.id.calender_view);
        time_table_list = (CustomListView) findViewById(R.id.time_table_list);
        mimg = (ImageView) findViewById(R.id.img);
        mservice_name = (TextView) findViewById(R.id.service_name);
        pick_up_service = (ImageView) findViewById(R.id.pick_up_service);
        delivery_service = (ImageView) findViewById(R.id.delivery_service);
        shiping_serivce = (ImageView) findViewById(R.id.shiping_service);

        pick_up_layout = (RelativeLayout) findViewById(R.id.pick_up_layout);
        delivery_layout = (RelativeLayout) findViewById(R.id.delivery_layout);
        shiping_layout = (RelativeLayout) findViewById(R.id.shiping_layout);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_back = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_back));
            }
        }

        branch_id = getIntent().getIntExtra("branch_id", 0);

//        LinearLayoutManager layoutManager1 = new LinearLayoutManager(TimeTableMangActivity.this);
//        time_table_list.setLayoutManager(layoutManager1);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_pickup = getResources().getIdentifier("pickup_icon_" + appColor, "drawable", getPackageName());
                pick_up_service.setImageDrawable(getResources().getDrawable(ic_pickup));

                int ic_delivery = getResources().getIdentifier("delivery_icon_" + appColor, "drawable", getPackageName());
                delivery_service.setImageDrawable(getResources().getDrawable(ic_delivery));

                int ic_shipping = getResources().getIdentifier("shipping_icon_" + appColor, "drawable", getPackageName());
                shiping_serivce.setImageDrawable(getResources().getDrawable(ic_shipping));
            }
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_notify = getResources().getIdentifier("notification_icon2x_" + appColor, "drawable", getPackageName());
                mimg.setImageDrawable(getResources().getDrawable(ic_notify));
                mimg.setTag(getResources().getDrawable(ic_notify));

                int ic_color = getResources().getIdentifier("C" + appColor, "color", getPackageName());

                calendarView.setHeaderColor(ic_color);
                mservice_name.setTextColor(Color.parseColor("#" + Constants.appColor));
            }
        }

        time_table_list.setVisibility(View.GONE);

        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {

                clickedDayCalendar = eventDay.getCalendar();

                new DatePickerBuilder(TimeTableMangActivity.this, listener)
                        .selectionColor(R.color.primaryColor);

                for (int j = 0; j < startdates.size(); j++) {

                    Calendar calendar = Calendar.getInstance();

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                    Date date = null;

                    try {
                        date = dateFormat.parse(startdates.get(j));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    calendar.setTime(date);
                    Log.i("TAG", "onResponse: " + calendar);

                    if (calendar.equals(clickedDayCalendar)) {

                        Date date2 = null;
                        String cal_date;

                        cal_date = dateFormat.format(clickedDayCalendar.getTime());

                        try {
                            startdates.clear();
                            timeslot.clear();
                            timeSlot_invoice_nos.clear();

                            for (int i = 0; i < scheduledOrdersEntities.size(); i++) {

                                if (!scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase("")) {
                                    if (order_type == scheduledOrdersEntities.get(i).getOrderType()) {
                                        if (cal_date.equalsIgnoreCase(scheduledOrdersEntities.get(i).getStart())) {
                                            if (!timeslot.contains(scheduledOrdersEntities.get(i).getTimeSlot())) {

                                                timeslot.add(scheduledOrdersEntities.get(i).getTimeSlot());
                                                Log.i("TAG", "onResponse: " + timeslot);

                                                TimeSlot_Invoice_no timeTableMang = new TimeSlot_Invoice_no();
                                                timeTableMang.setmTimeSlot(scheduledOrdersEntities.get(i).getTimeSlot());
                                                timeTableMang.setExp1(scheduledOrdersEntities.get(i).getExpired());
                                                timeTableMang.setOrder_id1(scheduledOrdersEntities.get(i).getOrderId());

                                                ArrayList<TimeTable_Invoiceno> timeTable_invoicenos = new ArrayList<>();

                                                if (timeslot.contains(scheduledOrdersEntities.get(i).getTimeSlot())) {

                                                    for (int k = 0; k < scheduledOrdersEntities.size(); k++) {
                                                        if (!scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase("")) {
                                                            if (scheduledOrdersEntities.get(i).getOrderType() == scheduledOrdersEntities.get(k).getOrderType()) {
                                                                if (scheduledOrdersEntities.get(i).getStart().equalsIgnoreCase(scheduledOrdersEntities.get(k).getStart())) {
                                                                    if (scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase(scheduledOrdersEntities.get(k).getTimeSlot())) {
                                                                        TimeTable_Invoiceno invoicenos = new TimeTable_Invoiceno();
                                                                        invoicenos.setInvoice_no(scheduledOrdersEntities.get(k).getInvoiceNo());
                                                                        invoicenos.setExp(scheduledOrdersEntities.get(k).getExpired());
                                                                        invoicenos.setOrder_id(scheduledOrdersEntities.get(k).getOrderId());
                                                                        Log.i("TAG", "invoice_no: " + invoicenos.getInvoice_no());
                                                                        timeTable_invoicenos.add(invoicenos);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    timeTableMang.setInvoicenos(timeTable_invoicenos);
                                                }

                                                timeSlot_invoice_nos.add(timeTableMang);
                                            }

                                        }
                                    }
                                }
                            }

                            if(timeSlot_invoice_nos.size() > 0) {
                                calendarView.setSelected(true);
                            }
                            else {
                                calendarView.setSelected(false);
                            }
                            time_table_list.setVisibility(View.VISIBLE);

                            mAdapter = new TimeTable_TimeslotAdapter(TimeTableMangActivity.this, timeSlot_invoice_nos);
                            time_table_list.setAdapter(mAdapter);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(TimeTableMangActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                Log.i("TAG", "onDayClick: " + clickedDayCalendar);
            }
        });

        pick_up_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mservice_name.setText("Pickup Service");
                order_type = 1;

                time_table_list.setVisibility(View.GONE);

                pick_up_layout.setBackgroundColor(getResources().getColor(R.color.timetable));
                delivery_layout.setBackgroundColor(getResources().getColor(R.color.primaryColor));
                shiping_layout.setBackgroundColor(getResources().getColor(R.color.primaryColor));

                try {
                    startdates.clear();
                    timeslot.clear();
                    timeSlot_invoice_nos.clear();

                    for (int i = 0; i < scheduledOrdersEntities.size(); i++) {

                        if (!scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase("")) {
                            if (order_type == scheduledOrdersEntities.get(i).getOrderType()) {
                                if (!timeslot.contains(scheduledOrdersEntities.get(i).getTimeSlot())) {

                                    timeslot.add(scheduledOrdersEntities.get(i).getTimeSlot());
                                    Log.i("TAG", "onResponse: " + timeslot);

                                    TimeSlot_Invoice_no timeTableMang = new TimeSlot_Invoice_no();
                                    timeTableMang.setmTimeSlot(scheduledOrdersEntities.get(i).getTimeSlot());
                                    timeTableMang.setExp1(scheduledOrdersEntities.get(i).getExpired());
                                    timeTableMang.setOrder_id1(scheduledOrdersEntities.get(i).getOrderId());

                                    ArrayList<TimeTable_Invoiceno> timeTable_invoicenos = new ArrayList<>();

                                    if (timeslot.contains(scheduledOrdersEntities.get(i).getTimeSlot())) {

                                        for (int j = 0; j < scheduledOrdersEntities.size(); j++) {
                                            if (!scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase("")) {
                                                if (scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase(scheduledOrdersEntities.get(j).getTimeSlot())) {
                                                    TimeTable_Invoiceno invoicenos = new TimeTable_Invoiceno();
                                                    invoicenos.setInvoice_no(scheduledOrdersEntities.get(j).getInvoiceNo());
                                                    invoicenos.setExp(scheduledOrdersEntities.get(j).getExpired());
                                                    invoicenos.setOrder_id(scheduledOrdersEntities.get(j).getOrderId());
                                                    Log.i("TAG", "invoice_no: " + invoicenos.getInvoice_no());
                                                    timeTable_invoicenos.add(invoicenos);
                                                }
                                            }
                                        }

                                        timeTableMang.setInvoicenos(timeTable_invoicenos);
                                    }

                                    timeSlot_invoice_nos.add(timeTableMang);
                                }
                            }
                        }
                    }

                    for (int i = 0; i < scheduledOrdersEntities.size(); i++) {
                        if (order_type == scheduledOrdersEntities.get(i).getOrderType()) {
                            if (!startdates.contains(scheduledOrdersEntities.get(i).getStart())) {

                                startdates.add(scheduledOrdersEntities.get(i).getStart());

                            }
                        }
                    }

                    Log.i("TAG", "onResponse: " + startdates);

                    int drawableId = Integer.parseInt(mimg.getTag().toString());

                    List<EventDay> eventDays = new ArrayList<>();

                    for (int j = 0; j < startdates.size(); j++) {

                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                        Date date = null;
                        try {
                            date = dateFormat.parse(startdates.get(j));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        calendar.setTime(date);
                        Log.i("TAG", "onResponse: " + calendar);
                        int ic_notify = getResources().getIdentifier("notification_icon2x_" + appColor, "drawable", getPackageName());
                        eventDays.add(new EventDay(calendar, getResources().getDrawable(ic_notify)));
                    }

                    calendarView.setEvents(eventDays);

                    Calendar min = Calendar.getInstance();
                    Calendar max = Calendar.getInstance();

                    min.add(Calendar.DAY_OF_MONTH, -1);
                    max.add(Calendar.MONTH, 3);

                    calendarView.setMinimumDate(min);
                    calendarView.setMaximumDate(max);

                    mAdapter = new TimeTable_TimeslotAdapter(TimeTableMangActivity.this, timeSlot_invoice_nos);
                    time_table_list.setAdapter(mAdapter);
                } catch (Exception e) {
                    e.printStackTrace();

                    Toast.makeText(TimeTableMangActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

            }
        });

        delivery_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mservice_name.setText("Delivery Service");
                order_type = 2;

                time_table_list.setVisibility(View.GONE);

                pick_up_layout.setBackgroundColor(getResources().getColor(R.color.primaryColor));
                delivery_layout.setBackgroundColor(getResources().getColor(R.color.timetable));
                shiping_layout.setBackgroundColor(getResources().getColor(R.color.primaryColor));

                try {
                    startdates.clear();
                    timeslot.clear();
                    timeSlot_invoice_nos.clear();

                    for (int i = 0; i < scheduledOrdersEntities.size(); i++) {

                        if (!scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase("")) {
                            if (order_type == scheduledOrdersEntities.get(i).getOrderType()) {
                                if (!timeslot.contains(scheduledOrdersEntities.get(i).getTimeSlot())) {

                                    timeslot.add(scheduledOrdersEntities.get(i).getTimeSlot());
                                    Log.i("TAG", "onResponse: " + timeslot);

                                    TimeSlot_Invoice_no timeTableMang = new TimeSlot_Invoice_no();
                                    timeTableMang.setmTimeSlot(scheduledOrdersEntities.get(i).getTimeSlot());
                                    timeTableMang.setExp1(scheduledOrdersEntities.get(i).getExpired());
                                    timeTableMang.setOrder_id1(scheduledOrdersEntities.get(i).getOrderId());

                                    ArrayList<TimeTable_Invoiceno> timeTable_invoicenos = new ArrayList<>();

                                    if (timeslot.contains(scheduledOrdersEntities.get(i).getTimeSlot())) {

                                        for (int j = 0; j < scheduledOrdersEntities.size(); j++) {
                                            if (!scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase("")) {
                                                if (scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase(scheduledOrdersEntities.get(j).getTimeSlot())) {
                                                    TimeTable_Invoiceno invoicenos = new TimeTable_Invoiceno();
                                                    invoicenos.setInvoice_no(scheduledOrdersEntities.get(j).getInvoiceNo());
                                                    invoicenos.setExp(scheduledOrdersEntities.get(j).getExpired());
                                                    invoicenos.setOrder_id(scheduledOrdersEntities.get(j).getOrderId());
                                                    Log.i("TAG", "invoice_no: " + invoicenos.getInvoice_no());
                                                    timeTable_invoicenos.add(invoicenos);
                                                }
                                            }
                                        }

                                        timeTableMang.setInvoicenos(timeTable_invoicenos);
                                    }

                                    timeSlot_invoice_nos.add(timeTableMang);
                                }
                            }
                        }
                    }

                    for (int i = 0; i < scheduledOrdersEntities.size(); i++) {
                        if (order_type == scheduledOrdersEntities.get(i).getOrderType()) {
                            if (!startdates.contains(scheduledOrdersEntities.get(i).getStart())) {

                                startdates.add(scheduledOrdersEntities.get(i).getStart());
                            }
                        }
                    }

                    Log.i("TAG", "onResponse: " + startdates);

                    int drawableId = Integer.parseInt(mimg.getTag().toString());

                    List<EventDay> eventDays = new ArrayList<>();

                    for (int j = 0; j < startdates.size(); j++) {

                        Calendar calendar = Calendar.getInstance();

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                        Date date = null;

                        try {
                            date = dateFormat.parse(startdates.get(j));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        calendar.setTime(date);
                        Log.i("TAG", "onResponse: " + calendar);
                        int ic_notify = getResources().getIdentifier("notification_icon2x_" + appColor, "drawable", getPackageName());
                        eventDays.add(new EventDay(calendar, getResources().getDrawable(ic_notify)));
                    }

                    calendarView.setEvents(eventDays);

                    Calendar min = Calendar.getInstance();
                    Calendar max = Calendar.getInstance();

                    min.add(Calendar.DAY_OF_MONTH, -1);
                    max.add(Calendar.MONTH, 3);

                    calendarView.setMinimumDate(min);
                    calendarView.setMaximumDate(max);

                    mAdapter = new TimeTable_TimeslotAdapter(TimeTableMangActivity.this, timeSlot_invoice_nos);
                    time_table_list.setAdapter(mAdapter);

                } catch (Exception e) {
                    e.printStackTrace();

                    Toast.makeText(TimeTableMangActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

            }
        });

        shiping_serivce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mservice_name.setText("Shipping Service");
                order_type = 3;

                time_table_list.setVisibility(View.GONE);

                pick_up_layout.setBackgroundColor(getResources().getColor(R.color.primaryColor));
                delivery_layout.setBackgroundColor(getResources().getColor(R.color.primaryColor));
                shiping_layout.setBackgroundColor(getResources().getColor(R.color.timetable));

                try {
                    startdates.clear();
                    timeslot.clear();
                    timeSlot_invoice_nos.clear();

                    for (int i = 0; i < scheduledOrdersEntities.size(); i++) {

                        if (!scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase("")) {
                            if (order_type == scheduledOrdersEntities.get(i).getOrderType()) {
                                if (!timeslot.contains(scheduledOrdersEntities.get(i).getTimeSlot())) {

                                    timeslot.add(scheduledOrdersEntities.get(i).getTimeSlot());
                                    Log.i("TAG", "onResponse: " + timeslot);

                                    TimeSlot_Invoice_no timeTableMang = new TimeSlot_Invoice_no();
                                    timeTableMang.setmTimeSlot(scheduledOrdersEntities.get(i).getTimeSlot());
                                    timeTableMang.setExp1(scheduledOrdersEntities.get(i).getExpired());
                                    timeTableMang.setOrder_id1(scheduledOrdersEntities.get(i).getOrderId());

                                    ArrayList<TimeTable_Invoiceno> timeTable_invoicenos = new ArrayList<>();

                                    if (timeslot.contains(scheduledOrdersEntities.get(i).getTimeSlot())) {

                                        for (int j = 0; j < scheduledOrdersEntities.size(); j++) {
                                            if (!scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase("")) {
                                                if (scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase(scheduledOrdersEntities.get(j).getTimeSlot())) {
                                                    TimeTable_Invoiceno invoicenos = new TimeTable_Invoiceno();
                                                    invoicenos.setInvoice_no(scheduledOrdersEntities.get(j).getInvoiceNo());
                                                    invoicenos.setExp(scheduledOrdersEntities.get(j).getExpired());
                                                    invoicenos.setOrder_id(scheduledOrdersEntities.get(j).getOrderId());
                                                    Log.i("TAG", "invoice_no: " + invoicenos.getInvoice_no());
                                                    timeTable_invoicenos.add(invoicenos);
                                                }
                                            }
                                        }

                                        timeTableMang.setInvoicenos(timeTable_invoicenos);
                                    }

                                    timeSlot_invoice_nos.add(timeTableMang);
                                }
                            }
                        }
                    }

                    for (int i = 0; i < scheduledOrdersEntities.size(); i++) {
                        if (order_type == scheduledOrdersEntities.get(i).getOrderType()) {
                            if (!startdates.contains(scheduledOrdersEntities.get(i).getStart())) {

                                startdates.add(scheduledOrdersEntities.get(i).getStart());
                            }
                        }

                    }

                    Log.i("TAG", "onResponse: " + startdates);

                    int drawableId = Integer.parseInt(mimg.getTag().toString());


                    List<EventDay> eventDays = new ArrayList<>();


                    for (int j = 0; j < startdates.size(); j++) {

                        Calendar calendar = Calendar.getInstance();

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                        Date date = null;

                        try {
                            date = dateFormat.parse(startdates.get(j));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        calendar.setTime(date);
                        Log.i("TAG", "onResponse: " + calendar);
                        int ic_notify = getResources().getIdentifier("notification_icon2x_" + appColor, "drawable", getPackageName());
                        eventDays.add(new EventDay(calendar, getResources().getDrawable(ic_notify)));
                    }


                    calendarView.setEvents(eventDays);

                    Calendar min = Calendar.getInstance();
                    Calendar max = Calendar.getInstance();

                    min.add(Calendar.DAY_OF_MONTH, -1);
                    max.add(Calendar.MONTH, 3);

                    calendarView.setMinimumDate(min);
                    calendarView.setMaximumDate(max);

                    mAdapter = new TimeTable_TimeslotAdapter(TimeTableMangActivity.this, timeSlot_invoice_nos);
                    time_table_list.setAdapter(mAdapter);

                } catch (Exception e) {
                    e.printStackTrace();

                    Toast.makeText(TimeTableMangActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
            }
        });

        new TimeTableMangResponse().execute();

    }

    private class TimeTableMangResponse extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            Constants.showLoadingDialog(TimeTableMangActivity.this);
            timeSlot_invoice_nos.clear();
            timeslot.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<TimeTableMang> call = apiService.getTimetablemang(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<TimeTableMang>() {
                @Override
                public void onResponse(Call<TimeTableMang> call, Response<TimeTableMang> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        TimeTableMang timeTableMang1 = response.body();
                        scheduledOrdersEntities = timeTableMang1.getData().getScheduledOrders();
                        try {

                            startdates.clear();
                            timeslot.clear();
                            timeSlot_invoice_nos.clear();

                            for (int i = 0; i < scheduledOrdersEntities.size(); i++) {

                                if (!scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase("")) {
                                    if (order_type == scheduledOrdersEntities.get(i).getOrderType()) {
                                        if (!timeslot.contains(scheduledOrdersEntities.get(i).getTimeSlot())) {

                                            timeslot.add(scheduledOrdersEntities.get(i).getTimeSlot());
                                            Log.i("TAG", "onResponse: " + timeslot);

                                            TimeSlot_Invoice_no timeTableMang = new TimeSlot_Invoice_no();
                                            timeTableMang.setmTimeSlot(scheduledOrdersEntities.get(i).getTimeSlot());
                                            timeTableMang.setExp1(scheduledOrdersEntities.get(i).getExpired());
                                            timeTableMang.setOrder_id1(scheduledOrdersEntities.get(i).getOrderId());

                                            ArrayList<TimeTable_Invoiceno> timeTable_invoicenos = new ArrayList<>();

                                            if (timeslot.contains(scheduledOrdersEntities.get(i).getTimeSlot())) {

                                                for (int j = 0; j < scheduledOrdersEntities.size(); j++) {
                                                    if (!scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase("")) {
                                                        if (scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase(scheduledOrdersEntities.get(j).getTimeSlot())) {
                                                            TimeTable_Invoiceno invoicenos = new TimeTable_Invoiceno();
                                                            invoicenos.setInvoice_no(scheduledOrdersEntities.get(j).getInvoiceNo());
                                                            invoicenos.setExp(scheduledOrdersEntities.get(j).getExpired());
                                                            invoicenos.setOrder_id(scheduledOrdersEntities.get(j).getOrderId());
                                                            Log.i("TAG", "invoice_no: " + invoicenos.getInvoice_no());
                                                            timeTable_invoicenos.add(invoicenos);
                                                        }
                                                    }
                                                }

                                                timeTableMang.setInvoicenos(timeTable_invoicenos);
                                            }

                                            timeSlot_invoice_nos.add(timeTableMang);
                                        }
                                    }
                                }
                            }

                            for (int i = 0; i < scheduledOrdersEntities.size(); i++) {
                                if (order_type == scheduledOrdersEntities.get(i).getOrderType()) {
                                    if (!startdates.contains(scheduledOrdersEntities.get(i).getStart())) {

                                        startdates.add(scheduledOrdersEntities.get(i).getStart());
                                    }
                                }
                            }

                            Log.i("TAG", "onResponse: " + startdates);

                            int drawableId = Integer.parseInt(mimg.getTag().toString());

                            List<EventDay> eventDays = new ArrayList<>();

                            for (int j = 0; j < startdates.size(); j++) {

                                Calendar calendar = Calendar.getInstance();

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                                Date date = null;

                                try {
                                    date = dateFormat.parse(startdates.get(j));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                calendar.setTime(date);
                                Log.i("TAG", "onResponse: " + calendar);
                                int ic_notify = getResources().getIdentifier("notification_icon2x_" + appColor, "drawable", getPackageName());
                                eventDays.add(new EventDay(calendar, getResources().getDrawable(ic_notify)));
                            }

                            calendarView.setEvents(eventDays);
                            calendarView.setEnabled(true);

                            Calendar min = Calendar.getInstance();
                            Calendar max = Calendar.getInstance();

                            min.add(Calendar.DAY_OF_MONTH, -1);
                            max.add(Calendar.MONTH, 3);

                            calendarView.setMinimumDate(min);
                            calendarView.setMaximumDate(max);

                            mAdapter = new TimeTable_TimeslotAdapter(TimeTableMangActivity.this, timeSlot_invoice_nos);
                            time_table_list.setAdapter(mAdapter);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(TimeTableMangActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        Constants.closeLoadingDialog();
                    } else {
                        Toast.makeText(TimeTableMangActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "onResponse: ");
                    }
                }

                @Override
                public void onFailure(Call<TimeTableMang> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(TimeTableMangActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(TimeTableMangActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(TimeTableMangActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareResetPasswordJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("BranchId", branch_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareResetPasswordJson: " + parentObj.toString());
        return parentObj.toString();
    }

    private OnSelectDateListener listener = new OnSelectDateListener() {
        @Override
        public void onSelect(List<Calendar> calendars) {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            Date date = null;

            String date1;

            date1 = dateFormat.format(clickedDayCalendar.getTime());

            try {
                date = dateFormat.parse(date1);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            calendars = (List<Calendar>) clickedDayCalendar;

            try {
                startdates.clear();
                timeslot.clear();
                timeSlot_invoice_nos.clear();

                for (int i = 0; i < scheduledOrdersEntities.size(); i++) {

                    if (!scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase("")) {
                        if (order_type == scheduledOrdersEntities.get(i).getOrderType()) {
                            if (date1.equalsIgnoreCase(scheduledOrdersEntities.get(i).getStart())) {
                                if (!timeslot.contains(scheduledOrdersEntities.get(i).getTimeSlot())) {

                                    timeslot.add(scheduledOrdersEntities.get(i).getTimeSlot());
                                    Log.i("TAG", "onResponse: " + timeslot);

                                    TimeSlot_Invoice_no timeTableMang = new TimeSlot_Invoice_no();
                                    timeTableMang.setmTimeSlot(scheduledOrdersEntities.get(i).getTimeSlot());
                                    timeTableMang.setExp1(scheduledOrdersEntities.get(i).getExpired());
                                    timeTableMang.setOrder_id1(scheduledOrdersEntities.get(i).getOrderId());

                                    ArrayList<TimeTable_Invoiceno> timeTable_invoicenos = new ArrayList<>();

                                    if (timeslot.contains(scheduledOrdersEntities.get(i).getTimeSlot())) {

                                        for (int k = 0; k < scheduledOrdersEntities.size(); k++) {
                                            if (!scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase("")) {
                                                if (scheduledOrdersEntities.get(i).getOrderType() == scheduledOrdersEntities.get(k).getOrderType()) {
                                                    if (scheduledOrdersEntities.get(i).getStart().equalsIgnoreCase(scheduledOrdersEntities.get(k).getStart())) {
                                                        if (scheduledOrdersEntities.get(i).getTimeSlot().equalsIgnoreCase(scheduledOrdersEntities.get(k).getTimeSlot())) {
                                                            TimeTable_Invoiceno invoicenos = new TimeTable_Invoiceno();
                                                            invoicenos.setInvoice_no(scheduledOrdersEntities.get(k).getInvoiceNo());
                                                            invoicenos.setExp(scheduledOrdersEntities.get(k).getExpired());
                                                            invoicenos.setOrder_id(scheduledOrdersEntities.get(k).getOrderId());
                                                            Log.i("TAG", "invoice_no: " + invoicenos.getInvoice_no());
                                                            timeTable_invoicenos.add(invoicenos);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        timeTableMang.setInvoicenos(timeTable_invoicenos);
                                    }

                                    timeSlot_invoice_nos.add(timeTableMang);
                                }

                            }
                        }
                    }
                }

                time_table_list.setVisibility(View.VISIBLE);

                mAdapter = new TimeTable_TimeslotAdapter(TimeTableMangActivity.this, timeSlot_invoice_nos);
                time_table_list.setAdapter(mAdapter);

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(TimeTableMangActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
            }
        }
    };
}
