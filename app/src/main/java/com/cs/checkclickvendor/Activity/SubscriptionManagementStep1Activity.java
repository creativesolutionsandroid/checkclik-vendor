package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.cs.checkclickvendor.Adapters.SubscriptionAdapter1;
import com.cs.checkclickvendor.Models.SubscriptionPackageResponce;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Activity.ForgotPasswordActivity.TAG;
import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

//*********Puli********//
public class SubscriptionManagementStep1Activity extends AppCompatActivity {

    ImageView backbtn;
    RecyclerView packagelistview;
    ArrayList<SubscriptionPackageResponce.DataEntity> subpacklist = new ArrayList<>();
    SubscriptionAdapter1 madapter;
    private int branch_id;
    SharedPreferences userPrefs;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subscription_step1_activity);
        packagelistview = (RecyclerView) findViewById(R.id.packagelist);
        backbtn = (ImageView) findViewById(R.id.back_btn);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        branch_id = getIntent().getIntExtra("branch_id", 0);

        new subpackageApi().execute();

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_back_btn = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
                backbtn.setImageDrawable(getResources().getDrawable(ic_back_btn));
            }
        }

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private class subpackageApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparecatJson();
            Constants.showLoadingDialog(SubscriptionManagementStep1Activity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SubscriptionManagementStep1Activity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<SubscriptionPackageResponce> call = apiService.getsubpacks(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SubscriptionPackageResponce>() {
                @Override
                public void onResponse(Call<SubscriptionPackageResponce> call, Response<SubscriptionPackageResponce> response) {
                    if (response.isSuccessful()) {
                        SubscriptionPackageResponce Catresponce = response.body();
                        try {
                            if (Catresponce.getStatus()) {

                                SubscriptionPackageResponce Response = response.body();
                                subpacklist = Response.getData();
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SubscriptionManagementStep1Activity.this);
                                packagelistview.setLayoutManager(mLayoutManager);
                                madapter = new SubscriptionAdapter1(SubscriptionManagementStep1Activity.this, subpacklist);
                                packagelistview.setAdapter(madapter);

                            } else {

                                String failureResponse = Catresponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), SubscriptionManagementStep1Activity.this);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(SubscriptionManagementStep1Activity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SubscriptionManagementStep1Activity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<SubscriptionPackageResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(SubscriptionManagementStep1Activity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SubscriptionManagementStep1Activity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String preparecatJson() {
        JSONObject parentObj = new JSONObject();
        try {

            parentObj.put("Id", branch_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "preparecatJson: " + parentObj);

        return parentObj.toString();
    }

}
