package com.cs.checkclickvendor.Activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Adapters.SideMenuAdapter;
import com.cs.checkclickvendor.CustomView.CircleImageView;
import com.cs.checkclickvendor.Fragment.AccessManagementFragment;
import com.cs.checkclickvendor.Fragment.AdvertisementFragment;
import com.cs.checkclickvendor.Fragment.CometChatFragment;
import com.cs.checkclickvendor.Fragment.DashBoardFragment;
import com.cs.checkclickvendor.Fragment.OrderFragment;
import com.cs.checkclickvendor.Fragment.ProductManagementFragment;
import com.cs.checkclickvendor.Fragment.ProfileFragment;
import com.cs.checkclickvendor.Fragment.RevenueFragment;
import com.cs.checkclickvendor.Fragment.SettingsFragment;
import com.cs.checkclickvendor.Fragment.SubscriptionManagementFragment;
import com.cs.checkclickvendor.Fragment.TimeTableFragment;
import com.cs.checkclickvendor.Fragment.VoucherFragment;
import com.cs.checkclickvendor.Models.UserStores;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.BranchColor;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class MainActivity extends AppCompatActivity {

    public static DrawerLayout drawer;
    private DrawerLayout mDrawerLayout;
    SideMenuAdapter mSideMenuAdapter;
    LinearLayout mDrawerLinear;
    ArrayList<String> sideMenuItems = new ArrayList<>();
    ArrayList<Integer> sideMenuImages = new ArrayList<>();
    ListView sideMenuListView;
    int itemSelectedPostion = 0;
    TextView userName;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    private static final String TAG = "TAG";
    Boolean settingfragment;
    BottomNavigationView navigation;
    private int userType = 1;
    private ArrayList<UserStores.MenuListEntity> menuListFromService = new ArrayList<>();

    com.lovejjfg.shadowcircle.CircleImageView profile_pic;

    FragmentManager fragmentManager = getSupportFragmentManager();
    int currentSelectedTab = 1; // 1 = dashboard, 2 = order, 3 = revenue, 4 = chat, 5 = profile


    UserStores.StoreListEntity storeListEntities;
    ArrayList<UserStores.StoreListEntity> allStoresList = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        userName = (TextView) findViewById(R.id.username);

        profile_pic = (com.lovejjfg.shadowcircle.CircleImageView) findViewById(R.id.profile_pic);

        userName.setText(userPrefs.getString("name", null));
        userType = userPrefs.getInt("userType", 1);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        disableShiftMode(navigation);
        navigation.setItemIconTintList(null);


        if (userPrefs.getInt("storeId", 0) == 0) {

            new getStoresApi().execute();

        } else {

            Gson gson1 = new Gson();
            String json1 = userPrefs.getString("store", null);
            Type type = new TypeToken<UserStores.StoreListEntity>() {
            }.getType();
            storeListEntities = gson1.fromJson(json1, type);

            BranchColor branchColor = new BranchColor();
            appColor = branchColor.BranchColor(storeListEntities.getPanelColor());
            Log.i("TAG", "onResponse: " + appColor);

        }

        if (appColor != null){

            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_profile = getResources().getIdentifier("default_user3x_" + appColor, "drawable", getPackageName());
                profile_pic.setImageDrawable(getResources().getDrawable(ic_profile));
            }

        }

        if (userType == 3) {
            prepareRecipientMenu();
            navigation.setVisibility(View.GONE);
        } else {
            new getSideMenuList().execute();
            navigation.setVisibility(View.VISIBLE);
        }

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_dashbord:
                    if (currentSelectedTab != 1) {
                        currentSelectedTab = 1;
                        Fragment dashbord_fragment = new DashBoardFragment();
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, dashbord_fragment).commit();
                        return true;
                    } else {
                        return false;
                    }
                case R.id.navigation_order:
                    if (currentSelectedTab != 2) {
                        currentSelectedTab = 2;
                        Fragment order_fragment = new OrderFragment();
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, order_fragment).commit();
                        return true;
                    } else {
                        return false;
                    }
                case R.id.navigation_revenue:
                    if (currentSelectedTab != 3) {
                        currentSelectedTab = 3;
                        Fragment revenueFragment = new RevenueFragment();
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, revenueFragment).commit();
                        return true;
                    } else {
                        return false;
                    }
                case R.id.navigation_chat:
                    if (currentSelectedTab != 4) {
                        currentSelectedTab = 4;
                        Fragment chatingFragment = new CometChatFragment();
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, chatingFragment).commit();
                        return true;
                    } else {
                        return false;
                    }
                case R.id.navigation_profile:
                    if (currentSelectedTab != 5) {
                        currentSelectedTab = 5;
                        Fragment profirfrgment = new ProfileFragment();
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, profirfrgment).commit();
                        return true;
                    } else {
                        return false;
                    }
            }
            return false;
        }
    };

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (sideMenuItems.get(position).equalsIgnoreCase("Dashboard")) {
                Fragment dashfragmet = new DashBoardFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, dashfragmet).commit();
                navigation.setVisibility(View.VISIBLE);
                navigation.getMenu().findItem(R.id.navigation_dashbord).setChecked(true);
            } else if (sideMenuItems.get(position).equalsIgnoreCase("Time Table Management")) {
                Fragment timefragmet = new TimeTableFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, timefragmet).commit();
                navigation.setVisibility(View.GONE);
            } else if (sideMenuItems.get(position).equalsIgnoreCase("Product Management")) {
                Fragment productfragmet = new ProductManagementFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, productfragmet).commit();
                navigation.setVisibility(View.GONE);
            } else if (sideMenuItems.get(position).equalsIgnoreCase("Access Management")) {
                Fragment accessfragmet = new AccessManagementFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, accessfragmet).commit();
                navigation.setVisibility(View.GONE);
            } else if (sideMenuItems.get(position).equalsIgnoreCase("Advertisement Management")) {
                Fragment advefragement = new AdvertisementFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, advefragement).commit();
                navigation.setVisibility(View.GONE);
            } else if (sideMenuItems.get(position).equalsIgnoreCase("Voucher Management")) {
                Fragment voucherfragmet = new VoucherFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, voucherfragmet).commit();
                navigation.setVisibility(View.GONE);
            } else if (sideMenuItems.get(position).equalsIgnoreCase("Subscription Management")) {
                Fragment subscriptionFragmet = new SubscriptionManagementFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, subscriptionFragmet).commit();
                navigation.setVisibility(View.GONE);
            } else if (sideMenuItems.get(position).equalsIgnoreCase("Settings")) {
                Fragment inboxfrahmet = new SettingsFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, inboxfrahmet).commit();
                navigation.setVisibility(View.GONE);
            } else if (sideMenuItems.get(position).equalsIgnoreCase("Contact Us")) {

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("plain/text");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
//                i.putExtra(Intent.EXTRA_SUBJECT, "Bakery & Company Experience");
//                i.putExtra(Intent.EXTRA_TITLE  , "Bakery & Company Experience");
                final PackageManager pm = getPackageManager();
                final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
                String className = null;
                for (final ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                        className = info.activityInfo.name;

                        if (className != null && !className.isEmpty()) {
                            break;
                        }
                    }
                }
                i.setClassName("com.google.android.gm", className);
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(MainActivity.this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                }

            }
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @SuppressLint("RestrictedApi")
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setPadding(0, 15, 0, 0);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    public void menuClick() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private class getSideMenuList extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(MainActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UserStores> call = apiService.getUserStores(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UserStores>() {
                @Override
                public void onResponse(Call<UserStores> call, Response<UserStores> response) {
                    if (response.isSuccessful()) {
                        Constants.closeLoadingDialog();
                        UserStores VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getData().getMenuList().size() > 0) {
                                menuListFromService = VerifyMobileResponse.getData().getMenuList();
                                prepareVendorAccessMenu();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserStores> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(MainActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userPrefs.getString("userId", "14"));
            parentObj.put("UserType", userPrefs.getInt("userType", 1));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    private void prepareVendorAccessMenu() {
        ArrayList<String> menuitem = new ArrayList<>();
        for (int i = 0; i < menuListFromService.size(); i++) {
            if (menuListFromService.get(i).getAppMenu()) {
                if (!menuitem.contains(menuListFromService.get(i).getMenuNameEn())) {
                    menuitem.add(menuListFromService.get(i).getMenuNameEn());
                    sideMenuItems.add(menuListFromService.get(i).getMenuNameEn());
                    sideMenuImages.add(fetchImage(menuListFromService.get(i).getMenuNameEn()));
                }
            }
        }

        sideMenuItems.add("Settings");
        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_settings = getResources().getIdentifier("settings2x_" + appColor, "drawable", getPackageName());
                sideMenuImages.add(ic_settings);
                sideMenuItems.add("Contact Us");

                int ic_contactId = getResources().getIdentifier("contact2x_" + appColor, "drawable", getPackageName());
                sideMenuImages.add(ic_contactId);
            }
        } else {
            sideMenuImages.add(R.drawable.settings2x);
            sideMenuItems.add("Contact Us");

            sideMenuImages.add(R.drawable.contact2x);
        }

        setSideMenu();
    }

    private void prepareRecipientMenu() {
        sideMenuItems.add("Product Management");
        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_prduct_normalId = getResources().getIdentifier("productnormal2x_" + appColor, "drawable-hdpi", getPackageName());
                sideMenuImages.add(ic_prduct_normalId);
                sideMenuItems.add("Profile Management");

                int ic_profile = getResources().getIdentifier("profilenormal2x_" + appColor, "drawable-hdpi", getPackageName());
                sideMenuImages.add(ic_profile);
                sideMenuItems.add("Settings");

                int ic_settings = getResources().getIdentifier("settings2x_" + appColor, "drawable-hdpi", getPackageName());
                sideMenuImages.add(ic_settings);
                sideMenuItems.add("Contact Us");

                int ic_contact = getResources().getIdentifier("contact2x_" + appColor, "drawable-hdpi", getPackageName());
                sideMenuImages.add(ic_contact);
            }
        } else {

            sideMenuImages.add(R.drawable.productnormal2x);
            sideMenuItems.add("Profile Management");

            sideMenuImages.add(R.drawable.profilenormal2x);
            sideMenuItems.add("Settings");

            sideMenuImages.add(R.drawable.settings2x);
            sideMenuItems.add("Contact Us");

            sideMenuImages.add(R.drawable.contact2x);
        }
        setSideMenu();
    }

    private Integer fetchImage(String menu) {
        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                if (menu.equalsIgnoreCase("Dashboard")) {
                    int ic_dashboard_selected = getResources().getIdentifier("dashboard_selected_" + appColor, "drawable", getPackageName());
                    return ic_dashboard_selected;
                } else if (menu.equalsIgnoreCase("Time Table Management")) {
                    int ic_date = getResources().getIdentifier("date_" + appColor, "drawable", getPackageName());
                    Log.i("TAG", "fetchImage: " + "date_" + appColor);
                    return ic_date;
                } else if (menu.equalsIgnoreCase("Product Management")) {
                    int ic_productnormal2x = getResources().getIdentifier("productnormal2x_" + appColor, "drawable", getPackageName());
                    return ic_productnormal2x;
                } else if (menu.equalsIgnoreCase("Access Management")) {
                    int ic_key2x = getResources().getIdentifier("key2x_" + appColor, "drawable", getPackageName());
                    return ic_key2x;
                } else if (menu.equalsIgnoreCase("Advertisement Management")) {
                    int ic_ads2x = getResources().getIdentifier("ads2x_" + appColor, "drawable", getPackageName());
                    return ic_ads2x;
                } else if (menu.equalsIgnoreCase("Voucher Management")) {
                    int ic_voucher2x = getResources().getIdentifier("voucher2x_" + appColor, "drawable", getPackageName());
                    return ic_voucher2x;
                } else if (menu.equalsIgnoreCase("Subscription Management")) {
                    int ic_subs2x = getResources().getIdentifier("subs2x_" + appColor, "drawable", getPackageName());
                    return ic_subs2x;
                }
            }
        } else {

            if (menu.equalsIgnoreCase("Dashboard")) {
                int ic_dashboard_selected = getResources().getIdentifier("dashboard_selected", "drawable", getPackageName());
                return ic_dashboard_selected;
            } else if (menu.equalsIgnoreCase("Time Table Management")) {
                int ic_date = getResources().getIdentifier("date", "drawable", getPackageName());
                Log.i("TAG", "fetchImage: " + "date_" + appColor);
                return ic_date;
            } else if (menu.equalsIgnoreCase("Product Management")) {
                int ic_productnormal2x = getResources().getIdentifier("productnormal2x", "drawable", getPackageName());
                return ic_productnormal2x;
            } else if (menu.equalsIgnoreCase("Access Management")) {
                int ic_key2x = getResources().getIdentifier("key2x", "drawable", getPackageName());
                return ic_key2x;
            } else if (menu.equalsIgnoreCase("Advertisement Management")) {
                int ic_ads2x = getResources().getIdentifier("ads2x", "drawable", getPackageName());
                return ic_ads2x;
            } else if (menu.equalsIgnoreCase("Voucher Management")) {
                int ic_voucher2x = getResources().getIdentifier("voucher2x", "drawable", getPackageName());
                return ic_voucher2x;
            } else if (menu.equalsIgnoreCase("Subscription Management")) {
                int ic_subs2x = getResources().getIdentifier("subs2x", "drawable", getPackageName());
                return ic_subs2x;
            }

        }
        return R.drawable.applogo2x;
    }

    private void setSideMenu() {
        sideMenuListView = (ListView) findViewById(R.id.side_menu_list_view);
        mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);

        sideMenuListView.setAdapter(mSideMenuAdapter);
        sideMenuListView.setOnItemClickListener(new DrawerItemClickListener());

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        DashBoardFragment homeScreenFragment = new DashBoardFragment();
        fragmentTransaction.add(R.id.fragment_layout, homeScreenFragment);
        fragmentTransaction.commit();
    }

    private class getStoresApi extends AsyncTask<String, String, String> {

        String inputStr;
        AlertDialog customDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson1();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.loading_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

//        ImageView loadingImage = (ImageView) dialogView.findViewById(R.id.loading);
//        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);
//        Glide.with(context).asGif().load(R.raw.loading_icon).into(loadingImage);

            CamomileSpinner spinner1 = (CamomileSpinner) dialogView.findViewById(R.id.spinner1);
            spinner1.start();

            spinner1.recreateWithParams(
                    MainActivity.this,
                    DialogUtils.getColor(MainActivity.this, R.color.black),
                    120,
                    true
            );

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the progressDialog take up the full width
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.45;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UserStores> call = apiService.getUserStores(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UserStores>() {
                @Override
                public void onResponse(Call<UserStores> call, Response<UserStores> response) {
                    if (response.isSuccessful()) {
                        if (customDialog != null) {
                            customDialog.dismiss();
                        }
                        UserStores VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getData().getStoreList().size() > 0) {
                                allStoresList = VerifyMobileResponse.getData().getStoreList();
                                userPrefsEditor.putInt("storeId", allStoresList.get(0).getId());

                                Gson gson = new Gson();
                                String json = gson.toJson(allStoresList.get(0));

                                userPrefsEditor.putString("store", json);
                                userPrefsEditor.commit();

                                Gson gson1 = new Gson();
                                String json1 = userPrefs.getString("store", null);
                                Type type = new TypeToken<UserStores.StoreListEntity>() {
                                }.getType();
                                storeListEntities = gson1.fromJson(json1, type);

                                BranchColor branchColor = new BranchColor();
                                appColor = branchColor.BranchColor(storeListEntities.getPanelColor());
                                Log.i("TAG", "onResponse: " + appColor);


                            } else {

                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserStores> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(MainActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    if (customDialog != null) {
                        customDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson1() {

        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userPrefs.getString("userId", ""));
            parentObj.put("UserType", userPrefs.getInt("userType", 1));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareVerifyMobileJson: " + parentObj.toString());

        return parentObj.toString();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

}