package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.cs.checkclickvendor.Models.VerifyMobileResponse;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class ForgotPasswordActivity extends AppCompatActivity {

    ImageView back_btn;
    EditText inputMobile;
    Button send;
    String strMobile;
    private TextInputLayout inputLayoutMobile;
    public static boolean isOTPSuccessful = false;
    public static boolean isResetSuccessful = false;
    private String serverOtp;
    public static final String TAG = "TAG";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgotpassword_activity);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        inputMobile = (EditText) findViewById(R.id.forgot_input_phone);
        send = (Button) findViewById(R.id.bt_continue);
        inputLayoutMobile = (TextInputLayout) findViewById(R.id.layout_forgot_input_phone);

//        int ic_back_btn = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
//        back_btn.setImageDrawable(getResources().getDrawable(ic_back_btn));
//
//        int ic_color = getResources().getIdentifier("C" + appColor, "color", getPackageName());
//        Drawable background = send.getBackground();
//
//        GradientDrawable shapeDrawable = (GradientDrawable) background;
//        shapeDrawable.setColor(ContextCompat.getColor(ForgotPasswordActivity.this, ic_color));

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        inputMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                if (text.matches("[0-9]+") && !text.contains("+966") && text.length() >= 1 && text.length() <= 9) {
                    inputMobile.removeTextChangedListener(this);
                    inputMobile.setText("+966 " + text);
                    inputMobile.setSelection(inputMobile.length());
                    inputMobile.addTextChangedListener(this);
                } else {
                    String textCopy = text;
                    textCopy = textCopy.replace("+966 ", "");
                    if (textCopy.matches("[0-9]+") && textCopy.length() <= 9) {

                    } else {
                        inputMobile.removeTextChangedListener(this);
                        text = text.replace("+966 ", "");
                        inputMobile.setText(text);
                        inputMobile.setSelection(inputMobile.length());
                        inputMobile.addTextChangedListener(this);
                    }
                }
            }
        });


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPasswordActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new verifyMobileApi().execute();
                    } else {
                        Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });

    }

    private void clearErrors() {
        inputLayoutMobile.setErrorEnabled(false);
    }

    private class MyTextWatcher implements TextWatcher {
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.layout_forgot_input_phone:
                    clearErrors();
                    break;
            }
        }
    }

    private boolean validations() {
        strMobile = inputMobile.getText().toString();
//        strMobile = strMobile.replace("+966 ","");

        if (strMobile.length() == 0) {
            inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));

            Constants.requestEditTextFocus(inputMobile, ForgotPasswordActivity.this);
            return false;
        }
        return true;
    }


//    private void displayResetPasswordDiaolg(){
//        Bundle args = new Bundle();
//        args.putString("mobile", strMobile);
//
//        isResetSuccessful = false;
//
//        final ResetPasswordActivity newFragment = ResetPasswordActivity.newInstance();
//        newFragment.setCancelable(false);
//        newFragment.setArguments(args);
//        newFragment.show(getSupportFragmentManager(), "forgot");
//
//        getSupportFragmentManager().executePendingTransactions();
//        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//                //do whatever you want when dialog is dismissed
//                if (newFragment != null) {
//                    newFragment.dismiss();
//                }
//
//                if(isResetSuccessful){
//                    setResult(RESULT_OK);
//                    finish();
//                }
//            }
//        });
//    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(ForgotPasswordActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPasswordActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyMobileResponse VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                Log.d(TAG, "OTP: " + VerifyMobileResponse.getData().getOTPCode());
                                showVerifyDialog(VerifyMobileResponse.getData().getOTPCode());
                            } else {
                                String failureResponse = VerifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), ForgotPasswordActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ForgotPasswordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            if (strMobile.contains("966")) {
                parentObj.put("MobileNo", strMobile.replace(" ",""));
                parentObj.put("FlagId", "2");
            } else {
                parentObj.put("MobileNo", strMobile);
                parentObj.put("FlagId", "4");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    private void showVerifyDialog(String otp) {
        Intent intent = new Intent(ForgotPasswordActivity.this, VerifyOTPActivity.class);
        intent.putExtra("screen", "forgot");
        intent.putExtra("MobileNo1", strMobile);
        intent.putExtra("OTP1", otp);
        startActivity(intent);
    }
}
