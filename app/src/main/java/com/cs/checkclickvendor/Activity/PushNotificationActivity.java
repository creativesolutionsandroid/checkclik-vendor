package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


import com.cs.checkclickvendor.Adapters.PushNotificationAdapteer;
import com.cs.checkclickvendor.Models.PushNotificationResponce;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class PushNotificationActivity extends AppCompatActivity {

    String TAG = "TAG";
    Switch order, customer, subscription, chatting, alert;
    Button done;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences userPrefs;
    ImageView back_btn;
    String strUserid;
    ListView pushlist;
    Boolean st1, st2, st3, st4, st5;
    private PushNotificationAdapteer mPushAdapter;
    int pos;
    public static ArrayList<PushNotificationResponce.PNConfig> pushnotificationarry = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pushnotification);
        userPrefs = PushNotificationActivity.this.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        strUserid = userPrefs.getString("userId", null);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        done = (Button) findViewById(R.id.pushdone);

        pushlist = (ListView) findViewById(R.id.pushlist);
        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_close = getResources().getIdentifier("close2x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_close));

                done.setBackgroundColor(Color.parseColor("#" + appColor));
            }
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        new Pushnotificationapi().execute();

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new preparepushsetapi().execute();
            }
        });
    }

    private class Pushnotificationapi extends AsyncTask<String, Integer, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparepushnotifactionJson();
            Constants.showLoadingDialog(PushNotificationActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<PushNotificationResponce> call = apiService.getpnotifaction(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<PushNotificationResponce>() {
                @Override
                public void onResponse(Call<PushNotificationResponce> call, Response<PushNotificationResponce> response) {
                    Log.d(TAG, "pushResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            pushnotificationarry = response.body().getData().getPNConfig();
                            Log.d(TAG, "cpushArryLists: " + pushnotificationarry.size());

                            mPushAdapter = new PushNotificationAdapteer(PushNotificationActivity.this, pushnotificationarry);
                            pushlist.setAdapter(mPushAdapter);
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), PushNotificationActivity.this);
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<PushNotificationResponce> call, Throwable t) {
                    Log.d(TAG, "pushFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(PushNotificationActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        Toast.makeText(PushNotificationActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(PushNotificationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String preparepushnotifactionJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id", 48);
            parentObj.put("FlagId", 2);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "preparepushnotifactionJson: " + parentObj);
        return parentObj.toString();
    }


    private class preparepushsetapi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparepushsetJson();
            Constants.showLoadingDialog(PushNotificationActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<PushNotificationResponce> call = apiService.getpnotifaction(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<PushNotificationResponce>() {
                @Override
                public void onResponse(Call<PushNotificationResponce> call, Response<PushNotificationResponce> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        PushNotificationResponce PushNotificationResponce = response.body();
                        try {
                            if (PushNotificationResponce.getStatus()) {
                                finish();
                                Toast.makeText(PushNotificationActivity.this, R.string.reset_success_msg, Toast.LENGTH_SHORT).show();

                            } else {

                                String failureResponse = PushNotificationResponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), PushNotificationActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(PushNotificationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<PushNotificationResponce> call, Throwable t) {
                    Log.d("TAG", "on failur: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(PushNotificationActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        Toast.makeText(PushNotificationActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(PushNotificationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;

        }

        private String preparepushsetJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("Id", 48);

                JSONArray jPNConfigarry = new JSONArray();

                for (int i = 0; i < pushnotificationarry.get(pos).getPNTypeId(); i++) {

                    if (pushnotificationarry.get(i).getPNTypeId() == 1) {

                        JSONObject orderObj = new JSONObject();
                        orderObj.put("Id", 6);
                        orderObj.put("PNTypeId", 1);
                        orderObj.put("Name", "Order");
                        orderObj.put("Status", pushnotificationarry.get(pos).getStatus());
                        jPNConfigarry.put(orderObj);
                    } else if (pushnotificationarry.get(i).getPNTypeId() == 2) {
                        JSONObject customerObj = new JSONObject();
                        customerObj.put("Id", 7);
                        customerObj.put("PNTypeId", 2);
                        customerObj.put("Name", "Customer Review");
                        customerObj.put("Status", pushnotificationarry.get(pos).getStatus());
                        jPNConfigarry.put(customerObj);
                    } else if (pushnotificationarry.get(i).getPNTypeId() == 3) {

                        JSONObject SubscriptionObj = new JSONObject();

                        SubscriptionObj.put("Id", 8);
                        SubscriptionObj.put("PNTypeId", 3);
                        SubscriptionObj.put("Name", "Subscription");
                        SubscriptionObj.put("Status", pushnotificationarry.get(pos).getStatus());
                        jPNConfigarry.put(SubscriptionObj);
                    } else if (pushnotificationarry.get(i).getPNTypeId() == 4) {
                        JSONObject ChattingObj = new JSONObject();

                        ChattingObj.put("Id", 9);
                        ChattingObj.put("PNTypeId", 4);
                        ChattingObj.put("Name", "Chatting");
                        ChattingObj.put("Status", pushnotificationarry.get(pos).getStatus());
                        jPNConfigarry.put(ChattingObj);

                    } else if (pushnotificationarry.get(i).getPNTypeId() == 5) {

                        JSONObject AlertObj = new JSONObject();

                        AlertObj.put("Id", 10);
                        AlertObj.put("PNTypeId", 5);
                        AlertObj.put("Name", "Alert");
                        AlertObj.put("Status", pushnotificationarry.get(pos).getStatus());
                        jPNConfigarry.put(AlertObj);

                    }

                }

                Log.i("TAG", "pushnotifactionJson: " + jPNConfigarry.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }

            return parentObj.toString();
        }

    }
}



