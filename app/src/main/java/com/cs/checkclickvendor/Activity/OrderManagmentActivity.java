package com.cs.checkclickvendor.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Models.OrderManagmentList;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class OrderManagmentActivity extends AppCompatActivity {

    View rootView;
    LinearLayout pending, scheduled, order;
    TextView pending_txt, scheduled_txt, order_txt, total_order, total_revenue;
    View pending_view, scheduled_view, order_view;
    String language, userId = "0";
    SharedPreferences userPrefs;
    public static boolean pending_boolean = false, scheduled_boolean = false, order_boolean = false;
    String start_date = "0", end_date = "0";
    RelativeLayout pending_orders, scheduled_orders, orders;
    TextView pending_urgent_order_count, pending_order_count, mselect_date;
    TextView transfer_order_count, delay_order_count, return_order_count, cancelled_order_count, order_history_count;
    TextView scheduled_today_order_count, scheduled_tommorrow_order_count, scheduled_order_count;

    LinearLayout pending_urgent_order_count1, pending_order_count1;
    LinearLayout transfer_order_count1, delay_order_count1, return_order_count1, cancelled_order_count1, order_history_count1;
    LinearLayout scheduled_today_order_count1, scheduled_tommorrow_order_count1, scheduled_order_count1;

    RelativeLayout pending_urgent_order_layout, pending_order_layout, order_status_layout;
    RelativeLayout transfer_order_layout, delay_order_layout, return_order_layout, cancelled_order_layout, order_history_layout;
    RelativeLayout schedule_today_order_layout, scheduled_tommorrow_order_layout, scheduled_order_layout;
    LinearLayout date_selection_layout;

    private TextView today, yesterday, last_7_days, last_30_days, this_month, last_month, custom_rang, apply, cancel;

    int mYear, mMonth, mDay;
    boolean isToday;
    String start = "", end = "";

    ImageView refresh, notification_img;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
//    BubbleLayout total_orders_layout;

    int branch_id;

    ArrayList<OrderManagmentList.DashboardCount> dashboardCounts = new ArrayList<>();
    ArrayList<OrderManagmentList.Pending> pendings = new ArrayList<>();
    ArrayList<OrderManagmentList.Scheduled> scheduleds = new ArrayList<>();
    ArrayList<OrderManagmentList.ProcessOrders> processOrders = new ArrayList<>();

    ImageView back_btn;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_status);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        branch_id = getIntent().getIntExtra("branchid", 0);

        back_btn = findViewById(R.id.back_btn);

        total_order = findViewById(R.id.total_orders);
        total_revenue = findViewById(R.id.total_revenue);

        pending_orders = findViewById(R.id.pending_orders_list);
        scheduled_orders = findViewById(R.id.schedule_orders_list);
        orders = findViewById(R.id.orders_list);

        pending_urgent_order_count = findViewById(R.id.pending_urgent_order_count);
        pending_order_count = findViewById(R.id.pending_order_count);

        scheduled_today_order_count = findViewById(R.id.schedule_today_order_count);
        scheduled_tommorrow_order_count = findViewById(R.id.schedule_tommorrow_order_count);
        scheduled_order_count = findViewById(R.id.schedule_order_count);

        transfer_order_count = findViewById(R.id.transfer_order_count);
        delay_order_count = findViewById(R.id.delayed_order_count);
        return_order_count = findViewById(R.id.return_order_count);
        cancelled_order_count = findViewById(R.id.cancelled_order_count);
        order_history_count = findViewById(R.id.order_history_count);

        pending_urgent_order_count1 = findViewById(R.id.pending_urgent_order_count1);
        pending_order_count1 = findViewById(R.id.pending_order_count1);

        scheduled_today_order_count1 = findViewById(R.id.schedule_today_order_count1);
        scheduled_tommorrow_order_count1 = findViewById(R.id.schedule_tommorrow_order_count1);
        scheduled_order_count1 = findViewById(R.id.schedule_order_count1);

        transfer_order_count1 = findViewById(R.id.transfer_order_count1);
        delay_order_count1 = findViewById(R.id.delayed_order_count1);
        return_order_count1 = findViewById(R.id.return_order_count1);
        cancelled_order_count1 = findViewById(R.id.cancelled_order_count1);
        order_history_count1 = findViewById(R.id.order_history_count1);

        pending_urgent_order_layout = findViewById(R.id.pending_urgent_order_layout);
        pending_order_layout = findViewById(R.id.pending_order_layout);

        schedule_today_order_layout = findViewById(R.id.schedule_today_order_layout);
        scheduled_tommorrow_order_layout = findViewById(R.id.schedule_tommorrow_order_layout);
        scheduled_order_layout = findViewById(R.id.schedule_order_layout);
        date_selection_layout = findViewById(R.id.date_selection_layout);

        transfer_order_layout = findViewById(R.id.transfer_order_layout);
        delay_order_layout = findViewById(R.id.delayed_order_layout);
        return_order_layout = findViewById(R.id.return_order_layout);
        cancelled_order_layout = findViewById(R.id.cancelled_order_layout);
        order_history_layout = findViewById(R.id.order_history_layout);

//        total_orders_layout = findViewById(R.id.total_orders_layout);

        order_status_layout = findViewById(R.id.order_status_layout);

        mselect_date = findViewById(R.id.select_date);

        pending = findViewById(R.id.pending);
        scheduled = findViewById(R.id.schedule);
        order = findViewById(R.id.order);

        pending_txt = findViewById(R.id.pending_txt);
        scheduled_txt = findViewById(R.id.schedule_txt);
        order_txt = findViewById(R.id.order_txt);

        pending_view = findViewById(R.id.pending_view);
        scheduled_view = findViewById(R.id.schedule_view);
        order_view = findViewById(R.id.order_view);

        notification_img = (ImageView) findViewById(R.id.notification_img);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {

                int ic_back_btn = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_back_btn));

                int ic_notify = getResources().getIdentifier("notification_icon3x_" + appColor, "drawable", getPackageName());
                notification_img.setImageDrawable(getResources().getDrawable(ic_notify));

                pending_txt.setTextColor(Color.parseColor("#" + Constants.appColor));
                pending_view.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                scheduled_view.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
                order_view.setBackgroundColor(Color.parseColor("#" + Constants.appColor));

                pending_urgent_order_count.setTextColor(Color.parseColor("#" + Constants.appColor));
                pending_order_count.setTextColor(Color.parseColor("#" + Constants.appColor));
                scheduled_order_count.setTextColor(Color.parseColor("#" + Constants.appColor));
                scheduled_today_order_count.setTextColor(Color.parseColor("#" + Constants.appColor));
                scheduled_tommorrow_order_count.setTextColor(Color.parseColor("#" + Constants.appColor));
                order_history_count.setTextColor(Color.parseColor("#" + Constants.appColor));
                transfer_order_count.setTextColor(Color.parseColor("#" + Constants.appColor));
                return_order_count.setTextColor(Color.parseColor("#" + Constants.appColor));
                delay_order_count.setTextColor(Color.parseColor("#" + Constants.appColor));
                cancelled_order_count.setTextColor(Color.parseColor("#" + Constants.appColor));

                int ic_color = getResources().getIdentifier("C50" + appColor, "color", getPackageName());
                Drawable background = pending_urgent_order_count1.getBackground();

                GradientDrawable shapeDrawable = (GradientDrawable) background;
                shapeDrawable.setColor(ContextCompat.getColor(OrderManagmentActivity.this, ic_color));

                Drawable background1 = pending_order_count1.getBackground();

                GradientDrawable shapeDrawable1 = (GradientDrawable) background1;
                shapeDrawable1.setColor(ContextCompat.getColor(OrderManagmentActivity.this, ic_color));

                Drawable background2 = scheduled_order_count1.getBackground();

                GradientDrawable shapeDrawable2 = (GradientDrawable) background2;
                shapeDrawable2.setColor(ContextCompat.getColor(OrderManagmentActivity.this, ic_color));

                Drawable background3 = scheduled_today_order_count1.getBackground();

                GradientDrawable shapeDrawable3 = (GradientDrawable) background3;
                shapeDrawable3.setColor(ContextCompat.getColor(OrderManagmentActivity.this, ic_color));

                Drawable background4 = scheduled_tommorrow_order_count1.getBackground();

                GradientDrawable shapeDrawable4 = (GradientDrawable) background4;
                shapeDrawable4.setColor(ContextCompat.getColor(OrderManagmentActivity.this, ic_color));

                Drawable background5 = cancelled_order_count1.getBackground();

                GradientDrawable shapeDrawable5 = (GradientDrawable) background5;
                shapeDrawable5.setColor(ContextCompat.getColor(OrderManagmentActivity.this, ic_color));

                Drawable background6 = delay_order_count1.getBackground();

                GradientDrawable shapeDrawable6 = (GradientDrawable) background6;
                shapeDrawable6.setColor(ContextCompat.getColor(OrderManagmentActivity.this, ic_color));

                Drawable background7 = return_order_count1.getBackground();

                GradientDrawable shapeDrawable7 = (GradientDrawable) background7;
                shapeDrawable7.setColor(ContextCompat.getColor(OrderManagmentActivity.this, ic_color));

                Drawable background8 = transfer_order_count1.getBackground();

                GradientDrawable shapeDrawable8 = (GradientDrawable) background8;
                shapeDrawable8.setColor(ContextCompat.getColor(OrderManagmentActivity.this, ic_color));

                Drawable background9 = order_history_count1.getBackground();

                GradientDrawable shapeDrawable9 = (GradientDrawable) background9;
                shapeDrawable9.setColor(ContextCompat.getColor(OrderManagmentActivity.this, ic_color));
            }
        }

        final BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(this).inflate(R.layout.bubble_date_range_selector, null);
        final PopupWindow popupWindow = BubblePopupHelper.create(this, bubbleLayout);
        final Random random = new Random();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        order_status_layout.setVisibility(View.VISIBLE);

        today = (TextView) bubbleLayout.findViewById(R.id.today);
        yesterday = (TextView) bubbleLayout.findViewById(R.id.yesterday);
        last_7_days = (TextView) bubbleLayout.findViewById(R.id.last_seven_days);
        last_30_days = (TextView) bubbleLayout.findViewById(R.id.last_thirty_days);
        this_month = (TextView) bubbleLayout.findViewById(R.id.current_mouth);
        last_month = (TextView) bubbleLayout.findViewById(R.id.last_mouth);
        custom_rang = (TextView) bubbleLayout.findViewById(R.id.custom_range);
        apply = (TextView) bubbleLayout.findViewById(R.id.apply);
        cancel = (TextView) bubbleLayout.findViewById(R.id.cancel);

        date_selection_layout.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                bubbleLayout.setVisibility(View.VISIBLE);
                int[] location = new int[2];
                view.getLocationInWindow(location);
                bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, (location[0] - 16), view.getHeight() + location[1]);

//                if (total_orders_layout.getVisibility() == View.VISIBLE) {
//                    total_orders_layout.setVisibility(View.GONE);
//                    order_status_layout.setVisibility(View.VISIBLE);
//                } else {
//                    total_orders_layout.setVisibility(View.VISIBLE);
//                    order_status_layout.setVisibility(View.GONE);
//                }
            }
        });

        today.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, currentTime;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                currentDate = df3.format(c.getTime());

                start_date = currentDate;
                end_date = currentDate;

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.white));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));


            }
        });

        yesterday.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, yesterdayDate;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                currentDate = df3.format(c.getTime());

                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("TAG", "before: " + currentDate);

                c.setTime(date);
                c.add(Calendar.DAY_OF_MONTH, -1);

                yesterdayDate = df3.format(c.getTime());
                Log.i("TAG", "after: " + yesterdayDate);


                start_date = yesterdayDate;
                end_date = currentDate;

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.white));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });

        last_7_days.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, startdate;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                currentDate = df3.format(c.getTime());

                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("TAG", "before: " + currentDate);

                c.setTime(date);
                c.add(Calendar.DAY_OF_MONTH, -7);

                startdate = df3.format(c.getTime());
                Log.i("TAG", "after: " + startdate);


                start_date = startdate;
                end_date = currentDate;

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.white));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });

        last_30_days.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, startdate;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                currentDate = df3.format(c.getTime());

                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("TAG", "before: " + currentDate);

                c.setTime(date);
                c.add(Calendar.DAY_OF_MONTH, -30);

                startdate = df3.format(c.getTime());
                Log.i("TAG", "after: " + startdate);


                start_date = startdate;
                end_date = currentDate;

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.white));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });

        this_month.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, startdate, smonth, syear;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat month = new SimpleDateFormat("MM", Locale.US);
                SimpleDateFormat year = new SimpleDateFormat("yyyy", Locale.US);

                currentDate = df3.format(c.getTime());
                smonth = month.format(c.getTime());
                syear = year.format(c.getTime());

                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("TAG", "before: " + currentDate);


                startdate = syear + "-" + smonth + "-" + "01";
                Log.i("TAG", "after: " + startdate);


                start_date = startdate;
                end_date = currentDate;

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.white));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));
            }
        });

        last_month.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, startdate, smonth, syear, firstday, lastday;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat month = new SimpleDateFormat("MM", Locale.US);
                SimpleDateFormat year = new SimpleDateFormat("yyyy", Locale.US);
                SimpleDateFormat day = new SimpleDateFormat("dd", Locale.US);

                currentDate = df3.format(c.getTime());


                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                Calendar nextNotifTime = Calendar.getInstance();
                nextNotifTime.add(Calendar.MONTH, 1);
                nextNotifTime.set(Calendar.DATE, 1);
                nextNotifTime.add(Calendar.DATE, -1);

                c.setTime(date);
                c.add(Calendar.MONTH, -1);

                smonth = month.format(c.getTime());
                syear = year.format(c.getTime());
                firstday = "01";
                c.getActualMaximum(Calendar.DAY_OF_MONTH);
                lastday = day.format(nextNotifTime.getTime());


                startdate = syear + "-" + smonth + "-" + firstday;
                Log.i("TAG", "after: " + startdate);
                currentDate = syear + "-" + smonth + "-" + lastday;
                Log.i("TAG", "before: " + currentDate);


                start_date = startdate;
                end_date = currentDate;

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.white));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });


        custom_rang.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                StartDatePicker();

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));

                today.setPadding(15, 15, 15, 15);
                yesterday.setPadding(15, 15, 15, 15);
                last_7_days.setPadding(15, 15, 15, 15);
                last_30_days.setPadding(15, 15, 15, 15);
                this_month.setPadding(15, 15, 15, 15);
                last_month.setPadding(15, 15, 15, 15);
                custom_rang.setPadding(15, 15, 15, 15);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.white));
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                start_date = "0";
                end_date = "0";
                bubbleLayout.setVisibility(View.INVISIBLE);
                order_status_layout.setVisibility(View.VISIBLE);

            }
        });

        apply.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

//
                if (start_date.equalsIgnoreCase("0") && end_date.equalsIgnoreCase("0")) {
                    String currentDate, currentTime;

                    final Calendar c = Calendar.getInstance();

                    SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                    currentDate = df3.format(c.getTime());

                    start_date = currentDate;
                    end_date = currentDate;

                    today.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                    yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                    today.setTextColor(getResources().getColor(R.color.white));
                    yesterday.setTextColor(getResources().getColor(R.color.black));
                    last_7_days.setTextColor(getResources().getColor(R.color.black));
                    last_30_days.setTextColor(getResources().getColor(R.color.black));
                    this_month.setTextColor(getResources().getColor(R.color.black));
                    last_month.setTextColor(getResources().getColor(R.color.black));
                    custom_rang.setTextColor(getResources().getColor(R.color.black));
                }
                new Orderstatusapi().execute();

                Log.i("TAG", "start end dates: " + start_date + " " + end_date);
                bubbleLayout.setVisibility(View.INVISIBLE);
                order_status_layout.setVisibility(View.VISIBLE);

            }
        });

        pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pending_boolean = true;
                scheduled_boolean = false;
                order_boolean = false;
                pending_txt.setTextColor(Color.parseColor("#" + Constants.appColor));
                scheduled_txt.setTextColor(getResources().getColor(R.color.black));
                order_txt.setTextColor(getResources().getColor(R.color.black));

                pending_view.setVisibility(View.VISIBLE);
                scheduled_view.setVisibility(View.GONE);
                order_view.setVisibility(View.GONE);

                pending_orders.setVisibility(View.VISIBLE);
                scheduled_orders.setVisibility(View.GONE);
                orders.setVisibility(View.GONE);

            }
        });

        scheduled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pending_boolean = false;
                scheduled_boolean = true;
                order_boolean = false;

                pending_txt.setTextColor(getResources().getColor(R.color.black));
                scheduled_txt.setTextColor(Color.parseColor("#" + Constants.appColor));
                order_txt.setTextColor(getResources().getColor(R.color.black));

                pending_view.setVisibility(View.GONE);
                scheduled_view.setVisibility(View.VISIBLE);
                order_view.setVisibility(View.GONE);

                pending_orders.setVisibility(View.GONE);
                scheduled_orders.setVisibility(View.VISIBLE);
                orders.setVisibility(View.GONE);

            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pending_boolean = false;
                scheduled_boolean = false;
                order_boolean = true;

                pending_txt.setTextColor(getResources().getColor(R.color.black));
                scheduled_txt.setTextColor(getResources().getColor(R.color.black));
                order_txt.setTextColor(Color.parseColor("#" + Constants.appColor));

                pending_view.setVisibility(View.GONE);
                scheduled_view.setVisibility(View.GONE);
                order_view.setVisibility(View.VISIBLE);

                pending_orders.setVisibility(View.GONE);
                scheduled_orders.setVisibility(View.GONE);
                orders.setVisibility(View.VISIBLE);

            }
        });


        new Orderstatusapi().execute();

        LocalBroadcastManager.getInstance(OrderManagmentActivity.this).registerReceiver(
                order_details_update, new IntentFilter("OrderdetailsUpdate"));

    }

    private BroadcastReceiver order_details_update = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String start = intent.getStringExtra("startdate");
            String end = intent.getStringExtra("enddate");

            start_date = start;
            end_date = end;

            Log.i("TAG", "order start end dates: " + start_date + " " + end_date);

            new Orderstatusapi().execute();

        }
    };

    private String prepareChangePasswordJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("BranchId", branch_id);
            parentObj.put("StartDate", start_date);
            parentObj.put("EndDate", end_date);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareChangePasswordJson: " + parentObj);

        return parentObj.toString();
    }

    private class Orderstatusapi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareChangePasswordJson();
            if (!((Activity) OrderManagmentActivity.this).isFinishing()) {
                Constants.showLoadingDialog(OrderManagmentActivity.this);
            }
//            showloaderAlertDialog();

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderManagmentList> call = apiService.orderstatus(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderManagmentList>() {
                @Override
                public void onResponse(Call<OrderManagmentList> call, Response<OrderManagmentList> response) {

                    Log.i("TAG", "onResponse: " + response.isSuccessful());
                    if (response.isSuccessful()) {
                        final OrderManagmentList Response = response.body();

                        dashboardCounts = Response.getData().getDashboardcount();
                        pendings = Response.getData().getPending();
                        scheduleds = Response.getData().getScheduled();
                        processOrders = Response.getData().getProcessorders();

                        total_order.setText("" + dashboardCounts.get(0).getDeliveredcount());
                        total_revenue.setText("" + Constants.priceFormat1.format(dashboardCounts.get(0).getTotalrevenue()) + " SAR");

                        pending_urgent_order_count.setText("" + pendings.get(0).getPendingurgent());
                        pending_order_count.setText("" + pendings.get(0).getPendingnonurgent());

                        scheduled_today_order_count.setText("" + scheduleds.get(0).getScheduledtoday());
                        scheduled_tommorrow_order_count.setText("" + scheduleds.get(0).getScheduledtomorrow());
                        scheduled_order_count.setText("" + scheduleds.get(0).getScheduledremaining());

                        transfer_order_count.setText("" + processOrders.get(0).getTransferorders());
                        delay_order_count.setText("" + processOrders.get(0).getDelayorders());
                        return_order_count.setText("" + processOrders.get(0).getReturnorders());
                        cancelled_order_count.setText("" + processOrders.get(0).getCancelledorders());
                        order_history_count.setText("" + processOrders.get(0).getClosedorders());

                        if (pending_boolean) {

                            pending_orders.setVisibility(View.VISIBLE);
                            scheduled_orders.setVisibility(View.GONE);
                            orders.setVisibility(View.GONE);

                            pending_urgent_order_count.setText("" + pendings.get(0).getPendingurgent());
                            pending_order_count.setText("" + pendings.get(0).getPendingnonurgent());

                        } else if (scheduled_boolean) {

                            pending_orders.setVisibility(View.GONE);
                            scheduled_orders.setVisibility(View.VISIBLE);
                            orders.setVisibility(View.GONE);

                            scheduled_today_order_count.setText("" + scheduleds.get(0).getScheduledtoday());
                            scheduled_tommorrow_order_count.setText("" + scheduleds.get(0).getScheduledtomorrow());
                            scheduled_order_count.setText("" + scheduleds.get(0).getScheduledremaining());


                        } else if (order_boolean) {

                            pending_orders.setVisibility(View.GONE);
                            scheduled_orders.setVisibility(View.GONE);
                            orders.setVisibility(View.VISIBLE);

                            transfer_order_count.setText("" + processOrders.get(0).getTransferorders());
                            delay_order_count.setText("" + processOrders.get(0).getDelayorders());
                            return_order_count.setText("" + processOrders.get(0).getReturnorders());
                            cancelled_order_count.setText("" + processOrders.get(0).getCancelledorders());
                            order_history_count.setText("" + processOrders.get(0).getClosedorders());

                        }

                        pending_urgent_order_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent a = new Intent(OrderManagmentActivity.this, OrderTypeActivity.class);
                                a.putExtra("BranchId", Response.getData().getBranchid());
                                a.putExtra("Type", 1);
                                a.putExtra("StatusId", pendings.get(0).getPendingurgentstatus());
                                a.putExtra("header", "Pending Urgent Orders");
                                startActivity(a);

                            }
                        });

                        pending_order_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent a = new Intent(OrderManagmentActivity.this, OrderTypeActivity.class);
                                a.putExtra("BranchId", Response.getData().getBranchid());
                                a.putExtra("Type", 1);
                                a.putExtra("StatusId", pendings.get(0).getPendingnonurgentstatus());
                                a.putExtra("header", "Pending Orders");
                                startActivity(a);

                            }
                        });

                        schedule_today_order_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent a = new Intent(OrderManagmentActivity.this, OrderTypeActivity.class);
                                a.putExtra("BranchId", Response.getData().getBranchid());
                                a.putExtra("Type", 2);
                                a.putExtra("StatusId", scheduleds.get(0).getScheduledtodaystatus());
                                a.putExtra("header", "Schedule orders for today");
                                startActivity(a);

                            }
                        });

                        scheduled_tommorrow_order_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent a = new Intent(OrderManagmentActivity.this, OrderTypeActivity.class);
                                a.putExtra("BranchId", Response.getData().getBranchid());
                                a.putExtra("Type", 2);
                                a.putExtra("StatusId", scheduleds.get(0).getScheduledtomorrowstatus());
                                a.putExtra("header", "Schedule orders for tommorrow");
                                startActivity(a);

                            }
                        });

                        scheduled_order_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent a = new Intent(OrderManagmentActivity.this, OrderTypeActivity.class);
                                a.putExtra("BranchId", Response.getData().getBranchid());
                                a.putExtra("Type", 2);
                                a.putExtra("StatusId", scheduleds.get(0).getScheduledremainingstatus());
                                a.putExtra("header", "Schedule orders");
                                startActivity(a);

                            }
                        });

                        transfer_order_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent a = new Intent(OrderManagmentActivity.this, OrderTypeActivity.class);
                                a.putExtra("BranchId", Response.getData().getBranchid());
                                a.putExtra("Type", 3);
                                a.putExtra("StatusId", processOrders.get(0).getTransferorderstatus());
                                a.putExtra("header", "Transfer Orders");
                                startActivity(a);

                            }
                        });

                        delay_order_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent a = new Intent(OrderManagmentActivity.this, OrderTypeActivity.class);
                                a.putExtra("BranchId", Response.getData().getBranchid());
                                a.putExtra("Type", 3);
                                a.putExtra("StatusId", processOrders.get(0).getDelayorderstatus());
                                a.putExtra("header", "Delayed Orders");
                                startActivity(a);

                            }
                        });

                        return_order_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent a = new Intent(OrderManagmentActivity.this, OrderTypeActivity.class);
                                a.putExtra("BranchId", Response.getData().getBranchid());
                                a.putExtra("Type", 3);
                                a.putExtra("StatusId", processOrders.get(0).getReturnorderstatus());
                                a.putExtra("header", "Return orders");
                                startActivity(a);

                            }
                        });

                        cancelled_order_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent a = new Intent(OrderManagmentActivity.this, OrderTypeActivity.class);
                                a.putExtra("BranchId", Response.getData().getBranchid());
                                a.putExtra("Type", 3);
                                a.putExtra("StatusId", processOrders.get(0).getCancelledorderstatus());
                                a.putExtra("header", "Cancelled orders");
                                startActivity(a);

                            }
                        });

                        order_history_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent a = new Intent(OrderManagmentActivity.this, OrderTypeActivity.class);
                                a.putExtra("BranchId", Response.getData().getBranchid());
                                a.putExtra("Type", 3);
                                a.putExtra("StatusId", processOrders.get(0).getClosedorderstatus());
                                a.putExtra("header", "Order History");
                                startActivity(a);

                            }
                        });

                        Log.i("TAG", "onResponse: " + Response.getMessage());

                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderManagmentActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OrderManagmentActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderManagmentList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderManagmentActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderManagmentActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OrderManagmentActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderManagmentActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OrderManagmentActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                        Log.i("TAG", "onFailure: " + t);
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }


    public void StartDatePicker() {


        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(OrderManagmentActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if (view.isShown()) {

                            if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                isToday = true;
                            } else {
                                isToday = false;
                            }
                            mYear = year;
                            mDay = dayOfMonth;
                            mMonth = monthOfYear;


                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                            Date date1 = null;

                            try {
                                date1 = df.parse(date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            start = df.format(date1);

                            Log.i("TAG", "onDateSet start: " + start);

                            EndDatePicker();
                        }


                    }
                }, mYear, mMonth, mDay);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String date;
        c.add(Calendar.YEAR, -1);
        date = dateFormat.format(c.getTimeInMillis());
        Log.i("TAG", "StartDatePicker: " + date);

        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//        int max_day = 0;
//        int max_days = max_day;
//        long max = TimeUnit.DAYS.toMillis(Long.parseLong(String.valueOf(max_days)));
//        Log.i("TAG", "maxdays+1: " + max_days);
        Calendar calendar = Calendar.getInstance();

        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.show();

    }

    public void EndDatePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(OrderManagmentActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if (view.isShown()) {

                            if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                isToday = true;
                            } else {
                                isToday = false;
                            }
                            mYear = year;
                            mDay = dayOfMonth;
                            mMonth = monthOfYear;


                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                            Date date1 = null;

                            try {
                                date1 = df.parse(date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            end = df.format(date1);

                            Log.i("TAG", "onDateSet end: " + end);

                            if (!start.equalsIgnoreCase("") || !end.equalsIgnoreCase("")) {

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                                Date datestart = null, dateend = null;

                                Log.i("TAG", "start end: " + start + " " + end);

                                try {
                                    datestart = dateFormat.parse(start);
                                    dateend = dateFormat.parse(end);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                Log.i("TAG", "datestart dateend: " + datestart + " " + dateend);

                                if (datestart.before(dateend)) {

                                    start_date = dateFormat.format(datestart);
                                    end_date = dateFormat.format(dateend);

                                } else {

                                    start_date = dateFormat.format(dateend);
                                    end_date = dateFormat.format(datestart);

                                }

                                Log.i("TAG", "startdate enddate: " + start_date + " " + end_date);


                            } else {


                            }

                        }


                    }
                }, mYear, mMonth, mDay);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String date;
        c.add(Calendar.YEAR, -1);
        date = dateFormat.format(c.getTimeInMillis());
        Log.i("TAG", "StartDatePicker: " + date);

        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//        int max_day = 0;
//        int max_days = max_day;
//        long max = TimeUnit.DAYS.toMillis(Long.parseLong(String.valueOf(max_days)));
//        Log.i("TAG", "maxdays+1: " + max_days);
        Calendar calendar = Calendar.getInstance();
        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.show();

    }


}
