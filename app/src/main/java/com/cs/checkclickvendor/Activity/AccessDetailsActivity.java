package com.cs.checkclickvendor.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickvendor.Adapters.AccessAdapter;
import com.cs.checkclickvendor.Adapters.AccessDetailsAdapter;
import com.cs.checkclickvendor.Models.AccessResponce;
import com.cs.checkclickvendor.R;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Utils.Utils.Constants.ACCESS_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.ADS_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.STORE_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class AccessDetailsActivity extends Activity {
    ImageView empimage, back_btn, notification_icon;
    TextView empname, emodetails, empid;
    ArrayList<AccessResponce.ListOfAccessUsersEntity> accessarry = new ArrayList<>();
    int pos;
    RecyclerView listview;
    AccessDetailsAdapter mAdadapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acces_details_activity);

        Intent intent = getIntent();
        accessarry = (ArrayList<AccessResponce.ListOfAccessUsersEntity>) getIntent().getSerializableExtra("List");
        pos = getIntent().getIntExtra("pos", 0);

        getIntent().putExtra("list", accessarry);
        getIntent().putExtra("pos", pos);
        Log.d("TAG", "activitypos: " + accessarry.size());

        empimage = (ImageView) findViewById(R.id.emeployeimage);
        empname = (TextView) findViewById(R.id.emeployename);
        emodetails = (TextView) findViewById(R.id.emeployecat);
        empid = (TextView) findViewById(R.id.emeployeid);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        notification_icon = (ImageView) findViewById(R.id.notification_icon);
        listview = (RecyclerView) findViewById(R.id.listview);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {

                int ic_notify = getResources().getIdentifier("notification_icon2x_" +
                        appColor, "drawable", getPackageName());
                notification_icon.setImageDrawable(getResources().getDrawable(ic_notify));

                int ic_back_btn = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_back_btn));

            }
        }
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        emodetails.setText(accessarry.get(pos).getPosiotionNameEn());
        empname.setText(accessarry.get(pos).getUserName());
        empid.setText("EMP ID : " + accessarry.get(pos).getAccessId());

        Glide.with(getApplicationContext())
                .load(ACCESS_IMAGE_URL + accessarry.get(pos).getAccessImage())
                .into(empimage);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AccessDetailsActivity.this);
        listview.setLayoutManager(mLayoutManager);
        mAdadapter = new AccessDetailsAdapter(getApplicationContext(), accessarry.get(pos).getMenusJson().get(pos).getBranches());
        listview.setAdapter(mAdadapter);


    }
}
