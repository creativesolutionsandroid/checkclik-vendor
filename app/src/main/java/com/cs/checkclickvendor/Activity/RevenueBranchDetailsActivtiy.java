package com.cs.checkclickvendor.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Adapters.RevenuDetailsAdapter;
import com.cs.checkclickvendor.Models.RevenuDetailsList;
import com.cs.checkclickvendor.Models.RevenueGraphList;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;
import com.hrules.charter.CharterLine;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class RevenueBranchDetailsActivtiy extends AppCompatActivity {

    TextView title_name, mselect_date, mselect_date1;
    CharterLine charterLine;

    LinearLayout cash, online, total;
    TextView cash_txt, online_txt, total_txt, total_earning, sales_revenu, operating_expenses, net_price;
    RecyclerView revenu_list;
    View cash_view, online_view, total_view;
    String language;
    SharedPreferences userPrefs;
    public static boolean cash_boolean = false, online_boolean = false, total_boolean = false;
    private TextView today, yesterday, last_7_days, last_30_days, this_month, last_month, custom_rang, apply, cancel;
    private TextView daily, weekly, monthly, yearly, apply1, cancel1;
    LinearLayout total_orders_layout, details_filter_layout;

    private float[] values;
    ArrayList<RevenueGraphList.FilteredGraph> filteredGraphs = new ArrayList<>();
    ArrayList<RevenuDetailsList.DetailSummery> detailSummeries = new ArrayList<>();
    ArrayList<RevenuDetailsList.EarningSummery> earningSummeries = new ArrayList<>();

    String start_date = "0", end_date = "0";
    int mYear, mMonth, mDay;
    boolean isToday;
    String start = "", end = "";
    private static final int CUSTOMRANGE = 1;

    String select_date_txt = "Today";
    String mtitle_name;
    int filter_id = 1, flag_id = 1, branchid;
    boolean daily_boolean = false, weekly_boolean = false, monthly_boolean = false, yearly_boolean = false;
    RevenuDetailsAdapter mAdapter;
    ImageView back_btn;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
//        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
//        language = LanguagePrefs.getString("language", "En");
////        if (language.equalsIgnoreCase("En")) {
        setContentView(R.layout.branchwise_revenue);
//        } else {
//            setContentView(R.layout.branchwise_revenue);
//        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        back_btn = findViewById(R.id.back_btn);

        mtitle_name = getIntent().getStringExtra("title_name");
        branchid = getIntent().getIntExtra("branchid", 0);

        mselect_date = findViewById(R.id.select_date);
        mselect_date1 = findViewById(R.id.select_date1);

        cash = findViewById(R.id.cash);
        online = findViewById(R.id.online);
        total = findViewById(R.id.total);

        cash_txt = findViewById(R.id.cash_txt);
        online_txt = findViewById(R.id.online_txt);
        total_txt = findViewById(R.id.total_txt);

        cash_view = findViewById(R.id.cash_view);
        online_view = findViewById(R.id.online_view);
        total_view = findViewById(R.id.total_view);

        today = (TextView) findViewById(R.id.today);
        yesterday = (TextView) findViewById(R.id.yesterday);
        last_7_days = (TextView) findViewById(R.id.last_seven_days);
        last_30_days = (TextView) findViewById(R.id.last_thirty_days);
        this_month = (TextView) findViewById(R.id.current_mouth);
        last_month = (TextView) findViewById(R.id.last_mouth);
        custom_rang = (TextView) findViewById(R.id.custom_range);
        apply = (TextView) findViewById(R.id.apply);
        cancel = (TextView) findViewById(R.id.cancel);

        daily = findViewById(R.id.daily);
        weekly = findViewById(R.id.weekly);
        monthly = findViewById(R.id.monthly);
        yearly = findViewById(R.id.yearly);
        apply1 = findViewById(R.id.apply1);
        cancel1 = findViewById(R.id.cancel1);

        total_earning = findViewById(R.id.total_earning);
        sales_revenu = findViewById(R.id.sales_revenu);
        operating_expenses = findViewById(R.id.operating_expenses);
        net_price = findViewById(R.id.net_price);
        title_name = findViewById(R.id.title_name);

        revenu_list = findViewById(R.id.revenu_list);

        charterLine = findViewById(R.id.charter_line);

        total_orders_layout = findViewById(R.id.total_orders_layout);
        details_filter_layout = findViewById(R.id.details_filter_layout);

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_back_btn = getResources().getIdentifier("ic_back_3x_" + appColor, "drawable", getPackageName());
                back_btn.setImageDrawable(getResources().getDrawable(ic_back_btn));
            }
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        title_name.setText("" + mtitle_name);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RevenueBranchDetailsActivtiy.this);
        revenu_list.setLayoutManager(mLayoutManager);

        new RevenuGraphApi().execute();
        new RevenuDetailsApi().execute();

        cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cash_boolean = true;
                online_boolean = false;
                total_boolean = false;

                cash_txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                online_txt.setTextColor(getResources().getColor(R.color.gray));
                total_txt.setTextColor(getResources().getColor(R.color.gray));

                cash_view.setVisibility(View.VISIBLE);
                online_view.setVisibility(View.GONE);
                total_view.setVisibility(View.GONE);

                flag_id = 1;
                new RevenuDetailsApi().execute();
            }
        });

        online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cash_boolean = false;
                online_boolean = true;
                total_boolean = false;

                cash_txt.setTextColor(getResources().getColor(R.color.gray));
                online_txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                total_txt.setTextColor(getResources().getColor(R.color.gray));

                cash_view.setVisibility(View.GONE);
                online_view.setVisibility(View.VISIBLE);
                total_view.setVisibility(View.GONE);

                flag_id = 2;
                new RevenuDetailsApi().execute();
            }
        });

        total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cash_boolean = false;
                online_boolean = false;
                total_boolean = true;

                cash_txt.setTextColor(getResources().getColor(R.color.gray));
                online_txt.setTextColor(getResources().getColor(R.color.gray));
                total_txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                cash_view.setVisibility(View.GONE);
                online_view.setVisibility(View.GONE);
                total_view.setVisibility(View.VISIBLE);

                flag_id = 3;
                new RevenuDetailsApi().execute();
            }
        });

        total_orders_layout.setVisibility(View.GONE);
        details_filter_layout.setVisibility(View.GONE);

        mselect_date.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                if (total_orders_layout.getVisibility() == View.VISIBLE) {
                    total_orders_layout.setVisibility(View.GONE);
                } else {
                    total_orders_layout.setVisibility(View.VISIBLE);
                }
            }
        });

        mselect_date1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (details_filter_layout.getVisibility() == View.VISIBLE) {
                    details_filter_layout.setVisibility(View.GONE);
                } else {
                    details_filter_layout.setVisibility(View.VISIBLE);
                }
            }
        });

        daily.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                daily.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                weekly.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                monthly.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yearly.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                daily.setPadding(16, 16, 16, 16);
                weekly.setPadding(16, 16, 16, 16);
                monthly.setPadding(16, 16, 16, 16);
                yearly.setPadding(16, 16, 16, 16);

                daily.setTextColor(getResources().getColor(R.color.white));
                weekly.setTextColor(getResources().getColor(R.color.black));
                monthly.setTextColor(getResources().getColor(R.color.black));
                yearly.setTextColor(getResources().getColor(R.color.black));

                daily_boolean = true;
                weekly_boolean = false;
                monthly_boolean = false;
                yearly_boolean = false;
            }
        });

        weekly.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                daily.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                weekly.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                monthly.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yearly.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                daily.setPadding(16, 16, 16, 16);
                weekly.setPadding(16, 16, 16, 16);
                monthly.setPadding(16, 16, 16, 16);
                yearly.setPadding(16, 16, 16, 16);

                daily.setTextColor(getResources().getColor(R.color.black));
                weekly.setTextColor(getResources().getColor(R.color.white));
                monthly.setTextColor(getResources().getColor(R.color.black));
                yearly.setTextColor(getResources().getColor(R.color.black));

                daily_boolean = false;
                weekly_boolean = true;
                monthly_boolean = false;
                yearly_boolean = false;
            }
        });

        monthly.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                daily.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                weekly.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                monthly.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                yearly.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                daily.setPadding(16, 16, 16, 16);
                weekly.setPadding(16, 16, 16, 16);
                monthly.setPadding(16, 16, 16, 16);
                yearly.setPadding(16, 16, 16, 16);


                daily.setTextColor(getResources().getColor(R.color.black));
                weekly.setTextColor(getResources().getColor(R.color.black));
                monthly.setTextColor(getResources().getColor(R.color.white));
                yearly.setTextColor(getResources().getColor(R.color.black));

                daily_boolean = false;
                weekly_boolean = false;
                monthly_boolean = true;
                yearly_boolean = false;

            }
        });

        yearly.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                daily.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                weekly.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                monthly.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yearly.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));

                daily.setPadding(16, 16, 16, 16);
                weekly.setPadding(16, 16, 16, 16);
                monthly.setPadding(16, 16, 16, 16);
                yearly.setPadding(16, 16, 16, 16);


                daily.setTextColor(getResources().getColor(R.color.black));
                weekly.setTextColor(getResources().getColor(R.color.black));
                monthly.setTextColor(getResources().getColor(R.color.black));
                yearly.setTextColor(getResources().getColor(R.color.white));

                daily_boolean = false;
                weekly_boolean = false;
                monthly_boolean = false;
                yearly_boolean = true;

            }
        });

        cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                details_filter_layout.setVisibility(View.GONE);
            }
        });

        apply1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (daily_boolean) {

                    mselect_date1.setText("Daily");
                    filter_id = 1;

                } else if (weekly_boolean) {

                    mselect_date1.setText("Weekly");
                    filter_id = 2;

                } else if (monthly_boolean) {

                    mselect_date1.setText("Monthly");
                    filter_id = 3;

                } else if (yearly_boolean) {

                    mselect_date1.setText("Yearly");
                    filter_id = 4;

                }

                new RevenuDetailsApi().execute();
                details_filter_layout.setVisibility(View.GONE);

            }
        });


        String currentDate, currentTime;

        final Calendar c = Calendar.getInstance();

        SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        currentDate = df3.format(c.getTime());

        start_date = currentDate;
        end_date = currentDate;

        today.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, currentTime;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                currentDate = df3.format(c.getTime());

                start_date = currentDate;
                end_date = currentDate;
                select_date_txt = "Today";

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(16, 16, 16, 16);
                yesterday.setPadding(16, 16, 16, 16);
                last_7_days.setPadding(16, 16, 16, 16);
                last_30_days.setPadding(16, 16, 16, 16);
                this_month.setPadding(16, 16, 16, 16);
                last_month.setPadding(16, 16, 16, 16);
                custom_rang.setPadding(16, 16, 16, 16);

                today.setTextColor(getResources().getColor(R.color.white));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));
            }
        });

        yesterday.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, yesterdayDate;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                currentDate = df3.format(c.getTime());

                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                c.setTime(date);
                c.add(Calendar.DAY_OF_MONTH, -1);

                yesterdayDate = df3.format(c.getTime());

                start_date = yesterdayDate;
                end_date = currentDate;
                select_date_txt = "Yesterday";

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(16, 16, 16, 16);
                yesterday.setPadding(16, 16, 16, 16);
                last_7_days.setPadding(16, 16, 16, 16);
                last_30_days.setPadding(16, 16, 16, 16);
                this_month.setPadding(16, 16, 16, 16);
                last_month.setPadding(16, 16, 16, 16);
                custom_rang.setPadding(16, 16, 16, 16);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.white));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));
            }
        });

        last_7_days.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, startdate;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat selected_dateformat = new SimpleDateFormat("dd MMM''yy", Locale.US);

                currentDate = df3.format(c.getTime());

                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                c.setTime(date);
                c.add(Calendar.DAY_OF_MONTH, -7);

                startdate = df3.format(c.getTime());

                start_date = startdate;
                end_date = currentDate;

                Date select_start_date = null, select_end_date = null;

                try {
                    select_start_date = df3.parse(startdate);
                    select_end_date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                select_date_txt = selected_dateformat.format(select_start_date) + " - " + selected_dateformat.format(select_end_date);

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(16, 16, 16, 16);
                yesterday.setPadding(16, 16, 16, 16);
                last_7_days.setPadding(16, 16, 16, 16);
                last_30_days.setPadding(16, 16, 16, 16);
                this_month.setPadding(16, 16, 16, 16);
                last_month.setPadding(16, 16, 16, 16);
                custom_rang.setPadding(16, 16, 16, 16);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.white));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });

        last_30_days.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, startdate;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat selected_dateformat = new SimpleDateFormat("dd MMM''yy", Locale.US);

                currentDate = df3.format(c.getTime());

                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("TAG", "before: " + currentDate);

                c.setTime(date);
                c.add(Calendar.DAY_OF_MONTH, -30);

                startdate = df3.format(c.getTime());
                Log.i("TAG", "after: " + startdate);

                start_date = startdate;
                end_date = currentDate;

                Date select_start_date = null, select_end_date = null;

                try {
                    select_start_date = df3.parse(startdate);
                    select_end_date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                select_date_txt = selected_dateformat.format(select_start_date) + " - " + selected_dateformat.format(select_end_date);

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(16, 16, 16, 16);
                yesterday.setPadding(16, 16, 16, 16);
                last_7_days.setPadding(16, 16, 16, 16);
                last_30_days.setPadding(16, 16, 16, 16);
                this_month.setPadding(16, 16, 16, 16);
                last_month.setPadding(16, 16, 16, 16);
                custom_rang.setPadding(16, 16, 16, 16);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.white));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });

        this_month.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, startdate, smonth, syear;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat month = new SimpleDateFormat("MM", Locale.US);
                SimpleDateFormat year = new SimpleDateFormat("yyyy", Locale.US);
                SimpleDateFormat selected_dateformat = new SimpleDateFormat("dd MMM''yy", Locale.US);

                currentDate = df3.format(c.getTime());
                smonth = month.format(c.getTime());
                syear = year.format(c.getTime());

                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("TAG", "before: " + currentDate);


                startdate = syear + "-" + smonth + "-" + "01";
                Log.i("TAG", "after: " + startdate);


                start_date = startdate;
                end_date = currentDate;

                Date select_start_date = null, select_end_date = null;

                try {
                    select_start_date = df3.parse(startdate);
                    select_end_date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                select_date_txt = selected_dateformat.format(select_start_date) + " - " + selected_dateformat.format(select_end_date);

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(16, 16, 16, 16);
                yesterday.setPadding(16, 16, 16, 16);
                last_7_days.setPadding(16, 16, 16, 16);
                last_30_days.setPadding(16, 16, 16, 16);
                this_month.setPadding(16, 16, 16, 16);
                last_month.setPadding(16, 16, 16, 16);
                custom_rang.setPadding(16, 16, 16, 16);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.white));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });

        last_month.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                String currentDate, startdate, smonth, syear, firstday, lastday;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat month = new SimpleDateFormat("MM", Locale.US);
                SimpleDateFormat year = new SimpleDateFormat("yyyy", Locale.US);
                SimpleDateFormat day = new SimpleDateFormat("dd", Locale.US);
                SimpleDateFormat selected_dateformat = new SimpleDateFormat("dd MMM''yy", Locale.US);

                currentDate = df3.format(c.getTime());


                Date date = null;

                try {
                    date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Calendar nextNotifTime = Calendar.getInstance();
                nextNotifTime.add(Calendar.MONTH, 1);
                nextNotifTime.set(Calendar.DATE, 1);
                nextNotifTime.add(Calendar.DATE, -1);

                c.setTime(date);
                c.add(Calendar.MONTH, -2);
                c.getActualMaximum(Calendar.DAY_OF_MONTH);
                lastday = day.format(nextNotifTime.getTime());
                Log.i("TAG", "lastday: " + lastday);

                smonth = month.format(c.getTime());
                syear = year.format(c.getTime());
                firstday = "01";

                startdate = syear + "-" + smonth + "-" + firstday;
                Log.i("TAG", "after: " + startdate);
                currentDate = syear + "-" + smonth + "-" + lastday;
                Log.i("TAG", "before: " + currentDate);


                start_date = startdate;
                end_date = currentDate;

                Date select_start_date = null, select_end_date = null;

                try {
                    select_start_date = df3.parse(startdate);
                    select_end_date = df3.parse(currentDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                select_date_txt = selected_dateformat.format(select_start_date) + " - " + selected_dateformat.format(select_end_date);

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                today.setPadding(16, 16, 16, 16);
                yesterday.setPadding(16, 16, 16, 16);
                last_7_days.setPadding(16, 16, 16, 16);
                last_30_days.setPadding(16, 16, 16, 16);
                this_month.setPadding(16, 16, 16, 16);
                last_month.setPadding(16, 16, 16, 16);
                custom_rang.setPadding(16, 16, 16, 16);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.white));
                custom_rang.setTextColor(getResources().getColor(R.color.black));

            }
        });


        custom_rang.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                Intent a = new Intent(RevenueBranchDetailsActivtiy.this, DatePickerActivity.class);
                startActivityForResult(a, CUSTOMRANGE);

                today.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));

                today.setPadding(16, 16, 16, 16);
                yesterday.setPadding(16, 16, 16, 16);
                last_7_days.setPadding(16, 16, 16, 16);
                last_30_days.setPadding(16, 16, 16, 16);
                this_month.setPadding(16, 16, 16, 16);
                last_month.setPadding(16, 16, 16, 16);
                custom_rang.setPadding(16, 16, 16, 16);

                today.setTextColor(getResources().getColor(R.color.black));
                yesterday.setTextColor(getResources().getColor(R.color.black));
                last_7_days.setTextColor(getResources().getColor(R.color.black));
                last_30_days.setTextColor(getResources().getColor(R.color.black));
                this_month.setTextColor(getResources().getColor(R.color.black));
                last_month.setTextColor(getResources().getColor(R.color.black));
                custom_rang.setTextColor(getResources().getColor(R.color.white));
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                start_date = "0";
                end_date = "0";
                total_orders_layout.setVisibility(View.GONE);


            }
        });

        apply.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

//
                if (start_date.equalsIgnoreCase("0") && end_date.equalsIgnoreCase("0")) {
                    String currentDate, currentTime;

                    final Calendar c = Calendar.getInstance();

                    SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                    currentDate = df3.format(c.getTime());

                    start_date = currentDate;
                    end_date = currentDate;

                    today.setBackground(getResources().getDrawable(R.drawable.total_orders_selected_bg));
                    yesterday.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    last_7_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    last_30_days.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    this_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    last_month.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));
                    custom_rang.setBackground(getResources().getDrawable(R.drawable.total_orders_unselected_bg));

                    today.setTextColor(getResources().getColor(R.color.white));
                    yesterday.setTextColor(getResources().getColor(R.color.black));
                    last_7_days.setTextColor(getResources().getColor(R.color.black));
                    last_30_days.setTextColor(getResources().getColor(R.color.black));
                    this_month.setTextColor(getResources().getColor(R.color.black));
                    last_month.setTextColor(getResources().getColor(R.color.black));
                    custom_rang.setTextColor(getResources().getColor(R.color.black));


                }
                mselect_date.setText(select_date_txt);
                new RevenuGraphApi().execute();

                String currentDate, currentTime;

                final Calendar c = Calendar.getInstance();

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                currentDate = df3.format(c.getTime());


                Log.i("TAG", "start end dates: " + start_date + " " + end_date);
                total_orders_layout.setVisibility(View.GONE);


            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CUSTOMRANGE && resultCode == RESULT_OK) {
            Log.d("TAG", "onActivityResult: " + data.getStringExtra("startdate"));
            start_date = data.getStringExtra("startdate");
            end_date = data.getStringExtra("enddate");

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            SimpleDateFormat selected_dateformat = new SimpleDateFormat("dd MMM''yy", Locale.US);

            Date datestart = null, dateend = null;

            Log.i("TAG", "start end: " + start_date + " " + end_date);

            try {
                datestart = dateFormat.parse(start_date);
                dateend = dateFormat.parse(end_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.i("TAG", "datestart dateend: " + datestart + " " + dateend);

            if (datestart.before(dateend)) {

                start_date = dateFormat.format(datestart);
                end_date = dateFormat.format(dateend);

            } else {

                start_date = dateFormat.format(dateend);
                end_date = dateFormat.format(datestart);

            }

            Log.i("TAG", "startdate enddate: " + start_date + " " + end_date);

            Date select_start_date = null, select_end_date = null;

            try {
                select_start_date = dateFormat.parse(start_date);
                select_end_date = dateFormat.parse(end_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            select_date_txt = selected_dateformat.format(select_start_date) + " - " + selected_dateformat.format(select_end_date);

        } else if (requestCode == CUSTOMRANGE && resultCode == RESULT_CANCELED) {
            start_date = "0";
            end_date = "0";
        }

    }

    private String prepareRevenuGraphJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId", userPrefs.getString("userId", null));
//            parentObj.put("StartDate", start_date);
//            parentObj.put("EndDate", end_date);
            parentObj.put("filterId", 5);
            parentObj.put("StoreId", userPrefs.getInt("storeId", 0));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj.toString());

        return parentObj.toString();
    }

    private class RevenuGraphApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareRevenuGraphJson();
//            Constants.showLoadingDialog(RevenueBranchDetailsActivtiy.this);
//            showloaderAlertDialog();

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<RevenueGraphList> call = apiService.getRevenuegraph(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<RevenueGraphList>() {
                @Override
                public void onResponse(Call<RevenueGraphList> call, Response<RevenueGraphList> response) {
                    if (response.isSuccessful()) {
                        RevenueGraphList updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessage();
                            Log.i("TAG", "onResponse: " + message);

                            filteredGraphs = updateorderList.getData().getFilteredgraph();


                            String current = null;

                            values = new float[filteredGraphs.size()];
                            for (int i = 0; i < filteredGraphs.size(); i++) {
                                values[i] = filteredGraphs.get(i).getCurrentamount();
                            }


                            charterLine.setValues(values);
                            charterLine.setAnimInterpolator(new BounceInterpolator());


                            charterLine.setLineColor(getResources().getColor(R.color.white));
                            charterLine.setIndicatorColor(getResources().getColor(R.color.white));
                            charterLine.setChartFillColor(Color.TRANSPARENT);
                            charterLine.setIndicatorSize(6);
                            charterLine.setStrokeSize(4);
                            charterLine.setLineColor(Color.TRANSPARENT);


                        } else {
                            String failureResponse = updateorderList.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    "Ok", RevenueBranchDetailsActivtiy.this);
                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RevenueBranchDetailsActivtiy.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(RevenueBranchDetailsActivtiy.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
//                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<RevenueGraphList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(RevenueBranchDetailsActivtiy.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RevenueBranchDetailsActivtiy.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(RevenueBranchDetailsActivtiy.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RevenueBranchDetailsActivtiy.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(RevenueBranchDetailsActivtiy.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Log.i("TAG", "onFailure: " + t);
//                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }


    private String prepareRevenuDetailsJson() {
        JSONObject parentObj = new JSONObject();

        try {

            parentObj.put("FilterId", filter_id);
            parentObj.put("FlagId", flag_id);
            parentObj.put("BranchId", branchid);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj.toString());

        return parentObj.toString();
    }

    private class RevenuDetailsApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareRevenuDetailsJson();
            Constants.showLoadingDialog(RevenueBranchDetailsActivtiy.this);
//            showloaderAlertDialog();

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<RevenuDetailsList> call = apiService.getRevenuedetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<RevenuDetailsList>() {
                @Override
                public void onResponse(Call<RevenuDetailsList> call, Response<RevenuDetailsList> response) {
                    if (response.isSuccessful()) {
                        RevenuDetailsList updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessage();
                            Log.i("TAG", "onResponse: " + message);

                            detailSummeries = updateorderList.getData().getDetailsummery();
                            earningSummeries = updateorderList.getData().getEarningsummery();

                            for (int i = 0; i < earningSummeries.size(); i++) {

                                total_earning.setText("" + Constants.priceFormat1.format(earningSummeries.get(i).getTotalearning()) + " SAR");
                                sales_revenu.setText("-  " + Constants.priceFormat1.format(earningSummeries.get(i).getSalesrefund()) + " SAR");
                                operating_expenses.setText("- " + Constants.priceFormat1.format(earningSummeries.get(i).getOperatingexpense()) + " SAR");
                                net_price.setText("" + Constants.priceFormat1.format(earningSummeries.get(i).getNetearning()) + " SAR");

                            }


                            mAdapter = new RevenuDetailsAdapter(RevenueBranchDetailsActivtiy.this, detailSummeries);
                            revenu_list.setAdapter(mAdapter);


                        } else {
                            String failureResponse = updateorderList.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    "Ok", RevenueBranchDetailsActivtiy.this);
                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RevenueBranchDetailsActivtiy.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(RevenueBranchDetailsActivtiy.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<RevenuDetailsList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(RevenueBranchDetailsActivtiy.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RevenueBranchDetailsActivtiy.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(RevenueBranchDetailsActivtiy.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
                    } else {
//                        if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(RevenueBranchDetailsActivtiy.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }else {
//                            Toast.makeText(RevenueBranchDetailsActivtiy.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                    }
                    Log.i("TAG", "onFailure: " + t);
                    Constants.closeLoadingDialog();

                }
            });
            return null;
        }
    }

}
