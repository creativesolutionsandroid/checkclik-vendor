package com.cs.checkclickvendor.Base;

public interface BasePresenter<V> {

    void attach(V baseView);

    void detach();


}
