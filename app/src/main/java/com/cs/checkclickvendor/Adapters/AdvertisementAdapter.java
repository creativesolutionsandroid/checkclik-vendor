package com.cs.checkclickvendor.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickvendor.Activity.AdvertiseOfferDetailsActivity;
import com.cs.checkclickvendor.Activity.AdvertisementDetailsActivity;
import com.cs.checkclickvendor.Models.AdavertisementResponce;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class AdvertisementAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<AdavertisementResponce.DataEntity> adveapterarry;
    Context context;
    String TAG = "TAG";
    boolean offer;

    public AdvertisementAdapter(Context context, ArrayList<AdavertisementResponce.DataEntity> adveapterarry, boolean offer) {
        this.context = context;
        this.offer = offer;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.adveapterarry = adveapterarry;

    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 2.5f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        Log.d(TAG, "adveadaptercount: " + adveapterarry.size());
        return adveapterarry.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((android.support.v7.widget.CardView) object);
    }

    public Object instantiateItem(ViewGroup container, final int pos) {

        View itemView = mLayoutInflater.inflate(R.layout.adave_active_adapter, container, false);
        TextView disscounttital, expiredate;
        ImageView productimage;
        CamomileSpinner spinner;

        disscounttital = (TextView) itemView.findViewById(R.id.discounted_price);
        expiredate = (TextView) itemView.findViewById(R.id.exprirydate);
        productimage = (ImageView) itemView.findViewById(R.id.image);
        spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);

        disscounttital.setText(adveapterarry.get(pos).getPercentage() + "% off on " + adveapterarry.get(pos).getNameEn());

        String date1 = adveapterarry.get(pos).getEndDate();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-ddHH'T'mm:ss", Locale.US);
        SimpleDateFormat sdf3 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        date1.replace(date1, "yyyy-MM-dd");
        try {
            Date datetime = format.parse(date1);
            date1 = sdf3.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        expiredate.setText("Expiry " + date1);

        spinner.start();
        spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );


        if (offer) {

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + adveapterarry.get(pos).getBannerImageEnName())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }
                    })


                    .into(productimage);

        } else {

            Glide.with(context)
                    .load(Constants.ADS_IMAGE_URL + adveapterarry.get(pos).getBannerImageEnName())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }
                    })


                    .into(productimage);
        }
        Log.d(TAG, "advepic: " + Constants.PRODUCTS_IMAGE_URL + adveapterarry.get(pos).getBannerImageEnName());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (offer) {

                    Intent a = new Intent(context, AdvertiseOfferDetailsActivity.class);
                    a.putExtra("advertiesarry", adveapterarry);
                    a.putExtra("ProductDetails", adveapterarry.get(pos).getJProductDetails());
                    a.putExtra("pos", pos);
                    context.startActivity(a);

                } else {

                    Intent a = new Intent(context, AdvertisementDetailsActivity.class);
                    a.putExtra("advertiesarry", adveapterarry);
                    a.putExtra("pos", pos);
                    context.startActivity(a);

                }

            }
        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((android.support.v7.widget.CardView) object);
    }
}
