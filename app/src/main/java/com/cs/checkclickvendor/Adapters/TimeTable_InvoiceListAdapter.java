package com.cs.checkclickvendor.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.checkclickvendor.Activity.OrderDetailsActivity;
import com.cs.checkclickvendor.Activity.OrderTypeActivity;
import com.cs.checkclickvendor.Models.TimeSlot_Invoice_no;
import com.cs.checkclickvendor.Models.TimeTable_Invoiceno;
import com.cs.checkclickvendor.R;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Activity.TimeTableMangActivity.branch_id;

public class TimeTable_InvoiceListAdapter extends RecyclerView.Adapter<TimeTable_InvoiceListAdapter.MyViewHolder> {

    Context context;
    AppCompatActivity activity;
    LayoutInflater inflater;
    String userid;

    ArrayList<TimeTable_Invoiceno> orderLists = new ArrayList<>();

    public TimeTable_InvoiceListAdapter(Context context, ArrayList<TimeTable_Invoiceno> ordersCounts) {
        this.context = context;
        this.orderLists = ordersCounts;
        this.userid = userid;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.timetable_invoice_list, parent, false);

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.invoice_no.setText("" + orderLists.get(position).getInvoice_no());
        if (position == 0){

            holder.img.setVisibility(View.VISIBLE);

        } else {

            holder.img.setVisibility(View.INVISIBLE);

        }

    }

    @Override
    public int getItemCount() {

        return orderLists.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView invoice_no;
        ImageView img;

        public MyViewHolder(final View convertView) {
            super(convertView);

            invoice_no = (TextView) convertView.findViewById(R.id.invoice_no);
            img = (ImageView) convertView.findViewById(R.id.img);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent a = new Intent(context, OrderDetailsActivity.class);
                    if (orderLists.get(getAdapterPosition()).getExp() == 0) {
                        a.putExtra("BranchId", branch_id);
                        a.putExtra("Type", 2);
                        a.putExtra("StatusId", 3);
                        a.putExtra("header", "Scheduled orders");
                        a.putExtra("orderid", orderLists.get(getAdapterPosition()).getOrder_id());
                    }
                    else if (orderLists.get(getAdapterPosition()).getExp() == 1) {
                        a.putExtra("BranchId", branch_id);
                        a.putExtra("Type", 3);
                        a.putExtra("StatusId", 2);
                        a.putExtra("header", "Delayed Orders");
                        a.putExtra("orderid", orderLists.get(getAdapterPosition()).getOrder_id());
                    }
                    else if (orderLists.get(getAdapterPosition()).getExp() == 2) {
                        a.putExtra("BranchId", branch_id);
                        a.putExtra("Type", 2);
                        a.putExtra("StatusId", 1);
                        a.putExtra("header", "Scheduled orders for today");
                        a.putExtra("orderid", orderLists.get(getAdapterPosition()).getOrder_id());
                    }
                    else if (orderLists.get(getAdapterPosition()).getExp() == 3) {
                        a.putExtra("BranchId", branch_id);
                        a.putExtra("Type", 2);
                        a.putExtra("StatusId", 2);
                        a.putExtra("header", "Scheduled orders for tomorrow");
                        a.putExtra("orderid", orderLists.get(getAdapterPosition()).getOrder_id());
                    }
                    context.startActivity(a);

                }
            });
        }

    }

}
