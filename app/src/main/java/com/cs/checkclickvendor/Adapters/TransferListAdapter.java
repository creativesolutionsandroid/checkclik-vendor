package com.cs.checkclickvendor.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.cs.checkclickvendor.Activity.TransferOrderActivity;
import com.cs.checkclickvendor.Models.EmployeeList;
import com.cs.checkclickvendor.R;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Activity.TransferOrderActivity.employee_Id;

public class TransferListAdapter extends RecyclerView.Adapter<TransferListAdapter.MyViewHolder> {

    Context context;
    AppCompatActivity activity;
    LayoutInflater inflater;
    String userid;
    boolean transfer = true;

    ArrayList<EmployeeList> orderLists = new ArrayList<>();

    private int selectedCheckBoxPosition = -1;

    public int getSelectedCheckBoxPosition() {
        return selectedCheckBoxPosition;
    }

    public TransferListAdapter(Context context, ArrayList<EmployeeList> ordersCounts) {
        this.context = context;
        this.orderLists = ordersCounts;
        this.userid = userid;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transfer_order_list, parent, false);

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        holder.employee_name.setText("" + orderLists.get(position).getName());

        if (position == selectedCheckBoxPosition) {
            holder.employee_img.setChecked(true);
            Log.d("TAG", "checkbox CHECKED at pos: " + position);
        } else {
            holder.employee_img.setChecked(false);
            Log.d("TAG", "checkbox UNCHECKED at pos: " + position);
        }


        holder.employee_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedCheckBoxPosition = position;
                employee_Id = orderLists.get(position).getEmployee_id();
                TransferOrderActivity.employee_name = orderLists.get(position).getName();
                Log.i("TAG", "employeeid: " + employee_Id);
                notifyDataSetChanged();

            }
        });

        holder.employee_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedCheckBoxPosition = position;
                employee_Id = orderLists.get(position).getEmployee_id();
                TransferOrderActivity.employee_name = orderLists.get(position).getName();

                Log.i("TAG", "employeeid: " + employee_Id);
                notifyDataSetChanged();

            }
        });


    }

    @Override
    public int getItemCount() {

        return orderLists.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView employee_name;
        CheckBox employee_img;
        RelativeLayout employee_layout;

        public MyViewHolder(final View convertView) {
            super(convertView);

            employee_name = (TextView) convertView.findViewById(R.id.employee_name);
            employee_img = (CheckBox) convertView.findViewById(R.id.employee_img);
            employee_layout = (RelativeLayout) convertView.findViewById(R.id.employee_layout);

        }

    }

}
