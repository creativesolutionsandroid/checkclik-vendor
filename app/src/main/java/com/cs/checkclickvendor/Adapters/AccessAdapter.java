package com.cs.checkclickvendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickvendor.Activity.AccessDetailsActivity;
import com.cs.checkclickvendor.Models.AccessResponce;
import com.cs.checkclickvendor.Models.BranchList;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Utils.Utils.Constants.ACCESS_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.ADS_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.PRODUCTS_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.RETURN_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.STORE_IMAGE_URL;

public class AccessAdapter extends RecyclerView.Adapter<AccessAdapter.MyViewHolder> {

    ArrayList<AccessResponce.ListOfAccessUsersEntity> accessarraylist = new ArrayList<>();
    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    public LayoutInflater inflater;

    public AccessAdapter(Context context, ArrayList<AccessResponce.ListOfAccessUsersEntity> accessarraylist) {
        this.context = context;
        this.activity = activity;
        this.accessarraylist = accessarraylist;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public AccessAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.access_adater, parent, false);
        return new AccessAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AccessAdapter.MyViewHolder holder, int pos) {
        holder.emeployename.setText(accessarraylist.get(pos).getUserName());
        holder.emeployecat.setText(accessarraylist.get(pos).getPosiotionNameEn());
        holder.emeployeid.setText("EMP ID: "+accessarraylist.get(pos).getAccessId());

        holder.spinner.start();
        holder.spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );

        Glide.with(context)
                .load(ACCESS_IMAGE_URL+accessarraylist.get(pos).getAccessImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.emeployeimage);
        Log.d(TAG, "accesimage: "+ACCESS_IMAGE_URL+accessarraylist.get(pos).getAccessImage());
    }

    @Override
    public int getItemCount() {
        return accessarraylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView emeployename,emeployeid,emeployecat;
        ImageView emeployeimage;
        CamomileSpinner spinner ;


        public MyViewHolder(View itemView) {
            super(itemView);
            emeployename = (TextView) itemView.findViewById(R.id.emeployename);
            emeployeid = (TextView) itemView.findViewById(R.id.emeployeid);
            emeployecat = (TextView) itemView.findViewById(R.id.emeployecat);
            emeployeimage=(ImageView)itemView.findViewById(R.id.emeployeimage);
             spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent= new Intent(context, AccessDetailsActivity.class);
                    intent.putExtra("List",accessarraylist);
                    intent.putExtra("pos",0);
                    Log.d(TAG, "pos: "+accessarraylist.size());
                    context.startActivity(intent);
                }
            });

        }
    }
}
