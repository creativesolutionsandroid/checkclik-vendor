package com.cs.checkclickvendor.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickvendor.Activity.VoucherDetailsActivity;
import com.cs.checkclickvendor.Models.VoucherDeleteResponce;
import com.cs.checkclickvendor.Models.VoucherResponce;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Rest.APIInterface;
import com.cs.checkclickvendor.Rest.ApiClient;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.cs.checkclickvendor.Utils.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;
import static com.cs.checkclickvendor.Utils.Utils.Constants.customDialog;

public class VoucherAdapter extends RecyclerView.Adapter<VoucherAdapter.MyViewHolder> {
    private Context context;
    private Activity activity;
    public static final String TAG = "TAG";
    ArrayList<VoucherResponce.DataEntity> couponArrayList = new ArrayList<>();
    int pos = 0;
    String userId;
    public LayoutInflater inflater;

    SharedPreferences.Editor userPrefEditor;
    SharedPreferences userPrefs;

    public VoucherAdapter(Context context, ArrayList<VoucherResponce.DataEntity> couponArrayList, Activity activity) {
        this.context = context;
        this.activity = activity;
        this.couponArrayList = couponArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.coupon_adapter, parent, false);
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int pos) {


        holder.mainstore.setText(couponArrayList.get(pos).getBranchNameEn());
        holder.discount.setText(couponArrayList.get(pos).getVoucherNameEn());
        holder.code.setText("Code : " + couponArrayList.get(pos).getCouponCode());

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                holder.vocher_per_layout.setBackgroundColor(Color.parseColor("#" + Constants.appColor));

                int ic_delete = context.getResources().getIdentifier("delete2x_" + appColor, "drawable", context.getPackageName());
                holder.coupondelete.setImageDrawable(context.getResources().getDrawable(ic_delete));
            }
        }

        String date = couponArrayList.get(pos).getEndDate();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        ;

        try {
            Date datetime = format.parse(date);
            date = sdf1.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "date: " + date);
        holder.validity.setText("Validity " + date);


        String date1 = couponArrayList.get(pos).getStartDate();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat sdf3 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        ;

        try {
            Date datetime = format1.parse(date1);
            date1 = sdf3.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "date: " + date1);
        holder.available.setText("Validity " + date1);
    }

    @Override
    public int getItemCount() {
        return couponArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mainstore, code, available, validity, discount;
        ImageView coupondelete;
        LinearLayout storelayout;
        RelativeLayout vocher_per_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            mainstore = (TextView) itemView.findViewById(R.id.emeployename);
            code = (TextView) itemView.findViewById(R.id.code);
            available = (TextView) itemView.findViewById(R.id.available);
            validity = (TextView) itemView.findViewById(R.id.validdate);
            discount = (TextView) itemView.findViewById(R.id.discount);
            coupondelete = (ImageView) itemView.findViewById(R.id.delete);
            storelayout = (LinearLayout) itemView.findViewById(R.id.storelayout);
            vocher_per_layout = (RelativeLayout) itemView.findViewById(R.id.voucher_per_layout);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, VoucherDetailsActivity.class);
                    intent.putExtra("voucherarry", couponArrayList);
                    intent.putExtra("pos", pos);
                    context.startActivity(intent);
                }
            });

            coupondelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showtwoButtonsAlertDialog();
                }
            });
        }
    }

    public void showtwoButtonsAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final TextView title = (TextView) dialogView.findViewById(R.id.title);
        final TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        final TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        final TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        desc.setText("Do you want to delete the coupon?");

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new deletvoucheapi().execute();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();

            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private class deletvoucheapi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            Constants.showLoadingDialog(activity);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<VoucherDeleteResponce> call = apiService.getvoucherdelet(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VoucherDeleteResponce>() {
                @Override
                public void onResponse(Call<VoucherDeleteResponce> call, Response<VoucherDeleteResponce> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        VoucherDeleteResponce resetPasswordResponse = response.body();
                        try {
                            if (resetPasswordResponse.getStatus()) {

                                Toast.makeText(context, R.string.success, Toast.LENGTH_SHORT).show();
                            } else {
                                String failureResponse = resetPasswordResponse.getMessage();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                        }
                        Constants.closeLoadingDialog();
                    }
                }

                @Override
                public void onFailure(Call<VoucherDeleteResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

            });
            return null;
        }
    }

    private String prepareResetPasswordJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id", userId);
            parentObj.put("StoreId", couponArrayList.get(pos).getStoreId());
            parentObj.put("FlagId", 4);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareResetPasswordJson: " + parentObj.toString());
        return parentObj.toString();
    }


}
