package com.cs.checkclickvendor.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;


import com.cs.checkclickvendor.Activity.PushNotificationActivity;
import com.cs.checkclickvendor.Models.PushNotificationResponce;
import com.cs.checkclickvendor.R;

import java.util.ArrayList;

public class PushNotificationAdapteer extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    String TAG ="TAG";

    public ArrayList<PushNotificationResponce.PNConfig> pushadapterarry = new ArrayList<>();

    public PushNotificationAdapteer(Context context, ArrayList<PushNotificationResponce.PNConfig> pushList) {
        this.context = context;
        this.pushadapterarry = pushList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        Log.d(TAG, "pushcount: "+ pushadapterarry.size());
        return pushadapterarry.size();
    }
    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView category;
        Switch catswitch;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        holder = new ViewHolder();

        convertView = inflater.inflate(R.layout.pushnotification_adapter, null);

        holder.category=(TextView)convertView.findViewById(R.id.cat);
        holder.catswitch=(Switch) convertView.findViewById(R.id.catswitch);
        holder.category.setText(pushadapterarry.get(position).getName());

        if (pushadapterarry.get(position).getStatus()){
            holder.catswitch.setChecked(true);
        }else {
            holder.catswitch.setChecked(false);
        }

      holder.catswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
              PushNotificationActivity.pushnotificationarry.get(position).setStatus(isChecked);
          }
      });

        return convertView;
    }

}
