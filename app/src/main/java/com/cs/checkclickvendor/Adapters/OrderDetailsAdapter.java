package com.cs.checkclickvendor.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickvendor.Activity.OrderDetailsActivity;
import com.cs.checkclickvendor.Models.OrderTypeList;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Activity.OrderDetailsActivity.orderDetailsLists;

public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.MyViewHolder> {

    Context context;
    AppCompatActivity activity;
    LayoutInflater inflater;
    String userid, language;
    int type, pos;


    public OrderDetailsAdapter(Context context, int pos, String language, int type, AppCompatActivity activity) {
        this.context = context;
        this.userid = userid;
        this.pos = pos;
        this.language = language;
        this.type = type;
        this.activity = activity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
//        if (language.equalsIgnoreCase("En")) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_details_list, parent, false);
//        } else {
//            itemView = LayoutInflater.from(parent.getContext())
//                    .inflate(R.layout.order_details_list_arabic, parent, false);
//        }

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        holder.item_name.setText("" + orderDetailsLists.get(pos).getItems().get(position).getNameen());
        holder.item_price.setText("" + Constants.priceFormat1.format(orderDetailsLists.get(pos).getItems().get(position).getPrice())+" SAR");
        holder.item_qty.setText("Qty : " + orderDetailsLists.get(pos).getItems().get(position).getQty());
        holder.manufactor_name.setText("by " + orderDetailsLists.get(pos).getItems().get(position).getSubcategoryen());

        Glide.with(context)
                .load(Constants.PRODUCTS_IMAGE_URL + orderDetailsLists.get(pos).getItems().get(position).getImage())
                .into(holder.item_img);
        holder.variants.setText("" + orderDetailsLists.get(pos).getItems().get(position).getVariantsname());

        Log.i("TAG", "onBindViewHolder: " + orderDetailsLists.get(pos).getItems().get(position).getVariantsname());
        Log.i("TAG", "item_id: " + orderDetailsLists.get(pos).getItems().get(position).getItemid());

//        holder.variants.setText("" + orderItems.get(position).getVariants().get());

        holder.reject.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                if (orderDetailsLists.get(pos).getItems().get(position).isAccept()) {

                    AlertDialog customDialog = null;
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = activity.getLayoutInflater();
                    int layout;
//                                        if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.comment_alert_dialog;
//                                        }else {
//                                            layout = R.layout.comment_alert_dialog_arabic;
//                                        }
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView cancel = (TextView) dialogView.findViewById(R.id.cancel);
                    final EditText comment = (EditText) dialogView.findViewById(R.id.comment);
                    TextView done = (TextView) dialogView.findViewById(R.id.done);


                    customDialog = dialogBuilder.create();
                    customDialog.show();

                    final AlertDialog finalCustomDialog1 = customDialog;
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            finalCustomDialog1.dismiss();

                        }
                    });

                    final AlertDialog finalCustomDialog = customDialog;
                    done.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (comment.getText().toString().equals("")) {

//                                                    if (language.equalsIgnoreCase("En")) {
                                Constants.showOneButtonAlertDialog("Please enter reason", context.getResources().getString(R.string.app_name),
                                        "Ok", activity);
//                                                    }else {
//                                                        showOneButtonAlertDialog("الرجاء كتابة السبب", getResources().getString(R.string.app_name_ar),
//                                                                getResources().getString(R.string.ok_ar), OrderDetailsActivity.this);

//                                                    }

                            } else {

                                orderDetailsLists.get(pos).getItems().get(position).setAccept(false);
                                orderDetailsLists.get(pos).getItems().get(position).setReject(true);

                                holder.reject.setBackground(context.getResources().getDrawable(R.drawable.reject_selected_bg));
                                holder.accept.setBackground(context.getResources().getDrawable(R.drawable.accept_bg));

                                holder.reject.setText("Rejected  X ");
                                holder.reject.setTextColor(context.getResources().getColor(R.color.white));
                                holder.accept.setText("Accept");
                                holder.accept.setTextColor(context.getResources().getColor(R.color.green));
                                holder.accept.setPadding(20, 20, 20, 20);

                                orderDetailsLists.get(pos).getItems().get(position).setComment(comment.getText().toString());
//                                                    orderstatus = "Cancel";
//                                                    customer_name = orderDetailsLists.get(finalI1).getOrderMain().getUserName();

                                finalCustomDialog.dismiss();
                                Log.i("TAG", "reject_isaccept: " + orderDetailsLists.get(pos).getItems().get(position).isAccept());
                                Log.i("TAG", "reject_isreject: " + orderDetailsLists.get(pos).getItems().get(position).isReject());

                            }

                        }
                    });

                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = activity.getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);

                } else if (orderDetailsLists.get(pos).getItems().get(position).isReject()) {

                    orderDetailsLists.get(pos).getItems().get(position).setReject(false);

                    holder.reject.setBackground(context.getResources().getDrawable(R.drawable.reject_bg));

                    holder.reject.setText("Reject");
                    holder.reject.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//                    holder.accept.setText("Accept");
//                    holder.accept.setTextColor(context.getResources().getColor(R.color.green));
//                    holder.accept.setPadding(20, 20, 20, 20);
                    Log.i("TAG", "reject_isaccept: " + orderDetailsLists.get(pos).getItems().get(position).isAccept());
                    Log.i("TAG", "reject_isreject: " + orderDetailsLists.get(pos).getItems().get(position).isReject());

                } else {

                    AlertDialog customDialog = null;
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = activity.getLayoutInflater();
                    int layout;
//                                        if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.comment_alert_dialog;
//                                        }else {
//                                            layout = R.layout.comment_alert_dialog_arabic;
//                                        }
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView cancel = (TextView) dialogView.findViewById(R.id.cancel);
                    final EditText comment = (EditText) dialogView.findViewById(R.id.comment);
                    TextView done = (TextView) dialogView.findViewById(R.id.done);


                    customDialog = dialogBuilder.create();
                    customDialog.show();

                    final AlertDialog finalCustomDialog1 = customDialog;
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            finalCustomDialog1.dismiss();

                        }
                    });

                    final AlertDialog finalCustomDialog = customDialog;
                    done.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (comment.getText().toString().equals("")) {

//                                                    if (language.equalsIgnoreCase("En")) {
                                Constants.showOneButtonAlertDialog("Please enter reason", context.getResources().getString(R.string.app_name),
                                        "Ok", activity);
//                                                    }else {
//                                                        showOneButtonAlertDialog("الرجاء كتابة السبب", getResources().getString(R.string.app_name_ar),
//                                                                getResources().getString(R.string.ok_ar), OrderDetailsActivity.this);

//                                                    }

                            } else {

                                orderDetailsLists.get(pos).getItems().get(position).setAccept(false);
                                orderDetailsLists.get(pos).getItems().get(position).setReject(true);

                                holder.reject.setBackground(context.getResources().getDrawable(R.drawable.reject_selected_bg));
                                holder.accept.setBackground(context.getResources().getDrawable(R.drawable.accept_bg));

                                holder.reject.setText("Rejected  X ");
                                holder.reject.setTextColor(context.getResources().getColor(R.color.white));
                                holder.accept.setText("Accept");
                                holder.accept.setTextColor(context.getResources().getColor(R.color.green));
                                holder.accept.setPadding(20, 20, 20, 20);

                                orderDetailsLists.get(pos).getItems().get(position).setComment(comment.getText().toString());
//                                                    orderstatus = "Cancel";
//                                                    customer_name = orderDetailsLists.get(finalI1).getOrderMain().getUserName();

                                Log.i("TAG", "reject_isaccept: " + orderDetailsLists.get(pos).getItems().get(position).isAccept());
                                Log.i("TAG", "reject_isreject: " + orderDetailsLists.get(pos).getItems().get(position).isReject());
                                finalCustomDialog.dismiss();

                            }

                        }
                    });

                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = activity.getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);

                }

                Log.i("TAG", "reject_isaccept: " + orderDetailsLists.get(pos).getItems().get(position).isAccept());
                Log.i("TAG", "reject_isreject: " + orderDetailsLists.get(pos).getItems().get(position).isReject());

            }
        });

        holder.accept.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

                orderDetailsLists.get(pos).getItems().get(position).setAccept(true);
                orderDetailsLists.get(pos).getItems().get(position).setReject(false);

                holder.reject.setBackground(context.getResources().getDrawable(R.drawable.reject_bg));
                holder.accept.setBackground(context.getResources().getDrawable(R.drawable.accept_selected_bg));

                holder.reject.setText("Reject");
                holder.reject.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                holder.accept.setText("Accepted");
                holder.accept.setTextColor(context.getResources().getColor(R.color.white));
                holder.accept.setPadding(20, 20, 45, 20);

                Log.i("TAG", "accept_isaccept: " + orderDetailsLists.get(pos).getItems().get(position).isAccept());
                Log.i("TAG", "accept_isreject: " + orderDetailsLists.get(pos).getItems().get(position).isReject());

            }
        });

        holder.accept_reject.setVisibility(View.VISIBLE);

        if (type == 2 || type == 3) {

            holder.accept_reject.setVisibility(View.GONE);

        }

    }

    @Override
    public int getItemCount() {
//        if (userid.equalsIgnoreCase("1")) {
//            return allAdminOrders.size();
//        } else {
//            return orderLists.size();
//        }
        return orderDetailsLists.get(pos).getItems().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView item_name, item_price, item_qty, manufactor_name, variants, reject, accept;
        ImageView item_img;
        LinearLayout accept_reject;

        public MyViewHolder(final View convertView) {
            super(convertView);

            item_name = (TextView) convertView.findViewById(R.id.item_name);
            item_price = (TextView) convertView.findViewById(R.id.item_price);
            item_qty = (TextView) convertView.findViewById(R.id.item_qty);
            manufactor_name = (TextView) convertView.findViewById(R.id.manufactor_name);
            variants = (TextView) convertView.findViewById(R.id.variants);
            reject = (TextView) convertView.findViewById(R.id.reject);
            accept = (TextView) convertView.findViewById(R.id.accept);

            accept_reject = (LinearLayout) convertView.findViewById(R.id.reject_accept);

            item_img = (ImageView) convertView.findViewById(R.id.item_img);


//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }

    }

}
