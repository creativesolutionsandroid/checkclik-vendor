package com.cs.checkclickvendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.checkclickvendor.Models.SubscriptionPackageResponce;
import com.cs.checkclickvendor.R;

import java.util.ArrayList;


public class SublistPackageListAdapter extends  RecyclerView.Adapter<SublistPackageListAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    public LayoutInflater inflater;
    ArrayList<SubscriptionPackageResponce.DataEntity> sublistpackagearrylist = new ArrayList<>();

    public SublistPackageListAdapter(Context context,  ArrayList<SubscriptionPackageResponce.DataEntity> subscritionlist) {
        this.context = context;
        this.activity = activity;
        this.sublistpackagearrylist = subscritionlist;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subscription_step1_activity, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int pos) {
        holder.packname.setText(sublistpackagearrylist.get(pos).getPlanNameEn());
        holder.expiredays.setText(sublistpackagearrylist.get(pos).getPlanExpiry());

    }

    @Override
    public int getItemCount() {
        return sublistpackagearrylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView packname,expiredays,economyexpire;
        LinearLayout iteamviem;


        public MyViewHolder(View itemView) {
            super(itemView);

            packname=(TextView)itemView.findViewById(R.id.packagename);
            expiredays=(TextView)itemView.findViewById(R.id.expriedays);
            iteamviem=(LinearLayout)itemView.findViewById(R.id.layout);

        }
    }
}
