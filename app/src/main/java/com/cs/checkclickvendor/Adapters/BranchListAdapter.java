package com.cs.checkclickvendor.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.cs.checkclickvendor.Activity.OrderManagmentActivity;
import com.cs.checkclickvendor.Models.BranchList;
import com.cs.checkclickvendor.R;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class BranchListAdapter extends RecyclerView.Adapter<BranchListAdapter.MyViewHolder> {

    Context context;
    AppCompatActivity activity;
    LayoutInflater inflater;
    String userid;

    ArrayList<BranchList.Branches> orderLists = new ArrayList<>();

    public BranchListAdapter(Context context, ArrayList<BranchList.Branches> ordersCounts) {
        this.context = context;
        this.orderLists = ordersCounts;
        this.userid = userid;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_fragment_list, parent, false);

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        holder.branch_name.setText("" + orderLists.get(position).getBranchnameen());
        holder.branch_name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        holder.branch_number.setText("Branch " + orderLists.get(position).getId());
        holder.branch_address.setText("" + orderLists.get(position).getFloorno() + ", " + orderLists.get(position).getBuildingno() + ", " + orderLists.get(position).getStreetno() + ", " + orderLists.get(position).getDistricten() + ", " + orderLists.get(position).getCityen() + ", " + orderLists.get(position).getRegionen() + ", " + orderLists.get(position).getCountryen());
        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_callIcon = context.getResources().getIdentifier("loaction_icon_" + appColor, "drawable", context.getPackageName());
                holder.location_img.setImageDrawable(context.getResources().getDrawable(ic_callIcon));
            }
        }

    }

    @Override
    public int getItemCount() {

        return orderLists.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView branch_name, branch_number, branch_address;
        ImageView location_img;

        public MyViewHolder(final View convertView) {
            super(convertView);

            branch_name = (TextView) convertView.findViewById(R.id.branch_name);
            branch_number = (TextView) convertView.findViewById(R.id.Branch_number);
            branch_address = (TextView) convertView.findViewById(R.id.branch_address);
            location_img = (ImageView) convertView.findViewById(R.id.location_img);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent a = new Intent(context, OrderManagmentActivity.class);
                    a.putExtra("branchid", orderLists.get(getAdapterPosition()).getId());
                    context.startActivity(a);

                }
            });
        }

    }

}
