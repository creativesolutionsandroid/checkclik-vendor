package com.cs.checkclickvendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickvendor.Activity.RevenueBranchDetailsActivtiy;
import com.cs.checkclickvendor.Models.BranchList;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import java.util.ArrayList;

public class RevenueBranchesListAdapter extends PagerAdapter {

    private LayoutInflater mLayoutInflater;
    private static Context context;
    ArrayList<BranchList.Branches> orderLists = new ArrayList<>();
    TextView branch_name, branch_address;
    ImageView branch_img;
    RelativeLayout revenu_layout;


    public RevenueBranchesListAdapter(Context context, ArrayList<BranchList.Branches> orderList) {
        this.context = context;
        this.orderLists = orderList;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ViewGroup) object);
    }

    public void destroyItem(View container, int position, Object object) {
        try {
            throw new UnsupportedOperationException("Required method destroyItem was not overridden");
        } catch (Exception e) {

        }
        destroyItem((ViewGroup) container, position, object);
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 1.2f; // You could display partial pages using a float value
        return (1 / nbPages);
    }


    @Override
    public int getCount() {

        int count = 0;
        count = orderLists.size();
        return count;
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {

        View convertView = mLayoutInflater.inflate(R.layout.revenue_fragment_list, view, false);

        branch_name = (TextView) convertView.findViewById(R.id.branch_name);
        branch_address = (TextView) convertView.findViewById(R.id.branch_address);
        branch_img = (ImageView) convertView.findViewById(R.id.img);
        revenu_layout = (RelativeLayout) convertView.findViewById(R.id.revenu_layout);
        CamomileSpinner spinner = (CamomileSpinner) convertView.findViewById(R.id.spinner);
        revenu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(context, RevenueBranchDetailsActivtiy.class);
                a.putExtra("title_name", orderLists.get(position).getBranchnameen());
                a.putExtra("branchid", orderLists.get(position).getId());
                context.startActivity(a);

            }
        });

//        if (language.equalsIgnoreCase("Ar")) {
//            mlayout.setRotationY(180);
//        }

        branch_name.setText("" + orderLists.get(position).getBranchnameen());
        branch_address.setText("" + orderLists.get(position).getFloorno() + ", " + orderLists.get(position).getBuildingno() + ", " + orderLists.get(position).getStreetno() + ", " + orderLists.get(position).getDistricten() + ", " + orderLists.get(position).getCityen() + ", " + orderLists.get(position).getRegionen() + ", " + orderLists.get(position).getCountryen());

        spinner.start();
        spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );


        Glide.with(context)
                .load(Constants.STORE_IMAGE_URL + orderLists.get(position).getBranchlogoimage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        spinner.setVisibility(View.GONE);
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        spinner.setVisibility(View.GONE);
                        return false;
                    }

                })


                .into(branch_img);

        Log.i("TAG", "instantiateItem: " + orderLists.get(position).getCountryen());


        view.addView(convertView);
        return convertView;
    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    public static void showViewContent(int position) {


//        position = pos

    }
}

//        extends RecyclerView.Adapter<RevenueBranchesListAdapter.MyViewHolder> {
//
//    Context context;
//    Activity activity;
//    LayoutInflater inflater;
//    String userid;
//
//    ArrayList<BranchList.Branches> orderLists = new ArrayList<>();
//
//    public RevenueBranchesListAdapter(Context context, ArrayList<BranchList.Branches> ordersCounts) {
//        this.context = context;
//        this.orderLists = ordersCounts;
//        this.userid = userid;
//        this.inflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    }
//
//    @Override
//    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.revenue_fragment_list, parent, false);
//
////     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
//        return new MyViewHolder(itemView);
//    }
//
//
//    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
//
//
//        holder.branch_name.setText("" + orderLists.get(position).getBranchnameen());
//        holder.branch_address.setText("" + orderLists.get(position).getFloorno() + ", " + orderLists.get(position).getBuildingno() + ", " + orderLists.get(position).getStreetno() + ", " + orderLists.get(position).getDistricten() + ", " + orderLists.get(position).getCityen() + ", " + orderLists.get(position).getRegionen() + ", " + orderLists.get(position).getCountryen());
//
//        Glide.with(context).load(Constants.STORE_IMAGE_URL + orderLists.get(position).getBranchlogoimage()).into(holder.branch_img);
//
//    }
//
//    @Override
//    public int getItemCount() {
//
//        return orderLists.size();
//
//    }
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//        TextView branch_name, branch_address;
//        ImageView branch_img;
//
//        public MyViewHolder(final View convertView) {
//            super(convertView);
//
//            branch_name = (TextView) convertView.findViewById(R.id.branch_name);
//            branch_address = (TextView) convertView.findViewById(R.id.branch_address);
//            branch_img = (ImageView) convertView.findViewById(R.id.img);
//
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, RevenueBranchDetailsActivtiy.class);
//                    context.startActivity(a);
//
//                }
//            });
//        }
//
//    }
//
//}

