package com.cs.checkclickvendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cs.checkclickvendor.Fragment.ProfileFragment;
import com.cs.checkclickvendor.Models.UserStores;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;

import java.util.ArrayList;


public class StoresSideMenuAdapter extends BaseAdapter {

    Context context;
    ArrayList<UserStores.StoreListEntity> stores = new ArrayList<>();
    public LayoutInflater inflater;

    public StoresSideMenuAdapter(Context context, ArrayList<UserStores.StoreListEntity> stores){
        this.context = context;
        this.stores = stores;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return stores.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        ImageView selectImage, menuImage;
        TextView storeName;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.list_stores_side_menu, null);

            holder.selectImage = (ImageView) convertView.findViewById(R.id.store_selection);
            holder.menuImage = (ImageView) convertView.findViewById(R.id.store_image);
            holder.storeName = (TextView) convertView.findViewById(R.id.store_name);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(position == ProfileFragment.itemSelectedPostion) {
            holder.selectImage.setVisibility(View.VISIBLE);
        }
        else {
            holder.selectImage.setVisibility(View.GONE);
        }



        holder.storeName.setText(stores.get(position).getStoreNameEn());
        Glide.with(context).load(Constants.STORE_IMAGE_URL+stores.get(position).getLogoCopy()).into(holder.menuImage);

        return convertView;
    }
}