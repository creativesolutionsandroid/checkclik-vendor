package com.cs.checkclickvendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.checkclickvendor.Models.SubscriptionPackageResponce2;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;

import java.util.ArrayList;

public class SubscriptionAdapter2 extends  RecyclerView.Adapter<SubscriptionAdapter2.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    public LayoutInflater inflater;
    ArrayList<SubscriptionPackageResponce2.DataEntity> subscritionlist = new ArrayList<>();

    public SubscriptionAdapter2(Context context,  ArrayList<SubscriptionPackageResponce2.DataEntity> subscritionlist) {
        this.context = context;
        this.activity = activity;
        this.subscritionlist = subscritionlist;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subscriptionadapter2, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int pos) {
       holder.daytext.setText(subscritionlist.get(pos).getNameEn());
       holder.noofaccess.setText(""+subscritionlist.get(pos).getNoOfAccess()+" ACCESS USERS");
       holder.nocoupns.setText(""+subscritionlist.get(pos).getNoOfCoupans()+" COUPONS");
       holder.couponsexpiredate.setText("COUPON DURATION "+subscritionlist.get(pos).getCoupanDuration()+" DAYS");
       holder.regations.setText(""+subscritionlist.get(pos).getCoupanNoOfRegions()+" REGIONS");
       holder.no_of_users.setText(""+subscritionlist.get(pos).getCoupanNoOfUsers()+" USERS");
       holder.no_of_adv.setText(""+subscritionlist.get(pos).getAdvNoOfOffers()+" ADVERTISEMENT");
       holder.no_of_advduration.setText("ADV DURACTION "+subscritionlist.get(pos).getAdvDuration()+" DAYS");
       holder.no_of_regions2.setText(""+subscritionlist.get(pos).getAdvNoOfRegions()+" REGIONS");
       holder.price.setText(Constants.priceFormat1.format(subscritionlist.get(pos).getDiscountedPrice()));
       holder.months.setText("/ "+subscritionlist.get(pos).getSlabName());

        if (pos==0){
            holder.layout.setBackgroundResource(R.drawable.shape_sublist_orange);
        }
        else if (pos==1){
            holder.layout.setBackgroundResource(R.drawable.shape_sublist_purpul);
        }
        else if (pos==2){
            holder.layout.setBackgroundResource(R.drawable.shape_sublist_green);
        }
        else if (pos==3){
            holder.layout.setBackgroundResource(R.drawable.shape_sublist_purpul);
        }
        else if (pos==4){
            holder.layout.setBackgroundResource(R.drawable.shape_sublist_orange);
        }

    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: "+subscritionlist.size());
        return subscritionlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView daytext, noofaccess, nocoupns,couponsexpiredate,regations,no_of_users,no_of_adv,no_of_advduration,no_of_regions2,price,months;

        LinearLayout layout ;

        public MyViewHolder(View itemView) {
            super(itemView);

            daytext = (TextView) itemView.findViewById(R.id.daytext);
            noofaccess = (TextView) itemView.findViewById(R.id.no_of_access);
            nocoupns = (TextView) itemView.findViewById(R.id.no_of_coupons);
            couponsexpiredate = (TextView) itemView.findViewById(R.id.couponsexpiredate);
            regations=(TextView) itemView.findViewById(R.id.regations);
            no_of_users=(TextView) itemView.findViewById(R.id.no_of_users);
            no_of_adv=(TextView) itemView.findViewById(R.id.no_of_adv);
            no_of_advduration=(TextView) itemView.findViewById(R.id.no_of_advduration);
            no_of_regions2=(TextView) itemView.findViewById(R.id.no_of_regions2);
            price=(TextView) itemView.findViewById(R.id.price);
            months=(TextView) itemView.findViewById(R.id.months);
            layout=(LinearLayout)itemView.findViewById(R.id.layout);

        }
    }



}
