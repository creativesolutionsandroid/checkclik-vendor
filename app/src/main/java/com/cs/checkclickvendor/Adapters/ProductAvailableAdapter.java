package com.cs.checkclickvendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickvendor.Models.ProductManagementResponce;
import com.cs.checkclickvendor.Models.ProductMangPagination;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Utils.Utils.Constants.OfferIP_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.PRODUCTS_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.RETURN_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.STORE_IMAGE_URL;


public class ProductAvailableAdapter extends RecyclerView.Adapter<ProductAvailableAdapter.MyViewHolder> {

    ArrayList<ProductMangPagination> productMainArryList = new ArrayList<>();
    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    public LayoutInflater inflater;
    int pos;
    boolean isOutOfStock;

    public ProductAvailableAdapter(Context context, ArrayList<ProductMangPagination> productMainArryList, boolean isOutOfStock) {
        this.context = context;
        this.activity = activity;
        this.isOutOfStock = isOutOfStock;
        this.productMainArryList = productMainArryList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public ProductAvailableAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_management_adapter, parent, false);
        return new ProductAvailableAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductAvailableAdapter.MyViewHolder holder, int pos) {

        if (productMainArryList.get(pos).getStore_type() == 1) {
            holder.varint.setVisibility(View.VISIBLE);
            holder.qty.setVisibility(View.VISIBLE);
            holder.qtyTitle.setVisibility(View.VISIBLE);
        } else {
            holder.varint.setVisibility(View.INVISIBLE);
            holder.qty.setVisibility(View.INVISIBLE);
            holder.qtyTitle.setVisibility(View.INVISIBLE);
        }

        holder.productname.setText(productMainArryList.get(pos).getProduct_name_en());
        holder.varint.setText(productMainArryList.get(pos).getVariant());
        holder.sellingprice.setText(Constants.priceFormat1.format(productMainArryList.get(pos).getSelling_price()) + " SAR");

        if (productMainArryList.get(pos).getInventery() == 1) {

            holder.qtyTitle.setVisibility(View.INVISIBLE);
            holder.qty.setText("");
            holder.qty.setBackground(context.getResources().getDrawable(R.drawable.lock));

        } else {

            holder.qtyTitle.setVisibility(View.VISIBLE);
            holder.qty.setBackground(context.getResources().getDrawable(R.drawable.product_qty_bg));
            holder.qty.setText("" + productMainArryList.get(pos).getStock_qty());

        }


        holder.spinner.start();
        holder.spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );

        Glide.with(context)
                .load(OfferIP_IMAGE_URL + productMainArryList.get(pos).getImage())

                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.prouctimage);

        if (isOutOfStock) {
            holder.qty.setVisibility(View.INVISIBLE);
            holder.qtyTitle.setVisibility(View.INVISIBLE);
        } else {
            if (productMainArryList.get(pos).getStore_type() == 1) {
                holder.qty.setVisibility(View.VISIBLE);
                holder.qtyTitle.setVisibility(View.VISIBLE);
            } else {
                holder.qty.setVisibility(View.INVISIBLE);
                holder.qtyTitle.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return productMainArryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productname, varint, sellingprice, qty, qtyTitle;
        ImageView prouctimage;
        CamomileSpinner spinner;

        public MyViewHolder(View itemView) {
            super(itemView);

            productname = (TextView) itemView.findViewById(R.id.producttitel);
            varint = (TextView) itemView.findViewById(R.id.variant);
            sellingprice = (TextView) itemView.findViewById(R.id.Sellingprice);
            qty = (TextView) itemView.findViewById(R.id.qty);
            qtyTitle = (TextView) itemView.findViewById(R.id.qtytext);
            prouctimage = (ImageView) itemView.findViewById(R.id.productimage);
            spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);
        }
    }
}
