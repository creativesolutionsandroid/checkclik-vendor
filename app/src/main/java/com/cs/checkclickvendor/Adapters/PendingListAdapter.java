package com.cs.checkclickvendor.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickvendor.Activity.OrderDetailsActivity;
import com.cs.checkclickvendor.Activity.OrderTypeActivity;
import com.cs.checkclickvendor.Models.OrderTypeList;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Activity.OrderTypeActivity.branchid;
import static com.cs.checkclickvendor.Activity.OrderTypeActivity.header_title;
import static com.cs.checkclickvendor.Activity.OrderTypeActivity.statusid;


public class PendingListAdapter extends RecyclerView.Adapter<PendingListAdapter.MyViewHolder> {

    Context context;
    ArrayList<OrderTypeList.Data> orderLists = new ArrayList<>();
    //    ArrayList<OrderTypelist.AllAdminOrders> allAdminOrders = new ArrayList<>();
    AppCompatActivity activity;
    LayoutInflater inflater;
    String userid, language;
    public static String mcomment = "", order_id = "", orderstatus = "", customer_name = "", customer_email = "", morder_id = "";
    SharedPreferences userPrefs;
    String itemimage, itemimage1, itemimage2;

    public PendingListAdapter(Context context, AppCompatActivity activity, ArrayList<OrderTypeList.Data> orderLists, String userid, String language) {
        this.context = context;
        this.orderLists = orderLists;
        this.userid = userid;
        this.language = language;
        this.activity = activity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
//        if (language.equalsIgnoreCase("En")) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_type_list, parent, false);
//        } else {
//            itemView = LayoutInflater.from(parent.getContext())
//                    .inflate(R.layout.order_type_list_arabic, parent, false);
//        }
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.no_of_items.setText("Number of units : " + orderLists.get(position).getItems().size()+" Item");

        holder.invoice_no.setText("Order No : " + orderLists.get(position).getInvoiceno());

        String[] mimage = null;
        mimage = orderLists.get(position).getMultipleimages().split(",");

        holder.spinner.start();
        holder.spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );


        if (mimage.length == 3 || mimage.length > 3) {

            holder.item1Layout.setVisibility(View.VISIBLE);
            holder.item2Layout.setVisibility(View.VISIBLE);
            holder.item3Layout.setVisibility(View.VISIBLE);

            itemimage = mimage[0];
            itemimage1 = mimage[1];
            itemimage2 = mimage[2];

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner.setVisibility(View.GONE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(holder.image);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage1)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner1.setVisibility(View.GONE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner1.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(holder.image1);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage2)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner2.setVisibility(View.GONE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner2.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(holder.image2);

        }
        else if (mimage.length == 2) {

            holder.item1Layout.setVisibility(View.VISIBLE);
            holder.item2Layout.setVisibility(View.VISIBLE);
            holder.item3Layout.setVisibility(View.GONE);

            itemimage = mimage[0];
            itemimage1 = mimage[1];

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage)
                    .into(holder.image);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage1)
                    .into(holder.image1);

        } else if (mimage.length == 1) {

            holder.item1Layout.setVisibility(View.VISIBLE);
            holder.item2Layout.setVisibility(View.GONE);
            holder.item3Layout.setVisibility(View.GONE);

            itemimage = orderLists.get(position).getMultipleimages();

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage)
                    .into(holder.image);
        }
    }

    @Override
    public int getItemCount() {

        return orderLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView no_of_items, invoice_no;
        ImageView image, image1, image2;
        RelativeLayout item1Layout, item2Layout, item3Layout;
        CamomileSpinner spinner,spinner1,spinner2;
        public MyViewHolder(View convertView) {
            super(convertView);

            no_of_items = (TextView) convertView.findViewById(R.id.no_of_items);
            invoice_no = (TextView) convertView.findViewById(R.id.invoice_no);
            image = (ImageView) convertView.findViewById(R.id.image);
            image1 = (ImageView) convertView.findViewById(R.id.image1);
            image2 = (ImageView) convertView.findViewById(R.id.image2);
            item1Layout = (RelativeLayout) convertView.findViewById(R.id.item1_layout);
            item2Layout = (RelativeLayout) convertView.findViewById(R.id.item2_layout);
            item3Layout = (RelativeLayout) convertView.findViewById(R.id.item3_layout);
            spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);
            spinner1 = (CamomileSpinner) itemView.findViewById(R.id.spinner1);
            spinner2 = (CamomileSpinner) itemView.findViewById(R.id.spinner2);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent a = new Intent(context, OrderDetailsActivity.class);
                    a.putExtra("BranchId", branchid);
                    a.putExtra("Type", OrderTypeActivity.type);
                    a.putExtra("StatusId", statusid);
                    a.putExtra("header", header_title);
                    a.putExtra("orderid", orderLists.get(getAdapterPosition()).getOrderid());
                    Log.i("TAG", "onClick: " + orderLists.get(getAdapterPosition()).getOrderid());
                    context.startActivity(a);

                }
            });
        }
    }
}