package com.cs.checkclickvendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickvendor.Activity.ProfileManagementActivity;
import com.cs.checkclickvendor.Models.BranchList;
import com.cs.checkclickvendor.R;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;
import com.lovejjfg.shadowcircle.CircleImageView;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Utils.Utils.Constants.PRODUCTS_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.RETURN_IMAGE_URL;
import static com.cs.checkclickvendor.Utils.Utils.Constants.STORE_IMAGE_URL;

public class ProfileBranchesAdapter extends RecyclerView.Adapter<ProfileBranchesAdapter.MyViewHolder> {

    ArrayList<BranchList.Branches> branchesArrayList = new ArrayList<>();
    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    public LayoutInflater inflater;
    int pos;

    public ProfileBranchesAdapter(Context context, ArrayList<BranchList.Branches> accessarraylist) {
        this.context = context;
        this.activity = activity;
        this.branchesArrayList = accessarraylist;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public ProfileBranchesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_profile_branches, parent, false);
        return new ProfileBranchesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileBranchesAdapter.MyViewHolder holder, int pos) {
        holder.branchName.setText(branchesArrayList.get(pos).getBranchnameen());
        holder.branchAddress.setText(branchesArrayList.get(pos).getAddress());
        holder.branchReviewsCount.setText("("+ branchesArrayList.get(pos).getRatingCount()+" Reviews)");
        holder.ratingBar.setRating(branchesArrayList.get(pos).getReview());

        holder.spinner.start();

        holder.spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );


        Glide.with(context)
                .load(STORE_IMAGE_URL+ branchesArrayList.get(pos).getBranchlogoimage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }
                })


                .into(holder.branchImage);

        if(branchesArrayList.get(pos).getAvailabilitystatus()) {
            holder.branchStatus.setText("Available");
            holder.branchStatus.setTextColor(context.getResources().getColor(R.color.green));
        }
        else {
            holder.branchStatus.setText("Unavailable");
            holder.branchStatus.setTextColor(context.getResources().getColor(R.color.red));
        }
    }

    @Override
    public int getItemCount() {
        return branchesArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView branchImage;
        private TextView branchName, branchReviewsCount, branchAddress, branchStatus;
        private RatingBar ratingBar;
        private RelativeLayout layout;
        CamomileSpinner spinner ;

        public MyViewHolder(View itemView) {
            super(itemView);
            branchName = (TextView) itemView.findViewById(R.id.branch_name);
            branchReviewsCount = (TextView) itemView.findViewById(R.id.reviews_count);
            branchAddress = (TextView) itemView.findViewById(R.id.branch_address);
            branchStatus=(TextView) itemView.findViewById(R.id.branch_status);
            spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingbar);
            branchImage = (CircleImageView) itemView.findViewById(R.id.branch_image);

            layout = (RelativeLayout) itemView.findViewById(R.id.layout);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProfileManagementActivity.class);
                    intent.putExtra("array", branchesArrayList);
                    intent.putExtra("pos", getAdapterPosition());
                    context.startActivity(intent);
                }
            });
        }
    }
}
