package com.cs.checkclickvendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.checkclickvendor.Activity.SubscriptionManagementStep1Activity;
import com.cs.checkclickvendor.Activity.TimeTableMangActivity;
import com.cs.checkclickvendor.Models.BranchList;
import com.cs.checkclickvendor.R;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.MyViewHolder> {

    ArrayList<BranchList.Branches> branchLists = new ArrayList<>();
    private Context context;
    public static final String TAG = "TAG";
    private String activity;
    public LayoutInflater inflater;
    int pos = 0;

    public TimeTableAdapter(Context context, ArrayList<BranchList.Branches> brancArrayList, String activity) {
        this.context = context;
        this.activity = activity;
        this.branchLists = brancArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_timetable, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int pos) {

        holder.branchname.setText(""+branchLists.get(pos).getBranchnameen());
        holder.branchnum.setText("Branch "+branchLists.get(pos).getId());
        holder.address.setText("" + branchLists.get(pos).getFloorno() + ", " + branchLists.get(pos).getBuildingno() + ", " + branchLists.get(pos).getStreetno() + ", " + branchLists.get(pos).getDistricten() + ", " + branchLists.get(pos).getCityen() + ", " + branchLists.get(pos).getRegionen() + ", " + branchLists.get(pos).getCountryen());
        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                int ic_notify = context.getResources().getIdentifier("loaction_icon_" + appColor, "drawable", context.getPackageName());
                holder.location_icon.setImageDrawable(context.getResources().getDrawable(ic_notify));
            }
        }

    }

    @Override
    public int getItemCount() {
        return branchLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView branchname, branchnum, address;
        ImageView location_icon;

        public MyViewHolder(View itemView) {
            super(itemView);

            branchname = (TextView) itemView.findViewById(R.id.branch_name);
            branchnum = (TextView) itemView.findViewById(R.id.Branch_number);
            address = (TextView) itemView.findViewById(R.id.branch_address);
            location_icon = (ImageView) itemView.findViewById(R.id.location_img);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(activity.equalsIgnoreCase("timetable")) {
                        Intent a = new Intent(context, TimeTableMangActivity.class);
                        a.putExtra("branch_id", branchLists.get(getAdapterPosition()).getId());
                        context.startActivity(a);
                    }
                    else {
                        Intent a = new Intent(context, SubscriptionManagementStep1Activity.class);
                        a.putExtra("branch_id", branchLists.get(getAdapterPosition()).getId());
                        context.startActivity(a);
                    }
                }
            });
        }
    }
}
