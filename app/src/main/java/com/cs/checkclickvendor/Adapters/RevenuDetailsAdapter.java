package com.cs.checkclickvendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.cs.checkclickvendor.Models.RevenuDetailsList;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;

import java.util.ArrayList;

public class RevenuDetailsAdapter extends RecyclerView.Adapter<RevenuDetailsAdapter.MyViewHolder> {

    Context context;
    ArrayList<RevenuDetailsList.DetailSummery> orderLists = new ArrayList<>();
    LayoutInflater inflater;
    String userid, language;
    public static String mcomment = "", order_id = "", orderstatus = "", customer_name = "", customer_email = "", morder_id = "";
    SharedPreferences userPrefs;

    public RevenuDetailsAdapter(Context context, ArrayList<RevenuDetailsList.DetailSummery> orderLists) {
        this.context = context;
        this.orderLists = orderLists;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
//        if (language.equalsIgnoreCase("En")) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.revenu_details_list, parent, false);
//        } else {
//            itemView = LayoutInflater.from(parent.getContext())
//                    .inflate(R.layout.order_type_list_arabic, parent, false);
//        }

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.month_name.setText("" + orderLists.get(position).getDays());

        holder.price.setText("" + Constants.priceFormat1.format(orderLists.get(position).getCurrentamount()) + " SAR");

        holder.no_of_orders.setText("" + orderLists.get(position).getOrdercount() + " Orders");

//        Glide.with(context)
//                .load(Arrays.asList("http://image1", "http://image2"))
//                .merge(new ImageMerger() {
//                    public Bitmap merge(List<Bitmap>, <TargetView>) { /* merge by any way */ }
//                })
//                .into(holder.image);

    }

    @Override
    public int getItemCount() {

        return orderLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView month_name, no_of_orders, price;

        public MyViewHolder(View convertView) {
            super(convertView);

            month_name = (TextView) convertView.findViewById(R.id.month_name);
            no_of_orders = (TextView) convertView.findViewById(R.id.no_of_orders);
            price = (TextView) convertView.findViewById(R.id.price);

        }

    }
}
