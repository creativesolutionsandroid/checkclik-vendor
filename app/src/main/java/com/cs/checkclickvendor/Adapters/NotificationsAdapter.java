package com.cs.checkclickvendor.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.cs.checkclickvendor.Models.Notifications;
import com.cs.checkclickvendor.R;

import java.util.ArrayList;
import java.util.List;

public class NotificationsAdapter extends RecyclerView.Adapter< NotificationsAdapter.MyViewHolder> {
    private Context context;
    public static final String TAG = "TAG";
    private List<Notifications.Data> notificationsArrayList = new ArrayList<>();

    public NotificationsAdapter(Context context, List<Notifications.Data> notificationsArrayList) {
        this.context = context;
        this.notificationsArrayList = notificationsArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_child, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.summary.setText(notificationsArrayList.get(position).getMessageEn());
        holder.discount.setText(notificationsArrayList.get(position).getSentDate());
    }

    @Override
    public int getItemCount() {
        return notificationsArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView summary, date, ourprice, discount;
        ImageView storeimage;
        RelativeLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            summary = (TextView) itemView.findViewById(R.id.n_summerytext);
            date = (TextView) itemView.findViewById(R.id.date);

            storelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }
}
