package com.cs.checkclickvendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickvendor.Activity.AccessDetailsActivity;
import com.cs.checkclickvendor.Models.AccessResponce;
import com.cs.checkclickvendor.R;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Utils.Utils.Constants.ADS_IMAGE_URL;

public class AccessDetailsAdapter extends RecyclerView.Adapter<AccessDetailsAdapter.MyViewHolder> {

    ArrayList<AccessResponce.BranchesEntity> accessarraylist = new ArrayList<>();
    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    public LayoutInflater inflater;

    public AccessDetailsAdapter(Context context, ArrayList<AccessResponce.BranchesEntity> accessarraylist) {
        this.context = context;
        this.activity = activity;
        this.accessarraylist = accessarraylist;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.access_details_adater, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int pos) {
        holder.emeployename.setText(accessarraylist.get(pos).getBranchNameEn());
//
//              Glide.with(context)
//                .load(ADS_IMAGE_URL+accessarraylist.get(pos).get())
//                .into(holder.emeployeimage);

    }
    @Override
    public int getItemCount() {
        return accessarraylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView emeployename,emeployeid,emeployecat;
        ImageView emeployeimage;

        public MyViewHolder(View itemView) {
            super(itemView);
            emeployename = (TextView) itemView.findViewById(R.id.emeployename);
//            emeployeid = (TextView) itemView.findViewById(R.id.emeployeid);
//            emeployecat = (TextView) itemView.findViewById(R.id.emeployecat);
            emeployeimage=(ImageView)itemView.findViewById(R.id.emeployeimage);


        }
    }
}
