package com.cs.checkclickvendor.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickvendor.Models.ProductStoreReviews;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;
import com.lovejjfg.shadowcircle.CircleImageView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class ProductReviewsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    List<ProductStoreReviews.Data> reviewsList;

    public ProductReviewsAdapter(Context context, List<ProductStoreReviews.Data> reviewsList) {
        this.context = context;
        this.reviewsList = reviewsList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return reviewsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView customerName, review, reviewDate;
        RatingBar ratingBar;
        CircleImageView profilePic;

    }
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.profile_reviews_list, null);

            holder.customerName = (TextView) convertView.findViewById(R.id.customer_name);
            holder.reviewDate = (TextView) convertView.findViewById(R.id.review_date);
            holder.review = (TextView) convertView.findViewById(R.id.comment);

            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingbar);

            holder.profilePic = (CircleImageView) convertView.findViewById(R.id.ac_profilepic);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.customerName.setText(reviewsList.get(position).getNameen());
        holder.review.setText(reviewsList.get(position).getComments());

        holder.ratingBar.setRating(reviewsList.get(position).getRating());

        Glide.with(context)
                .load(Constants.USERS_IMAGE_URL + reviewsList.get(position).getProfilephoto())
                .into(holder.profilePic);

        String date1 = reviewsList.get(position).getCreatedon();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd'th' MMM yyyy", Locale.US);

        try {
            Date datetime = inputFormat.parse(date1);
            date1 = outputFormat.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.reviewDate.setText(""+date1);

        return convertView;
    }
}
