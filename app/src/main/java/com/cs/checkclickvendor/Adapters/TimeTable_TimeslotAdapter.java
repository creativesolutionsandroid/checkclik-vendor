package com.cs.checkclickvendor.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickvendor.Activity.OrderDetailsActivity;
import com.cs.checkclickvendor.Activity.OrderManagmentActivity;
import com.cs.checkclickvendor.Fragment.ProfileFragment;
import com.cs.checkclickvendor.Models.BranchList;
import com.cs.checkclickvendor.Models.TimeSlot_Invoice_no;
import com.cs.checkclickvendor.Models.TimeTableMang;
import com.cs.checkclickvendor.Models.TimeTable_Invoiceno;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Activity.TimeTableMangActivity.branch_id;

public class TimeTable_TimeslotAdapter extends BaseAdapter {

    Context context;
    TimeTable_InvoiceListAdapter mAdapter;

    ArrayList<TimeSlot_Invoice_no> orderLists = new ArrayList<>();
    public LayoutInflater inflater;

    public TimeTable_TimeslotAdapter(Context context, ArrayList<TimeSlot_Invoice_no> ordersCounts){
        this.context = context;
        this.orderLists = ordersCounts;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return orderLists.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView time_duration;
        RecyclerView invoice_list;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.timetablemang_time_list, null);

            holder.time_duration = (TextView) convertView.findViewById(R.id.time_duration);
            holder.invoice_list = (RecyclerView) convertView.findViewById(R.id.invoice_list);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.time_duration.setText("" + orderLists.get(position).getmTimeSlot());

        holder.time_duration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(context, OrderDetailsActivity.class);
                if (orderLists.get(position).getExp1() == 0) {
                    a.putExtra("BranchId", branch_id);
                    a.putExtra("Type", 2);
                    a.putExtra("StatusId", 3);
                    a.putExtra("header", "Scheduled orders");
                    a.putExtra("orderid", orderLists.get(position).getOrder_id1());
                }
                else if (orderLists.get(position).getExp1() == 1) {
                    a.putExtra("BranchId", branch_id);
                    a.putExtra("Type", 3);
                    a.putExtra("StatusId", 2);
                    a.putExtra("header", "Delayed Orders");
                    a.putExtra("orderid", orderLists.get(position).getOrder_id1());
                }
                else if (orderLists.get(position).getExp1() == 2) {
                    a.putExtra("BranchId", branch_id);
                    a.putExtra("Type", 2);
                    a.putExtra("StatusId", 1);
                    a.putExtra("header", "Scheduled orders for today");
                    a.putExtra("orderid", orderLists.get(position).getOrder_id1());
                }
                else if (orderLists.get(position).getExp1() == 3) {
                    a.putExtra("BranchId", branch_id);
                    a.putExtra("Type", 2);
                    a.putExtra("StatusId", 2);
                    a.putExtra("header", "Scheduled orders for tomorrow");
                    a.putExtra("orderid", orderLists.get(position).getOrder_id1());
                }
                context.startActivity(a);

            }
        });

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(context);
        holder.invoice_list.setLayoutManager(layoutManager1);

        mAdapter = new TimeTable_InvoiceListAdapter(context, orderLists.get(position).getInvoicenos());
        holder.invoice_list.setAdapter(mAdapter);

        return convertView;
    }
}

