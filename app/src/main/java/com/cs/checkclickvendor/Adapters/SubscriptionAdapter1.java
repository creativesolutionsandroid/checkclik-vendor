package com.cs.checkclickvendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.checkclickvendor.Activity.SubscriptionManagementStep2Activity;
import com.cs.checkclickvendor.Models.SubscriptionPackageResponce;
import com.cs.checkclickvendor.R;
import com.cs.checkclickvendor.Utils.Utils.Constants;

import java.util.ArrayList;

import static com.cs.checkclickvendor.Utils.Utils.Constants.appColor;

public class SubscriptionAdapter1 extends RecyclerView.Adapter<SubscriptionAdapter1.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    public LayoutInflater inflater;
    ArrayList<SubscriptionPackageResponce.DataEntity> subpacklist = new ArrayList<>();

    public SubscriptionAdapter1(Context context, ArrayList<SubscriptionPackageResponce.DataEntity> subpacklist) {
        this.context = context;
        this.activity = activity;
        this.subpacklist = subpacklist;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subscription1_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int pos) {

        holder.packagename.setText(subpacklist.get(pos).getPlanNameEn());
        holder.expriedays.setText(subpacklist.get(pos).getNoDays());

        if (appColor != null) {
            if (!appColor.equalsIgnoreCase("0") || !appColor.equalsIgnoreCase("")) {
                holder.packagename.setTextColor(Color.parseColor("#" + Constants.appColor));
                holder.expired_on_txt.setTextColor(Color.parseColor("#" + Constants.appColor));
                holder.expriedays.setTextColor(Color.parseColor("#" + Constants.appColor));
                holder.day_txt.setTextColor(Color.parseColor("#" + Constants.appColor));
//        holder.renew_btn.setTextColor(Color.parseColor("#"+Constants.appColor));

                int ic_color = context.getResources().getIdentifier("C" + appColor, "color", context.getPackageName());
                Drawable background = holder.renew_btn.getBackground();

                GradientDrawable shapeDrawable = (GradientDrawable) background;
                shapeDrawable.setColor(ContextCompat.getColor(context, ic_color));
            }
        }

        if (subpacklist.get(pos).getPlanId() == 0) {

            holder.layout.setBackgroundResource(R.drawable.shape_sublist_orange);

        } else if (subpacklist.get(pos).getPlanId() == 1) {

            holder.layout.setBackgroundResource(R.drawable.shape_sublist_lightgreen);

        }
    }

    @Override
    public int getItemCount() {
        return subpacklist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView packagename, expriedays, expired_on_txt, day_txt;
        LinearLayout layout;
        LinearLayout renew_btn;

        public MyViewHolder(View itemView) {
            super(itemView);
            packagename = (TextView) itemView.findViewById(R.id.packagename);
            expriedays = (TextView) itemView.findViewById(R.id.expriedays);
            expired_on_txt = (TextView) itemView.findViewById(R.id.expired_on_txt);
            day_txt = (TextView) itemView.findViewById(R.id.day_txt);
            renew_btn = (LinearLayout) itemView.findViewById(R.id.renew_btn);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, SubscriptionManagementStep2Activity.class);
                    context.startActivity(intent);
                }
            });

        }
    }
}
